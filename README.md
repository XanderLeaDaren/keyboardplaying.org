# _Keyboard Playing_

This repository contains all the source and contents for the websites [keyboardplaying.org](https://keyboardplaying.org) and [keyboardplaying.fr](https://keyboardplaying.fr).



## Content

### Posts and stories to come

> A [public Gitlab board](https://gitlab.com/keyboardplaying/keyboardplaying.org/-/boards/1340192) is available with all planned subjects.
> If you have a Gitlab account, you should be able to vote on issues to show you are interested in seeing them published as soon as possible.


## Contributing

Merge requests are welcome, to enhance the source or fix mistakes in the content.
To contribute a blog post or a story, please don't hesitate to contact me beforehand.



## Typography

This section is intended as a shortcut to get special characters that may not be easily available on all systems.

### Special characters

| Name                  | Preview | Unicode | Cp1252 | Comment                      |
| --------------------- | :-----: | :-----: | :----: | ---------------------------- |
| Plus sign             |   `+`   | U+002B  | N/A    ||
| No-break space        |   ` `   | U+00A0  | 0160   | FR: With `:` and inside `«»` |
| Middle dot            |   `·`   | U+00B7  | 0183   ||
| Multiplication sign   |   `×`   | U+00D7  | N/A    ||
| Thin space            |   ` `   | U+2009  | N/A    | FR: around `—` |
| Em Dash               |   `—`   | U+2014  | 0151   ||
| Single quotation mark |   `’`   | U+2019  | 0146   ||
| Horizontal ellipsis   |   `…`   | U+2026  | 0133   ||
| Narrow no-break space |   ` `   | U+202F  | N/A    | FR: Before `!`, `?` and `;`, and as thousand separator |
| Asterism              |   `⁂`   | U+2042  | 8258   ||
| Minus sign            |   `−`   | U+2212  | N/A    ||



### Esperanto additional letters

ĈĜĤĴŜŬ\
ĉĝĥĵŝŭ



## Running and building the site

### Technological stack

The website is built using the [Hugo] static generator.
Its compilation and deployment are automated through [Gitlab CI/CD](https://about.gitlab.com/product/continuous-integration/).

This site was inspired from [Netlify's Victor Hugo template](https://github.com/netlify-templates/victor-hugo).
The main idea is to have a website statically generated using [Hugo] while relying on [Node.js] to pilot the building process.
[Scripts](https://gohugo.io/hugo-pipes/js/) and [styles](https://gohugo.io/hugo-pipes/scss-sass/) are compiled using Hugo's pipes.


### Quick start

1. Prerequisites: you should have [Git](https://git-scm.com), [Node.js] and NPM installed.
2. Clone the repository.
3. Run `npm i` to install all required Node dependencies.
4. Run one of the following commands to test out the website:
   * `npm start`: Starts the website locally (`http://localhost:1313` for English, `http://localhost:1314` for French);
   * `npm run preview`: Same as `npm start`, but also includes draft and future posts (excluded by default);
   * `npm run build`: Builds the static version of the site in `dist/en` and `dist/fr`.


### Create a new post or story

[Archetypes](https://gohugo.io/content-management/archetypes/) are defined to make the creation of new posts easier.
To create a new post, use the following command: `npx hugo new blog/<path-to-my-post>/index.md`.


### Notes and warnings

- SVG icons are not watched.
If you need to add an icon to the SVG sprite, you will need to restart the script to have it taken into account.

- Hugo can minify the generated HTML.
However, to ease the debugging of templates, it was chosen to activate this option only for the building of sites.
This may result in differences between the locally served versions and the final build.



## License

### Source code

The source code includes everything that is generic and used to build the website: HTML template, JavaScript scripts, SCSS stylesheets, Node, Rollup and other tools configuration ...

**All source code is licensed under [the MIT license](https://mit-license.org/).**

Don't hesitate to draw inspiration from it (yeah, copy-pasting is authorized if it helps you).


### Web site content

This repository also includes the content published on the website.

**Unless otherwise noted, all contents are licensed under a [Creative Commons Attribution 4.0 International license](https://creativecommons.org/licenses/by/4.0/).**
Exceptions may be highlighted in the page's front matter or as a license file in the same directory as that content.


### Exceptions

Some sources and images are available in this repo but are not my property.
Most of them were embedded to take advantage of modern HTTP protocols (so that your browser won't have to open a connection to a third-party site).
They remain the property of their respective authors.

> This website includes Font Awesome Pro icons I bought a license for.
> Those icons are protected by [the Font Awesome Pro License](https://fontawesome.com/license).

> Duke images are shared by Yannick Kirschhoffer, under the [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.

> [XKCD comics](https://xkcd.com/) are licensed under the [CC BY-NC 2.5](https://creativecommons.org/licenses/by-nc/2.5/) license.

> [Vincent Déniel's drawings](https://vincentdnl.com/drawings/) are licensed under the [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) license.


[Hugo]: https://gohugo.io
[Node.js]: https://nodejs.org/en/
[Rollup]: https://rollupjs.org/
