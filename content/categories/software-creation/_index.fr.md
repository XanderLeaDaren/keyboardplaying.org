---
title: Création logicielle
description: |-
  La création logicielle couvre une variété de métiers, allant du développeur à l'architecte, sans oublier l'analyste et bien d'autres.
  Cette catégorie adresse des sujets pour toutes ces personnes et d'autres, dont des conseils d'outils et bonnes pratiques de développement, et des idées pour l'architecture ou la gestion de projet.
weight: 3
slug: creation-logicielle

termCover:
  banner: false
  src: fabian-grohs-GVASc0_Aam0-unsplash.jpg
  alt: Une page de cahier comportant des notes sur des fonctionnalités et algorithmes, à côté d'un clavier
  by: Fabian Grohs
  authorLink: https://unsplash.com/@grohsfabian
  link: https://unsplash.com/photos/GVASc0_Aam0
  license: Unsplash
---
