---
title: Software creation
description: |-
  We all partake in software creation, as developers, architects, analysts...
  This category covers topics for all these people and probably more, from coding tools and best practices to architecture and management recommendations.
weight: 3
slug: software-creation

termCover:
  banner: false
  src: fabian-grohs-GVASc0_Aam0-unsplash.jpg
  alt: Notes about software features and algorithms on a white notebook, next to a keyboard
  by: Fabian Grohs
  authorLink: https://unsplash.com/@grohsfabian
  link: https://unsplash.com/photos/GVASc0_Aam0
  license: Unsplash
---
