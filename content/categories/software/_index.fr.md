---
title: Logiciel & numérique
description: |-
  Le logiciel et le numérique nous entourent, ils nous facilitent la vie.
  Ce ne sont pour autant pas une panacée et nous ne savons pas toujours tout à leur sujet.
  Voici quelques pensées sur le logiciel et le numérique, et peut-être quelques idées pour mieux les exploiter.
weight: 3
slug: logiciel-numerique

termCover:
  banner: false
  src: thisisengineering-raeng-3Z3gnA99PL8-unsplash.jpg
  alt: Ordinateur portable affichant un logiciel de conception assistée par ordinateur.
  by: ThisisEngineering RAEng
  authorLink: https://unsplash.com/@thisisengineering
  link: https://unsplash.com/photos/3Z3gnA99PL8
  license: Unsplash
---
