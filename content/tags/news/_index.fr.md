---
title: Actualités
description: |-
  On peut espérer que les billets d'un blogue réagissent un tant soit peu aux actualités.
  Ceux associés à ce mot-clé le font ou en annoncent eux-mêmes.
slug: actualites

termCover:
  banner: false
  src: elijah-o-donnell-t8T_yUgCKSM-unsplash.jpg
  alt: Un homme lit un journal en train de brûler
  by: Elijah O'Donnell
  authorLink: https://unsplash.com/@elijahsad
  link: https://unsplash.com/photos/t8T_yUgCKSM
  license: Unsplash
---
