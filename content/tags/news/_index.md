---
title: News
description: |-
  It can be expected from a blog to react to the news.
  The posts with this tag do, or are news themselves.
slug: news

termCover:
  banner: false
  src: elijah-o-donnell-t8T_yUgCKSM-unsplash.jpg
  alt: A man reads a burning newspaper
  by: Elijah O'Donnell
  authorLink: https://unsplash.com/@elijahsad
  link: https://unsplash.com/photos/t8T_yUgCKSM
  license: Unsplash
---
