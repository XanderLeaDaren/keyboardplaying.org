---
title: Node & NPM
description: |-
  First intended as tools for building JavaScript-based server-side solutions, Node and NPM have brought a set of new tools to front-end development.
weight: 11
---
