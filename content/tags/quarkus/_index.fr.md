---
title: Quarkus
description: 
  Quarkus est une pile technologique Java conçue pour Kubernetes, OpenJDK HotSpot et GraalVM.
  Elle est construite à partir de bibliothèques et standards Java soigneusement sélectionnés.
  Elle apporte un temps de démarrage ultra-rapide, une empreinte mémoire minime et donne même la possibilité de compiler votre code en application native pour des performances encore meilleures.
weight: 16

termCover:
  banner: false
  src: quarkus-logo.jpg
  alt: Le logo de Quarkus
  by: Quarkus
  link: https://quarkus.io
  license: CC BY 3.0
---
