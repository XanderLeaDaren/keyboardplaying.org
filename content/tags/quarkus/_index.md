---
title: Quarkus
description: 
  Quarkus is a Kubernetes Native Java stack tailored for OpenJDK HotSpot and GraalVM, crafted from handpicked Java libraries and standards.
  It brings amazingly fast boot time, low memory footprint and gives the possibility to compile your code to a native application for even better performances.
weight: 16

termCover:
  banner: false
  src: quarkus-logo.jpg
  alt: Quarkus's logo
  by: Quarkus
  link: https://quarkus.io
  license: CC BY 3.0
---
