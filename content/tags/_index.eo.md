---
title: Etikedoj
description: |-
  Ne necesas enkonduki vin al la koncepto de "etikedoj."
  Se vi volas malpli detalan klasifikon, rigardu niajn  [kategoriojn](/blogo/kategorioj/).
url: /blogo/etikedoj
---
