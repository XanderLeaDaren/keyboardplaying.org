---
title: Best practices
description: |-
    Design patterns are famous because they offer optimal solutions to usual software creation issues.
    In the same way, many habits are common in software creation while not being optimized.
    That's why best practices exist: share with all a way to do things that seems better than others in most cases.
    And since it's about sharing, the blog seems to be a good place.
weight: 17
slug: best-practices
---
