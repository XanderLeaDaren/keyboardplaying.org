---
title: Développement
description: |-
  Pour écrire du code, il nous faut des outils et des bonnes pratiques.
  Ici est le lieu idéal pour partager autour de ces sujets.
weight: 5
slug: developpement

termCover:
  banner: false
  src: fabian-grohs-XMFZqrGyV-Q-unsplash.jpg
  alt: MacBook affichant un éditeur de code, à côté d'un carnet avec des notes sur le programme à développer
  by: Fabian Grohs
  authorLink: https://unsplash.com/@grohsfabian
  link: https://unsplash.com/photos/XMFZqrGyV-Q
  license: Unsplash
---
