---
title: Langue française
description: |-
  On donne souvent des bonnes pratiques quand il s'agit de développement, mais la langue suit aussi une grammaire.
  Pour elle aussi, des bonnes pratiques et recommandations sont une bonne chose.
weight: 5
slug: langue-francaise
---
