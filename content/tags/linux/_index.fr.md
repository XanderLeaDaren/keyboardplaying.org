---
title: Linux
description: |-
  Linux est un système d'exploitation libre et ouvert.
  Son utilisation implique des habitudes différentes, dont certaines peuvent ressembler à des astuces pour le non-initié.
weight: 10
slug: linux
---
