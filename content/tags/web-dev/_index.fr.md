---
title: Développement web
description: |-
  Le développement web s'appuie sur de nombreuses technologies : HTML, CSS, JavaScript, WebAssembly...
  Et les outils dans ce domaine sont nombreux et intéressants.
  Ça vaut le coup d'y jeter un œil.
weight: 10
slug: developpement-web

termCover:
  banner: false
  src: aral-tasher-vWsubsaym3o-unsplash.jpg
  alt: Un écran partagé, avec un éditeur de code à gauche et plusieurs logos à droite, dont ceux de frameworks web
  by: Aral Tasher
  authorLink: https://unsplash.com/@araltasher
  link: https://unsplash.com/photos/vWsubsaym3o
  license: Unsplash
---
