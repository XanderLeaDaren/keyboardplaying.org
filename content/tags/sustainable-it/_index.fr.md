---
title: Numérique responsable
description: |-
  Le numérique est un outil fantastique qui peut rendre le monde meilleur.
  Nous l'utilisons pourtant souvent sans tenir compte de ses impacts sur l'environnement ou la société.
  Nous souhaitons partager des idées pour faire du numérique --- et plus particulièrement de la création logicielle --- quelque chose de durable plutôt qu'un autre boulet pour nous couler plus rapidement.
weight: 20
slug: numerique-responsable

termCover:
  banner: false
  src: ai-for-good.jpg
  alt: Le cercle des objectifs de développement durable de l'ONU
  by: ITU Pictures
  authorLink: https://www.flickr.com/photos/itupictures/
  link: https://www.flickr.com/photos/itupictures/39621400230/
  license: CC BY 2.0
---
