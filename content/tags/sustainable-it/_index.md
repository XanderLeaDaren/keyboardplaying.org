---
title: Sustainable digital
description: 
  Digital is a great tool to make the world a better place.
  Yet, we are often  oblivious about its impacts on the environment or the society.
  Here, we'd like to share some ideas about how to turn digital---especially software creation---into something sustainable rather than something that will drown us faster.
weight: 20
slug: sustainable-it
aliases: [ /tags/sustainable-digital ]

termCover:
  banner: false
  src: ai-for-good.jpg
  alt: The circle of UN's sustainable development goals
  by: ITU Pictures
  authorLink: https://www.flickr.com/photos/itupictures/
  link: https://www.flickr.com/photos/itupictures/39621400230/
  license: CC BY 2.0
---
