---
title: Oracle
description: |-
  Oracle Corporation sells database software and technology, and enterprise software products.
  It is widely known for its databases and the fact that it bought Sun's JDK a few years back.
weight: 12
slug: oracle

termCover:
  banner: false
  src: oracle-headquarters--davidlohr-bueso.jpg
  alt: Oracle Headquarters in Redwood City
  by: Davidlohr Bueso
  authorLink: https://www.flickr.com/photos/daverugby83/
  link: https://www.flickr.com/photos/daverugby83/8259922368
  license: CC BY 2.0
---
