---
title: Tags
description: |-
  Is there a need to explain what tags are?
  If you want a less detailed sorting of the posts, have a look at our [categories](/blog/categories/).
url: /blog/tags
---
