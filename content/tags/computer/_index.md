---
title: Computer
description: |-
  How could we develop without a computer?
  As in, the physical thing.
  These posts relates to desktops or laptops, or anything with a CPU actually.
weight: 5
slug: computer

termCover:
  banner: false
  src: caspar-camille-rubin-7SDoly3FV_0-unsplash.jpg
  alt: The interior of a computer shows through its clear facade.
  by: Caspar Camille Rubin
  authorLink: https://unsplash.com/@casparrubin
  link: https://unsplash.com/photos/7SDoly3FV_0
  license: Unsplash
---
