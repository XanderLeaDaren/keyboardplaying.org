---
title: Ordinateur
description: |-
  Comment pourions-nous développer sans un ordinateur ?
  Oui, le machin bien matériel qui sert à saisir et exécuter notre virtuel.
  Ces billets portent sur les ordinateurs, portables ou de bureau, ou n'importe quoi avec un processeur, en fait.
weight: 5
slug: ordinateur

termCover:
  banner: false
  src: caspar-camille-rubin-7SDoly3FV_0-unsplash.jpg
  alt: L'intérieur d'un ordinateur est visible à travers une façade transparente.
  by: Caspar Camille Rubin
  authorLink: https://unsplash.com/@casparrubin
  link: https://unsplash.com/photos/7SDoly3FV_0
  license: Unsplash
---
