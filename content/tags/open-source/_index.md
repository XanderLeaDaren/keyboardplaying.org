---
title: Open Source
description: |-
  Open sourcing your software---or hardware---means that people will know how it works, will be able to modify it and to continue using it when you can no longer offer support.
weight: 17
slug: open-source
---
