---
title: Databases
description: |-
  Most software projects need a repository of some kind to store their data.
  And most of those use a database, that can be structured (SQL) or not (NoSQL).
weight: 5
slug: database

termCover:
  banner: false
  src: campaign-creators-IKHvOlZFCOg-unsplash.jpg
  alt: Someone draws a diagram on glass. The photo is focused on "database."
  by: Campaign Creators
  authorLink: https://unsplash.com/@campaign_creators
  link: https://unsplash.com/photos/IKHvOlZFCOg
  license: Unsplash
---
