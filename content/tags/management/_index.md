---
title: On Managing Coders & Software Projects
description: 
  Developers and project managers are different kind of people.
  Yet, they have to work together to achieve great results in great projects.
  We'd love to share some insight for both parties, based on our own experience as developers and team leaders.
weight: 6
slug: managing-coders-software-projects

termCover:
  banner: false
  src: austin-distel-wD1LRb9OeEo-unsplash.jpg
  alt: Three sitting men listen to a fouorth one using a whiteboard with sticky notes
  by: Austin Distel
  authorLink: https://unsplash.com/@austindistel
  link: https://unsplash.com/photos/wD1LRb9OeEo
  license: Unsplash
---
