---
title: Download proxy
description: |-
  This tool was designed to help you download files you need for your work.

  When working on the network of a company, you may have all permissions to do what you want on your machine and still be unable to download a file you need because of restrictive firewall rules.
  This proxy bypasses it by redirecting the stream of the download or transforming it so that it will be compliant with those.

layout: download-proxy
scripts: [ dlproxy ]

tipping: true

intro: |-
  Read more about this tool [here](/blog/2020/04/download-proxy-back/).

  This tool's only purpose is to save time to perform legit actions.
  **You are solely responsible for the way you use this tool.**

proxy:
  url: https://kp-dlproxy.herokuapp.com/dl/
  transformations:
  - key: idt
    code: IDENTITY
    label: Redirect download but don't alter file
    description: |-
        This is the simplest way to use the download proxy.
        This solution is adapted when the only rule preventing your download is about the domain you're downloading from.
  - key: zip
    code: ZIP
    label: Zip file
    description: |-
        The proxy returns the file in a zip archive.
        You know what to do next…
  - key: imz
    code: IMAGE_ZIP
    label: Unzippable image
    description: |-
        This transformation returns an image which contains the original file as a zip.
        You can open this image with an archiving software, such as [7-Zip](https://www.7-zip.org/) or [WinRAR](https://www.rarlab.com/download.htm).
        The `unzip` command is a Linux-compatible solution.
  - key: b64
    code: BASE64
    label: Encode download to base64
    description: |-
        Provides a base64-encoded version of your download.
        To decode it, you may use the following commands:

        ```sh
        # On Linux or Git Bash
        base64 -d file.txt.b64 > file.txt
        # On Windows
        certutil -decode file.txt.b64 file.txt
        ```
  - key: jpg
    code: FAKE_JPEG
    label: Fake picture
    description: |-
        Adds JPEG's [magic numbers](https://en.wikipedia.org/wiki/Magic_number_(programming)#Format_indicators) to the downloaded file.

        The revert transformation consists in removing the first three and last two bytes/characters of the file.
        Under Linux, you can use the following command:

        ```sh
        sed 's/^\xFF\xD8\xFF//g' file.exe.jpg | sed 's/\xFF\xD9$//' > file.exe
        ```
---
