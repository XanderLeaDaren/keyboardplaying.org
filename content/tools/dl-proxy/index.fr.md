---
title: Download proxy
description: |-
  Cet outil a été conçu pour vous aider à télécharger ce dont vous avez besoin pour travailler.

  Lorsque vous travaillez sur le réseau d'une société, il se peut que vous ayez tous les droits sur votre machine sans pour autant pouvoir télécharger le fichier dont vous avez besoin, à cause du pare-feu de l'entreprise.
  Ce proxy permet de contourner cet obstacle en redirigeant le flux, voire en le transformant afin qu'il puisse passer le mur.

layout: download-proxy
scripts: [ dlproxy ]

tipping: true

intro: |-
  Vous pouvez en lire davantage [ici](/blogue/2020/04/download-proxy-retour/).

  L'unique but de cet outil est de vous faire gagner du temps pour des actions légitimes.
  **Vous êtes entièrement responsable de la façon dont vous utilisez cet outil.**

proxy:
  url: https://kp-dlproxy.herokuapp.com/dl/
  transformations:
  - key: idt
    code: IDENTITY
    label: Redirection sans altération
    description: |-
        C'est l'utilisation la plus simple du proxy.
        Elle est adaptée quand la règle vous empêchant de télécharger se base sur l'adresse d'origine.
  - key: zip
    code: ZIP
    label: Fichier Zip
    description: |-
        Le proxy vous retourne le fichier que vous souhaitez télécharger dans un fichier Zip.
        Vous savez quoi faire ensuite…
  - key: imz
    code: IMAGE_ZIP
    label: Image à décompresser
    description: |-
        Cette transformation retourne une image qui contient le fichier d'origine sous forme d'un zip.
        Vous pourrez ouvrir ce fichier avec un programme d'archivage, comme [7-Zip](https://www.7-zip.org/) ou [WinRAR](https://www.rarlab.com/download.htm).
        La commande `unzip` est une autre possibilité sous Linux.
  - key: b64
    code: BASE64
    label: Encodage en base64
    description: |-
        Cette transformation vous fournit une version en base64 de votre téléchargement.
        Pour le décoder, vous pouvez utiliser les commandes suivantes :

        ```sh
        # Sous Linux ou Git Bash
        base64 -d file.txt.b64 > file.txt
        # Sous Windows
        certutil -decode file.txt.b64 file.txt
        ```
  - key: jpg
    code: FAKE_JPEG
    label: Fausse image
    description: |-
        Ajoute les [nombres magiques](https://fr.wikipedia.org/wiki/Nombre_magique_(programmation)#Indicateur_de_format) du format JPEG.

        La transformation inverse consiste à retirer les trois premiers et deux derniers octets/caractères du fichier.
        Sous Linux, vous pouvez utiliser la commande suivante :

        ```sh
        sed 's/^\xFF\xD8\xFF//g' file.exe.jpg | sed 's/\xFF\xD9$//' > file.exe
        ```
---
