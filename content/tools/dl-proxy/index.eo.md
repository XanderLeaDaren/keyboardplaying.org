---
title: Download Proxy (elŝuta prokurilo)
description: |-
  Ĉi tiu ilo estas konceptata por help vi elŝuti dosieroj vi bezonas por labori.
  
  Kiam vi laboras en la reto de kompanio, vi eble havas ĉiujn permesojn fari tion, kion vi volas en via maŝino, kaj tamen ne povas elŝuti dosieron, kiun vi bezonas, pro restrikta fajroŝirmilo.
  Ĉi tiu prokurilo preterpasas ĝin redirektante la fluon de la elŝuto aŭ transformante ĝin tiel ke ĝi konformiĝos al tiu.

layout: download-proxy
scripts: [ dlproxy ]

tipping: true

intro: |-
  <!--Legu pli pri la prokurilo [ĉi tie](/blog/2020/04/download-proxy-back/).-->

  Ĉi tiu ilo nur celas ŝpari tempon por plenumi legitimajn agojn.
  **La uzanto respondecas nur pri la uzo de ĉi tiu ilo.**

proxy:
  url: https://kp-dlproxy.herokuapp.com/dl/
  transformations:
  - key: idt
    code: IDENTITY
    label: Alidirekti la elŝuto sed ne modifi la dosiero
    description: |-
        Ĝi estas la plej simpla maniero uzi la elŝutan prokurilon.
        Ĉi tiu solvo adaptiĝas kiam la sola regulo malebliganta vian elŝutadon temas pri la domajno de kiu vi elŝutas.
  - key: zip
    code: ZIP
    label: Densigi dosiero
    description: |-
        La prokurilo elŝutas la dosieron en zip-arkivo.
        Vi scias, kion fari poste…
  - key: imz
    code: IMAGE_ZIP
    label: Maldensigebla bildo
    description: |-
        Ĉi tiu transformo redonas bildon, kiu enhavas la originalan dosieron kiel zip.
        Vi povas malfermi ĉi tiun bildon per ar softwareiva programaro, kiel ekzemple [7-Zip](https://www.7-zip.org/) aŭ [WinRAR](https://www.rarlab.com/download.htm) .
        La komando `unzip` estas solvo Linuks-kongrua. 
  - key: b64
    code: BASE64
    label: Ĉifri elŝuton al base64
    description: |-
        Provizas base64-koditan version de via elŝuto.
        Por deĉifri ĝin, vi povas uzi la jenajn komandojn: 

        ```sh
        # En Linukso aŭ Git Baŝo
        base64 -d file.txt.b64 > file.txt
        # En Vindozo
        certutil -decode file.txt.b64 file.txt
        ```
  - key: jpg
    code: FAKE_JPEG
    label: Falsa bildo
    description: |-
        Aldonas la formindikiloj de JPEG al la elŝutita dosiero.

        La reverta transformo konsistas en forigo de la unuaj tri kaj lastaj du bajtoj/signoj de la dosiero.
        En Linukso, vi povas uzi la jenan komandon: 

        ```sh
        sed 's/^\xFF\xD8\xFF//g' file.exe.jpg | sed 's/\xFF\xD9$//' > file.exe
        ```
---
