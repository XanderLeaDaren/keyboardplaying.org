---
date: 2020-03-02T21:40:50+01:00
title: How to Choose Your OJDBC Driver
subtitle: Which is the abscissa, which is the ordinate?
slug: choose-ojdbc-driver
description: |-
  If you ever had to choose an OJDBC driver, you may have wondered about the numbers and versions.
  Or not, as long as it works, which is almost always.
  I'll explain, though.
author: chop
categories: [ software-creation ]
tags: [ java, maven, database, oracle ]
keywords: [ java, maven, database, oracle, ojdbc, jdbc driver ]

references:
- id: ojdbc-faq
  name: Oracle JDBC FAQ
  url: https://www.oracle.com/database/technologies/faq-jdbc.html
  lang: en
- id: medium-ojdbc-maven
  name: Oracle JDBC drivers on Maven Central
  title: true
  url: https://medium.com/oracledevs/oracle-jdbc-drivers-on-maven-central-64fcf724d8b
  date: 09/2019
  lang: en
  author:
    name: Kuassi Mensah, Medium
- id: mvn-central-ojdbc
  name: ojdbc artifacts on Maven Central
  url: https://search.maven.org/search?q=g:com.oracle.ojdbc
---

When you develop a Java program that accesses a database, you're likely to need a JDBC driver.
When that database is an Oracle product, you keep that O for OJDBC.

For years, I just used the version an architect had selected.
Then, I became the architect and I had to understand which version to choose.
It's not that complicated, just not really well explained.
Let me try to contribute...

<!--more-->


## A Quick Note on JDBC

"JDBC" stands for "Java Database Connectivity."
This describes an API for the Java programming language and defines how a client may access a database.

I won't get technical here.
In most cases, you need to add a JDBC driver which matches the database you aim to connect to, and which may bring optimizations depending on the engine.
The version of the driver often depends on the version of the database.

In case of Oracle JDBC (OJDBC), there's often some confusion because there's the version, and then, there's also this number in the Jar's name.


## What OJDBC's Versions Mean

### Explaining the Many Numbers Implied

If you download your JDBC driver from Oracle's website, once you solve the riddle of finding the [page of interest][ojdbc-download], you first need to select the version of Oracle database.
Once you get there, you have several options.
For instance, **[for Oracle 19.3][ojdbc-19-download], you must choose from ojdbc10 and ojdbc8**.
But what are those?

<aside><p>
The number after <code>ojdbc</code> relates to the version of the JDK.
</p></aside>

Well, the number between `ojdbc` and `.jar` specifies which JDK version this driver was designed for.
For instance, `ojdbc8.jar` was written for JDK 8.
Of course, it will work with JDK 9, 10 and later---well, maybe it will stop working somewhere down the line, but I guess you'll have updated your database by then.


### Habits With Maven {#maven-habits}

In almost all cases in my experience, we used Maven to build our projects.
This means having an OJDBC artifact in our repository.

Every time, this Jar was uploaded to the third party repository, with the following characteristics:

`groupId`
: some package name including `oracle` or `ojdbc` somewhere

`artifactId`
: the name of the Jar (e.g. `ojdbc8`)

`version`
: the version of the database


### What About Newer Databases With Older Programs?

Wait, I said that for Oracle 19, only ojdbc8 and 10 were available.
But you're working on a ten-year-old corporate project that's still in Java 6.
All the company databases are being upgraded to 19, though.
What will you do?

Theoretically, you should upgrade your Java version to be able to upgrade the driver.
Oracle's OJDBC FAQ is quite explicit on this topic:

> **What are the Oracle JDBC releases Vs JDK versions?**
>
> The Oracle JDBC driver is always compliant to the latest JDK version in each of the new releases.
> In some versions, JDBC drivers support multiple JDK versions.
> Use the table below to choose the correct JDBC driver based on your preferred JDK version.
>
> | Oracle Database Version | JDBC Jar files specific to the release |
> | ----------------------- | -------------------------------------- |
> | 19.3                    | **ojdbc10.jar** with JDK10, JDK11<br>**ojdbc8.jar** with JDK8, JDK9, JDK11 |
> | 18.3                    | **ojdbc8.jar** with JDK8, JDK9, JDK10, JDK11 |
> | 12.2 or 12cR2           | **ojdbc8.jar** with JDK 8               |
> | 12.1 or 12cR1           | **ojdbc7.jar** with JDK 7 and JDK 8<br>**ojdbc6.jar** with JDK 6 |
> | 11.2 or 11gR2           | **ojdbc6.jar** with JDK 6, JDK 7 and JDK 8<br>(Note: JDK7 and JDK8 are supported in 11.2.0.3 and 11.2.0.4 only)<br>**ojdbc5.jar** with JDK 5 |
>
> {{< ref-cite "ojdbc-faq" >}}

_Yet_, there's no need to panic.
Basically, yes, upgrading is a best practice.
From the same FAQ, we can quote the following:

> The recommendation is that JDBC driver version should always be either same as or higher than the Oracle database version in order to leverage the latest capabilities of the driver.
>
> {{< ref-cite "ojdbc-faq" >}}

And I still insist there's no need to panic: you can be almost sure that your application will still work with another version.
That's probably why we don't really know how to choose our OJDBC driver and don't really care about learning that.


## Why Almost Nobody Gets It or Cares

### It's Counter Intuitive

There's one good reason why people don't always get this double number thing: it goes against traditional versioning.
It's even clearer if you look at it from a Maven perspective.

In a Maven repository, the GAV (group, artifact, version) is a set of coordinates for a dependency.
The `groupId` and `artifactId` define the identity of it: `org.springframework.boot:spring-boot-starter-parent` is a starter kit for building Spring Boot projects.
You'll usually look those up first, and then check what versions are available, those versions being iterations of something semantically identical.

<aside><p>
For OJDBC, the version is the main identifier of a lib that is available in several forms.
</p></aside>

With OJDBC, you have to go the other way around: you need to know your version first, because it somehow defines the nature of the Jar you're looking for: the OJDBC driver in version 19.3.0.0 is a driver for Oracle 19.3.
Only then can you look the available artifacts and choose which one is best for you.

People coming from the Maven world may browse the dependencies under the correct group, see that `ojdbc10` is above `ojdbc8` and just assume that it's better because it's higher.
Then, not knowing, they might just take the highest available version.
And you know what?
It would probably work.[^fn-jdk-version]

[^fn-jdk-version]: The most important constraint for this example is that the driver is compatible with the JDK of the project.


### It Works Either Way

Why should we spend so much time learning what those numbers mean and how to choose them when just taking the higher ones work?

<aside><p>
No need to spend time on picking a version, this one works.
</p></aside>

Developers love to solve problems, but good developers are also lazy.
They love spending days writing a script that'll save them hours by sparing them a repetitive task.

But being lazy also means that, when working on a topic that is of no interest to them, they'll settle for "that works."
Why spend more time perfecting something that already work?
Project managers are too rarely helpful on this front.
How many times have I heard: "It works, leave it be. No need to spend more on that."

Don't put it all on the developer, though.
Where there's a student, there should be a teacher.
That's where the architect/lead dev/tech comes in.
None of them ever explained this to me and I learned only when I looked for it.

Anyway, yes, doing the lazy thing will probably work, whatever the versions you select.
SQL doesn't evolve that much, so you'll miss some of the latest features, maybe some performances, but an older driver should do the trick.
If you can't get the driver specific to your database, Oracle recommends using a more recent one.

> **What is the JDBC and RDBMS interoperability matrix or the certification matrix?**
>
> Please refer to the table that covers the JDBC driver interoperability matrix for the supported Oracle database versions.
> The recommendation is that JDBC driver version should always be either same as or higher than the Oracle database version in order to leverage the latest capabilities of the driver.
>
> | Interoperability Matrix | Database 19.3 | Database 18.3 | Database 12.2 and 12.1 | Database 11.2.0.4 |
> | ----------------------- | ------------- | ------------- | ---------------------- | ----------------- |
> | **JDBC 19.3**           | Yes           | Yes           | Yes                    | Yes               |
> | **JDBC 18.3**           | Yes           | Yes           | Yes                    | Yes               |
> | **JDBC 12.2 and 12.1**  | Yes           | Yes           | Yes                    | Yes               |
> | **JDBC 11.2.0.4**       | Yes           | Yes           | Yes                    | Yes               |
>
> {{< ref-cite "ojdbc-faq" >}}


## An Anecdote

On a funny note, for a long time, Java versions were long prefixed with `1.`.
Java 5 was thus really Java 1.5.
Most developers know that this prefix is useless and historical, but it might have some pretty funny consequences quite soon: a developer on my team recently used `ojdbc14` for a legacy project.
Yes, it is correct: it is the OJDBC driver for Java 1.4 (and Oracle 10 in this case).

Now, this month, Java 14 will be released and will be the new LTS version, so there's a good chance that the driver for Oracle 20 will be available as `ojdbc14` again.
The revival of an older line...


## The Icing on the Cake

<aside><p>Oracle JDBC drivers are now available on Maven Central.</p></aside>

Since last September, and starting with release 19.3.0.0, [Oracle JDBC drivers are available on Maven Central][medium-ojdbc-maven].
They respected the habits I wrote upon [above](#maven-habits).

You can find them by looking for the [`groupId` com.oracle.ojdbc][mvn-central-ojdbc].


## The Takeaway

When looking for an Oracle JDBC driver:
- select the version in accord with your Oracle database;
- pick `ojdbcN` were _N_ is the highest lower or equal to the version of the JDK compiling the project;
- download from Maven Central if you use an Oracle 19.3 database or later;
- explain to your team which version you use and why.


{{% references %}}

[ojdbc-download]: https://www.oracle.com/database/technologies/appdev/jdbc-downloads.html
[ojdbc-19-download]: https://www.oracle.com/database/technologies/appdev/jdbc-ucp-19c-downloads.html
