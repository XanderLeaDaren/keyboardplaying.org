---
date: 2020-10-06T07:21:32+02:00
title: Installing Scrivener 3 on Linux
subtitle: Would you care for some wine to get started?
slug: install-scrivener-3-linux
description: |-
  Scrivener doesn't provide support for Linux.
  Yet, all hope is not lost.
cover:
  banner: false
  src: scrivener-kubuntu.jpg
  alt: Scrivener 3 splash screen
  by: chop
  license: CC BY-NC 4.0
author: chop
categories: [ writing, software ]
tags: [ linux ]
keywords: [ wine, scrivener, winetricks ]

references:
- id: pletcher-scrivener3-linux
  name: "Update: Scrivener 3 on Linux"
  title: true
  url: https://medium.com/@tpletcher/update-scrivener-3-on-linux-8cea1e2739a
  date: 08/2020
  lang: en
  author:
    name: Thomas Pletcher
---

As I'm preparing for my first attempt at [NaNoWriMo], I know I must have my writing software ready.
I used [Scrivener] in the past and know it's a solution I love to use.
My only problem was: my mobility OS is Linux, which Scrivener doesn't provide support for.

But that's no longer a problem as [Thomas Pletcher proved][pletcher-scrivener3-linux] that the latest betas of [Scrivener 3] work with [Wine].
Here's how.


<!--more-->

## Installing wine

To make Scrivener run on Linux, we'll use [Wine], a compatibilty layer for running Windows applications on POSIX systems.

**Note**: This tutorial was written for people who don't know what Wine is and are probably installing it only to run Scrivener.
If you have a more advanced knowledge, you may wish to use prefixes.
Please do. I won't here.

Now, to the flesh of it:

1. We'll need Wine, [Winetricks] and winbind.
If those are available in your distribution's repos (they are for Ubuntu), installing them can be achieved with this command: `sudo apt install wine winetricks winbind`

2. The Wine installer in the Ubuntu repository has a slight defect: it doesn't create an important symbolic link, meaning you'll have to create yourself: `sudo ln -s /usr/share/doc/wine/examples/wine.desktop /usr/share/applications/`


## Provisioning Wine

Now that Wine is installed, you need to add the Windows components you will need to install and run Scrivener.

1. Optional: `winetricks dotnet corefonts` will install Windows default fonts (e.g. Times New Roman).
You may have already installed those from another source (e.g. `sudo apt install ttf-mscorefonts-installer`), in which case this is not required for you.
2. `winetricks win7` will install a Windows 7 architecture.
3. `winetricks dotnet48` will install .NET 4.8 (Scrivener needs at least .NET 4.6).
Check the "restart now" when required. Don't worry, you won't have to reboot.
4. `winetricks speechsdk`: the SpeechSDK is not listed as a requirement for Scrivener, but I couldn't run it without it.


## Installing Scrivener

1. You need to go and download Scrivener 3 (at the time of this writing, you can get the latest beta [here][Scrivener 3.0 beta]).
2. You should right-click your .exe file and open it with _Wine Windows Program Loader_ (may be the default, but also may be under _Open With_).

That's it!
At the end of the installation, Wine should have created an entry for Scrivener in your start menu, and you should be able to run it.
Don't hesitate to give additional tips as comments.



{{% references %}}
[NaNoWriMo]: https://nanowrimo.org/
[Scrivener]: https://www.literatureandlatte.com/scrivener/overview
[Scrivener 3]: https://www.literatureandlatte.com/scrivener-3-for-windows-update
[Scrivener 3.0 Beta]: https://www.literatureandlatte.com/forum/viewtopic.php?f=57&t=40621
[Wine]: https://www.winehq.org/
[Winetricks]: https://wiki.winehq.org/Winetricks
