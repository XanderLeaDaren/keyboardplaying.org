---
date: 2020-10-06T07:21:32+02:00
title: Installer Scrivener 3 sous Linux
subtitle: Un peu de vin pour démarrer ?
slug: installer-scrivener-3-linux
description: |-
  Scrivener ne fournit pas de support pour Linux.
  Pourtant, il reste un espoir.
cover:
  banner: false
  src: scrivener-kubuntu.jpg
  alt: L'écran de démarrage de Scrivener 3
  by: chop
  license: CC BY-NC 4.0
author: chop
categories: [ writing, software ]
tags: [ linux ]
keywords: [ wine, scrivener, winetricks ]

references:
- id: pletcher-scrivener3-linux
  name: "Update: Scrivener 3 on Linux"
  title: true
  url: https://medium.com/@tpletcher/update-scrivener-3-on-linux-8cea1e2739a
  date: 08/2020
  lang: en
  author:
    name: Thomas Pletcher
---

Tandis que je me prépare pour ma première tentative au [NaNoWriMo], je sais que j'aurai besoin d'un logiciel pour m'organiser.
J'ai utilisé [Scrivener] par le passé et je sais que c'est parfaitement adapté pour mon usage.
Mon seul problème : j'utilise Linux en déplacement, pour lequel Scrivener n'offre pas de support.

Mais ce n'est plus un problème, ainsi que l'a révélé [Thomas Pletcher proved][pletcher-scrivener3-linux] : les dernières betas de [Scrivener 3] fonctionnent avec [Wine].
Voilà comment.


<!--more-->

## Installation de Wine

Pour faire tourner Scrivener sous Linux, nous utiliserons [Wine], qui offre une couche de compatibilité pour exécuter des programmes Windows sur des systèmes POSIX.

**Note** : Ce tutoriel est écrit pour des personnes qui ne connaissent pas Wine et ne l'utiliseront probablement que pour Scrivener.
Si vous avez une connaissance plus avancée, vous voudrez peut-être utiliser un préfixe dédié.
Faites, je vous en prie. Je n'en parlerai pas ici.

Maintenant, pour faire court :

1. Nous aurons besoin de Wine, [Winetricks] et winbind.
Si ceux-ci sont disponibles dans les repos de votre distribution (c'est le cas pour Ubuntu), les installer se fera via la commande : `sudo apt install wine winetricks winbind`

2. L'installateur de Wine du dépôt Ubuntu a un petit défaut : il ne crée pas le lien symbolique qui permet de lancer l'application.
Pas grave, on va le faire à la main : `sudo ln -s /usr/share/doc/wine/examples/wine.desktop /usr/share/applications/`


## Configuration de Wine

Maintenant que Wine est installé, il va falloir ajouter les composants Windows dont Scrivener aura besoin.

1. Facultatif : `winetricks dotnet corefonts` installera les polices par défaut Windows (p. ex. Times New Roman).
Vous les avez peut-être déjà installées d'une autre façon (p. ex. `sudo apt install ttf-mscorefonts-installer`), en quel cas cette étape serait redondante.
2. `winetricks win7` préparera une architecture Windows 7.
3. `winetricks dotnet48` installera .NET 4.8 (Scrivener a besoin de .NET 4.6 ou plus). Cochez la case « Redémarrer maintenant » quand on vous le proposera.
Pas d'inquiétude, cela ne fera pas redémarrer votre machine.
4. `winetricks speechsdk`: le SpeechSDK n'est pas noté parmi les prérequis pour Scrivener, mais je n'ai pas pu l'exécuter avant de l'installer.


## Installation de Scrivener

1. Il vous faut télécharger Scrivener 3 (à l'heure de l'écriture de ces lignes, vous pouvez télécharger la dernière bêta [ici][Scrivener 3.0 beta]).
2. En faisant un clic droit sur le fichier .exe obtenu, vous devriez pouvoir l'ouvrir avec _Wine Windows Program Loader_ (cela devrait être l'option par défaut, mais elle pourrait aussi se cacher dans le sous-menu _Ouvrir avec_).

C'est tout !
À la fin de l'installation, Wine devrait avoir créé un raccourci pour Scrivener dans votre liste d'applications.
Vous devriez pouvoir le démarre en cliquant dessus.
N'hésitez pas à partager d'autres astuces dans les commentaires.



{{% references %}}
[NaNoWriMo]: https://nanowrimo.org/
[Scrivener]: https://www.literatureandlatte.com/scrivener/overview
[Scrivener 3]: https://www.literatureandlatte.com/scrivener-3-for-windows-update
[Scrivener 3.0 Beta]: https://www.literatureandlatte.com/forum/viewtopic.php?f=57&t=40621
[Wine]: https://www.winehq.org/
[Winetricks]: https://wiki.winehq.org/Winetricks
