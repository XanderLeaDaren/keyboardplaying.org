---
date: 2020-09-28T13:53:39+02:00
title: Supprimer les espaces en fin de ligne dans MS Word et LibreOffice
subtitle: Faisons un peu de ménage.
slug: supprimer-espaces-fin-ligne-ms-word-libreoffice
description: |-
  La rédaction d'un document passe par de nombreuses manipulations, qui mènent parfois à des espaces oubliés en fin de lignes.
  Généralement invisibles, ils ne gênent pas forcément, mais si vous préférez vous assure de leur absence, voici une astuce.
author: chop
categories: [ writing, software ]
tags: [ tips ]
keyword: [ ms word, microsoft word, libreoffice, libreoffice writer, nettoyage d'espaces, fin de ligne ]

references:
- id: howtogeek-replace-special-chars
  name: How to Find and Replace Special Characters in Microsoft Word
  title: true
  url: https://www.howtogeek.com/364843/how-to-find-and-replace-special-characters-in-microsoft-word/
  date: 07/2019
  lang: en
  author:
    name: Amelia Griggs, How-To Geek
---

Si vous aimez livrer des documents propres, il vous arrive certainement d'afficher les caractères invisibles dans Word et de parfois constater que certains paragraphes se terminent par `·¶`.
Oui, du fait des manipulations que subit un document lors de sa rédaction, il n'est pas rare d'avoir des espaces en fin de ligne.
Un réflexe qui me vient du développement est de les supprimer, mais Word ne fournit aucun outil pour le faire automatiquement.
Ou peut-être que si ?

<!--more-->

## Les conseils (presque) inutiles

En cherchant rapidement hier, je n'ai trouvé que les conseils suivants :
* « Alignez à droite, les espaces en fin de ligne seront évidents, vous n'aurez plus qu'à supprimer. »
* « Affichez les caractères invisibles, vous n'aurez plus qu'à supprimer. »

En résumé, débrouillez-vous pour les voir et supprimez-les à la main.
C'est entendable pour un document de deux pages, mais ce n'est bien entendu pas une solution viable s'il en fait trois-cents.
**Si vous voyez ces conseils pour un grand document, oubliez-les** (en particulier le premier qui pourrait détruire votre mise en page).


## Ma solution pour MS Word

Word permet de chercher et remplacer des caractères spéciaux (consultez [ce lien][howtogeek-replace-special-chars] pour découvrir comment et voir des exemples d'application).
En sachant cela, il suffit de trouver la bonne recherche et le bon remplacement, ce que j'ai fait hier soir.
La fenêtre _Remplacer_ peut être ouverte avec le raccourci <kbd>Ctrl</kbd> + <kbd>H</kbd>.

**Pour supprimer vos espaces de fin de ligne dans MS Word, recherchez les valeurs `^w^p` et remplacez avec `^p`.**
Exécutez le _Remplacer tout_ plusieurs fois (jusqu'à ce qu'il n'y ait plus de remplacement, pour le cas où vous auriez plusieurs espaces en fin de ligne).

* `^w` représente une espace (normale, insécable…).
* `^p` est une marque de paragraphe (le `¶`).

Vous pouvez aussi remplacer les `^w^l` par `^l` avec un plus fort risque d'impact sur la mise en page si vous justifiez vos textes (`^l` représente le retour à la ligne `⮐`).


## Et pour LibreOffice Writer ?

Pour LibreOffice Writer, la solution est encore plus simple (pour un habitué des expressions régulières).
Nous allons là encore utiliser la fenêtre _Remplacer_ (à ouvrir avec le raccourci <kbd>Ctrl</kbd> + <kbd>H</kbd>)

Dans _Autres options_, cochez _Expressions régulières_.

**Dans LibreOffice Writer, vous pouvez remplacer `\s+(\r?(\n|$))` par `$1` pour supprimer vos espaces de fin de ligne.**
Une seule exécution doit supprimer tous les espaces parasites.

L'expression se décompose de la façon suivante :
* `\s+` représente un ou plusieurs caractères d'espacements ;
* `(\r?(\n|$))` représente un retour chariot (`\r?\n`) ou une fin de paragraphe (`\r?$`), `\r?` servant uniquement pour être compatible avec le format de retour chariot de Windows ;
* `$1` représente le premier groupe entre parenthèses (`(\r?(\n|$))`) tel qu'il a été trouvé dans le texte (on reprend ce qui a été trouvé).

Les expressions régulières sont un vaste sujet que je n'aborderai pas davantage ici, mais n'hésitez pas à poser vos questions si vous en avez.

J'espère vous avoir été utile.

{{% references %}}
