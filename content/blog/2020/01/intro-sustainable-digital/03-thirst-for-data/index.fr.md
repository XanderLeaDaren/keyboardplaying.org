---
date: 2020-01-27T23:56:40+01:00
title: La donnée est le nouveau pétrole
subtitle: All your data belong to us.
slug: intro-numerique-responsable/donnee-nouveau-petrole
description: |-
  Il semblerait que le but de toutes les solutions numériques de nos jours soit de collecter des données à notre sujet.
  Quelques pensées.
cover:
  src: franki-chamaki-1K6IQsQbizI-unsplash.jpg
  alt: Des lettres en néon affichentNeon "Data has a better idea"
  by: Franki Chamaki
  link: https://unsplash.com/photos/1K6IQsQbizI
  authorLink: https://unsplash.com/@franki
  license: Unsplash
author: chop
categories: [ software ]
tags: [ sustainable-it ]
keywords: [ donnée, data, vie privée, big techs, intelligence artificielle, IA, RGPD, publicité, ADN ]
series: [ intro-sustainable-digital ]

references:
- id: internet-human-face
  name: The Internet with a Human Face
  title: true
  url: https://idlewords.com/talks/internet_with_a_human_face.htm
  date: 05/2014
  lang: en
  author:
    name: Maciej Cegłowski
- id: cnet-facial-recognition
  name: Facial recognition could take over, one 'convenience' at a time
  title: true
  url: https://www.cnet.com/news/at-ces-facial-recognition-creeps-into-everything/
  date: 01/2020
  lang: en
  author:
    name: Alfred Ng, CNET News
- id: forbes-stats-data
  name: How Much Data Do We Create Every Day? The Mind-Blowing Stats Everyone Should Read
  title: true
  url: https://www.forbes.com/sites/bernardmarr/2018/05/21/how-much-data-do-we-create-every-day-the-mind-blowing-stats-everyone-should-read/
  date: 05/2018
  lang: en
  author:
    name: Bernard Marr, Forbes
- id: data-never-sleeps
  name: Data Never Sleeps 7.0
  title: true
  url: https://www.domo.com/learn/data-never-sleeps-7
  date: 2019
  lang: en
  author:
    name: Domo
- id: nbcnews-fb-leveraged-data
  name: Leaked documents show Facebook leveraged user data to fight rivals and help friends
  title: true
  url: https://www.nbcnews.com/news/all/leaked-documents-show-facebook-leveraged-user-data-fight-rivals-help-n1076986
  date: 11/2019
  lang: en
  author:
    name: Olivia Solon & Cyrus Farivar, NBC News
- id: krebs-fb-passwords-plain
  name: Facebook Stored Hundreds of Millions of User Passwords in Plain Text for Years
  title: true
  url: https://krebsonsecurity.com/2019/03/facebook-stored-hundreds-of-millions-of-user-passwords-in-plain-text-for-years/
  date: 03/2019
  lang: en
  author:
    name: Krebs on Security
- id: forbes-fb-db-leaked
  name: Unsecured Facebook Databases Leak Data Of 419 Million Users
  title: true
  url: https://www.forbes.com/sites/daveywinder/2019/09/05/facebook-security-snafu-exposes-419-million-user-phone-numbers/
  date: 09/2019
  lang: en
  author:
    name: Davey Winder, Forbes
- id: amz-alexa-transcripts
  name: Answers from Amazon to Senator Coons about Alexa policies
  url: https://www.coons.senate.gov/imo/media/doc/Amazon%20Senator%20Coons__Response%20Letter__6.28.19%5B3%5D.pdf
  date: 06/2019
  lang: en
  author:
    name: Brian Huseman, Amazon
- id: google-speech-data
  name: More information about our processes to safeguard speech data
  title: true
  url: https://www.blog.google/products/assistant/more-information-about-our-processes-safeguard-speech-data/
  date: 07/2019
  lang: en
  author:
    name: David Monsees, Google
- id: wiki-rgpd
  name: General Data Protection Regulation
  title: true
  url: https://fr.wikipedia.org/wiki/R%C3%A8glement_g%C3%A9n%C3%A9ral_sur_la_protection_des_donn%C3%A9es
  lang: fr
  author:
    name: Wikipédia
- id: numerama-ccpa
  name: Le RGPD californien est sur le point de devenir une réalité
  title: true
  url: https://www.numerama.com/politique/596130-le-rgpd-californien-est-sur-le-point-de-devenir-realite.html
  date: 12/2019
  lang: fr
  author:
    name: Julien Lausson, Numérama
- id: shift-rapport
  name: Déployer la sobirété numérique
  title: true
  url: https://theshiftproject.org/article/rapport-intermediaire-deployer-sobriete-numerique/
  lang: fr
  author:
    name: The Shift Project
- id: amnesty-data-threat
  name: Les géants de la surveillance. Le modèle économique de Facebook et Google menace les droits humains (extraits)
  title: true
  url: https://www.amnesty.org/fr/documents/pol30/1404/2019/fr/
  date: 11/2019
  author:
    name: Amnesty International
- id: nytimes-clearview
  name: The Secretive Company That Might End Privacy as We Know It
  title: true
  url: https://www.nytimes.com/2020/01/18/technology/clearview-privacy-facial-recognition.html
  date: 01/2020
  lang: en
  author:
    name: Kashmir Hill, The New York Times
- id: nature-genomic-surveillance
  name: Crack down on genomic surveillance
  title: true
  url: https://www.nature.com/articles/d41586-019-03687-x
  date: 12/2019
  lang: en
  author:
    name: Yves Moreau, Nature
- id: cbs-biobank
  name: California Biobank Stores Every Baby's DNA; Parents Unaware Of Practice
  title: true
  url: https://sanfrancisco.cbslocal.com/2018/05/08/california-biobank-stores-baby-dna-access/
  date: 05/2018
  lang: en
  author:
    name: Julie Watts, CBS SF BayArea
- id: frama-health-data-hub
  name: Hold-up sur les données de santé. Patients et soignants unissons-nous
  title: true
  url: https://framablog.org/2019/11/29/hold-up-sur-les-donnees-de-sante-patients-et-soignants-unissons-nous/
  date: 11/2019
  lang: fr
  author:
    name: Framatophe, Framasoft
- id: nextinpact-bercy-datamining
  name: Bercy généralise l'utilisation du datamining
  title: true
  url: https://www.nextinpact.com/article/29938/108554-bercy-generalise-utilisation-datamining
  date: 01/2020
  lang: fr
  author:
    name: Jean-Marc Manach, NextInpact
---

Tous les projets dont on entend parler aujourd'hui semblent porter sur les données --- ou l'intelligence artificielle, ce qui revient basiquement au même.
La donnée n'est pas quelque chose de nouveau, mais l'enthousiasme qui l'entoure croît rapidement.
Nous ne sommes pas toujours conscients de ce qu'un site ou une application récolte à notre sujet, poussant les législateurs à légiférer sur ce qu'une société peut ou ne peut pas faire avec les données d'un utilisateur.

En quoi cela concerne-t-il le secteur de la création logicielle ?
Du simple fait de leur quantité, les données que nous collectons ne pourraient pas être traitées en une vie sans notre aide numérique.
Cela nous rend au moins en partie responsables de ce qui en est fait.

<!--more-->

## Nous donnons nos données

### Les données que nous produisons

Il y a quelques années, [certains se sont inquiétés des données que nous publiions sur Internet et l'usage qui pourrait en être fait][internet-human-face].
Les réseaux sociaux nous ont encouragés à partager encore davantage.
Cela semblait presque innocent au début.
Puis est arrivé l'Internet des objets.
Tout a été connecté et produit encore plus de données à vendre à des sociétés ou des annonceurs.
Les constructeurs souhaitent à présent faire pareil en [intégrant la reconnaissance faciale à tous leurs produits][cnet-facial-recognition].

<aside><p>Quatre-vingt-dix pour cent des données dans le monde ont été générées au cours des deux dernières années.</p></aside>

En 2018, [Forbes a publié quelques statistiques][forbes-stats-data].
L'auteur de l'article a par exemple précisé que 90 % des données mondiales avaient été générés au cours de deux années précédentes.
L'infographie [_Data Never Sleeps_][data-never-sleeps] de Domo donne quelques chiffres de ce qu'il se passe chaque minute.
Chaque gazouillis, chaque statut, chaque clic produit une donnée qui peut être vendue à un annonceur, pour l'aider à mieux comprendre vos habitudes et en déduire de nouvelles stratégies.

Une étude plus récente par IDC estime que **le volume de données stockées dans le monde atteindra 175 Zo en 2025**, soit **5,3 fois plus qu'en 2018**.

{{< figure src="size-global-datasphere.png" alt="Un graphique en barres représentant l'évolution annuelle de la taille de la datasphère" attr="Source : _Data Age 2025---The Digitization of the World from Edge to Core_" attrlink="https://www.seagate.com/fr/fr/our-story/data-age-2025/" >}}

<aside><p>« Si c’est gratuit, vous êtes le produit. »</p></aside>

Ces données sont un business et une devise.
On sait par exemple que [Facebook a donné un accès privilégié aux données de ses utilisateurs à Amazon, car cette dernière investissait dans la publicité sur Facebook][nbcnews-fb-leveraged-data].



### Pourquoi nous leur abandonnons ces données

<aside><p>Au début, nous n'avions pas le choix ou pas la connaissance.</p></aside>

Au début, nous n'avions pas réellement le choix, ni vraiment conscience de ce qui commençait.
Nous parcourions Internet et des cookies étaient stockés sur nos ordinateurs, mais nous ne savons pas vraiment ce qu'ils étaient.
Des pubs ont accroché nos yeux et nos clics, et nous avons produit des données sans même le savoir.
Nous avons commencé à poster, tweeter, produire nos propres contenus, pensant le partager avec nos amis, sans réaliser que nous le partagions aussi avec d'imposantes sociétés.

Puis, la récolte des données est devenue plus omniprésente, via les objets connectés, les assistants, etc.
Je ne parviens pas à retrouver la source, mais l'un des fondateurs de Google a déclaré il y a quelques années que les gens veulent être assistés.
Ils veulent un rappel qu'ils doivent acheter du lait quand ils passent à côté de l'épicerie.
Cela implique que ces personnes accepteraient de laisser Google savoir où ils sont et ce que contient leur réfrigérateur.
C'est ainsi que nous avons commencé à sciemment partager nos données avec de grandes compagnies.
Des choses parfois basiques, mais parfois bien plus intimes ou liées à notre santé : notre activité et notre forme physiques, notre pression sanguine, notre génome...

**Le jeu en vaut-il la chandelle ? Et devons-nous faire confiance à ces compagnies ?**



### Ce qu'il arrive à vos données

Avez-vous jamais pris le temps de réfléchir à ce qu'il advient de vos données ?
Pensez-vous que ces sociétés ont mérité votre confiance ?
Juste en 2019, Facebook a redécouvert [des centaines de millions de mots de passe stockés en clair][krebs-fb-passwords-plain] et [des données utilisateurs accessibles sur un serveur oublié][forbes-fb-db-leaked].

Très bien, tout le monde fait des erreurs --- bien que je trouve celles-ci difficilement excusables au vu des moyens dont Facebook bénéficie ---, mais même le traitement normal de vos données pourrait vous surprendre.
Savez-vous que [des transcriptions de vos requêtes Alexa sont sauvegardées][amz-alexa-transcripts] ?
Ou que [des « experts » humains peuvent écouter vos demandes à Google Home][google-speech-data] ?

Êtes-vous toujours à l'aise ?
Les législateurs ne le sont pas.


## La loi à la rescousse des utilisateurs

En 2016, l'Union européenne a adopté le [Règlement général sur la protection des données (RGPD)][wiki-rgpd].
Il s'applique à toute entité traitant des informations personnelles de ressortissants européens.
Sans entrer dans les détails, l'esprit de la loi permet aux utilisateurs de savoir :
- quelles sont les données collectées à leur sujet ;
- dans quel but ;
- combien de temps elles seront conservées ;
- avec qui elles seront partagées.

Ce règlement impose également que les utilisateurs donnent _explicitement_ leur accord à l'exploitation de leurs données.
Les bannières déclarant qu'« en poursuivant votre navigation, vous acceptez... » ne sont pas en accord avec le RGPD.

Un dernier point intéressant est qu'un fournisseur de service ne peut pas vous refuser l'accès à son service car vous refusez de partager vos informations, à l'exception des données nécessaires au bon fonctionnement de ce service.

Bien entendu, le RGPD serait inutile sans mesure incitative.
Comme toujours, l'argent le plus incitatif que l'on ait trouvé pour les grandes sociétés, donc ceux qui l'enfreignent s'exposent au risque d'une amende allant jusqu'à 20 millions d'euros ou 4 % de leur chiffre d'affaires mondial, le montant le plus élevé étant retenu.

Vous pourriez penser qu'il ne s'agit que d'une tendance européenne.
Ce n'est pas le cas !
La Californie, le foyer de la Silicon Valley et des grandes sociétés numériques, a voté le [California Consumer Privacy Act][numerama-ccpa], dont les intentions sont fortement similaires à celles du RGPD.

Pour en revenir aux bases, il s'agit de redonner aux utilisateurs la connaissance et le contrôle de leurs données.


## Le cercle vicieux de la collection de données

[Le rapport 2019 « Déployer la sobriété numérique »][shift-rapport] de [The Shift Project](https://theshiftproject.org/) l'a exprimé succinctement et avec une éloquence supérieure à la mienne : le volume croissant des données force les infrastructures qui les transportent, les traitent et les stockent à se développer, permettant de nouveaux usages.
Loin de calmer notre soif d'informations, ces nouveautés vont créer une nouvelle gourmandise.

On tombe ainsi dans un cercle vicieux : avoir davantage de données signifie être capable de traiter plus de données, ce qui signifie vouloir plus de données.
Ceci va bien entendu renforcer [l'empreinte environnementale du numérique][footprint].
Cela veut également dire que nous ne prenons pas trop le [temps de réfléchir][creating-the-future] à la valeur ajoutée de ces traitements ou au respect de la vie privée.


## Le meilleur des mondes

Je voudrais rester court aujourd'hui, mais n'hésitez pas à suivre les liens quand ils sont disponibles.

Nous avons vu que Facebook et Google --- entre autres --- ont accès à d'énormes quantités de données.
[Amnesty International estime que leur business model menace les droits de la personne][amnesty-data-threat].

Vous n'avez probablement jamais entendu parlé de Clearview AI, mais si une photo de vous a un jour été publiée sur Internet, [ce logiciel est presque certainement capable de vous reconnaitre][nytimes-clearview], même si vous couvrez votre visage.
À titre indicatif, sa base de données contient 3 milliards de visages, contre « seulement » 411 millions pour celle du FBI.

Des généticiens s'inquiètent d'une autre forme de surveillance, encore plus intrusive : [il suffirait de relativement peu d'échantillons pour mettre en place une surveillance génétique d'un groupe ciblé][nature-genomic-surveillance].
Pour aggraver les choses, [l'ADN des bébés nés en Californie en 2018 pourrait avoir été stocké et vendu à des chercheurs privés][cbs-biobank].
Certains généticiens français ignorent sciemment les restrictions du RGPD et partagent des données génomiques de leurs patients avec d'autres équipes de recherche sans accord des intéressés.
Notre génome est pourtant l'information la plus intime que nous pouvons partager ; c'est une part importante de qui nous sommes.
Il ne semble pourtant pas traité avec plus de respect que n'importe quelle autre donnée.

Ce ne sont que quelques exemples et je n'aborde même pas les scandales qui fleurissent autour de la reconnaissance faciale, [la centralisation de toutes nos données santé chez Microsoft][frama-health-data-hub], [la généralisation du datamining pour détecter la fraude][nextinpact-bercy-datamining]...
Tout le monde est intéressé par vos données, qu'il s'agisse des sociétés, des forces de l'ordre ou maintenant de l'état.

Et si on étend la réflexion au futur, qu'arrivera-t-il quand Google vous proposera d'assurer votre crédit ?
Serez-vous à l'aise d'y souscrire en sachant qu'ils ont l'intégralité de vos données Google Fit ?



## Avant de se quitter

Je comprends que la plupart d'entre nous obéit aux instructions et pense que c'est tout ce qu'elles ou ils peuvent faire, mais se cacher derrière les ordres semble lâche.
Notre rôle, dans la création logicielle, ne se limite pas à taper sur un clavier.
Vous savez des choses que vos supérieurs ignorent --- pas parce que vous développez, juste parce que chaque être humain a une connaissance et une expérience qui lui sont propres --- et vous devriez toujours pouvoir partager votre opinion avec votre chef de projet ou client si vous le pensez nécessaire.

Et oui, là où il y a de la donnée, on voit souvent de l'IA de nos jours, mais avez-vous vraiment besoin d'IA pour ce que vous créez ?
Les effets de mode rendent rarement une solution meilleure et j'ai souvent l'impression que l'IA est utilisée comme argument de vente uniquement.
Mais j'y reviendrai dans [le dernier billet de cette série][creating-the-future].


{{% references %}}

[footprint]: {{< relref path="blog/2020/01/intro-sustainable-digital/01-footprint-of-digital" >}}
[creating-the-future]: {{< relref path="blog/2020/01/intro-sustainable-digital/04-creating-the-future" >}}
