---
date: 2020-02-26T21:49:19+02:00
title: What's up? (February 2020)
#subtitle: No funny subtitle found
slug: whats-up
description: |-
  A dark theme and clearer taxonomies, and ideas in the pipe for the following...
author: chop
categories: [ kp.org ]
tags: [ news ]
keywords: [ blog, dark theme, taxonomies, download proxy ]
---

It's been a while since I last posted an update about the website.
Not my favorite kind of post, but you'll see I've not been entirely idle.


<!--more-->

## What's Changed?

### A Dark Theme

If you browse the website, this will be the more obvious: I turned off the lights.
This has been a preoccupation since [day 1]({{< relref path="blog/2019/10/21-hello-world" >}}), now come true.

I also dropped the blue for some green, as it should match the [sustainable theme]({{< relref path="/tags/sustainable-it" >}}) I hope to write more about in the future.

Now, maybe you like light themes better.
In that case, just let me know.
I've prepared the SCSS so that it'll be easy to make a light variant.
How to make it efficiently and in a sustainable way may not be that easy, but who doesn't love a challenge?


### Clearer Taxonomies

I felt that categories and tags were becoming a mess, so I decided to make more general categories and turn the categories that were to specific into tags.
This may not look like much, but I hope it'll help making this easier to navigate the website for occasional readers.


## What's Next?

New posts, of course.
I've got many ideas from explanations I've shared at work---and if it was useful to my colleagues, there must be other people it will be useful to.

Now, my main task should be finishing the work I started about the [download proxy](https://gitlab.com/keyboardplaying/download-proxy).
This was an appreciated feature of the previous version of KeyboardPlaying.org and a great project to rewrite from the dirty PHP it was to the great, light Quarkus Java project it will soon be---actually, it already is; I just need to deploy it and give it a nice interface.
