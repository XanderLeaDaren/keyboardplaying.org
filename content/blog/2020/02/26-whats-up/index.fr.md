---
date: 2020-02-26T21:49:19+02:00
title: Quoi d'neuf ? (février 2020)
#subtitle: No funny subtitle found
slug: quoi-d-neuf
description: |-
  Un thème sombre, des taxonomies plus claires et des idées pour la suite...
author: chop
categories: [ kp.org ]
tags: [ news ]
keywords: [ blogue, thème sombre, taxonomies, download proxy ]
---

Cela fait un moment que je n'ai pas partagé un billet qui parle de ce site.
Ce ne sont pas mes favoris, mais vous verrez que je n'ai pas été tout à fait inactif.


<!--more-->

## Qu'est-ce qui a changé ?

### Un thème sombre

Si vous naviguez sur le site, c'est le plus évident : j'ai éteint les lumières.
C'était une préoccupation depuis [le début]({{< relref path="blog/2019/10/21-hello-world" >}}), enfin réglée.

J'ai également remplacé le bleu par du vert, qui devrait s'accorder à la thématique du [numérique responsable]({{< relref path="/tags/sustainable-it" >}}) que j'espère développer à l'avenir.

Mais peut-être préfériez-vous le thème clair ?
Si c'est le cas, dites-le moi.
J'ai préparé mon SCSS de sorte à pouvoir générer facilement une variante claire.
La seule question sera de savoir comment le faire de façon efficace et responsable, mais qui n'aime pas un défi ?


### Des taxonomies plus claires

Je trouvais que les catégories et mots-clés devenaient un peu confus.
Les catégories sont maintenant plus générales et celles qui étaient trop précises sont devenues des mots-clés.
Ça n'a l'air de rien, mais j'espère que cela facilitera la navigation pour des lecteurs occasionnels.


## Et ensuite ?

De nouveaux billets, bien entendu !
J'ai encore plein d'idées et de nouvelles explications que j'ai données au travail ---- et si des collègues l'ont trouvé utile, j'ose espéré que ce sera le cas d'autres développeurs.

Mais la principale tâche à terminer portera sur le [download proxy](https://gitlab.com/keyboardplaying/download-proxy).
C'était un outil apprécié sur la précédente version de KeyboardPlaying.org et sa réécriture depuis un PHP tout sale vers un super Java Quarkus était un super projet --- cette partie est pratiquement terminée dans les faits ; il ne me reste qu'à la déployer et lui construire une jolie interface.
