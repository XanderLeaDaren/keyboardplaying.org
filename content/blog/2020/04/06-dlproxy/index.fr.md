---
date: 2020-04-06T07:21:32+02:00
title: Le Download Proxy est de retour
subtitle: Contournez un pare-feu contrariant et concentrez-vous sur votre travail
slug: download-proxy-retour
description: |-
  Le Download Proxy est un outil que j'ai conçu pour contourner les pare-feu qui ralentissaient mon travail.
  Voici la v2.
cover:
  banner: false
  src: peter-holmes-ladder-stile-over-stone-wall.jpg
  alt: Échalier surplombant un mur de pierres
  by: Peter Holmes
  link: https://www.geograph.org.uk/photo/5150108
  authorLink: https://www.geograph.org.uk/profile/17959
  license: CC BY 2.0
author: chop
categories: [ projects, kp.org ]
tags: [ java, quarkus ]
keyword: [ download proxy, pare-feu, php, java, quarkus, base64 ]

preface: |-
  Une nouvelle version du Download Proxy a été publiée, avec de nouvelles fonctionnalités.
  Lisez le billet associé [ici](/blogue/2021/01/nouvelles-transformations-download-proxy/).
---

Il y a quelques années, j'ai eu des difficultés à télécharger des fichiers dont j'avais besoin pour travailler.
Il n'est pas inhabituel de donner aux développeurs des droits plus élevés sur leur machine, mais cela ne s'étend pratiquement jamais au pare-feu.
En conséquence, ils ont le droit d'installer tous les logiciels utiles, mais ils sont pourtant incapables de les télécharger sans des échanges pénibles avec un service de sécurité parfois situés dans une autre ville.
C'est pénible.
Et c'est pourquoi j'ai créé le Download Proxy.


<!--more-->

## La petite histoire

Le Download Proxy est la première chose qui a existé sur keyboardplaying.org.
Il était basé sur un besoin que je partage avec plusieurs développeurs que je connais.
Certains d'entre eux ont utilisé la v1 du proxy jusqu'à ce que je le décommissionne il y a quelques mois.

Quand j'ai commencé, je travaillais pour une compagnie d'assurance.
J'ai découvert que leur pare-feu bloquait les téléchargements principalement sur base du type de fichier.
Quelques tests rapides ont prouvé que changer l'extension du fichier téléchargé suffisait à le contourner.

J'ai bricolé un outil PHP pour faire précisément ça : rediriger le flux d'un téléchargement en modifiant quelques détails.
Toutefois, PHP n'est pas (ou plus ?) ma spécialité et le code est resté sale.
Je n'ai pas osé le publier par peur de failles de sécurité.

Les pare-feu ont évolué, rendant ces transformations obsolètes.
J'ai ajouté la possibilité de compresser les fichiers à la volée, mais les pare-feu ont ensuite commencé à analyser le contenu des zips et les bloquer lorsqu'ils en étaient incapables --- à cause d'un mot de passe, par exemple...

J'ai laissé l'outil à l'abandon pendant un temps, utilisant d'autres moyens de contournement dans l'intervalle.



## L'arrivée de la version 2 (beta)

Je viens de la publier, en même temps que ce billet.
Elle est disponible [ici]({{< relref path="/tools/dl-proxy" >}}).

Son utilisation est simple : saisissez l'adresse, choisissez une transformation à appliquer, validez et patientez.

Les transformations disponibles sont les suivantes :
* **Pas de transformation** : le fichier est téléchargé depuis le domaine du proxy au lieu de l'adresse originale.
Simple et efficace pour contrer des règles basées sur le domaine.
* **Encodage en base64** : télécharge un fichier texte contenant la représentation du fichier original.
Le fichier sera plus volumineux que l'original, et certains pare-feu prendront du temps à vouloir l'analyser, mais cela a été mon approche par défaut depuis quelque temps et je n'ai pas encore été bloqué en l'utilisant.

J'ai fait quelques tests rapides, mais il se peut qu'il rencontre des difficultés sur des fichiers plus volumineux.
N'hésitez pas à [signaler un problème](https://gitlab.com/keyboardplaying/download-proxy/issues) pour que je puisse améliorer les choses.



## Le changement technologique

Comme beaucoup de développeurs, j'adore jouer avec de nouvelles piles et technologies.
Je suis plus à l'aise avec Java et j'ai décidé d'y revenir afin de mieux sécuriser le code.
Je meurs d'envie de jouer avec [Quarkus] depuis un moment et cela a été l'opportunité.
Les performances et optimisations qu'il apporte sont impressionnantes, mais c'est certainement un sujet à aborder dans un autre billet.

J'ai tenté de concevoir une solution plus générique, où les transformations s'approchent davantage de plug-ins, afin de pouvoir les ajouter aisément.
J'en ai déjà [quelques-unes](https://gitlab.com/keyboardplaying/download-proxy/-/issues?label_name%5B%5D=Transformations) en tête, que j'espère pouvoir concrétiser dans les mois à venir.

[Quarkus]: https://quarkus.io
