---
date: 2020-04-06T07:21:32+02:00
title: The Download Proxy's Back
subtitle: Bypass a bothersome firewall and get back to work
slug: download-proxy-back
description: |-
  The Download Proxy is a tool I created a few years back to circumvent firewall rules that prevented me from working.
  Now, the v2 is here.
cover:
  banner: false
  src: peter-holmes-ladder-stile-over-stone-wall.jpg
  alt: Ladder stile over stone wall
  by: Peter Holmes
  link: https://www.geograph.org.uk/photo/5150108
  authorLink: https://www.geograph.org.uk/profile/17959
  license: CC BY 2.0
author: chop
categories: [ projects, kp.org ]
tags: [ java, quarkus ]
keyword: [ download proxy, firewall, php, java, quarkus, base64 ]

preface: |-
  A new version of the Download Proxy was release, with new features.
  See the related post [here](/blog/2021/01/new-transformations-download-proxy/).
---

A few years back, I had problems downloading files I needed in order to work.
It's not uncommon for developers to have higher access rights on their machine, but this almost never applies to the firewall.
So, you may have the right to install all software you need and yet still be unable to download it without a two-day-long exchange with a security service located in another city.
That's annoying.
And that's when I came up with the Download Proxy.


<!--more-->

## Download Proxy, v1

The Download Proxy was actually the project I built keyboardplaying.org around.
It was based on a need I and other developers I know often have, and I know some of them used it still when I decommissioned v1.

When I started it, I was working for an insurance company and discovered that its firewall was mainly blocking downloads based on the file's mimetype.
Some quick tests proved that simply changing the downloaded file's extension was enough to bypass it.

I came up with a PHP tool to do just that: redirect a download feed while changing some information.
However, PHP's not my strong suit (anymore?), and it remained dirty.
I didn't dare release the source code for fear of security flaws.

Firewalls evolved and those transformations were not enough anymore.
I added the possibility to zip files on the fly, but firewall later began analyzing zips' content or block them if they couldn't---say, because of a password...

And I let that tool be for a while, using other ways to bypass firewalls.



## Version 2 (beta) Is Here

I just published it, along with this post.
It's available [here]({{< relref path="/tools/dl-proxy" >}}).

Using it is quite straightforward: provide the URL of the file you want to download, select the transformation to apply, submit and wait for it.

The transformations available for now are the following:
* **No transformation**: the file is downloaded from the domain of the proxy instead of the original domain.
Great and efficient to counter domain-based rules.
* **Base64 encoding**: download a text file which is the base64 representation of your download.
It will be larger than the original, and some firewall will take some time trying to analyze it, but it's been my go-to approach for a while and it's never been blocked.

Please keep in mind I just ran some quick tests.
It still remains to be tested on larger files.
Please don't hesitate to [signal any issue](https://gitlab.com/keyboardplaying/download-proxy/issues) to make it better.



## The Technological Shift

Like many developers, I like to toy with new stacks and technologies.
I'm much more at ease with Java and I went back to that in order to better control the code security.
I've been wanting to play with [Quarkus] for a while and this was the opportunity.
The performances and optimizations it allows are impressive, but this may be a topic for another post.

I tried to build something more generic, where I can design transformations as plugins, so that adding new ones will be easy.
I have [some](https://gitlab.com/keyboardplaying/download-proxy/-/issues?label_name%5B%5D=Transformations) in mind, already, that will hopefully come in the coming months.

[Quarkus]: https://quarkus.io
