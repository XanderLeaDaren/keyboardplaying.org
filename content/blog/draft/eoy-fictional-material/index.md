---
date: 2020-12-31T14:00:00+01:00
title: A Review of My 2020+2021 Inspiration Sources
slug: review-fictional-readings-2020-2021
description: |-
  Yearly roundup: What did I read that made me a better writer, this year?

author: chop
categories:
  - writing
tags:
draft: true
---

## Novels

- [ ] _Seveneves_


## Podcasts

- [ ] _Aliens et les garçons_
