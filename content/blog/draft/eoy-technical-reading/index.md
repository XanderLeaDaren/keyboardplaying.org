---
date: 2020-12-30T14:00:00+01:00
title: A Review of My 2020+2021 Technical Readings
slug: review-technical-readings-2020-2021
description: |-
  Yearly roundup: What did I read that made me a better software creator, this year?

author: chop
categories: [ software-creation  ]
tags: [ management, sustainable-it, programming ]
draft: true
---


## Whitepapers & reports

- [ ] _Culture Code_ by Octo
- [ ] _Déployer la sobriété numérique_ by the Shift Project
