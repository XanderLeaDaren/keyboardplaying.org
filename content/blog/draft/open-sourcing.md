---
draft: true
---

- [ ] Willingness to connect human brain, not thinking of social impacts
- [ ] Open sourcing as a part of a solution
  - [ ] Longer life: (cf. smartphones)
  - [ ] FSF + Windows 7 : https://www.nextinpact.com/brief/windows-7---la-free-software-foundation-demande-a-microsoft-de-liberer-le-code-source-11017.htm
  - [ ] Fight against captivity by providing alternatives (quantified self services; problems of standards + XKCD)
  - [ ] Applicable to hardware (lincrevable)
