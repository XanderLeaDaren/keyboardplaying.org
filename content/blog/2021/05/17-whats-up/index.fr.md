---
date: 2021-05-17T07:00:00+02:00
title: Quoi d'neuf ? (mai 2021)
#subtitle: A witty line as a subtitle
slug: quoi-d-neuf
description: |-
  Des SVG, des bibliographies, des séries et un message à Google en faveur de votre vie privée ; voici les derniers changements sur le site.
author: chop
categories: [ kp.org ]
tags: [ news ]
keywords: [ bibliographie, références, taxonomie, svg, graphiques, séries, tracking, google, chrome, FLoC ]

references:
- id: cleanuptheweb
  name: Nettoyons le web !
  url: https://cleanuptheweb.org/fr/
  lang: fr
---

Deux mois depuis le dernier bilan.
Qu'est-ce qui a changé depuis ?

<!--more-->

## Qu'est-ce qui a changé ?

### Bibliographie

Dans la recherche scientifique, il est essentiel de citer ses sources lors de l'écriture d'un article.
Vos lecteurs doivent pouvoir reproduire tout ce que vous avez fait et confronter votre compréhension du matériel d'origine avec la leur.

Jusqu'à présent, lorsque mes billets s'appuyaient sur des recherches, je faisais attention à inclure des liens dans le corps de mon texte, mais il n'y avait pas de façon uniforme de lister mes sources.
J'ai réglé ce souci en mettant en place un système (assez basique) de gestion de références.
À la fin de tous les billets concernés, il y aura une section « Sources », avec les liens vers les articles les plus pertinents ou les références que j'ai trouvées et utilisées.


### Graphiques SVG

Certains graphiques ont été remplacés par des SVG, pour une empreinte plus légère et une meilleure accessibilité.
Au début, j'ai fait ces remplacements à contrecœur, avec le sentiment de perdre un peu de l'identité du site.
Après coup, je trouve cependant que le rendu précis des nouveaux graphiques est appréciable et bienvenu, et s'intègre mieux à la charte graphique.

J'ai déjà écrit un billet dédié au sujet, plus de détails à propos de ces changements sont disponibles [ici]({{< relref path="/blog/2021/03/29-svg-instead-of-images" >}}).



### Une façon d'identifier les billets faisant partie d'une série

Jusqu'à présent, si je voulais faire une série de billets, il fallait que je le fasse à la main.
C'était assez pénible et peu flexible, sans parler des soucis de maintenance.

J'ai donc finalement passé un peu de temps à trouver une solution, et vous pouvez trouver les séries déjà publiées [ici][kp-series].



### Pas de FLoC sur ce site

Google a promis de faire disparaître le tracking par les tiers dans Chrome, en le remplaçant par leur solution maison, FLoC.
Je n'entrerai pas dans les détails ici, je me limiterai à dire que vous l'acceptez si vous ne le refusez pas (opt-out par défaut).
Pour vous, la solution pour ne pas être tracé(e) via FLoC est d'arrêtr d'utiliser Chrome ou un navigateur basé sur Chrome.

Les administrateurs de site web peuvent dire à Google qu'ils ne souhaitent pas que leur site soit utilisé par ce système.
C'est ce que j'ai fait.
Peut-être, si assez d'idéalistes et grands sites rejoignent le mouvement, que Google comprendra que nous ne voulons pas être traqués. Du tout.
Ni par des cookies, ni dans des cohortes.

Si vous voulez en savoir davantage, la page [Nettoyons le web !][cleanuptheweb] donne quelques sources intéressantes.



## Et ensuite ?

Je n'ai pas beaucoup de changements techniques prévus dans l'immédiat, mais j'ai plusieurs sujets sur lesquels j'aimerais écrire.
I don't have much technical changes in store for the near future, but I have several topics I'd like to write about.
À propos des logiciels libres, open source, des licences, de l'Espéranto…
J'ai commencé à en préparer certains, je réfléchis encore aux autres, mais j'ai de la matière pour le futur proche.

Et le livre sur lequel nous travaillons depuis maintenant un an sera bientôt disponible sur les grandes plateformes de distribution françaises.
Plus à ce sujet prochainement.


{{% references %}}
[kp-series]: {{< relref path="/series/" >}}
