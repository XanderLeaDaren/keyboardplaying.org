---
date: 2021-05-17T07:00:00+02:00
title: What's Up? (May 2021)
#subtitle: A witty line as a subtitle
slug: whats-up
description: |-
  SVG, references, series and telling Google to respect your privacy; those are the latest changes on the website.
author: chop
categories: [ kp.org ]
tags: [ news ]
keywords: [ bibliography, references, taxonomy, investigation, svg, charts, series, tracking, google, chrome, FLoC ]

references:
- id: cleanuptheweb
  name: Clean up the web!
  url: https://cleanuptheweb.org/
  lang: en
---

Two months since the last update.
What's changed since then?

<!--more-->

## What's Changed?

### Bibliography

In scientific research, quoting your sources is an essential part of writing an article.
Your readers must be able to reproduce all you did and to confront you with their own understanding of your sources.

Until now, when I did research to write a post, I took care of adding numerous links in the body of the post, but there was no uniform way to list my sources.
I fixed that by implementing a (somewhat crude) reference system.
At the end of all posts that required some research, there will be a "sources" section, with links to the most relevant articles or references I found and used.


### SVG Charts

Some charts have been replaced with inline SVGs, for a lighter footprint and a better accessibility.
At first, I did so with a heavy heart, fearing I was losing a bit of the website's identity.
Looking back, though, I feel the crispy look of the new SVG charts is nice and welcome, and fits the general look of the website better.

I already blogged about this, more details about what changed and measurements about what it meant are available in [this post]({{< relref path="/blog/2021/03/29-svg-instead-of-images" >}}).



### A Way to Follow Posts That Are Part of a Series

Until now, when I wanted to make a series of posts, I needed to include the links and titles of the other pages in the body of my article.
This was quite cumbersome and would become a pain during maintenance as I intend to write more of those in the future if I can.

So I finally did something about it, and you can find the series I already published [here][kp-series].



### Telling Google to FLoC off

Google promised to make third-party tracking disappear in Chrome and is replacing it with their new solution, FLoC.
I won't get into the details here, just tell that it's opt-out by default.
For you, the way to fully opt-out of FLoC is to stop using Chrome or a Chrome-based browser.

Website administrators can tell Google that they don't want their site to be used for this system.
That's just what I did.
Maybe, if enough idealists and important websites do this, Google will understand we don't want to be tracked. At all.
Neither with cookies, nor in cohorts.

If you want to know more, the [Clean up the web!][cleanuptheweb] page has some interesting sources.



## What's Next?

I don't have much technical changes in store for the near future, but I have several topics I'd like to write about.
About free software, open-source software, licensing, Esperanto…
I've begun preparing some, I'm still thinking about the others, but I have some material for the near future.

And the book we've been working on for a whole year will hit the shelves on most big French platforms, so that's a big piece that's done.
More about that soon.


{{% references %}}
[kp-series]: {{< relref path="/series/" >}}
