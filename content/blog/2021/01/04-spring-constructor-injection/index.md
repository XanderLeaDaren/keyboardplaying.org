---
date: 2021-01-04T06:31:35+01:00
title: "[Best practice] Use Constructor-Based Dependency Injection with Spring"
subtitle: Old habits die hard, but die they must
slug: spring-constructor-dependency-injection
description: |-
    For those who've been using Spring for a while, it's common to apply on `@Autowired` on fields.
    It's not the Spring Team's recommendation, though.
author: chop
categories: [ software-creation ]
tags: [ best-practices, spring-boot, quarkus, java, programming ]
keywords: [ best practice, dependency injection, java, spring, spring boot, quarkus, autowired, test, oop, immutable, lombok ]

references:
- id: baeldung-constructor-injection
  name: Constructor Dependency Injection in Spring
  title: true
  url: https://www.baeldung.com/constructor-injection-in-spring
  lang: en
  author:
    name: Baeldung
- id: quarkus-arc
  name: Quarkus Dependency Injection
  title: true
  url: https://quarkus.io/blog/quarkus-dependency-injection/
  date: 07/2019
  lang: en
  author:
    name: Martin Kouba, Quarkus
---


If you've been using Spring for a while, or copy-pasting some web tutorials or examples, you've probably put some `@Autowired` annotations on private fields.
This does work, but it's not the best way to do it.

<!--more-->


## [TL;WR](https://www.urbandictionary.com/define.php?term=tl%3Bwr)

Don't use `@Autowired`, create a constructor that takes all required fields as parameters.

If your class needs several constructors, annotate the one Spring should use with `@Autowired`.


## Spring Team's Recommendation

If you have an Ultimate license of [IntelliJ IDEA][idea], if you annotate a private field with `@Autowired`, a warning should appear.
Expanding it, you should see the following:

> Field injection is not recommended\
> Inspection info: Spring Team recommends: "_Always use constructor based dependency injection in your beans. Always use assertions for mandatory dependencies_".



## Using Constructor-Based Dependency Injection

### Annotation-Based Configuration

<aside><p>Everything that Spring must inject should be a parameter of the constructor.</p></aside>

Imagine a `HelloService` that needs Spring to inject a `HelloRepository` and a property from the `application.properties` file.
The way to do so is to add a constructor that will take these parameters instead of annotating the private fields.

```java
@Service
public class HelloService {

    private HelloRepository repository;
    private String fromName;

    public HelloService(
        HelloRepository repository,
        @Value("${kp.hello.from.name}") String fromName
    ) {
        this.repository = repository;
        this.fromName = fromName;
    }

    // Logic here
}
```

From Spring 4.3, the `@Autowired` can be omitted for classes with only one constructor.
With previous versions or classes that have more than one constructor, the annotation will indicate Spring which constructor to use.

Behind the curtains, when Spring runs, it discovers the `HelloService` via the `@Service` annotation and know it must be loaded.
Then it will discover that there is a constructor whose parameters it will try to supply.
It will search in the context for those and will try to pass those.
Expect an exception if one of those is not available.


### XML-Based Configuration

XML remains a valid way to configure Spring (I had a hard time quitting it, actually), and it still works for constructor-based injection.
For instance:

```xml
<bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
    <property name="location" value="application.properties"/>
</bean>

<!-- ... -->

<bean id="helloService" class="org.keyboardplaying.demo.HelloService">
    <constructor-arg index="0" ref="helloRepository"/>
    <constructor-arg index="1" value="${kp.hello.from.name}"/>
</bean>
```

There, no discovery is left to Spring, as everything is explicitly configured.



## Pros & Cons

### Pro: Testability

What if we wrote our `HelloService` the "traditional" way?

```java
@Service
public class HelloService {

    @Autowired
    private HelloRepository repository;

    @Value("${kp.hello.from.name}")
    private String fromName;

    // Logic here
}
```

<aside><p>Constructor-based injection makes beans more testable.</p></aside>

Now imagine you want to unit test this.
First, you'd have **no solution to set those fields** except using the reflection API, which completely breaks encapsulation.

Another con of field injection would be that **you can't ensure the bean has been fully initialized**, which may result in random `NullPointerException` as your code would try to call a dependency that would not have been set.


### Pro: OOP Paradigm

From the Object-Oriented Programming point of view, **using constructor is more natural to initialize objects**.


### Pro/con: Verbosity

Complex services may require many dependencies.
Associated constructors can become quite cumbersome.

But, as [Baeldung][baeldung-constructor-injection] highlighted, this may entice you to be more cautious about the number of dependencies of your service.
Maybe write more specialized components and benefit from intelligent composition.



## My Going Further

### Making Beans Immutable

<aside><p>All dependencies can be made <code>final</code>.</p></aside>

Since all dependencies of a service are set at the time of instantiation, why not make them `final`?
This prevents any other component to change it at runtime, due to a security intrusion or a programming mistake for instance.


### Not Writing the Constructor

<aside><p>Project Lombok can do the heavy lifting of writing constructors in your stead.</p></aside>

I've [not yet had time to write][lombok-kp-post] about [Project Lombok][lombok], but it shares one great purpose with Spring Boot: avoid boilerplate code.

Why spend time writing constructors, or updating them when the dependencies of a component change?
Lombok uses reflection to generate these at compilation time.

For instance, you could do the following:

```java
@Service
@AllArgsConstructor
public class HelloService {

    private HelloRepository repository;

    // Logic here
}
```

Or, making the service immutable, we could obtain:

```java
@Service
@RequiredArgsConstructor
public class HelloService {

    private final HelloRepository repository;

    // Logic here
}
```

You can have a look at [Lombok's documentation][lombok-constructor] to see the difference between the `@AllArgsConstructor` and `@RequiredArgsConstructor`.

There is, nonetheless, one big restriction to this use of Lombok: this cannot be used for constructors that require annotated parameters (such as parameters annotated with `@Value`).
In my experience, however, most Spring beans can be initialized this way.



## Works with Quarkus, Too

Quarkus's simplified dependency injection, [ArC][quarkus-arc], is not a full CDI implementation.
However, it brings the possibility of using [constructor injection][quarkus-arc-constructor].

[I recently took advantage of it][dlproxy-arc], instead of using `@Inject`, which is actually quite similar to `@Autowired`.



## Conclusion

Having other thoughts worthy of note about the use of constructor-based dependency injection?
Please, share with us.



{{% references %}}

[idea]: https://www.jetbrains.com/idea/

[lombok-kp-post]: https://gitlab.com/keyboardplaying/keyboardplaying.org/-/issues/31
[lombok]: https://www.projectlombok.org/
[lombok-constructor]: https://www.projectlombok.org/features/constructor

[quarkus-arc-constructor]: https://quarkus.io/blog/quarkus-dependency-injection/#simplified-constructor-injection
[dlproxy-arc]: https://gitlab.com/keyboardplaying/download-proxy/-/commit/eba6541b8e8d99a2034d079f669d3a0f3787c264
