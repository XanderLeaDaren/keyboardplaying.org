---
date: 2021-01-04T06:31:35+01:00
title: "[Bonne pratique] Utilisez l'injection par constructeur avec Spring"
subtitle: Les vieilles habitudes ont la vie dure, mais il faut leur faire la peau
slug: spring-injection-constructeur
description: |-
    Pour ceux qui utilisent Spring depuis longtemps, il est courant d'annoter les champs avec `@Autowired`.
    Ce n'est pourtant pas ce que recommande l'équipe Spring.
author: chop
categories: [ software-creation ]
tags: [ best-practices, spring-boot, quarkus, java, programming ]
keywords: [ bonne pratique, injection de dépendance, java, spring, spring boot, quarkus, autowired, test, poo, immuable, lombok ]

references:
- id: baeldung-constructor-injection
  name: Constructor Dependency Injection in Spring
  title: true
  url: https://www.baeldung.com/constructor-injection-in-spring
  lang: en
  author:
    name: Baeldung
- id: quarkus-arc
  name: Quarkus Dependency Injection
  title: true
  url: https://quarkus.io/blog/quarkus-dependency-injection/
  date: 07/2019
  lang: en
  author:
    name: Martin Kouba, Quarkus
---


Si vous utilisez Spring depuis longtemps, ou si vous copiez-collez quelques tutos ou solutions trouvés sur la toile, vous avez certainement quelques champs privés annotés avec `@Autowired`.
Ça fonctionne, mais il y a une meilleure façon de faire.

<!--more-->


## La version courte

N'utilisez pas `@Autowired`, créez un constructeur qui prend en paramètres toutes les dépendances requises.

Si votre classe a besoin de plusieurs constructeurs, annotez celui que Spring doit utiliser avec `@Autowired`.


## La recommandation de l'équipe Spring

Si vous avez une licence Ultimate d’[IntelliJ IDEA][idea], lorsque vous annotez un champ privé avec `@Autowired`, un avertissement devrait apparaître.
En affichant toutes ses infos, voici ce que vous devriez pouvoir lire :

> Field injection is not recommended\
> Inspection info: Spring Team recommends: "_Always use constructor based dependency injection in your beans. Always use assertions for mandatory dependencies_".

Soit, en français :

> L'injection par champ n'est pas recommandée\
> Infos sur l'inspection : Recommandation de l'équipe Spring : « _Utilisez toujours l'injection de dépendance par constructeur dans vos _beans_. Utilisez toujours les assertions pour les dépendances obligatoires_ ».


## Faire de l'injection de dépendance par constructeur

### Avec une configuration par annotation

<aside><p>Tout ce que Spring doit injecter est un paramètre du constructeur.</p></aside>

Imaginons un `HelloService` qui a besoin que Spring lui injecte un `HelloRepository` et une propriété de l'`application.properties`.
Pour y parvenir, ces deux dépendances doivent être des paramètres du constructeur plutôt que des champs privés annotés.

```java
@Service
public class HelloService {

    private HelloRepository repository;
    private String fromName;

    public HelloService(
        HelloRepository repository,
        @Value("${kp.hello.from.name}") String fromName
    ) {
        this.repository = repository;
        this.fromName = fromName;
    }

    // Logic here
}
```

À partir de la version 4.3 de Spring, l'annotation `@Autowired` peut être omise pour les classes avec un seul constructeur.
Pour les versions précédentes ou les classes avec plusieurs constructeurs, elle doit être utilisée pour indiquer à Spring quel constructeur utiliser.

En arrière-boutique, quand Spring démarrera, il découvrira le `HelloService` grâce à l'annotation `@Service` et saura qu'il doit être initialisé.
Il découvrira ensuite le constructeur dont il devra fournir les paramètres demandés.
Il les cherchera dans son contexte.
Attendez-vous à une exception s'il ne peut pas tous les y trouver.


### Avec une configuration par XML

XML reste une solution valide pour configurer Spring (j'ai eu du mal à passer aux annotations), et il peut toujours être utilisé pour configurer une injection par constructeur.
Par exemple :

```xml
<bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
    <property name="location" value="application.properties"/>
</bean>

<!-- ... -->

<bean id="helloService" class="org.keyboardplaying.demo.HelloService">
    <constructor-arg index="0" ref="helloRepository"/>
    <constructor-arg index="1" value="${kp.hello.from.name}"/>
</bean>
```

Dans ce cas, Spring ne fera pas d'introspection puisqu'on lui a déjà tout spécifié explicitement.



## Avantages & inconvénients

### Avantage : testabilité

Examinons un `HelloService` écrit de façon « traditionnelle ».

```java
@Service
public class HelloService {

    @Autowired
    private HelloRepository repository;

    @Value("${kp.hello.from.name}")
    private String fromName;

    // Logic here
}
```

<aside><p>L’injection par constructeur facilite le test des _beans_.</p></aside>

À présent, imaginez que vous voulez faire des tests unitaires.
En premier lieu, **vous ne pourriez pas injecter ces champs** sans passer par la réflexion, qui est en totale opposition avec l'encapsulation.

Un autre inconvénient de l'injection par champ est que **vous ne pouvez pas garantir que le _bean_ a été complètement initialisé**.
Vous pourriez tomber sur des `NullPointerException` si votre code fait appel à une dépendance qui n'a pas été prise en compte à la mise en place de vos tests.


### Avantage : paradigme POO

Du point de vue de la programmation orientée objet, **il est plus intuitif d'utiliser un constructeur pour créer un objet**.


### Avantage/inconvénient : verbosité

Les services complexes peuvent avoir de nombreuses dépendances.
Les constructeurs associés peuvent devenir assez encombrants.

Cependant, comme [Baeldung][baeldung-constructor-injection] le souligne, cela peut nous inciter à faire plus attention au nombre de dépendances de nos services.
Peut-être pouvons-nous écrire des composants plus spécialisés et tirer parti de composition intelligente.
Maybe write more specialized components and benefit from intelligent composition.



## Là où je vais plus loin

### Rendre les _beans_ immuables

<aside><p>Toutes les dépendances peuvent être <code>final</code>.</p></aside>

Puisque toutes les dépendances d'un service sont initialisées à l'instanciation, pourquoi ne pas les rendre `final` ?
Cela évite qu'un autre composant puisse le modifier à l'exécution, que ce soit lié à une erreur de programmation ou à une intrusion.


### Ne pas écrire les constructeurs

<aside><p>Le Projet Lombok peut s’occuper des tâches pénibles d’écriture des constructeurs à votre place.</p></aside>

Je [n'ai (toujours) pas écrit][lombok-kp-post] au sujet du [Projet Lombok][lombok], mais il a un grand but commun avec Spring Boot : éviter le code _boilerplate_.

Pourquoi passer du temps à écrire des constructeurs et les maintenir à jour lorsque les dépendances évoluent ?
Lombok utilise la réflexion pour les générer au moment de la compilation.

Par exemple, nous pourrions modifier notre service comme suit :

```java
@Service
@AllArgsConstructor
public class HelloService {

    private HelloRepository repository;

    // Logic here
}
```

Ou, si nous rendons le service immuable, nous obtiendrions :

```java
@Service
@RequiredArgsConstructor
public class HelloService {

    private final HelloRepository repository;

    // Logic here
}
```

Je vous laisse consulter la [documentation de Lombok][lombok-constructor] pour voir les différences entre `@AllArgsConstructor` et `@RequiredArgsConstructor`.

Il y a toutefois une grande restriction à cette utilisation de Lombok : on ne peut pas passer un paramètre annoté (comme les paramètres `@Value`).
Par expérience, la plupart des _beans_ Spring peuvent cependant être instanciés de cette façon.



## Et avec Quarkus ?

L'injection de dépendances simplifiée de Quarkus, [ArC][quarkus-arc], n'est pas une implémentation complète de CDI.
Toutefois, elle apporte la possibilité d'utiliser [l'injection par constructeur][quarkus-arc-constructor].

[J'en ai récemment profité][dlproxy-arc], au lieu de m'appuyer sur `@Inject`, qui a les mêmes défauts qu’`@Autowired`..



## Conclusion

Si vous avez d'autres remarques intéressantes sur l'utilisation de l'injection de dépendance par constructeur, partagez avec nous.



{{% references %}}

[idea]: https://www.jetbrains.com/idea/

[lombok-kp-post]: https://gitlab.com/keyboardplaying/keyboardplaying.org/-/issues/31
[lombok]: https://www.projectlombok.org/
[lombok-constructor]: https://www.projectlombok.org/features/constructor

[quarkus-arc-constructor]: https://quarkus.io/blog/quarkus-dependency-injection/#simplified-constructor-injection
[dlproxy-arc]: https://gitlab.com/keyboardplaying/download-proxy/-/commit/eba6541b8e8d99a2034d079f669d3a0f3787c264
