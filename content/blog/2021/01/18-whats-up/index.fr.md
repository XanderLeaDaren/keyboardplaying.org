---
date: 2021-01-18T07:00:00+01:00
title: Quoi d'neuf ? (janvier 2021)
#subtitle: No funny subtitle found
slug: quoi-d-neuf
description: |-
  La version française a son propre domaine, l'amélioration progressive permet un meilleur support et un projet perso explique pourquoi KP est resté silencieux.
author: chop
categories: [ kp.org ]
tags: [ news, sustainable-it, web-dev ]
keywords: [ carrefour des imaginaires, dégradation élégante, support, internet explorer, IE, esbuild, rollup, download proxy ]
---


Keyboard Playing a été calme pendant plusieurs mois en 2020.
Le site n'a pas été oublié et reprend des couleurs avec quelques changements au cours des semaines passées.


<!--more-->

## Ce qui a ralenti l'activité

### Un livre est sorti, avec des nouvelles par votre serviteur

Pourquoi le site a-t-il été « mis en pause » pendant si longtemps ?
Eh bien, principalement parce que j'ai travaillé à l'auto-édition d'un livre.

{{< figure src="/img/carrefour/cover-v1-small.jpg" link="/img/carrefour/cover-v1.jpg" title="La couverture de notre livre" >}}

Il s'agit d'un recueil de nouvelles et d'illustrations des membres de la [Communauté du Stylo][commudustylo] dont je fais partie.
Il est composé de participations à nos [défis du stylo][bimonthly], écrites par {{< author "tony" >}}, {{< author "ngorzo" >}}, {{< author "sancho" >}} et moi-même, et dessinées par {{< author "vinzouille" >}}, {{< author "ngorzo" >}} et {{< author "captain-jahmaica" >}}.

Si vous êtes curieux, je vous invite à lire [les leçons que j'ai tirées de la gestion de ce projet][side-project-lessons], ou vous pouvez [acheter ce livre][carrefour-bookelis] pour soutenir les projets de notre petite communauté.


## Ce qui a changé sur le site

### Amélioration progressive (ou dégradation élégante)

Depuis que Hugo a inclus [ESBuild][esbuild], j'ai été tenté d'abandonner ma compilation Rollup.
J'avais cependant peur, car cela impliquait de se priver de la transpilation Babel, et donc certainement de devoir arrêter de supporter les anciens navigateurs.

Puis [_The Ethically-Trained Programmer_ a écrit un billet][kill-ie11] dont j'ai tiré deux idées intéressantes :

* IE11 n'est plus supporté, alors pourquoi vouloir offrir la même expérience qu'à travers des navigateurs plus modernes ?
* Sans tester sous IE et en faisant simplement confiance à Babel, la probabilité que le script soit effectivement compatible est plutôt mince.

Bien entendu, je savais tout ça, mais il est toujours bon de se le faire rappeler.
Je me suis dit que je pouvais peut-être supporter IE11 différemment, m'assurer que les fonctionnalités étaient disponibles sans les mêmes interactions.
Un test rapide avec IE11 a prouvé immédiatement qu'il ne fonctionnait pas du tout…

Je me suis inspiré des techniques que Carl Johnson présentait dans [son billet][kill-ie11] pour alléger mon JavaScript, me permettant de [le compiler avec Hugo][hugo-esbuild] au lieu de Rollup + Babel.
J'ai ainsi pu réduire l'empreinte des scripts de Keyboard Playing tout en améliorant le support pour les anciens navigateurs.
J'éprouve une certaine satisfaction du résultat et suis d'accord avec Carl : abandonner le support pour IE11 est un levier d’[amélioration progressive][wiki-progressive-enhancement] (ou peut-être une forme de [dégradation élégante][wiki-graceful-degradation]).


### Nouvelles transformations pour le Download Proxy

Le Download Proxy fonctionne très bien et m'a débloqué à quelques occasions depuis son déploiement l'an dernier, mais l'encodage en base64 n'est pas la transformation la plus pratique à utiliser.
J'en ai ajouté, dont deux permettent de récupérer le fichier original sans avoir recours à la ligne de commande.
Lisez le [billet à ce sujet][dl-proxy-new] pour en apprendre davantage.


### Un domaine .fr tout neuf

J'ai finalement craqué en novembre, [j'ai acheté `keyboardplaying.fr`][tweet-fr-domain] pour remplacer le pas si élégant `fr.keyboardplaying.org`.
Ce n'est pas un grand changement, mais ça devrait simplifier la mémorisation de l'adresse.


## Et ensuite ?

Du côté du blogue, il reste une tripotée de sujets sur lesquels écrire.
J'essaie à présent de préparer des brouillons et d'écrire les billets en avance afin de pouvoir les publier plus régulièrement, mais je serai déjà satisfait de [parvenir à assurer un billet chaque mois][tweet-resolution].

Le site n'est pas encore tout à fait ce que j'en imaginais.
Je souhaite toujours créer une feuille de style pour l'impression, des pages pour les auteurs et d'autres idées que je repousse régulièrement.

Bien entendu, d'autres projets s'ajoutent à mon emploi du temps, quelques travaux d’[écriture créative][stories] m'attendent…
Nous verrons bien dans quel ordre les choses se feront.


[commudustylo]: https://twitter.com/commudustylo
[bimonthly]: {{< relref path="/inspirations/bimonthly" >}}
[side-project-lessons]: {{< relref path="/blog/2020/12/21-managing-personal-project" >}}
[carrefour-bookelis]: https://www.bookelis.com/romans/44365-Carrefour-des-imaginaires.html

[esbuild]: https://github.com/evanw/esbuild
[hugo-esbuild]: https://gohugo.io/hugo-pipes/js/
[kill-ie11]: https://blog.carlmjohnson.net/post/2020/time-to-kill-ie11/
[wiki-progressive-enhancement]: https://fr.wikipedia.org/wiki/Am%C3%A9lioration_progressive
[wiki-graceful-degradation]: https://fr.wikipedia.org/wiki/Tol%C3%A9rance_aux_pannes

[dl-proxy-new]: {{< relref path="/blog/2021/01/11-dlproxy-2-1" >}}

[tweet-fr-domain]: https://twitter.com/KeyboardPlaying/status/1329462046497398785
[tweet-resolution]: https://twitter.com/KeyboardPlaying/status/1346050491512401921

[stories]: {{< relref path="/stories" >}}
