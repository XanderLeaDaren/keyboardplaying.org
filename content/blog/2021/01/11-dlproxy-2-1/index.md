---
date: 2021-01-11T07:00:00+01:00
title: New Transformations for the Download Proxy
subtitle: Let's get back to work!
slug: new-transformations-download-proxy
description: |-
  Version 2.1 of the Download Proxy brings new and easier-to-use transformations.
author: chop
categories: [ projects, kp.org ]
tags: [ java, quarkus ]
keyword: [ download proxy, firewall, base64, zip, image ]

references:
- id: htg-image-zip
  name: How to Hide Zip Files Inside a Picture Without any Extra Software in Windows
  title: true
  url: https://www.howtogeek.com/119365/how-to-hide-zip-files-inside-a-picture-without-any-extra-software/
  date: 09/2016
  lang: en
  author:
    name: Taylor Gibb, How-To Geek
- id: jpeg-magic-numbers
  name: Magic number (programming) — Format indicators
  title: true
  url: https://www.howtogeek.com/119365/how-to-hide-zip-files-inside-a-picture-without-any-extra-software/
  lang: en
  author:
    name: Wikipedia
---

Last year, I posted about the [v2.0 of the Download Proxy][dlproxy-v2.0].
At the time, the main event was the change of the technological stack and a restricted set of transformations.
Today, I happily publish [a version 2.1][dlproxy].
Here are the main changes.

<!--more-->


## What's Past Is Prologue

Sometimes, when working in [software creation][software-creation], we have to make do with corporate firewalls that don't take our specific needs into accounts.
Developers may have administration rights to install any tool they deem fit on their machine, while not having the possibility to download them.
The purpose of the proxy is to make our work smoother.

**Please note this is not an incitation to bypass all rules, just a help to save you some time.**

If you wish to know more about how I came to create the Download Proxy, its history, the technological stack… please read [my previous post][dlproxy-v2.0].



## The Available Transformations

The main purpose of this release was to add new transformations.
The existing ones were either limited in their ability to bypass a firewall or in their ease of use.


### Identity

This (non-)transformation simply redirects the stream from an address to another.
Put simply, instead of downloading your file from ablockeddomain.com, you'll get it from keyboardplaying.org.

This transformation is useful only in case the file is authorized but the domain serving it is not.


### Base64 encoding

Here, the proxy encodes the file in [Base64][base64].
It works with most firewalls as it hides the real content of the file.
It may take some time to download as the firewall inspects the content, though.
The file will also be larger than the original.

Another inconvenient of this transformation is that you'll need a command line to revert it to the original file.

On Linux or with Git Bash, you can use the following:
```sh
base64 -d myfile.exe.b64 > myfile.exe
```

On Windows, you can use certutil:
```sh
certutil -decode myfile.exe.b64 myfile.exe
```


### *New!* Zip {#zip}

This transformation returns the file you want to download inside a zip.
This was the secret weapon of the v1.1 of the Download Proxy, ten years ago.
Firewalls didn't detect that the files I downloaded were jars, since they were instead jars in zips.

But firewalls evolved, inspected the content of zips and it began to fail.
That's when the Base64 appeared in the Download Proxy.

Yet, it may serve you and still allow you to bypass firewalls.
Don't hesitate to use it: unzipping a file is easier than decoding a Base64 file.


### *New!* Unzippable image

This transformation returns the same thing as [the Zip transformation](#zip), but hidden behind an image.
The downloaded file is detected as an image, can be displayed as an image, but most of all, it can be opened with an archiving software like [7-Zip].

The idea was inspired from [a post on How To Geek][htg-image-zip].


### *New!* Fake jpeg

This transformation adds [JPEG's magic numbers](https://en.wikipedia.org/wiki/Magic_number_(programming)#Format_indicators) at the beginning and end of the file.
Firewall should recognize these as images, and they can hardly block all pictures as that would make most websites unusable.

This one was inspired from other colleagues who had used it to bypass their own corporate firewall.
I only miss one element: the script that returns the jpeg file to its original form.

That's what makes this transformation the least usable of all: you have to remove three bytes at the beginning of the file and two at the end.
You can do it in a text editor, such as [Notepad++][notepadpp].

Or, if your system has `sed` available, you may use this command:
```sh
sed 's/^\xFF\xD8\xFF//g' file.exe.jpg | sed 's/\xFF\xD9$//' > file.exe
```


## The Next Step

I'm not sure I'll create new transformations, though suggestions are welcome.

However, there's an idea for a new major feature: corporate firewalls have a tendency to block some very useful domains.
At a previous client, all blogspot addresses were unreachable.
This is quite damageable as many developers use blogs to share their discoveries, which have solved more than once complicated problems I was encountering.

Another example is my current main client, where all access to the *.oracle.com domain is blocked since the Java license changed.
Since I don't know any mirror for the official Javadoc or doc for [Oracle SQL][oracle], it's quite a bother.

An idea I've had for quite some time is to create a proxy browser: give it the URL of a page and the proxy redirects all stream, by updating all relative and absolute URLs on this domain.
This could be the main feature of the v3 of the proxy.



## Where Can I Find It?

I've designed a UI for it that you can find [here][dlproxy-ui].
If you're more interested in Swagger UI or OpenAPI documentation, go to the deployed instance [there][dlproxy].
Finally, if you wish to have a look at the sources or even contribute, find it [on Gitlab][dlproxy-gl].

And now that the firewall is no longer an issue, be productive!



{{% references %}}

[dlproxy-v2.0]: {{< relref path="/blog/2020/04/06-dlproxy" >}}
[dlproxy]: https://kp-dlproxy.herokuapp.com/
[dlproxy-ui]: {{< relref path="tools/dl-proxy" >}}
[dlproxy-gl]: https://gitlab.com/keyboardplaying/download-proxy
[software-creation]: {{< relref path="/categories/software-creation" >}}
[base64]: https://en.wikipedia.org/wiki/Base64
[7-Zip]: https://www.7-zip.org/
[notepadpp]: https://notepad-plus-plus.org/
[oracle]: {{< relref path="/tags/oracle" >}}
