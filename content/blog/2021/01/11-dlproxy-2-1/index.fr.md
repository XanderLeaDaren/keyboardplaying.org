---
date: 2021-01-11T07:00:00+01:00
title: De nouvelles transformations pour le Download Proxy
subtitle: Et on peut se concentrer sur le boulot !
slug: nouvelles-transformations-download-proxy
description: |-
  La version 2.1 du Download Proxy apporte de nouvelles transformations, plus faciles à utiliser.
author: chop
categories: [ projects, kp.org ]
tags: [ java, quarkus ]
keyword: [ download proxy, pare-feu, base64, zip, image ]

references:
- id: htg-image-zip
  name: How to Hide Zip Files Inside a Picture Without any Extra Software in Windows
  title: true
  url: https://www.howtogeek.com/119365/how-to-hide-zip-files-inside-a-picture-without-any-extra-software/
  date: 09/2016
  lang: en
  author:
    name: Taylor Gibb, How-To Geek
- id: jpeg-magic-numbers
  name: Magic number (programming) — Format indicators
  title: true
  url: https://www.howtogeek.com/119365/how-to-hide-zip-files-inside-a-picture-without-any-extra-software/
  lang: en
  author:
    name: Wikipedia
---

L'an dernier, j'ai publié une [v2.0 de mon Download Proxy][dlproxy-v2.0].
À l'époque, la principale nouveauté était le changement de pile technologique et une liste restreinte de transformations.
Aujourd'hui, c'est avec plaisir que je peux partager [une version 2.1][dlproxy].
En voici les principaux changements.

<!--more-->


## Le passé fait le contexte

Dans le domaine de la [création logicielle][software-creation], il faut parfois faire avec un pare-feu d'entreprise qui ne tient pas compte de nos besoins spécifiques.
Les développeurs ont parfois des droits d'administration sur leur machine afin de pouvoir installer tous les outils dont ils ont besoin et ne peuvent pourtant pas les télécharger.
Le but du proxy est de faciliter notre travail.

**Merci de garder en tête qu'il ne s'agit pas d'une incitation à contourner toutes les règles, juste un coup de pouce pour vous faire gagner du temps.**

Si vous souhaitez en savoir davantage sur l'histoire du projet, le pourquoi de sa création, sa pile technologique… je vous invite à lire [mon billet précédent][dlproxy-v2.0].



## Les transformations disponibles

Le principal but de cette version était d'ajouter de nouvelles transformations.
Les existantes étaiet limitées soit dans le contournement possible des pare-feu, soit dans leur facilité d'utilisation.


### Téléchargement à l'identique

Cette (non-)transformation redirige simplement le flux d'une adresse vers une autre.
En bref, au lieu de télécharger un fichier depuis undomainebloqué.com, vous l'obtiendrez depuis keyboardplaying.fr.

Elle ne vous sera utile que dans les cas où le fichier est autorisé mais pas le domaine le mettant à disposition.


### Encodage en base64

Ici, on retourne le fichier encodé en [base64].
Cela fonctionne avec la plupart des pare-feu, le contenu réel étant dissimulé.
Le pare-feu risque malgré tout d'essayer d'analyser le contenu du fichier, ce qui peut prendre un peu de temps.
Le fichier sera également plus volumineux que l'original.

Un autre inconvénient de cette transformation est qu'il vous faudra une invite de commande pour ramener le fichier à son état initial.

Sous Linux ou avec Git Bash, vous pouvez utiliser ceci :
```sh
base64 -d myfile.exe.b64 > myfile.exe
```

Sous Windows, vous pouvez vous appuyer sur certutil :
```sh
certutil -decode myfile.exe.b64 myfile.exe
```


### *Nouveauté !* Zip {#zip}

C'était la fonctionnalité phare de la version 1.1, il y a dix ans !
Télécharger le fichier dans un zip.
Les pare-feu ne détectaient pas qu'il s'agissait d'un jar, puisque c'était un jar dans un zip.

Malheureusement, les pare-feu ont évolué, ont commencé à inspecter le contenu des zips…
C'est là que j'ai commencé à jouer en base64.

Cette transformation pourrait néanmoins encore vous servir et décompresser un fichier reste bien plus aisé que de décoder du base64.


### *Nouveauté !* Image à décompresser

Cette transformation retourne la même chose que [Zip](#zip), mais caché derrière une image.
Le fichier est reconnu comme une image, peut être affiché comme une image, mais plus que tout, il peut être ouvert avec un logiciel comme [7-Zip].

L'idée a été inspirée [d'un billet sur How To Geek][htg-image-zip].


### *Nouveauté !* Faux jpeg

Cette transformation ajoute [les nombres magiques du format JPEG](https://fr.wikipedia.org/wiki/Nombre_magique_(programmation)#Indicateur_de_format) au début et à la fin du fichier.
Les pare-feu devraient les reconnaître comme des fichiers images, et bloquer toutes les images sur Internet rendrait la plupart des sites web inutilisables.

Celle-ci a été inspirée de collègues qui l'ont utilisée pour contourner leur propre pare-feu d'entreprise.
Il ne me manque qu'un élément : le script qui permet de repasser de jpeg au fichier original.

C'est ce qui fait de cette transformation la moins utilisable de celles que je propose : il vous faut retirer les trois premiers et deux derniers octets du fichier.
Vous pouvez le faire dans un éditeur de texte comme [Notepad++][notepadpp].

Ou, si `sed` est disponible sur votre système, vous pouvez utiliser cette commande :
```sh
sed 's/^\xFF\xD8\xFF//g' file.exe.jpg | sed 's/\xFF\xD9$//' > file.exe
```


## La prochaine étape

Je ne pense pas ajouter de nouvelle transformation, bien que les suggestions restent bienvenues.

Toutefois, une idée me trotte en tête depuis un moment : les pare-feu d'entreprise ont tendance à bloquer des domaines utiles.
Chez un de mes clients, toutes les adresses blogspot étaient inaccessibles.
C'est plutôt dommageable étant donné que de nombreux développeurs utilisent les blogs pour partager leurs découvertes, qui m'ont aidé à résoudre des problèmes plus d'une fois.

Un autre exemple est mon client principal actuel, où toutes les adresses *.oracle.com sont bloquées depuis le changement de licence Java.
Étant donné que je ne connais aucun miroir pour la Javadoc officielle ou la documentation de la syntaxe [SQL d'Oracle][oracle], c'est plutôt gênant.

Mon idée serait de créer un navigateur proxy : vous lui donnez l'adresse d'une page et le proxy redirige tous les flux, en changeant les adresses relatives ou absolues sur ce domaine.
Cela pourrait être la nouveauté majeure de la v3 du proxy.



## Où trouver le proxy ?

J'ai conçu une interface utilisateur que vous pourrez trouver [ici][dlproxy-ui].
Si vous êtes davantage intéressé par un Swagger UI ou une documentation OpenAPI, allez sur l'instance en ligne [là][dlproxy].
Enfin, si vous souhaitez regarder les sources ou contribuer, c'est possible [sur Gitlab][dlproxy-gl].

Et maintenant que le pare-feu ne vous bloque plus, bonne productivité à vous !



{{% references %}}

[dlproxy-v2.0]: {{< relref path="/blog/2020/04/06-dlproxy" >}}
[dlproxy]: https://kp-dlproxy.herokuapp.com/
[dlproxy-ui]: {{< relref path="tools/dl-proxy" >}}
[dlproxy-gl]: https://gitlab.com/keyboardplaying/download-proxy
[software-creation]: {{< relref path="/categories/software-creation" >}}
[base64]: https://fr.wikipedia.org/wiki/Base64
[7-Zip]: https://www.7-zip.org/
[notepadpp]: https://notepad-plus-plus.org/
[oracle]: {{< relref path="/tags/oracle" >}}
