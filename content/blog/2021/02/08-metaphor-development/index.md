---
date: 2021-02-08T07:00:00+01:00
title: Use Metaphors when Speaking with Non-Technical People
#subtitle: A witty line as a subtitle
slug: metaphor-software-development-project
description: |-
  Exchanging with clients is part of our normal jobs, but it's not part of the client's to understand what we do.
  Sometimes, an image can help go a long way facilitating mutual comprehension.
author: chop
categories: [ software-creation ]
tags: [ management ]
keywords: [ communication, software architecture, house, comics, technical debt ]
---

There's [an image I like][metaphor] when I speak about software development: the construction of a house.
Like any metaphor, it has limits, but it helps non-technical people understand what their requests mean to us by comparing them with something they can understand.

<!--more-->

## Building a Software Project Is Like Building a House

It's quite simple, really: your software project is a house (or maybe a building if it's a big one).
The idea is not mine!
Ten years ago, my project manager was renovating the house he built and such comparisons came to him naturally.

Once, the client wanted to discuss a feature and he answered, "We can't do that now. That'd be like adding a window, but we don't even have the walls up yet."
That, the client understood.
We could have spent time explaining that communication channels couldn't work until there was an engine that provides actual data, but can you see how that can be confusing to someone who doesn't work in the field?

Put yourself in your client's shoes: they don't know a thing about the code or technology behind their project, and they hire us so that they don't have to.
They ask questions and we answer with gibberish.
We seem to type random words on a screen and, a few weeks or months later, we deliver a new feature or application.
Might as well be a feat of demonic magic!

But everybody knows that building or maintaining a house requires to lay down bricks, cementing them properly, not neglecting the foundations, pipes, electricity…
Using a metaphor can help us explain what we do by giving them something they can relate to.


## Some Example Illustrations by Vincent Déniel

A few months back, I discovered [Vincent Déniel][vincentdnl]'s [drawings][techdrawings], which I love because they describe quite accurately, with a dry humor, what we live every day.

What's more, he's used this metaphor in several of his drawings.
Here's a selection of my favorites.
Don't hesitate to browse [his gallery][techdrawings] for more.


### The Wonderful Explanation of the Technical Debt

[![A client in front of a broken house complains about the time required to add a window](/img/vincentdnl/technical-debt.png)](https://vincentdnl.com/drawings/technical-debt)

Technical debt is an investment you made at one time to save some time, but like any debt, interests build up, so you'd better reimburse it as fast as you can.


### Revisiting "I Want a Facebook-Like Site in Three Weeks"

[![A client asks to build a skyscraper in three weeks](/img/vincentdnl/a-simple-website.png)](https://vincentdnl.com/drawings/a-simple-website)


### The Myth of the Full-Stack Developer

[![A manager asks a superequipped man to fix the roof, do plumbing, paint the house…](/img/vincentdnl/full-stack-developer.png)](https://vincentdnl.com/drawings/full-stack-developer)


### Other House-Building-Related Drawings

There are several other drawings in the same style:

- [It's not because we see the app that it works.](https://vincentdnl.com/drawings/demo-day)
- [You should build things before making them look good.](https://vincentdnl.com/drawings/priorities)
- [Going fast may now not pay in the end.](https://vincentdnl.com/drawings/testing-tdd)
- [Let the specialists use the tool they think is appropriate.](https://vincentdnl.com/drawings/questioning)
- [Be careful how you break your monolith into microservices.](https://vincentdnl.com/drawings/breaking-the-monolith)


## Find Metaphors You'll Be at Ease With

Of course, the house building is only one possible metaphor.
You should find something you're at ease with.
I sometimes used a car as an image, the dashboard and commands being the interface the engine, the back end.

Do you use metaphors, too?
Care to share in the comments?


[metaphor]: {{< relref path="/stories/2019/11/29-wc-metaphore" >}}
[vincentdnl]: https://twitter.com/vincentdnl/
[techdrawings]: https://vincentdnl.com/drawings/
