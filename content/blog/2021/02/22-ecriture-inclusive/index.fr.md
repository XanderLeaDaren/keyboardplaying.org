---
date: 2021-02-22T07:00:00+01:00
title: L'écriture inclusive, c'est quoi ?
subtitle: Ce n'est pas qu'une histoire de point milieu
slug: ecriture-inclusive
description: |-
  Lorsque l'on parle de l'écriture inclusive, c'est souvent pour décrier son usage et sa lisibilité.
  Et si on faisait un point ?
author: chop
categories: [ writing ]
tags: [ langue-francaise ]
keywords: [ écriture inclusive ]

references:
- id: femme-jecris-ton-nom
  name: Femme, j'écris ton nom…
  title: true
  url: https://www.vie-publique.fr/sites/default/files/rapport/pdf/994001174.pdf
  date: 1999
  lang: fr
  author:
    name: Centre National de la Recherche Scientifique, Institut National de la Langue Française
---

La semaine dernière, j'ai vu un [fil de gazouillis][tw-thread-jolivet] au sujet d'une proposition de loi visant à interdire l'écriture inclusive dans les textes administratifs, avec des arguments valables et malheureusement quelques amalgames.
J'ai eu envie de remettre un peu les choses à plat, sans chercher à défendre la forme proposée (mais un peu partisan du fond quand même).
On y va calmement ?

<!--more-->


## Pourquoi l'écriture inclusive

Dans mon expérience (certes limitée à l'anglais et l'allemand), les langues germaniques portent assez peu le genre : les adjectifs ne s'accordent pas, seuls les articles et pronoms le portent[^fn-surprise].
En anglais, si le pronom reprend un nom dont on ne connaît pas le genre, on préfèrera utiliser un pronom dérivé de la forme plurielle (en empruntant un exemple à [Wikipédia](https://en.wikipedia.org/wiki/Gender_neutrality_in_languages_with_gendered_third-person_pronouns#Standard_usage) : « _Where a recipient of an allowance under section 4 absents **themself** from Canada, payment of the allowance shall ..._ »).
On a bien entendu le pronom neutre _it_, mais on évite de l'utiliser pour parler d'êtres humains.

Revenons à des langues plus chaudes, plus latines : le neutre, l'ingenré n'existe pas parmi nos pronoms.
En français, il y a bien _on_, mais son usage en tant que pronom neutre et indéfini crée vite des phrases très peu naturelles dans une conversation, qui pourraient rapidement rappeler un texte juridique : _on fera bien attention à avoir sa pièce d'identité sur soi_.

<aside><p>Le but est de permettre une égalité de représentation des deux sexes dans la langue.</p></aside>

Si l'ingenré n'existe pas, il faut trancher d'une façon ou d'une autre.
Dès le primaire, j'ai entendu dire que « le masculin l'emporte sur le féminin »[^fn-sapir-whorf].
Cela amène rapidement à tout écrire au masculin, même lorsque cela couvre d'autres populations[^fn-population].

Voici ce que souhaite l'écriture inclusive, ainsi que d'autres approches : permettre une égalité de représentation des deux sexes.
La parité dans la langue.



[^fn-surprise]: Avec parfois quelques surprises : la petite fille se traduit en allemand _das Mädchen_, neutre.

[^fn-sapir-whorf]: Au-delà des préoccupations présentées dans le cadre de l'écriture inclusive, on pourrait aussi se demander comment cette domination du masculin dans la langue influe sur notre conception de la société.
Je vous invite par exemple à découvrir l'[hypothèse de Sapir-Whorf](https://fr.wikipedia.org/wiki/Hypoth%C3%A8se_de_Sapir-Whorf) si vous n'en connaissez pas les grandes lignes.

[^fn-population]: J'entends par là non seulement les femmes, mais aussi toutes les personnes qui ne se définissent pas de façon binaire et souffrent tout autant des stéréotypes de sexe.


## Quelles sont les solutions proposées ?

Les tenants de l'écriture inclusive ont rédigé un manuel présentant leurs propositions.
Il est disponible [sur leur site][ecriture-inclusive][^fn-bypass-mail].

Leur proposition se décompose en trois points principaux :

> * Accorder en genre les noms de fonctions, grades, métiers et titres
> * User du féminin et du masculin, que ce soit par l'énumération par ordre alphabétique, l'usage d'un point milieu, ou le recours aux termes épicènes
> * Ne plus employer les antonomases du nom commun « **F**emme » et « **H**omme »

Je vais passer rapidement sur ces propositions, de la moins à la plus crispante.


[^fn-bypass-mail]: Si vous souhaitez le consulter mais que, comme moi, vous êtes frileux à partager votre email pour télécharger un document, une recherche de « manuel écriture inclusive » dans votre moteur favori devrait rapidement vous permettre de trouver ce PDF.


### Pas d'antonomase

Ça, c'est vite fait : quand on parle des « droits de l'Homme », on parle de l'Homme en tant qu'humanité, dont la femme fait partie.
La proposition est donc le manifester à l'écrit : plutôt que « droits de l'Homme », on parlerait des « droits humains ».

Cela ne me semble pas très compliqué.
Ce n'est guère qu'un pli à prendre, un petit changement d'habitude.


### Accorder en genre les titres

Là encore, l'effort est minime et beaucoup le font désormais : lorsque l'on cite la fonction, le grade, le métier ou le titre d'une femme, il convient de le mettre au féminin.
Ces noms existent depuis longtemps et ne sont pas des inventions récentes.

Le _Manuel d'écriture inclusive_ recommande de consulter le guide [_Femme j'écris ton nom…_][femme-jecris-ton-nom], qui contient plus de deux-mille tels noms au féminin et au masculin.


### User du féminin et du masculin

C'est là qu'on commence à cristalliser le plus de critiques et de crispations.

Le principe de base est d'assurer qu'on ne se limite pas au masculin générique mais qu'on inclut bien les autres populations dans le discours.


#### Recours aux termes épicènes

Il s'agit de mots dont la forme ne varie pas avec le genre (p. ex. un/une artiste, un/une cadre, un/une membre).
Ils permettent un équilibre de représentation sans effort particulier.


#### Mention par ordre alphabétique

Parce qu'on met les deux sexes sur un pied d'égalité, il n'y a pas de raison d'en citer un avant l'autre.
L'idée est donc de laisser l'ordre alphabétique décider de l'ordre.

> _elle et il_ ; tous les Acadiens, toutes les Acadiennes ; celles et ceux[^fn-exemple]

[^fn-exemple]: Les exemples sont tirés du _Manuel d'écriture inclusive_, mais je me suis permis d'utiliser des points-virgules pour mieux séparer les propositions.


#### Utilisation du point milieu

Ce point est certainement celui qui a causé la plus grande levée de boucliers à l'arrivée de l'écriture inclusive, à tel point qu'on résume souvent l'écriture inclusive à cet aspect.
Pour citer le manuel :

> L’utilisation du point milieu en composant le mot comme suit : racine du mot + suffixe masculin + point milieu + suffixe féminin.
> On ajoutera un point milieu supplémentaire suivi d’un « s », si l’on veut indiquer le pluriel.
>
> Quelques exemples : _acteur·rice·s_, _ingénieur·e·s_, _ceux·elles_, _sénior·e·s_.

Il y a de bonnes raisons de ne pas aimer cette écriture :

- les outils de lecture d'écran ne sont pas (encore ?) équipés pour y faire face ;
- on nous propose d'utiliser un caractère qu'aucun clavier actuel ne permet de saisir facilement.


### Et si on laissait tomber le point milieu ?

<aside><p>Le point milieu n’est qu’une proposition de nouvelle forme pour une pratique plutôt ancienne.</p></aside>

En vérité, on inclut depuis des décennies les formes féminines que met en exergue le point milieu, seul le caractère à changé.
Les parenthèses et la barre oblique sont ce que j'ai toujours connu.
En reprenant les exemples du manuel, cela donnerait « à l'ancienne » : _acteurs/actrices_, _ingénieur(e)s_, _ceux/celles_, _sénior(e)s_.

J'ai également vu d'autres propositions prédatant le _Manuel_ utiliser des points ou des tirets : _ingénieur.e.s_ ou _ingénieur-e-s_.
Je préfère personnellement les parenthèses qui évitent de détourner la « sémantique » d'une autre ponctuation.
L'avantage du point milieu est d'utiliser un caractère qui n'est pas associé à une signification autre et qui ne nécessite pas forcément de caractère fermant (_un·e ingénieur·e_ contre _un(e) ingénieur(e)_).
Il est donc des cas où il est plus « léger ».

Si c'est le point milieu qui vous rebute, je viens de vous proposer trois tournures alternatives pour remplir le même objectif.
L'autre raison courante de refuser cette pratique est qu'elle alourdit la lecture, l'écriture, demande de la réflexion (du moins le temps d'en faire une habitude).


## C'est tout !… ?

J'ai fait le tour de ce qui est dans le _Manuel d'écriture inclusive_, mais certainement pas de toutes les pratiques que l'on place désormais sous ce nom.


### L'accord au plus proche ?

J'avais lu cette proposition lorsque je m'étais intéressé pour la première fois à l'écriture inclusive : plutôt que de choisir la règle « le masculin l'emporte », l'accord (en genre) pourrait être fait avec le nom le plus proche.
Par exemple, on écrirait « les hommes et les femmes sont allé**e**s », car « femmes » est plus proche du participe passé « allé » qu'« hommes ».

En pratique, je n'ai pas vu cette proposition mise en œuvre.
J'imagine que si, elle est belle sur le papier, elle pose d'autres questions :
- Quid de l'accord en nombre ? (« l'homme et la femme sont allée » ?)
- Dans certains cas, il n'est plus possible de dire si l'objet accordé se rapporte au dernier mot ou au groupe (« Aujourd'hui, j'ai donné des encadrements et formations diverses » -> « divers » porte-t-il uniquement sur les formations ou qualifie-t-il aussi les encadrements ?).

L'accord au masculin semble pour l'instant faire encore consensus.


### Les contractions

J'ai parlé plus haut d'un [thread de gazouillis][tw-thread-jolivet] et d'une proposition de loi.
Plusieurs contractions y sont mises en avant :

> Au hasard de publications parfois officielles, nous découvrons des mots nouveaux : « iels » pour « ils/elles », « toustes » pour « tous/toutes », « celleux » pour « celles/ceux », « Cher·e·s lecteur·rice·s déterminé·e·s ». #ecritureinclusive

Certains arguments sont mis assez justement en avant, notamment le fait que des inquiétudes apparaissent pour les personnes atteintes de troubles tels que la dyslexie, pour qui ces contractions peuvent complexifier la lecture, ou les populations étrangères voulant apprendre le français, qui pourraient être confuses par ces néologismes.

Encore une fois, ces contractions ne figurent pas au sein du _Manuel d'écriture inclusive_, et mettre « ils/elles » résout déjà le problème ciblé par ce manuel.
Je pense qu'il est tout à fait possible de défendre une représentation égalitaire des sexes dans la langue par l'utilisation de formes développées si cela semble plus lisible.
Je suppose également que les plateformes de microblogging comme Twitter, avec leurs limitations sur le nombre de caractères, ont favorisé l'émergence de telles contractions.


### Alors quoi ?

L'écriture inclusive n'est pas qu'une question de langue, mais une question de société.
Pour ou contre n'est pas forcément la bonne question à se poser.
Comprendre les enjeux sont importants.
Une fois qu'ils sont intégrés, peut-être pourrait-on prendre un peu de recul pour revoir les propositions du _Manuel_ et les adapter afin qu'elles ne soient une gêne pour personne.

Comme en informatique, à un problème unique, les solutions sont multiples.
Le manuel n'a pas forcément pris en compte tous les paramètres, mais il y a au moins cinq ans de recul sur ces idées.
Il est certainement temps de passer outre la réaction épidermique et regarder si tout est vraiment à jeter.


{{% references %}}

[tw-thread-jolivet]: https://twitter.com/FJolivet36/status/1359861383127515141
[hce-guide-pratique]: https://www.haut-conseil-egalite.gouv.fr/stereotypes-et-roles-sociaux/travaux-du-hce/article/guide-pratique-pour-une
[ecriture-inclusive]: https://www.motscles.net/ecriture-inclusive
