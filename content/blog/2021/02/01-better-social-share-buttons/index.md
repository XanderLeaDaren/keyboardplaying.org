---
date: 2021-02-01T07:00:00+01:00
title: Better Social and Share Buttons
subtitle: RTFM, but know that it sometimes hides options
slug: better-social-share-buttons
description: |-
    Social and share buttons are everywhere, relying on scripts and many external resources.
    But do you wanna know a secret?
    You could do the same with simple web links!
author: chop
categories: [ software-creation ]
tags: [ web-dev, ux, sustainable-digital ]
keywords: [ big techs, privacy, ethics, footprint, advertisement, dillo, gemini, facebook, twitter, linkedin, pinterest ]
---

Social and share buttons seem to be a must-have nowadays, but following the official docs for these may not be the best option for an efficient solution.

<!--more-->

## What's Wrong with the Documented Solutions

When you look at the official doc for including a social button, at [Facebook][fb-share-doc] or [Twitter][tw-tweet-btn] for instance, the solution you are given includes JavaScript, which is not ideal on at least three accounts.


### Ethics

<aside><p>Social buttons scripts track web users without their consent.</p></aside>

First and foremost, what's often reproached to those scripts is that they include/are trackers.
Some of these companies are known to use these scripts to build your visitors' profile [even if they don't have an account on their service][fb-shadow-profile].

So, by including those in your page, you help big tech companies collecting data about users that are too often non-informed and non-willing.
OK, that's less true now that GDPR requires consent before loading this kind of script, but that's assuming that websites are well made and don't load external "partner" resources _before_ this consent is given.[^fn-gdpr]

There are alternative ways to make share buttons (we'll see them below), but I suspect this is the main reason why the documentation does not speak about them.
[Data is the new ~~gold~~ ~~oil~~ blood][data], so why propose a clean way if there's one that'll help collect it?

[^fn-gdpr]: Spoiler: in many cases, websites still store cookies and load scripts before consent is given, which is contrary to the GDPR.


### User Experience

Another issue with these scripts (and all the other scripts that load parts of a website such as ads) is that they'll often slow down the first meaningful paint or make the page flicker until all scripts are loaded and executed.
In both cases, bad news in terms of user experience.

Plus, it won't work if the user doesn't have JavaScript active.
Okay, that's true of most websites, but that's precisely why minimalist browsers like [Dillo][dillo] or protocols like [Gemini][gemini] are encountering a growing success.
Visitors would like to refocus on the information they're looking for and stop being distracted.


### Environment

Finally, you may have read a previous post I wrote about the [footprint of digital][footprint][^fn-footprint].
Scripts have an environmental footprint.
Anything that goes over the web does.

That's why I'm trying to keep this website minimalist, why there's not a big, nice picture of share buttons at the top of this page.

So, if you could avoid including a script that'll in turn fetch CSS, images and other scripts, you'd spare the planet a bit.

[^fn-footprint]: If you haven't, I suggest [you go][footprint] right after finishing this post.
It is likely my best bit yet.



## What You Can Do Instead

### A Simple Link Suffices

<aside><p>The share scripts could be replaced with simple links.</p></aside>

I wrote earlier that there are alternative ways to build share buttons.
If you inspect the bottom of this page, you'll see that the buttons I use are simply… web links.
Nothing more.

Actually, you could even see [the template responsible for these][gitlab-share-links] on the website's repo.


### Some Generators Examples

This solution is so simple that several link generator exists.

* The [Share Link Generator](http://www.sharelinkgenerator.com/) is quite pretty and has a clean, non-cluttered UI.
* The [Share Link Creator](https://www.websiteplanet.com/webtools/sharelink/)'s UI is a bit heavier, but enter your page's address once to get all share links at once.
* The [Social Share Link Generator](https://www.coderstool.com/share-social-link-generator) is impressive because of the array of share possibilities (including apps such as WhatsApp, Telegram and so on).


### Templates for Your Website

Let's have a few examples.
If you wish to include a share link to your page (https://yoursite.com/your-page), you should use the following links.[^fn-links]

[^fn-links]: For the sake of simplicity, all examples use clear URLs, but I'd recommend URL-encoding all query parameters.

In all cases, the link will open the corresponding social network, preparing an empty message with a link to your page.
The visitor will be able to edit it before submitting, or cancel it altogether.

For Facebook, Twitter and LinkedIn, you can just try with the buttons at the bottom on the page.
Nothing will appear on your profile before you confirm.


#### Facebook

Share link: `https://www.facebook.com/sharer/sharer.php?u=https://yoursite.com/your-page`


#### Twitter

Share link: `https://twitter.com/intent/tweet?url=https://yoursite.com/your-page&title=Your Title&text=You can add a message&via=aLittleBirdie`

As you can see, you can add some parameters to this URL.
You can include:
* `title`: your page's title;
* `via`: your Twitter profile;
* `text`: some predefined message.

All of these parameters are optional.

This link will open Twitter and prepare a tweet.
In the case of this example, the tweet would read: _Your Title You can add a message https://yoursite.com/your-page via @aLittleBirdie_
It can, of course, be edited before sending.


#### LinkedIn

Share link: `https://www.linkedin.com/sharing/share-offsite/?url=https://yoursite.com/your-page`


#### Pinterest

Share link: `https://pinterest.com/pin/create/button/?url=https://yoursite.com/your-page&media=https://yoursite.com/your-image.jpg&description=Your Title`

Here, you can specify a media and a description.
I won't be able to give you a link to test this one, though.
You'll have to try it out with your own pages. :)



[fb-share-doc]: https://developers.facebook.com/docs/plugins/share-button/
[tw-tweet-btn]: https://publish.twitter.com/?buttonType=TweetButton&widget=Button
[fb-shadow-profile]: https://theconversation.com/shadow-profiles-facebook-knows-about-you-even-if-youre-not-on-facebook-94804
[dillo]: https://www.dillo.org/
[gemini]: https://gemini.circumlunar.space/
[footprint]: {{< relref path="/blog/2020/01/intro-sustainable-digital/01-footprint-of-digital" >}}
[data]: {{< relref path="/blog/2020/01/intro-sustainable-digital/03-thirst-for-data" >}}
[gitlab-share-links]: https://gitlab.com/keyboardplaying/keyboardplaying.org/-/blob/master/layouts/partials/share-links.html
