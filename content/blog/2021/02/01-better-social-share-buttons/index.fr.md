---
date: 2021-02-01T07:00:00+01:00
title: De meilleurs boutons de partage
subtitle: RTFM, mais le manuel n'est pas toujours exhaustif
slug: meilleurs-boutons-partage
description: |-
    Les boutons de partage sur les réseaux sociaux sont partout, s'appuyant sur des scripts et ressources externes.
    Je peux vous confier un secret ?
    Vous pourriez faire la même chose avec de simples liens !
author: chop
categories: [ software-creation ]
tags: [ web-dev, ux, sustainable-it ]
keywords: [ big techs, vie privée, éthique, empreinte, publicité, dillo, gemini, facebook, twitter, linkedin, pinterest ]
---

Les boutons de partage « social » semblent être un prérequis à tout site web, de nos jours.
Les réseaux concernés partagent une documentation sur comment en ajouter à votre site, mais sans vous présenter les solutions les plus efficientes.

<!--more-->

## Ce qui ne va pas avec les solutions documentées

Lorsque vous regardez la documentation officielle pour inclure un bouton de partage, chez [Facebook][fb-share-doc] ou [Twitter][tw-tweet-btn] par exemple, on vous indique nécessairement d'utiliser JavaScript, ce qui n'est pas idéal pour au moins trois raisons.


### L'éthique

<aside><p>Les scripts des boutons sociaux traquent les visiteurs sans leur consentement.</p></aside>

Tout d'abord, on reproche souvent à ces scripts leur côté « traqueur ».
Les grandes sociétés derrière leur maintenance les utilisent en effet pour construire des profils de vos visiteurs, y compris [de ceux n'ayant pas de compte sur ces plateformes][fb-shadow-profile].

Par conséquent, en les incluant sur votre page, vous aidez les _big techs_ à collecter des informations sur des visiteurs qui ne sont pas toujours informés ni volontaires.
Il est vrai que c'est un peu moins vrai depuis que le RGPD impose de recueillir le consentement avant de charger ce genre de scripts, mais c'est en supposant que les sites web soient bien réalisés et ne chargent pas les ressources « partenaires » _avant_ que ce consentement soit donné.[^fn-gdpr]

Les réseaux sociaux offrent des alternatives (dont nous parlerons ci-dessous), mais je suspecte que c'est la raison principale pour ne pas les mettre en avant.
[La donnée est le nouvel ~~or~~ ~~pétrole~~ sang][data], alors pourquoi se priver de la manne que les sites web offrent gratuitement dans l'espoir de voir leur contenu partagé ?

[^fn-gdpr]: [Divulgâchage](https://fr.wiktionary.org/wiki/divulg%C3%A2chage) : dans de nombreux cas, les sites stockent encore des cookies et chargent des scripts avant recueil du consentement, ce qui est contraire au RGPD.


### L'expérience utilisateur

Un autre problème avec ces scripts (comme tous les scripts qui chargent des morceaux de sites, comme les publicités) est qu'ils ralentissent souvent le premier affichage pertinent de la page, ou la font clignoter et se déformer au fur et à mesure que les éléments apparaissent.
Dans les deux cas, cela dégrade l'expérience utilisateur.

Par ailleurs, cela ne fonctionnera pas si l'utilisateur n'utilise pas JavaScript.
Cela semble aujourd'hui vrai de la plupart des sites, mais c'est [justement la raison][frama-web-complique] pour laquelle des navigateurs comme [Dillo][dillo] ou des protocoles comme [Gemini][gemini], minimalistes, rencontrent un succès croissant.
Les visiteurs souhaiteraient se focaliser sur l'information qui les intéresse et se passer des distractions.


### L'environnement

Enfin, vous avez peut-être lu mon billet sur [l'empreinte du numérique][footprint][^fn-footprint].
Les scripts ont une empreinte environnementale.
C'est le cas de tout ce qui transite par Internet.

C'est pour cette raison que j'essaie de garder ce site minimaliste, qu'il n'y a pas une grande et belle image de boutons de partage en haut de cette page.

Un petit geste simple pour la planète serait donc d'éviter d'inclure un script qui va à son tour aller chercher du CSS, des images et d'autres scripts.

[^fn-footprint]: Si ce n'est pas le cas, je vous invite [à le lire][footprint] après avoir terminé celui-ci.
C'est probablement ma meilleure contribution sur ce site.



## La solution alternative

### De simples liens suffisent

<aside><p>Les scripts pour boutons de partage peuvent se remplacer par de simples liens.</p></aside>

J'ai écrit plus haut qu'il existe des alternatives pour avoir des boutons de partage.
Si vous inspectez ceux en bas de cette page, vous verrez qu'il ne s'agit… que de liens hypertextes.
Rien de plus banal.
I wrote earlier that there are alternative ways to build share buttons.

Vous pouvez même consulter [le modèle qui génère ces boutons][gitlab-share-links] sur le dépôt du site.


### Quelques exemples de générateurs

Cette solution est si simple que de nombreux générateurs de liens sociaux existent.

* [Share Link Generator](http://www.sharelinkgenerator.com/) est plutôt joli, avec une interface claire et sobre.
* L'interface de [Share Link Creator](https://www.websiteplanet.com/webtools/sharelink/) est un peu plus encombrée, mais il a l'avantage de générer tous les liens en une seule saisie.
* [Social Share Link Generator](https://www.coderstool.com/share-social-link-generator) est impressionnant par la diversité des réseaux et outils qu'il couvre.
Il génère notamment des liens pour des applications comme WhatsApp et Telegram.


### Des patrons pour votre site

Je vous propose quelques exemples.
Si vous souhaitez inclure un lien de partage vers votre page (https://votresite.fr/votre-page), vous pouvez utiliser les liens suivants.[^fn-links]

[^fn-links]: Pour des raisons de lisibilité, tous les paramètres sont affichés en clair, mais je vous incite à les encoder.

Dans tous les cas, le lien ouvrira le réseau correspondant, préparant un message vide avec un lien vers votre page.
Le visiteur aura la possibilité de l'éditer avant de le partager, ou d'annuler complètement.

Pour Facebook, Twitter et LinkedIn, vous pouvez essayer avec les boutons en bas de page.
Rien n'apparaîtra sur votre profil sans confirmation de votre part.


#### Facebook

Lien de partage : `https://www.facebook.com/sharer/sharer.php?u=https://votresite.fr/votre-page`


#### Twitter

Lien de partage : `https://twitter.com/intent/tweet?url=https://votresite.fr/votre-page&title=Votre titre&text=Vous pouvez ajouter un message&via=unPetitOiseau`

Comme vous pouvez le voir, vous pouvez ajouter des paramètres :
* `title` : le titre de votre page ;
* `via` : votre profil Twitter ou celui de votre site ;
* `text` : un message prédéfini.

Tous ces paramètres sont facultatifs.

Le lien ouvrira Twitter avec un gazouillis préparé.
Dans le cas de l'exemple, le gazouillis serait le suivant : _Votre titre Vous pouvez ajouter un message https://votresite.fr/votre-page via @unPetitOiseau_
Le message peut bien entendu être modifié avant envoi.


#### LinkedIn

Lien de partage : `https://www.linkedin.com/sharing/share-offsite/?url=https://votresite.fr/votre-page`


#### Pinterest

Lien de partage : `https://pinterest.com/pin/create/button/?url=https://votresite.fr/votre-page&media=https://votresite.fr/votre-image.jpg&description=Votre titre`

Ici, vous pouvez spécifier une image et une description.
Je suis dans l'incapacité de vous fournir un lien d'exemple pour le tester.
Il vous faudra l'essayer avec vos propres pages. :)



[fb-share-doc]: https://developers.facebook.com/docs/plugins/share-button/
[tw-tweet-btn]: https://publish.twitter.com/?buttonType=TweetButton&widget=Button
[fb-shadow-profile]: https://theconversation.com/shadow-profiles-facebook-knows-about-you-even-if-youre-not-on-facebook-94804
[frama-web-complique]: https://framablog.org/2020/12/30/le-web-est-il-devenu-trop-complique/
[dillo]: https://www.dillo.org/
[gemini]: https://gemini.circumlunar.space/
[footprint]: {{< relref path="/blog/2020/01/intro-sustainable-digital/01-footprint-of-digital" >}}
[data]: {{< relref path="/blog/2020/01/intro-sustainable-digital/03-thirst-for-data" >}}
[gitlab-share-links]: https://gitlab.com/keyboardplaying/keyboardplaying.org/-/blob/master/layouts/partials/share-links.html
