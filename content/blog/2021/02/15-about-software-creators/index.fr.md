---
date: 2021-02-15T07:00:00+01:00
title: Pourquoi je n'aime pas le mot « développeur »
#subtitle: A witty line as a subtitle
slug: pourquoi-aime-pas-mot-developpeur
description: |-
    « Développeur » est souvent utilisé pour décrire une variété de métiers.
    Pourtant, malgré le large ensemble de compétence qu'il recouvre, je le trouve souvent réductif.
    C'est pourquoi j'ai tenté autre chose sur ce site.
author: chop
categories: [ software-creation ]
#tags: [ management ]
keywords: [ semantic, communication, carrière ]
---

Vous l'aurez remarqué ou pas, mais j'essaie d'éviter les termes « développeur », « codeur » et « programmeur » sur Keyboard Playing, sauf lorsque je parle de l'activité d'écrire du code.
Il y a une raison : dans notre domaine, il est courant d'utiliser ces mots pour décrire une variété de métiers.
N'importe qui touchant de près ou de loin au code peut s'appeler un développeur.

<!--more-->

Pourtant, j'ai souvent l'impression que le mot « développeur » est réducteur, qu'il ne décrit pas l'ensemble du travail réalisé par ceux qui travaillent sur un projet informatique.
En détail, qu'est-ce que je reproche à ce mot ?


## Un peu de tempérance

Les idées présentées dans ce billet peuvent sembler caricaturales.
Je sais que les choses sont beaucoup plus claires et plus nuancées dans l'esprit de beaucoup de monde.
J'ai pourtant le sentiment — sans en avoir discuté — que des amalgames et incompréhensions peuvent avoir lieu pour tous ceux qui n'ont jamais connu la vie au cœur d'une équipe pendant un projet informatique.
C'est de ceci que je parle ici.


## De façon imagée

Pour reprendre [ma métaphore habituelle pour les projets informatiques][metaphor], imaginez une maison en construction.
Il y a une équipe entière derrière un chantier pareil.

Il y a le client, celui qui veut la maison.
Il y a le chef de projet, celui qui gère le contrat et qui s'assure que tout avance comme prévu.
Il y a un leader technique, qui a plusieurs rôles ; il peut être à la fois :
- l'ingénieur expert qui décide de tous les matériaux, contraintes techniques, _et cetera_ ;
- l'architecte, qui dessine les plans de comment les choses s'agencent et se connectent ;
- le constructeur expérimenté, qui peut accompagner ses équipiers sur des sujets qu'ils rencontrent pour la première fois.

Et il y a les développeurs.
Ils sont tous les autres constructeurs : maçons, plombiers, électriciens, couvreurs…


## « Développeur » semble réducteur

Utiliser un seul mot pour toutes ces spécialités différentes semble réducteur.
Cela renforce le sentiment que les développeurs sont interchangeables, que l'on peut juste remplacer quelqu'un par quelqu'un d'autre.
Comme on peut s'attendre à ce que n'importe quel maçon sache poser des briques, on pourrait croire que n'importe quel développeur est capable d'écrire des lignes de code.

Bien entendu, ce n'est pas aussi simple !
Écrire des lignes de code nécessite une compréhension et du recul.
Bien sûr, tous les développeurs ne sont pas égaux.
Nous avons tous nos spécialités, nos préférences, les sujets que nous maîtrisons davantage.


## Certains développeurs utilisent le mot pour éviter des tâches

Plus dérangeant encore, certains développeurs aiment cette vision simplifiée.
Un développeur écrit du code, point.
Ils ne veulent pas avoir à _réfléchir_, ce devrait être une tâche pour d'autres.

J'ai entendu plus d'une fois des développeurs se plaindre qu'on leur demandait des choses ne relevant pas de leur rôle.
Concevoir une interface, un algorithme, réparer une anomalie inattendue…
C'est pourtant quelque chose qu'on leur demande souvent d'après mon expérience, mais on m'a dit que, dans d'autres cultures[^fn-cultures], tout doit être clairement analysé et détaillé : les développeurs ne font qu'écrire le code.

[^fn-cultures]: J'ai entendu ceci d'amis et collègues qui ont travaillé sur des projets externalisés à l'étranger ou qui se sont eux-mêmes expatriés.
Ils l'ont dit d'au moins trois cultures différentes qui n'ont a priori pas de liens entre elles.

En toute franchise, c'est une vision que je ne comprends pas vraiment.
Lorsque l'on me demande de décrire mon rôle de leader technique, je réponds généralement quelque chose comme « je suis _juste_ un développeur expérimenté ».
Mais comment un développeur expérimenté peut-il devenir un leader s'ils écrivent du code sans jamais y réfléchir ?


## « Développeur » est exclusif

Sur un projet informatique, il y a de nombreuses façons de contribuer.
Écrire le code n'est qu'un aspect.
On peut aussi parler du modèle physique de données (le MPD, alias la structure de votre BdD).
Il n'est pas rare que l'on confie à des analystes la tâche de le définir.

Définir un MPD est une tâche technique, à n'en pas douter.
Elle demande en fait une double casquette, car il faut comprendre les données que l'on manipule et ce que l'on veut en faire pour en créer une représentation efficace, tout en maîtrisant le SGBD[^fn-sgbd] utilisé pour ne pas tomber dans les petits pièges qui lui sont propres.

[^fn-sgbd]: SGBD = Système de Gestion de Base de Données ; oui, c'est loin pour moi aussi, les cours.
C'est le moteur de BdD que vous avez choisi (Oracle, PgSQL, MySQL/MariaDB…), chacun ayant ses petites spécificités et ses astuces à connaître.

On peut choisir un autre exemple.
Si votre projet a une interface utilisateur, elle comportera certainement des éléments graphiques (des images, des icônes…).
Sans eux, votre application ne serait pas la même.
Pourtant, cela fait partie des tâches que les développeurs préfèrent généralement éviter car hors de leur domaine de compétence/confort.

À partir de là, laisser croire que les développeurs sont les seuls impliqués dans la construction du projet semble minimiser la construction d'autres acteurs.


## Une alternative ?

C'est bien gentil de critiquer, mais ai-je mieux à proposer ?
J'ai en tout cas une idée, un tantinet plus générique sans être _trop_ générique.

Au fil des ans, j'ai testé plusieurs dénominations pour ma bio sur les réseaux sociaux (toujours en anglais nonobstant) : _coding addict_, _programming artist_[^fn-borrowed]…
J'ai finalement souvent peur de paraître pédant, hautain.

J'ai vu des designers UX se qualifier de « créateurs d'expérience » et ça m'a inspiré : peu importe notre spécialité, nous faisons plus qu'écrire du code, **nous créons du logiciel**.
C'est pourquoi j'essaie de remplacer « développeur » et « développement » par « [créateurice/création logiciel(le)][soft-creation][^fn-inclusive] ».

[^fn-inclusive]: Ne vous offensez pas de la version inclusive de « créateur » ici, les femmes ont beaucoup à nous apporter.
Je ferai prochainement un billet sur l'écriture inclusive.

Cette terminologie est plus large, elle inclut plus de compétences que simplement « écrire du code » (les « créateurs d'expérience » peuvent être des créateurs logiciels également).
C'est une tentative de donner plus d'espace aux personnes travaillant dans notre domaine pour qu'ils puissent se développer.

Quel est votre avis sur la question ?


[^fn-borrowed]: This one, I borrowed from [Arkanosis](https://github.com/arkanosis).

[metaphor]: {{< relref path="/blog/2021/02/08-metaphor-development" >}}
[metaphor-story]: {{< relref path="/stories/2019/11/29-wc-metaphore" >}}
[soft-creation]: {{< relref path="/categories/software-creation" >}}
