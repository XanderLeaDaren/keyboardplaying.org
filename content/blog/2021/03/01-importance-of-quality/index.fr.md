---
date: 2021-03-01T07:00:00+01:00
title: De l'importance de maîtriser sa dette technique
subtitle: Ou la modération en toute chose d'Aristote
slug: importance-maitriser-dette-technique
description: |-
  Ne pas négliger la qualité, mais ne pas trop en faire non plus.
  Il y a un compromis à trouver, souvent perdu entre la pression des chefs de projet et la vision artistique des leaders techniques.
author: chop
categories: [ software-creation ]
tags: [ management ]
keywords: [ qualité, gestion de projet, dette technique, maison, ux, dx ]
---

Au cours de ma pas si longue carrière, je me suis souvent retrouvé face à un de mes directeurs sur un sujet : la qualité.
Nous sommes tous deux d'avis que c'est nécessaire pour nos projets, mais avons bizarrement une perception différente du seuil à viser.
Quelques réflexions.

<!--more-->

[^fn-love]: Ça ne m'empêche pas de l'adorer, mais ce n'est pas le sujet.

## Parlons un peu de mon cas

Je pense que je ne suis pas le seul, mais je travaille généralement en plusieurs étapes, par itérations, d'une façon assez proche de celle que recommandent les approches Agiles.
Si on découpe très grossièrement ma façon de faire, on obtient deux étapes :
1. Faire un truc qui marche.
2. Améliorer ce truc et en faire une solution qui est « mieux ».

« Mieux » est toujours subjectif.
Mon directeur me connaissait depuis un moment et je pense qu'il croit que, pour moi, ça signifie « plus joli, plus abstrait… »
C'est dans cet esprit qu'il me dit toujours : « Le mieux est l'ennemi du bien, » signifiant par là que ce sera un coût et que je risque d'empirer les choses plutôt que de les améliorer pour aucune valeur perçue par l'utilisateur.

<aside><p>Améliorer pour moi aujourd’hui, c’est m’assurer de la maintenabilité future.</p></aside>

Je ne peux pas le lui reprocher : plus jeune, j'ai pu lui donner des raisons de penser ça.
Aujourd'hui cependant, quand j'arrive à l'étape d'amélioration, je veux avant tout m'assurer que le code sera maintenable dans le futur, ce qui implique des tâches que tout codeur est réputé détester :
- vérifier que le code est correct, nettoyer le code obsolète issu des essais successifs ;
- correctement documenter et commenter tout ce qui pourrait ne pas être évident en première lecture[^fn-doc] ;
- tester tout ce qui doit l'être, avec commentaires et documentation selon le besoin pour les tests également.
Ce dernier point implique un focus sur les spécifications fonctionnelles et non fonctionnelles[^fn-nfr].

Ça pourrait être du bon sens.
Alors, d'où vient notre divergence d'opinions ?

[^fn-doc]: J'ai été de ceux qui pensent que le code est sa propre documentation, mais je sais à présent que quelques commentaires peuvent faire gagner beaucoup de temps à la compréhension d'un algorithme complexe, voire éviter des non-sens.
[^fn-nfr]: Les spécifications non fonctionnelles sont les critères qui permettent de juger du bon fonctionnement du système plutôt que de son bon comportement.
Par exemple, « doit répondre en moins de 500 ms » est une spécification non fonctionnelle.


## Perspectives managériale et technique

Quand vous travaillez dans une ESN[^fn-esn], vous vendez un projet avant qu'il ne commence à un prix que vous estimez.
Tout le jeu consiste à trouver le juste prix, pas trop cher pour que le client vous préfère à la concurrence, pas trop bas parce que la réalisation du projet vous coûte des sous.
Une fois qu'il a été vendu, la deuxième partie du jeu est de s'assurer que les coûts sont inférieurs au prix de vente, ce qui vous permet de dégager quelques bénéfices.

<aside><p>Le chef de projet doit penser aux clients et utilisateurs <em>maintenant</em>, le réalisant doit également penser à ses pairs à venir.</p></aside>

Alors quand un jeune soi-disant expert technique vous dit que la solution fonctionne mais qu'il doit encore travailler dessus, vous avez tendance à penser « Enfer, je pourrais livrer ça et avoir un client content, mais je vais perdre encore un peu d'argent à cause d'un esthète. Si ça fonctionne, arrêtons-nous là ! »

En face, si vous êtes comme moi (le moi d'aujourd'hui, un peu plus posé que le jeune je-sais-tout de l'époque), vous ne pensez pas que ce travail additionnel soit du temps perdu, mais simplement quelque chose que le client attend.
S'assurer que le code est maintenable n'est pas un coût, c'est un investissement.

Imaginons que ce même client revienne quelques années plus tard avec des demandes d'évolution.
Si l'équipe est allée trop vite la première fois, ces changements seront coûteux.
Un développeur mécontent pourrait même dire que les créateurs du truc ont travaillé comme des ***** (je vous laisse choisir le qualificatif utilisé).
Bonjour le ressenti pour le client !
Il a dépensé sans compter et tout ce qu'il en a tiré est ce code pourri.
Le bouche-à-oreille pourrait rapidement abîmer votre image de marque.

Pour résumer, mon directeur se concentre sur la livraison d'un produit conforme en temps et en heure, tandis que je pense également à la maintenance, le débogage, les évolutions qui devront être faits dessus.
En d'autres termes, il pense uniquement à l'expérience utilisateur maintenant tandis que je pense à l'expérience utilisateur maintenant et à l'expérience développeur dans le futur.

[^fn-esn]: Entreprise de services du numérique, une société à laquelle d'autres sociétés font appel pour les aider dans la réalisation de leurs projets informatiques.


## La notion de dette technique

Qu'est-ce que la dette technique ?
C'est ce que vous avez refusé de payer à un instant _t_ et qui revient vous hanter par la suite.
Vous _saviez_ qu'il fallait s'en occuper, mais vous aviez d'autres priorités donc vous l'avez laissé à ce moment.

<aside><p>La dette technique est une économie ponctuelle qui se paye avec intérêts.</p></aside>

Les intérêts se sont cumulés : tandis que le temps passait, vous avez ajouté du code et des fonctionnalités par-dessus cette base incorrecte.
Puis, un jour, vous décidez qu'il est temps de corriger ça, soit parce que vous aviez acté que ce serait fait, soit parce que vous _devez_ le faire pour pouvoir faire votre prochaine tâche.
Plus vous avez attendu, plus vous aurez de travail : naturellement, le point de base est toujours là, mais tout ce qui a été construit en l'utilisant comme postulat est potentiellement impacté[^fn-tests].

Voilà de quoi il s'agit quand on parler de maîtriser sa dette technique : s'assurer que, si je dois faire évoluer mon logiciel dans le futur, je pourrai le faire sereinement sans préalablement devoir corriger une grande liste de tickets abandonnés.

Et ce rapide billet n'aborde même pas le coût de suivi et correction des dizaines de bogues que peut entraîner un problème de dette technique.

[^fn-tests]: Si vous êtes dans ce cas, j'espère que vous avez bien mis en place vos tests unitaires.
Cela vous aidera à vous concentrer sur votre travail sans vous inquiéter de potentielles régressions.


## N'allez pas trop loin

<aside><p>Arrêtez-vous à ce qu'il est pertinent de faire, mais faites-le.</p></aside>

La surqualité et la suringénierie sont des réalités.
C'était certainement celles qui inquiétaient mon directeur.

Pour les chefs de projet, je vous recommande d'échanger avec votre équipe pour voir s'il y a une vraie dette technique ou s'ils veulent juste aller plus loin parce que ce serait « mieux ».

Pour les techniciens, gardez en mémoire que vous ne travaillez pas pour la beauté du code.
Vous le faites pour atteindre un but, offrir une fonctionnalité.
Ne créez pas une solution ultra-générique pour gérer un cas unique, ne mettez pas en place un gestionnaire d'add-ons pour un process qui n'en aura jamais besoin…
Ne construisez pas une centrale à charbon pour allumer une ampoule.
Ce sont d'autres formes de dette technique : un code bien si complexe que celles et ceux qui devront le maintenir verseront toujours une petite larme et ne seront jamais à l'aise.

En revanche, si vous avez besoin de faire quelque chose parce que c'est un risque pour le futur, dites-le simplement au chef de projet[^fn-politician].

[^fn-politician]: À de rares occasions, ils peuvent être plus politiciens que chefs de projet.
Déployer de nouvelles fonctionnalités peut leur sembler plus important que de se protéger de risques futurs.
Par le passé, j'ai prévenu le chef de produit de trois risques importants liés à de la dette technique.
Il m'a répondu : « Tu as raison, mais il nous faut des fonctionnalités pour l'instant. »
Moins d'un moins plus tard, un incident critique est survenu, lié à un des risques que j'avais soulignés.
J'ai été fortement tenté de tout laisser brûler.
Heureusement, j'ai pensé en priorité à mes équipiers et aux utilisateurs embêtés.


## Petite illustration

Avant de nous séparer, je voulais revenir à [mon image habituelle][image] : vous construisez une maison.
Vous voyez qu'un tuyau n'est pas comme il devrait, mais vous voulez aller vite et ce sera « suffisant » pour l'instant : l'eau va quand même là où elle devrait.
Puisque c'est « suffisant », vous faites comme si c'était terminé : vous fermez le mur et vous carrelez.

Un jour, vous voudrez ajouter un autre évier, ou simplement répare le tuyau.
Il faut d'abord retirer le carrelage et le plâtre, puis s'occuper du tuyau, avant de tout remettre.
Mais peut-être que modifier ce qui est dans le mur vous oblige à changer le mur lui-même, ce qui peut avoir un impact sur la charpente…

Et on ne parle encore une fois pas des diverses fuites (bogues) que cette non-conformité a générées par sa simple présence et qu'on a épongées du mieux qu'on pouvait en sachant que le tuyau devait être un jour remplacé.

On pourrait en dire bien plus sur la qualité et la dette technique, mais ce billet devait rester court.
Si le sujet vous intéresse, nous pourrons essayer d'aller plus loin à l'avenir.
Le message ici est le suivant : retarder la qualité ou nier son besoin revient à accumuler de la dette technique et ses intérêts, que quelqu'un aura un jour à payer.

{{< figure src="/img/vincentdnl/technical-debt.png"
  link="https://vincentdnl.com/drawings/technical-debt"
  alt="Un chef de projet insatisfait se plaint qu'il est long d'ajouter une nouvelle fenêtre à une maison bancale."
  caption="_« Je ne comprends pas pourquoi c’est si long d’ajouter une nouvelle fenêtre. »_"
  attr="Vincent Déniel (CC BY-NC 4.0)"
  attrlink="https://twitter.com/vincentdnl" >}}



[image]: {{< relref path="/blog/2021/02/08-metaphor-development" >}}
