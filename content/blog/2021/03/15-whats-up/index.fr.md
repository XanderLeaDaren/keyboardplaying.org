---
date: 2021-03-15T07:00:00+01:00
title: Quoi d'neuf ? (mars 2021)
#subtitle: A witty line as a subtitle
slug: quoi-d-neuf
description: |-
    La page d'accueil est un tantinet plus utile, trouver des contenus en lien avec la page courante est plus simple, et une version Esperanto du site pourrait faire son apparition avant la fin de l'année.
author: chop
categories: [ kp.org ]
tags: [ news ]
keywords: [ blogue, histoires, contenu lié, esperanto, download proxy ]
---

Deux mois se sont écoulés depuis le dernier « Quoi d'neuf ? »
Il est temps de voir les principaux changements depuis lors.

<!--more-->

## Ce qui a changé

### Amélioration de la page d'accueil

Cela faisait un bon moment que je souhaitais améliorer la page d'accueil.
Les liens vers les différentes entrées du menu ne suffisaient pas à se faire une idée du type de contenu derrière.
J'ai donc modifié tout ça pour afficher le dernier billet du blogue, la dernière histoire et le projet mis à jour le plus récemment[^fn-tool] sous les liens qui existaient déjà.

[^fn-tool]: Pour l'instant, cette catégorie ne contient que le Download Proxy, mais j'espère avoir le temps d'en ajouter à l'avenir.


### Contenu lié

Lorsque je lis un article, j'aime bien savoir que d'autres sont disponibles qui pourraient m'aider à avoir une vision plus complète du sujet.
Il y avait déjà les mots-clés et les catégories, mais ces approches n'étaient pas très ciblées.

J'ai donc fait appel [au mécanisme d'Hugo pour les contenus liés][hugo-related] et l'ai configuré pour utiliser deux sortes de mots-clés : les _tags_ que vous voyez et les _keywords_ qui ne sont pas affichés.
Cela a nécessité de repasser sur l'ensemble de mes billets pour _ajouter_ ces _keywords_, que je n'avais pas placés initialement.

Les résultats sont parfois surprenants, comme un billet que vous attendez pas qui n'aapparaît pas, ou un qui n'a a priori pas grand-chose à voir mais qui figure dans le top 2.
Toutefois, dans l'ensemble, on obtient des liens vers d'autres pages du site potentiellement intéressante pour vous.
Still, on the whole, it provides links to potentially interesting pages on the website.


## Et ensuite ?

### Une version du site en Espéranto ?

J'ai finalement commencé l'apprentissage de l'Espéranto le mois dernier.
J'aime la philosophie derrière cette langue et j'envisage de publier une version de ce site en Espéranto.
Traduire l'existant prendrait du temps, mais ce serait également un excellent moyen de progresser et d'apprendre le vocabulaire qui me servirait le plus pour mon travail.
Un ami [me suggère déjà des ressources utiles][tw-eo-rsc].
Encore un peu d'exercice et je pense que je pourrai démarrer la pratique.

Keyboard Playing en Espéranto[^fn-kp-eo], ça vous botte ?
Quels billets ou quelles histoires souhaiteriez-vous voir traduites en premier ?

[^fn-kp-eo]: Je suppose que ça donnerait _Ludi kun klavaroj_, mais le site conserverait son nom _Keyboard Playing_, comme pour la version française.

[hugo-related]: https://gohugo.io/content-management/related/
[tw-eo-rsc]: https://twitter.com/XanderLeaDaren/status/1365177830334738435
