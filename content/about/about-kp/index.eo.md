---
title: Pri ĉi tiu retejo
slug: pri-keyboard-playing
description: |-
    Keyboard Playing estas persona retejo, pensita kiel loko, kie pluraj kontribuantoj povas dividi.
    Ĉi tiu paĝo ofertas rigardon al gia historio kaj ideoj por subteni nin.
scripts: [ lazy ]
---

## Kiu kris ĉi tiu retejo?

Estas mi.
Mi estas {{< author "chop" >}}.
Mi estas ~~programisto~~ programokerinto.
Laŭ multaj homoj en ĉi tiu kampo, mi emis havi loko, kie eksperimenti kaj dividi miaj rezultoj.

Mi havis blogojn en la pasinteco, sed neniam kun mia propra domajno.
Ĝi änĝis la tagon, kiam mi estis komenconta mision kun Bootstrap.
Mi decidis konstrui mian propran retejon por lerni ĝin, kaj poste publikigis ĝin.
Tiam mi registris mian domajnan nomon.


## Kio estas tiu nomo?

"_Keyboard Playing_" povas ŝajni abstrakta.
Programistoj ofte elektas domajnon bazitan sur sia nomo aŭ pseŭdo.
Mi kreis ion, sed mi esperis, ke aliaj kontribuos.

Mi ankaŭ ne volas fari ion ekskluzive pri programoj.
Mi ne estas artisto, sed mi ĉiam amis muzikon kaj mi esperis verki delonge.
Kaj mi jam ŝatis klavarojn.

Muziko, programado, verkado…
Pianoj, komputiloj, tajpiloj.
Jen!
Ni ĉiuj ludas niajn klavarojn laŭ nia maniero.



## Subteno estas bonvena {#support}

### Mono?

Ĉi tiu retejo ne estis farita por enspezi monon.
Mi ne vendas viajn datumojn ([mi ne kolektas ilin]({{< relref path="/about/privacy-policy" >}})).
Estas neniuj anoncoj nek spuroj.
<!--Estas neniuj anoncoj [nek spuroj](< relref path="/blog/2021/02/01-better-social-share-buttons" >).-->

Estas tempo, ke mi dividas, volonte kaj libere.
Vi povas doni monon se vi volas.
Ĉi tiu helpas pagi la gastigon.


C'est du temps que je vous donne, gratuitement et avec plaisir.
Vous pouvez laisser un pourboire si vous le souhaitez.
Cela contribuera au paiement des frais de fonctionnement.
[Alklaku ĉi tie][paypalme] se vi interesiĝas.
La sumo estas, kion vi volas.

Se vi volas akiri ion helpante nin, vi ankaŭ povas [aĉeti nin libro][carrefour-bookelis] (nur en la franca).
Ĉiuj profitoj estos reinvestitaj en estontaj projektoj de  [la Komunumo de la Plumo][tw-commu].

{{< figure src="/img/carrefour/cover-v2-small.jpg" link="/img/carrefour/cover-v2.jpg" title="La kovrilo de la libro" >}}


### Aliaj manieroj subteni

Estas pli efikaj manieroj subteni nin.

Ĉu vi ŝatas ion, kion ni dividis?
Simple scii, kion ni faris, estas utila, _tio_ estas vera intigo.

Ne hezitu reagi aŭ komenti.
Diru al ni kiel pliboniĝi.
Ajna konstrua reago estas pozitiva.

Perbuŝo ankaŭ estas bonvena: se io ŝajnas interesa, bonvolu dividi ĝin.
Aŭ [simple sendu mesaĝon al ni][tw-kp].

Dankon, bonan tempon en la retejo.

[paypalme]: {{< param "tipLink" >}}
[carrefour-bookelis]: https://www.bookelis.com/romans/44365-Carrefour-des-imaginaires.html
[tw-commu]: https://twitter.com/commudustylo
[tw-kp]: https://twitter.com/KeyboardPlaying
