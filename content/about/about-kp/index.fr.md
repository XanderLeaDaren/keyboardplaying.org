---
title: À propos du site
slug: a-propos-keyboard-playing
description: |-
    Keyboard Playing est un site personnel, imaginé comme un endroit où plusieurs contributeurs pourraient partager.
    Cette page vous offre un aperçu de son histoire et des idées pour nous soutenir.
scripts: [ lazy ]
---

## Qui a créé ce site ?

C'est moi.
Moi, {{< author "chop" >}}.
Je suis un ~~développeur~~ créateur logiciel.
Comme beaucoup de personnes dans ce domaine, j'ai eu envie d'un endroit où expérimenter et partager mes résultats.

J'ai eu des blogues par le passé, mais jamais sur un domaine à moi.
Ça a changé le jour où on m'a prévenu que je commençais une mission avec Bootstrap.
J'ai décidé de construire mon propre site pour apprendre et de le publier.
C'est ainsi que j'ai fait enregistrer mon domaine.



## C'est quoi, ce nom ?

« _Keyboard Playing_ » peut sembler abstrait.
Les développeurs choisissent souvent un domaine basé sur leur nom ou pseudo.
Je créais quelque chose, mais en espérant que d'autres contribueraient.

Je ne voulais pas non plus faire quelque chose qui soit exclusivement tourné vers le logiciel.
Je ne suis pas un artiste moi-même, mais j'ai toujours adoré la musique et j'espérais écrire depuis longtemps déjà.
Et j'avais déjà une certaine obsession des claviers.

Musique, développement logiciel, écriture…
Pianos, ordinateurs, machines à écrire.
Et voilà !
Nous jouons tous de nos claviers à notre manière.



## Le soutien est bienvenu {#support}

### De l'argent ?

Ce site n'est pas là pour gagner de l'argent.
Je ne vends pas vos données ([je ne les collecte pas]({{< relref path="/about/privacy-policy" >}})).
Il n'y a pas de publicité [ni de traceurs]({{< relref path="/blog/2021/02/01-better-social-share-buttons" >}}).

C'est du temps que je vous donne, gratuitement et avec plaisir.
Vous pouvez laisser un pourboire si vous le souhaitez.
Cela contribuera au paiement des frais de fonctionnement.
[Cliquez ici][paypalme] si cela vous intéresse.
Le montant est libre.

Si vous préférez acquérir quelque chose tout en nous aidant, vous pouvez aussi [acheter notre livre][carrefour-bookelis].
Tous les bénéfices seront réinvestis dans des projets futurs de [la Communauté du Stylo][tw-commu].

{{< figure src="/img/carrefour/cover-v2-small.jpg" link="/img/carrefour/cover-v2.jpg" title="La couverture du livre" >}}


### D'autres formes de soutien

Il y a des formes de soutien plus efficaces que l'argent.

Est-ce que vous avez aimé quelque chose que nous avons partagé ?
Le simple fait de savoir que nos productions sont appréciées ou utiles, _ça_, c'est une vraie motivation.

N'hésitez pas à réagir, commenter.
Dites-nous comment nous améliorer.
Toute forme de retour constructif est positive.

La publicité est bienvenue également : si ça vous semble digne d'intérêt, n'hésitez pas à le partager.
Ou [envoyez-nous juste un message][tw-kp].

Merci, passez un bon moment sur le site.


[paypalme]: {{< param "tipLink" >}}
[carrefour-bookelis]: https://www.bookelis.com/romans/44365-Carrefour-des-imaginaires.html
[tw-commu]: https://twitter.com/commudustylo
[tw-kp]: https://twitter.com/KeyboardPlaying
