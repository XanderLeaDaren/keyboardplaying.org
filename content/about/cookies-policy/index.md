---
title: Cookies Policy
slug: cookies-policy
description: |-
  This Cookies Policy explains what cookies are and how we use them.
  You should read this policy so you can understand what type of cookies we use, or the information we collect using cookies and how that information is used.
cover:
  src: mae-mu-kID9sxbJ3BQ-unsplash.jpg
  alt: Brown cookies in a round bowl.
  by: Mae Mu
  link: https://unsplash.com/photos/kID9sxbJ3BQ
  authorLink: https://unsplash.com/@picoftasty
  license: Unsplash
---

This Cookies Policy explains how Keyboard Playing ("**we**," "**us**," or "**our**") uses cookies to recognize you when you visit our websites at https://keyboardplaying.org ("**Websites**").
It explains what these technologies are and why we use them, as well as your rights to control our use of them.


## Summary for Humans

Cookies are small data files that are placed on your device when you visit a website.
We don't use them, but our CDN provider, [Cloudflare], does, for security purpose.

Since this is an essential cookie for the correct operation of our Website, there is no Cookie Consent Manager.


## What are cookies?

Cookies are small data files that are placed on your computer or mobile device when you visit a website.
Cookies are widely used by website owners in order to make their websites work, or to work more efficiently, as well as to provide reporting information.

Cookies set by the website owner (in this case, Keyboard Playing) are called "first-party cookies."
Cookies set by parties other than the website owner are called "third-party cookies."
Third-party cookies enable third-party features or functionality to be provided on or through the website (e.g. advertising, interactive content and analytics).
The parties that set these third-party cookies can recognize your computer both when it visits the website in question and also when it visits certain other websites.


## Why do we use cookies?

We use third-party cookies for several reasons.
Some cookies are required for technical reasons in order for our Websites to operate, and we refer to these as "essential" or "strictly necessary" cookies.
Third parties serve cookies through our Websites for security purposes.
This is described in more detail below.

The specific types of first- and third-party cookies served through our Websites and the purposes they perform are described below (please note that the specific cookies served may vary depending on the specific Online Properties you visit).


## How can you control cookies?

Essential cookies cannot be rejected as they are strictly necessary to provide you with services.
Since we use only essential cookies, your only means of rejecting them is to avoid using our Websites completely.

You may also set or amend your web browser controls to accept or refuse cookies.
If you choose to do so, you may still use our Websites though your access to some functionality and areas of our Websites may be restricted.
As the means by which you can refuse cookies through your web browser controls vary from browser to browser, you should visit your browser's help menu for more information.

The specific types of first- and third-party cookies served through our Websites and the purposes they perform are described in the table below (please note that the specific cookies served may vary depending on the specific Online Properties you visit).


## Essential website cookies

These cookies are strictly necessary to provide you with services available through our Websites and to use some of its features, such as access to secure areas.

Name
: __cfduid

Purpose
: Used by our CDN [Cloudflare] to identify individual clients behind a shared IP address, and apply security settings on per-client basis.

Provider
: .keyboardplaying.org

Service
: Cloudflare ([view Service privacy policy](https://www.cloudflare.com/privacypolicy/))

Country
: United States

Type
: http_cookie

Expires in
: 1 month


## Do we serve targeted advertising?

We do not serve any advertising and don't share your browsing information with any third party.


## How often will we update this Cookie Policy?

We may update this Cookie Policy from time to time in order to reflect, for example, changes to the cookies we use or for other operational, legal or regulatory reasons.
Please therefore revisit this Cookie Policy regularly to stay informed about our use of cookies and related technologies.


## Where can you get further information?

If you have any questions about our use of cookies or other technologies, please email us at
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.

[Cloudflare]: https://cloudflare.com
