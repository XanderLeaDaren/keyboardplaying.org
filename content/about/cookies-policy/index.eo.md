---
title: Kuketa politiko
slug: kuketa-politiko
description: |-
  Ĉi tiu kuketa politiko klarigas kio kuketoj estas kaj kiel ni uzas ilin.
  Vi devus legi ĉi tiu politiko por ke vi komprenu la informojn, kiujn ni kolektas per kuketoj, kaj kiel tiuj informoj estas uzataj.
cover:
  src: mae-mu-kID9sxbJ3BQ-unsplash.jpg
  alt: Brown cookies in a round bowl.
  by: Mae Mu
  link: https://unsplash.com/photos/kID9sxbJ3BQ
  authorLink: https://unsplash.com/@picoftasty
  license: Unsplash
---

Ĉi tiu kuketa politiko klarigas kiel Keyboard Playing ("**ni**", "**nin**" aŭ "**nia**") uzas kuketojn por rekoni vin kiam vi vizitas niajn retejojn ĉe https://eo.keyboardplaying.org ("**Retejoj**").
Ĝi klarigas, kiajn kiel viajn rajtojn regi nian uzadon de ilin.


## Resumo por homoj

Kuketoj estas malgrandaj datumaj dosieroj, kiuj estas metitaj sur vian aparaton kiam vi vizitas retejon.
Ni ne uzas ilin, sed nia CDN-provizanto, [Cloudflare], faras ĝin por sekureca celo.

Ĉar ĉi tiu estas esenca kuketo por la ĝusta funkciado de nia retejo, ne ekzistas Kuketo-Konsenta Administranto.


## Kio estas kuketoj?

Kuketoj estas malgrandaj datumoj dosieroj, kiuj estas metitaj sur vian komputilon aŭ poŝtelefonon kiam vi vizitas retejon.
Kuketoj estas vaste uzataj de retejposedantoj por funkciigi siajn retejojn, aŭ por funkcii pli efike, kaj ankaŭ por doni raportajn informojn.

Kuketoj definitaj de la posedanto de la retejo (ĉi-kaze Keyboard Playing) nomiĝas "unuaj partiaj kuketoj."
Kuketoj definitaj de aliaj krom la posedanto de la retejo nomiĝas "triaj partiaj kuketoj."
Triaj partiaj kuketoj ebligas triaj partiaj trajtoj aŭ funkcij esti provizataj sur aŭ tra la retejo (ekz. reklamado, interaga enhavo kaj analizado).
La partioj, kiuj agordas ĉi tiujn triajn partiajn kuketojn, povas rekoni vian komputilon kiam ĝi vizitas la koncernan retejon kaj ankaŭ kiam ĝi vizitas iujn aliajn retejojn.


## Kial ni uzas kuketojn?

Ni uzas triajn partiajn kuketojn pro pluraj kialoj.
Iuj kuketoj necesas pro teknikaj kialoj por ke niaj retejoj funkciu, kaj ni nomas ĉi tiujn "esencaj" aŭ "strikte necesaj" kuketoj.
Triaj partiaj servas kuketojn per niaj Retejoj por sekurecaj celoj.
Ĉi tio priskribita pli detale sube.

La specifaj specoj de unuaj kaj triaj partiaj kuketoj servitaj per niaj Retejoj kaj la celoj, kiujn ili plenumas, estas priskribitaj sube (bonvolu noti, ke la specifaj kuketoj servitaj povas varii laŭ la specifaj Interrataj Propaĵoj, kiujn vi vizitas).


## Kiel vi povas regi kuketojn?

Esencaj kuketoj ne povas esti malakceptitaj, ĉar ili estas strikte necesaj pro provizi servojn al vi.
Ĉar ni uzas nur esencajn kuketojn, via sola rimedo por rifuzi ilin estas eviti uzi niajn Retejojn tute.

Vi ankaŭ povas agordi aŭ modifi viajn retumilajn kontrolojn por akcepti aŭ rifuzi kuketojn.
Se vi elektas fari tion, vi ankoraŭ povas uzi niajn Retejojn  kvankam via aliro al iuj funkcioj kaj areoj de niaj Retejoj eble estos limigita.
Ĉar la rimedoj per kiuj vi povas rifuzi kuketojn per viaj retumilaj kontroloj varias de retumilo al retumilo, vi devas viziti la helpmenuon de via retumilo por pliaj informoj.

La specifaj specoj de unuaj kaj triaj partiaj kuketoj seritaj per niaj Retejoj kaj la celoj, kiujn ili plenumas, estas priskribitaj en la sub tabelo (bonvolu noti, ke la specifaj kuketoj servitaj povas varii laŭ la specifaj Interrataj Propraĵoj, kiujn vi vizitas).


## Esencaj retejaj kuketoj

Ĉi tiuj kuketoj estas strikte necesaj por provizi al vi servojn disponeblajn per niaj retejoj kaj por uzi iujn el ĝiaj trajtoj, kiel aliron al sekuraj areoj.

Nomo
: __cfduid

Celo
: Uzata de nia CDN [Cloudflare] por identigi individuajn klientojn malantaŭ komuna IP-adreso, kaj apliki sekurecajn agordojn laŭ klienta bazo.

Provizanto
: .keyboardplaying.org

Servo
: Cloudflare ([vidi privatecan politikon](https://www.cloudflare.com/privacypolicy/))

Lando
: Usono

Tajpo
: http_cookie

Eksvalidiĝas en
: 1 monato


## Ĉu ni servas celitan reklamadon?

Ni ne servas reklamadon kaj ne dividas viajn foliumajn informojn kun iu ajn triaj partiaj.


## Kiom ofte ni ĝisdatigos ĉi tiun kuketan politikon?

Ni eble ĝisdatigos ĉi tiun kuketan politiko de tempo al tempo, por reflekti, ekzemple, ŝanĝojn al la kuketoj, kiujn ni uzas aŭ pro aliaj funkciaj, juraj aŭ reguligaj kialoj.
Bonvolu do revidi ĉi tiun kuketan politikon regule por resti informita pri nia uzo de kuketoj kaj rilataj teknologioj.


## Kie vi povas akiri pliajn informojn?

Se vi havas demandojn pri nia uzo de kuketoj aŭ aliaj teknologioj, bonvolu retpoŝti nin al
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.

[Cloudflare]: https://cloudflare.com
