---
title: Privacy Policy
slug: privacy-policy
description: |-
  This Privacy Policy describes our policies and procedures about the collection, use and disclosure of your information when you use the service, and tells you about your privacy rights and how the law protects you.
cover:
  src: matthew-henry-fPxOowbR6ls-unsplash.jpg
  alt: Two women look security cameras.
  by: Matthew Henry
  link: https://unsplash.com/photos/fPxOowbR6ls
  authorLink: https://unsplash.com/@matthewhenry
  license: Unsplash
---

Thank you for choosing to be part of our community at Keyboard Playing ("**we**," "**us**," or "**our**").
We are committed to protecting your personal information and your right to privacy.
If you have any questions or concerns about our policy or our practices with regard to your personal information, please contact us at
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.

When you visit our website https://keyboardplaying.org, and use our services, you trust us with your personal information.
We take your privacy very seriously.
In this privacy policy, we seek to explain to you in the clearest way possible what information we collect, how we use it and what rights you have in relation to it.
We hope you take some time to read through it carefully, as it is important.
If there are any terms in this privacy policy that you do not agree with, please discontinue use of our Sites and our services.

This privacy policy applies to all information collected through our website (such as https://keyboardplaying.org), and/or any related services or events (we refer to them collectively in this privacy policy as the "**Services**").

**Please read this privacy policy carefully as it will help you make informed decisions about sharing your personal information with us.**


## Summary for Humans

### What information do we collect, and to what purpose?

#### Comments and publicly available information

When submitting a comment on Keyboard Playing, you voluntarily provide the following information:

- **Your first name, last name and/or nickname**.
This will be used to publicly identify you as the author of this comment.

- **Your email address**.
This address will be used to find your [Gravatar](https://gravatar.com) if you have one.
It will be stored as an irreversible hash, meaning that no one---including us---will be capable of reading it.

This data will only be used to display publicly your comment on the website.
It will be retained as long as your comment will be available for display, but can be erased upon request.


#### Direct contact

When you email us, you send data including your email address and probably your name.
That information is used only to reply to your inquiry and will be preserved at most 90 days past this reply.


#### Derivative information

When you use, visit or navigate the services, we automatically collect data for security and operation of our services.
This data is not sufficient to identify you and will not be cross-referenced for any other purpose than solving an incident.
They will be retained for 15 days and then anonymized for analytics purpose.

The data we collect in this category are limited to:

- **Your IP (Internet Protocol) address**.
The anonymized version we keep for analytics is gives a hint about your general location.

- **Characteristics about the browser you used**.
We will store the "user agent," which describes the family and version of the browser you use to visit the website.

- **Referrer URL**.
If you arrived on our website through a link on another site, it may pass information known as the "Referrer URL".
This is useful to understand how people discover our website.


### Will your information be shared with anyone?

No, except with your consent or to comply with laws.


### How can you see, modify or delete the data we have about you?

If you have any question or request about your data, please email us at
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.



## What information do we collect?

### Personal information you disclose to us

_**In Short**: We collect personal information that you provide to us._

We collect personal information that you voluntarily provide to us when participating in activities on the Services or otherwise contacting us.

The personal information that we collect depends on the context of your interactions with us and the Services and the products and features you use.
The personal information we collect can include the following:

**Publicly Available Personal Information**.
We collect first name, last name and nickname.

All personal information that you provide to us must be true and accurate.


### Information automatically collected

_**In Short**: Some information---such as IP address and/or browser characteristics---is collected automatically when you visit our Services._

We automatically collect certain information when you visit, use or navigate the Services.
This information does not reveal your specific identity (like your name or contact information) but may include device and usage information, such as your IP address, browser and device characteristics, operating system, language preferences, referring URLs, device name, country, location, information about how and when you use our Services and other technical information.
This information is primarily needed to maintain the security and operation of our Services, and for our internal analytics and reporting purposes.

**Online Identifiers**.
We collect IP (Internet Protocol) addresses, applications and referring URLs.


## Will your information be shared with anyone?

_**In Short**: We only share information with your consent, to comply with laws, to provide you with services, or to protect your rights._

We may process or share data based on the following legal basis:

- **Consent**:
We may process your data if you have given us specific consent to use your personal information in a specific purpose.

- **Legal Obligations**:
We may disclose your information where we are legally required to do so in order to comply with applicable law, governmental requests, a judicial proceeding, court order, or legal process, such as in response to a court order or a subpoena (including in response to public authorities to meet national security or law enforcement requirements).

- **Vital Interests**:
We may disclose your information where we believe it is necessary to investigate, prevent, or take action regarding potential violations of our policies, suspected fraud, situations involving potential threats to the safety of any person and illegal activities, or as evidence in litigation in which we are involved.


## Who will your information be shared with?

_**In Short**: We don't knowingly share information with any third parties._

We don't share or disclose your information to any third parties.
If we have processed your data based on your consent and you wish to revoke your consent, please contact us.


## Do we use cookies and other tracking technologies?

_**In Short**: We may use cookies to collect and store your information._

We may use cookies and similar tracking technologies to access or store information.
Specific information about how we use such technologies and how you can refuse certain cookies is set out in our Cookie Policy: [{{< ref path="/about/cookies-policy" >}}]({{< relref path="/about/cookies-policy" >}}).


## How long do we keep your information?

_**In Short**: We keep your information for as long as necessary to fulfill the purposes outlined in this privacy policy unless otherwise required by law._

We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy policy, unless a longer retention period is required by law (such as tax, accounting or other legal requirements).
No purpose in this policy will require us keeping your personal information for longer than 90 days.

When we have no ongoing legitimate need to process your personal information, we will either delete or anonymize it.


## How do we keep your information safe?

_**In Short**: We aim to protect your personal information through a system of organizational and technical security measures._

We have implemented appropriate technical and organizational security measures designed to protect the security of any personal information we process.
However, please also remember that we cannot guarantee that the internet itself is 100% secure.
Although we will do our best to protect your personal information, transmission of personal information to and from our Services is at your own risk.
You should only access the services within a secure environment.


## Do we collect information from minors?

_**In Short**: We do not knowingly collect data from or market to children under 18 years of age._

We do not knowingly solicit data from or market to children under 18 years of age.
By using the Services, you represent that you are at least 18 or that you are the parent or guardian of such a minor and consent to such minor dependent’s use of the Services. 
If we learn that personal information from users less than 18 years of age has been collected, we will take reasonable measures to promptly delete such data from our records.
If you become aware of any data we have collected from children under age 18, please contact us at
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.


## What are your privacy rights?

_**In Short**: In some regions, such as the European Economic Area, you have rights that allow you greater access to and control over your personal information. You may review, change, or delete your information at any time._

In some regions (like the European Economic Area), you have certain rights under applicable data protection laws.
These may include the right
(i) to request access and obtain a copy of your personal information;
(ii) to request rectification or erasure;
(iii) to restrict the processing of your personal information;
and (iv) if applicable, to data portability.
In certain circumstances, you may also have the right to object to the processing of your personal information.
To make such a request, please use the contact details provided below.
We will consider and act upon any request in accordance with applicable data protection laws.

If we are relying on your consent to process your personal information, you have the right to withdraw your consent at any time.
Please note, however, that this will not affect the lawfulness of the processing before its withdrawal.

If you are resident in the European Economic Area and you believe we are unlawfully processing your personal information, you also have the right to complain to your local data protection supervisory authority.
You can find their contact details here: http://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm.

**Cookies and similar technologies**:
Most Web browsers are set to accept cookies by default.
If you prefer, you can usually choose to set your browser to remove cookies and to reject cookies.
If you choose to remove cookies or reject cookies, this could affect certain features of our Services.
For further information, please see our Cookie Policy: [{{< ref path="/about/cookies-policy" >}}]({{< relref path="/about/cookies-policy" >}}).


## Data breach

A privacy breach occurs when there is unauthorized access to or collection, use, disclosure or disposal of personal information.
You will be notified about data breaches when Keyboard Playing believes you are likely to be at risk or serious harm.
For example, a data breach may be likely to result in serious financial harm or harm to your mental or physical well-being.
In the event that Keyboard Playing becomes aware of a security breach which has resulted or may result in unauthorized access, use or disclosure of personal information, Keyboard Playing will promptly investigate the matter and notify the applicable Supervisory Authority not later than 72 hours after having become aware of it, unless the personal data breach is unlikely to result in a risk to the rights and freedoms of natural persons.


## Do California residents have specific privacy rights?

_**In Short**: Yes, if you are a resident of California, you are granted specific rights regarding access to your personal information._

California Civil Code Section 1798.83, also known as the "Shine the Light" law, permits our users who are California residents to request and obtain from us, once a year and free of charge, information about categories of personal information (if any) we disclosed to third parties for direct marketing purposes and the names and addresses of all third parties with which we shared personal information in the immediately preceding calendar year.
If you are a California resident and would like to make such a request, please submit your request in writing to us using the contact information provided below.

If you are under 18 years of age, reside in California, and have a registered account with the Services, you have the right to request removal of unwanted data that you publicly post on the Services.
To request removal of such data, please contact us using the contact information provided below, and include the email address associated with your account and a statement that you reside in California.
We will make sure the data is not publicly displayed on the Services, but please be aware that the data may not be completely or comprehensively removed from our systems.


## Do we make updates to this policy?

_**In Short**: Yes, we will update this policy as necessary to stay compliant with relevant laws._

We may update this privacy policy from time to time.
The updated version will be indicated by an updated "Revised" date and the updated version will be effective as soon as it is accessible.
If we make material changes to this privacy policy, we may notify you either by prominently posting a notice of such changes or by directly sending you a notification.
We encourage you to review this privacy policy frequently to be informed of how we are protecting your information.


## How can you contact us about this policy?

If you have questions or comments about this policy, you may email us at
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.


## How can you review, update or delete the data we collect from you?

Based on the laws of some countries, you may have the right to request access to the personal information we collect from you, change that information, or delete it in some circumstances.
To request to review, update, or delete your personal information, please email us at
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.
We will respond to your request within 30 days.
