---
title: Politique de confidentialité
slug: politique-confidentialite
description: |-
  Cette politique de confidentialité décrit nos politiques et procédures de collecte, d'utilisation et de partage de vos informations lorsque vous utilisez le service, et vous informe sur vos droits en matière de confidentialité et comment la loi vous protège.
cover:
  src: matthew-henry-fPxOowbR6ls-unsplash.jpg
  alt: Deux femmes regardent des caméras de sécurité.
  by: Matthew Henry
  link: https://unsplash.com/photos/fPxOowbR6ls
  authorLink: https://unsplash.com/@matthewhenry
  license: Unsplash
---

Merci d'avoir choisi de faire part de la communauté Keyboard Playing (« **nous** », « **notre** » ou « **nos** »).
Nous nous engageons à protéger vos informations personnelles et votre droit à la confidentialité.
Si vous avez des questions ou préoccupations au sujet de notre politique ou de nos pratiques concernant vos informations personnelles, merci de nous contacter à l'adresse
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.

Lorsque vous visitez notre site Web https://keyboardplaying.fr et utilisez nos Services, vous témoignez de votre confiance.
Nous prenons votre vie privée très au sérieux.
Dans cette politique de confidentialité, nous voulons expliquer de la façon la plus claire possible quelles informations nous collectons, comment nous les utilisons et quels sont vos droits à leur égard.
Nous espérons que vous prendrez le temps de la lire attentivement, car c'est important.
Si vous êtes en désaccord avec certains termes de cette politique de confidentialité, nous vous invitons à cesser d'utiliser nos Sites et services.

Cette politique de confidentialité s'applique à toutes les informations collectées via notre site Web (comme https://keyboardplaying.fr) ainsi qu'à tout service ou évènement connexe (nous les appelons collectivement « **Services** » dans cette politique de confidentialité).

**Merci de lire attentivement cette politique de confidentialité, car elle vous aidera à prendre des décisions éclairées sur le partage de vos informations personnelles avec nous.**


## Résumé pour les humains

### Quelles informations collectons-nous et dans quel but ?

#### Commentaires et informations accessibles au public

Lorsque vous soumettez un commentaire, vous fournissez volontairement les informations suivantes :

- **Vos prénom, nom et / ou surnom**.
Ils seront utilisés pour vous identifier publiquement comme l'auteur d'un commentaire.

- **Votre adresse email**.
Cette adresse sera utilisée pour trouver votre [Gravatar](https://gravatar.com) si vous en avez un.
Elle sera stockée sous forme d'une empreinte irréversible, ce qui signifie que personne --- nous y compris --- ne sera capable de le lire.

Ces données ne font l'objet d'aucun traitement autre que l'affichage public de votre commentaire sur le site.
Elles seront conservées aussi longtemps que votre commentaire sera disponible à l'affichage, mais peut être supprimé sur demande.


#### Contact direct

Lorsque vous nous envoyez un email, votre message contient votre adresse et probablement votre nom.
Ces informations ne sont utilisées que pour répondre à votre demande et seront conservées au plus 90 jours après cette réponse.


#### Informations dérivées

Lorsque vous utilisez, visitez ou parcourez nos services, nous collectons automatiquement certaines informations pour assurer la sécurité et le fonctionnement de nos services.
Ces données ne suffisent pas à vous identifier et ne seront pas recoupées avec d'autres dans un but autre que la résolution d'un incident.
Elles seront conservées 15 jours, puis anonymisées à des fins statistiques.

Les données que nous collectons dans cette catégorie se limitent à :

- **Votre adresse IP** (Internet Protocol).
La version anonymisée conservée pour les statistiques donne une idée imprécise de votre localisation.

- **Les caractéristiques du navigateur que vous avez utilisé**.
Nous stockons le « _user agent_ », qui décrit la famille et la version du navigateur que vous utilisez pour parcourir le site.

- **_Referrer URL_**.
Si vous êtes arrivé sur notre site via un lien sur un autre site, celui-ci peut transmettre une information appelée « _referrer URL_ ».
Elle nous est utile pour comprendre comment vous avez découvert notre site.


### Vos informations seront-elles partagées ?

Non, sauf avec votre consentement ou pour respecter la loi.


### Comment pouvez-vous voir, modifier ou supprimer les informations que nous possédons sur vous ?

Si vous avez la moindre question ou demande concernant vos données, veuillez nous envoyer un email à l'adresse
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.



## Quelles informations collectons-nous ?

### Informations personnelles que vous nous communiquez

_**En bref** : Nous collectons les données personnelles que vous nous communiquez._

Nous collectons des informations personnelles que vous nous fournissez volontairement lorsque vous participez à des activités sur les Services ou lorsque vous nous contactez.

Les informations personnelles que nous collectons dépendent du contexte de vos interactions avec nous et des Services et produits et fonctionnalités que vous utilisez.
Les informations personnelles que nous collectons peuvent inclure les éléments suivants :

**Informations personnelles accessibles au public**.
Nous collectons vos prénom, nom et surnom.

Toutes les informations personnelles fournies doivent être véridiques et exactes.


### Informations collectées automatiquement

_**En bref** : Certaines informations --- comme l'adresse IP ou les caractéristiques du navigateur --- sont collectées automatiquement lorsque vous visitez nos Services._

Nous collectons automatiquement certaines informations lorsque vous visitez, utilisez ou parcourez les Services.
Ces informations ne révèlent pas votre identité spécifique (comme votre nom ou vos coordonnées) mais peuvent inclure des informations sur l'appareil et l'utilisation, telles que votre adresse IP, les caractéristiques du navigateur et de l'appareil, le système d'exploitation, les préférences de langue, les URL de référence, le nom de l'appareil, le pays, l'emplacement, des informations sur comment et quand vous utilisez nos Services et d'autres informations techniques.
Ces informations sont principalement nécessaires pour maintenir la sécurité et le fonctionnement de nos Services, ainsi qu'à des fins d'analyse et de rapport.

**Identifiants en ligne**.
Nous collectons les adresses IP (Internet Protocol), les applications et les URL de référence.


## Vos informations seront-elles partagées ?

_**En bref** : Nous ne partageons vos informations qu'avec votre consentement, pour nous conformer aux lois, pour vous fournir des services, ou pour protéger vos droits._

Nous pouvons traiter ou partager des données sur la base juridique suivante :

- **Consentement** : Nous pouvons traiter vos données si vous nous avez donné un consentement spécifique pour utiliser vos informations personnelles dans un but spécifique.

- **Obligations légales** :
Nous pouvons divulguer vos informations lorsque nous sommes légalement tenus de le faire afin de nous conformer à la loi applicable, aux demandes gouvernementales, à une procédure judiciaire, à une ordonnance d'un tribunal ou à une procédure judiciaire, par exemple en réponse à une ordonnance d'un tribunal ou à une assignation (y compris en réponse aux autorités publiques pour répondre aux exigences de sécurité nationale ou d'application de la loi).

- **Intérêts vitaux** :
Nous pouvons divulguer vos informations lorsque nous pensons qu'il est nécessaire d'enquêter, de prévenir ou de prendre des mesures concernant les violations potentielles de nos politiques, les suspicions de fraude, les situations impliquant des menaces potentielles pour la sécurité de toute personne et des activités illégales, ou comme preuve dans un litige dans lequel nous sommes impliqués.


## Avec qui vos informations seront-elles partagées ?

_**En bref** : Nous ne partageons d'information avec aucun tiers._

Nous ne partageons ni ne divulguons vos informations à des tiers.
Si nous avons traité vos données sur la base de votre consentement et que vous souhaitez révoquer votre consentement, veuillez nous contacter.


## Utilisons-nous des cookies ou d'autres technologies de suivi ?

_**En bref** : Nous pouvons utiliser des cookies pour collecter et stocker vos informations._

Nous pouvons utiliser des cookies pour accéder ou stocker des informations.
Des informations spécifiques sur la façon dont nous utilisons ces technologies et sur la façon dont vous pouvez refuser certains cookies sont définies dans notre politique en matière de cookies : [{{<ref path="/about/cookies-policy">}}]({{<relref path="/about/cookies-policy ">}}).


## Combien de temps conservons-nous vos informations ?

_**En bref** : Nous conservons vos informations aussi longtemps que nécessaire pour atteindre les objectifs décrits dans la présente politique de confidentialité, sauf disposition contraire de la loi._

Nous ne conserverons vos informations personnelles que le temps nécessaire aux fins énoncées dans la présente politique de confidentialité, sauf si une période de conservation plus longue est requise par la loi (comme la fiscalité, la comptabilité ou d'autres exigences légales).
Aucun objectif de cette politique ne nous obligera à conserver vos informations personnelles pendant plus de 90 jours.

Lorsque nous n'avons aucun besoin commercial légitime en cours de traiter vos informations personnelles, nous les supprimerons ou les rendrons anonymes.


## Comment protégeons-nous vos informations ?

_**En bref** : Nous visons à protéger vos informations personnelles grâce à un système de mesures de sécurité organisationnelles et techniques._

Nous avons mis en œuvre des mesures de sécurité techniques et organisationnelles appropriées conçues pour protéger la sécurité des informations personnelles que nous traitons.
Cependant, n'oubliez pas que nous ne pouvons pas garantir que l'Internet lui-même est 100 % sécurisé.
Bien que nous fassions de notre mieux pour protéger vos informations personnelles, la transmission d'informations personnelles vers et depuis nos services est à vos propres risques.
Vous ne devez accéder aux services que dans un environnement sécurisé.


## Collectons-nous des informations de mineurs ?

_**En bref** : Nous ne collectons pas sciemment de données auprès des enfants de moins de 18 ans ni ne les commercialisons._

Nous ne sollicitons pas sciemment des données auprès d'enfants de moins de 18 ans ni ne les commercialisons.
En utilisant les Services, vous déclarez que vous avez au moins 18 ans ou que vous êtes le parent ou le tuteur d'un tel mineur et que vous consentez à l'utilisation de ces services par une personne à charge mineure.
Si nous apprenons que des informations personnelles d'utilisateurs de moins de 18 ans ont été collectées, nous prendrons des mesures raisonnables pour supprimer rapidement ces données de nos dossiers.
Si vous prenez connaissance de données que nous avons collectées auprès d'enfants de moins de 18 ans, veuillez nous contacter à
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.


## Quels sont vos droits en matière de confidentialité ?

_**En bref** : Dans certaines régions, comme l'Espace économique européen, vous disposez de droits qui vous permettent d'accéder et de contrôler davantage vos informations personnelles. Vous pouvez consulter, modifier ou supprimer vos informations à tout moment._

Dans certaines régions (comme l'Espace économique européen), vous disposez de certains droits en vertu des lois applicables sur la protection des données.
Ceux-ci peuvent inclure le droit
(i) de demander l'accès et d'obtenir une copie de vos informations personnelles ;
(ii) de demander la rectification ou l'effacement ;
(iii) de restreindre le traitement de vos informations personnelles ;
et (iv) le cas échéant, à la portabilité des données.
Dans certaines circonstances, vous pouvez également avoir le droit de vous opposer au traitement de vos informations personnelles.
Pour faire une telle demande, veuillez utiliser les coordonnées ci-dessous.
Nous prendrons en considération et agirons sur toute demande conformément aux lois applicables en matière de protection des données.

Si nous comptons sur votre consentement pour traiter vos informations personnelles, vous avez le droit de retirer votre consentement à tout moment.
Veuillez toutefois noter que cela n'affectera pas la légalité du traitement avant son retrait.

Si vous résidez dans l'Espace économique européen et que vous pensez que nous traitons illégalement vos informations personnelles, vous avez également le droit de porter plainte auprès de votre autorité locale de contrôle de la protection des données.
Vous pouvez trouver leurs coordonnées ici : https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm.

**Cookies et technologies similaires** :
La plupart des navigateurs Web sont configurés pour accepter les cookies par défaut.
Si vous préférez, vous pouvez généralement choisir de configurer votre navigateur pour supprimer les cookies et refuser les cookies.
Si vous choisissez de supprimer les cookies ou de rejeter les cookies, cela pourrait affecter certaines fonctionnalités de nos Services.
Pour plus d'informations, veuillez consulter notre politique en matière de cookies : [{{< ref path="/about/cookies-policy" >}}]({{< relref path="/about/cookies-policy" >}}).


## Fuite de données

Une fuite de confidentialité se produit en cas d'accès ou de collecte, d'utilisation, de divulgation ou d'élimination non autorisés d'informations personnelles.
Vous serez averti des violations de données lorsque Keyboard Playing pense que vous êtes susceptible de courir un risque ou de subir un préjudice grave.
Par exemple, une violation de données peut entraîner des dommages financiers graves ou nuire à votre bien-être mental ou physique.
Dans le cas où Keyboard Playing prend connaissance d'une violation de la sécurité qui a entraîné ou peut entraîner un accès, une utilisation ou une divulgation non autorisés des informations personnelles, Keyboard Playing enquêtera rapidement sur la question et informera l'autorité de surveillance compétente au plus tard 72 heures après avoir pris connaissance à moins que la violation des données personnelles n'entraîne peu de risques pour les droits et libertés des personnes physiques.


## Les résidents de Californie ont-ils des droits de confidentialité spécifiques ?

_**En bref** : Oui, si vous résidez en Californie, vous disposez de droits spécifiques concernant l'accès à vos informations personnelles._

La section 1798.83 du Code civil de Californie, également connue sous le nom de loi « _Shine the Light_ », permet à nos utilisateurs qui sont des résidents de Californie de demander et d'obtenir de nous, une fois par an et gratuitement, des informations sur les catégories d'informations personnelles (le cas échéant) communiquées à des tiers à des fins de marketing direct et les noms et adresses de tous les tiers avec lesquels nous avons partagé des informations personnelles au cours de l'année civile précédente.
Si vous êtes un résident de Californie et que vous souhaitez faire une telle demande, veuillez nous soumettre votre demande par écrit en utilisant les coordonnées fournies ci-dessous.

Si vous avez moins de 18 ans, résidez en Californie et avez un compte enregistré auprès des Services, vous avez le droit de demander la suppression des données indésirables que vous publiez publiquement sur les Services.
Pour demander la suppression de ces données, veuillez nous contacter en utilisant les coordonnées fournies ci-dessous et inclure l'adresse email associée à votre compte et une déclaration que vous résidez en Californie.
Nous nous assurerons que les données ne sont pas affichées publiquement sur les Services, mais sachez que les données peuvent ne pas être complètement supprimées de nos systèmes.


## Mettons-nous à jour cette politique ?

_**En bref** : Oui, nous mettrons à jour cette politique si nécessaire pour rester en conformité avec les lois applicables._

Nous pouvons mettre à jour cette politique de confidentialité de temps en temps.
La version mise à jour sera indiquée par une date «Révisée» mise à jour et la version mise à jour sera effective dès qu'elle sera accessible.
Si nous apportons des modifications importantes à cette politique de confidentialité, nous pouvons vous en informer soit en affichant bien en vue un avis de ces modifications, soit en vous envoyant directement une notification.
Nous vous encourageons à consulter fréquemment cette politique de confidentialité pour être informé de la façon dont nous protégeons vos informations.


## Comment pouvez-vous nous contact à propos de cette politique ?

Si vous avez des questions ou commentaires sur cette politique, envoyez-nous un message à l'adresse
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.


## Comment pouvez-vous consulter, mettre à jour ou supprimer les données que nous collectons auprès de vous ?

Selon les lois de certains pays, vous pouvez avoir le droit de demander l'accès aux informations personnelles que nous collectons auprès de vous, de modifier ces informations ou de les supprimer dans certaines circonstances.
Pour demander la révision, la mise à jour ou la suppression de vos informations personnelles, veuillez nous envoyer un email à
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.
Nous répondrons à votre demande dans les 30 jours.
