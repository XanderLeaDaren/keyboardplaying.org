---
title: Privateca politiko
slug: privateca-politiko
description: |-
  Ĉi tiu regularo pri privateco priskribas kiel ni uzas datumojn pri vi, kaj klarigas viaj rajtoj.
cover:
  src: matthew-henry-fPxOowbR6ls-unsplash.jpg
  alt: Du virinoj rigardas sekureckameraoj.
  by: Matthew Henry
  link: https://unsplash.com/photos/fPxOowbR6ls
  authorLink: https://unsplash.com/@matthewhenry
  license: Unsplash
---

Dankon pro via elekto esti parto de nia komunumo ĉe Keyboard Playing ("**ni**", "**nin**" aŭ "**nia**").
Ni sindevigi protekti viajn personajn datumojn kaj viaj rajtoj al privateco.
Se vi havas demandojn aŭ zorgojn pri nia politiko aŭ nia praktikoj rilate al viaj personaj datumoj, ne hezitu kontakti nin ĉe
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.

Kiam vi vizitas nian retejon https://eo.keyboardplaying.org, kaj uzas niajn servojn, vi konfidas viaj personaj datumoj al nin.
Ni traktas vian priveticon tre serioze.
En ĉi tiu privateca politiko, ni provas klarigi kiajn datumoj ni kolektas, kiel ni uzas ilin, kaj kiajn rajtojn vi havas rilate al ilin.
Ni esperas, ke vi prenos iom da tempo por liras ĝin atente, ĉar ĝi gravas.
Se estas iuj kondiĉoj en ĉi tiu politico, kun kiuj vi ne konsentas, bonvolu ĉesi uzi niajn retjojn kaj servojn.

Ĉi tiu privateca potikiko validas por ĉiuj datumoj kolektitaj per nia retejo (kiel https://keyboardplaying.org), kaj/aŭ iuj rilataj servoj aŭ eventoj (ni nomas ili kolektive "**Servoj**" en ĉi tiu privateca politiko).

**Bonvolu legi atente ĉi tiu privatecan politikon, ĉar ĝi helpos vin fari informitajn decidojn pri dividado de viaj personaj datumoj.**


## Resumo por homoj

### Kiajn datumojn ni kolektas, kaj por kiu celo?

#### Komentoj kaj publike haveblaj datumoj

Kiam vi komentas paĝo de Keyboard Playing, vi libervole donas la jenajn datumojn:

- **Via antaŭnomo, familinomo kaj/aŭ pseŭdonimo**.
Ĉi tiu estos uzata por publike identigi vin kiel la aŭtoron de viaj komentoj.

- **Via retpoŝta adreso**.
Ni uzas ĉi tiu por trovi vian [Gravatar](https://gravatar.com), se vi havas.
Ĝi estos konservita kiel neinversigebla haketaĵo.
Signifas ke neniu, eĉ ne ni, povos legi ĝin.

Ĉi tiuj datumoj estos uzataj nur por montri publike viajn komentojn en la retejo.
Ĝi restos tiel longe kiel via komento estos videbla, sed viŝebla laŭ peto.


#### Rekta kontakto

Kiam vi retpoŝtigas nin, vi sendas datumoj pri vin, inter kuij via retpoŝta adreso kaj probable via nomo.
Tiuj informoj estas uzatas nur por respondi via demando, kaj konserviĝos maksimume 90 tagoj post tiu respondo.


#### Derivaĵoj

Kiam vi uzas, vizitas aŭ navigas la Servojn, ni aŭtomate kolektas datumojn por sekureco kaj funkciado de niaj servoj.
Tiu datumo ne sufiĉas por identigi vin kaj ne estos krusreferencitaj, krom por solvi okazaĵojn.
Ili estos konservataj dum 15 tagoj kaj anonimigitadaj por statistiko.

- **via IP-adreso** (interreta protokolo).
La anonimigita versio, kiun ni uzas por analitiko, donas aludon pri via ĝeneralo loko.

- **Karakterizaĵoj pri la retumilo, kiun vi uzis**.
Ni konservos la "user agent," kiu priskribas la familion kaj version de la retumilo, kiun vi uzas por viziti la retejon.

- **Referrer URL** (referenda URL).
Se vi alvenis al nia retejo per ligilo en alia retejo, ĝi eble transdonos informon konatan kiel la "Referrer URL."
Tio utilas por kompreni kiel vi malkovris nian retejon.


### Ĉu via informoj estis dividitaj kun iu ajn?

Ne, krom kun via konsento aŭ por plenumi leĝojn.


### Kiel vi povas vidi, modifi aŭ forviŝi la datumojn, kiujn ni havas pri vi?

Se vi havas demandon aŭ peton pri viaj datumoj, bonvolu retpoŝti nin al
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.



## Kiajn datumoj ni kolektas?

### Personaj datumoj, kiujn vi malkaŝas al ni

_**Resume**: Ni kolektas personajn datumojn, kiujn vi donas al ni._

Ni kolektas personajn datumojn, ke vi libervole donas al vi, kiam vi partoprenas en agadoj en la Servoj aŭ alimaniere kontaktas nin.

La personaj informoj, kiujn ni kolektas, dependas de la kuntesto de viaj interagoj kun ni kaj la Servoj kaj la produktoj kaj funkcioj vi uzas.
La personaj informoj, kiujn ni kolektas, povas inklusivi la jenajn:
The personal information we collect can include the following:

**Publike havebla persona informoj**.
Ni kolektas antaŭnomojn, familinomojn kaj pseŭdonimojn.

Ĉiuj personaj informoj, kiujn vi donas al ni, devas esti veraj kaj ĝustaj.


### Informoj aŭtomate kolektikaj

_**Resume**: Iuj datumoj---kiel IP-adreso kaj/aŭ retumilaj trajtoj---estas kolektikaj aŭtomate kiam vi vizitas niajn Servojn._

Ni aŭtomate kolektas iujn datumojn kiam vi vizitas, uzas aŭ navigas la Servojn.
Tiuj informojn ne malkaŝas vian specifan identecon (kiel via nomo aŭ kontaktinformoj) sed povas inkluzivi aparatajn kaj uzajn informojn, kiel via IP-adreso, retumilo, referencaj adresoj, aparata nomo, lando, loko, informoj pri kiel kaj kiam vi uzas niajn Servojn kaj aliajn teknikajn informojn.
Tiuj informoj ĉefe necesas por konservi la sekurecon kaj funkciadon de niaj Servoj, kaj pror niaj internaj analizaj kaj raportal celoj.

**Interretaj identigiloj**.
Ni kolektas IP-adresojn, aplikojn kaj referencajn adresojn.


## Ĉu viaj informoj estas dividitaj kun iu ajn?

_**Resume**: Ni nur dividas datumoj kun via konsento, por plenumi leĝojn, por provizi servojn al vi aŭ por protekti viajn rajtojn.

Ni povas prilabori aŭ dividi datumojn surbaze de la sekva jura bazo:

- **Konsento**:
Ni eble prilaboras viajn datumojn se vi donis al ni specifan konsenton por uzi viajn personajn informojn en specifa celo.

- **Laŭleâj devigoj**:
Ni povas malkaŝi viajn informojn, kie ni laŭleĝe devigas tion plenumi konformajn leĝojn, registarajn petojn, juĝan procedon, kortuman ordonon aŭ juran proceson (inkluzive responde al publikaj aŭtoritatoj por plenumi postulojn pri nacia sekureco aŭ policoj).

- **Esencaj interesoj**:
Ni eble malkaŝas viajn informojn, kie ni kredas, ke necesas esplori, malhelpi aŭ agi pri eblaj malobservoj de niaj politikoj, suspektata fraŭdo, situacioj kun eblaj minacoj al la sekureco de iu ajn persono kaj kontraŭleĝaj agadoj, aŭ kiel evidenteco en proceso, en kiu ni partoprenas.


## Kun kiu via datumojn estos dividita?

_**Resume**: Ni ne konscie dividas informojn kun eksteraj liverantoj._

Ni ne dividas aŭ malkaŝas viajn informojn al eksteral liverantoj.
Se ni prilaboris viajn datumojn surbaze de via konsento kaj vi volas revoki vian konsenton, bonvolu kontakti nin.


## Ĉu ni uzas kuketojn kaj aliajn suprajn teknologiojn?

_**Resume**: Ni eble uzas kuketojn por kolekti kaj konservi viajn datumojn._

Ni eble uzas kuketoj kaj similajn teknologiojn por aliri aŭ stoki informojn.
Specifaj informoj pri kiel ni uzas tiajn teknologiojn kaj kiel vi povas rifuzi iujn kuketoj estas presentitaj en nia Kuketa 
We may use cookies and similar tracking technologies (like web beacons and pixels) to access or store information.
Specific information about how we use such technologies and how you can refuse certain cookies is set out in our kuketa politiko: [{{< ref path="/about/cookies-policy" >}}]({{< relref path="/about/cookies-policy" >}}).


## Kiom longe ni konservas viajn datumojn?

_**Resume**: Ni konservas viaj datumojn tiom longe kiom necesas por plenumi la cetlojn priskribitajn en ĉi tiu privetica politiko, krom se laŭleĝe alie postulite._

Ni konservos viajn personajn informojn nur dum ĝi necesos por la celoj difinitaj en ĉi tiu privateca politiko, krom se leĝo postulas pli longan konservandon (kiel impostoj, kontado aŭ aliaj juraj postuloj). Neniu celo en ĉi tiu politiko devigos nin konservi viajn personajn informojn dum pli ol 90 tagoj.

Kiam ni ne havas dŭran legitiman bezonon prilabori viajn personajn informojn, ni aŭ forviŝos aŭ anonimigos ilin.


## Kiel ni gardas viajn datumojn sekure?

_**In Short**: Ni celas protekti viajn personajn datumojn per sistemo de organizaj kaj teknikaj sekurecaj agadoj._

Ni efktivigis taŭgajn teknikajn kaj sekurecajn agadojn desegnitajn por protekti la sekurecon de iuj personaj informoj, kiujn ni prilaboras.
Tamen bonvolu memori, ke ni ne povas garantii, ke la interreto mem estas 100% sekura.
Kvankam ni faros nian eblon protekti viajn personajn informojn, transdono de personaj informoj al kaj de niaj Servoj estas sub via propra risko.
Vi devas aliri la servojn nur en sekura medio.


## Ĉu ni kolektas datumojn de neplenaĝuloj?

_**Resume**: Ni ne intence kolektas datumojn de aŭ vendas al infanoj sub 18 jaroj._

Ni ne konscie petas datumojn de aŭ vendas al infanoj sub 18 jaroj.
Uzante la Servojn, vi deklars, ke vi estas almenaŭ 18-jara aŭ ke vi estas la gepatro aŭ kuratoro de tia neplenaĝo kaj konsentas pri la uzo de la Servoj fare de tia neplenaĝulo.
Se ni ekscios, ke personaj informoj de uzantoj mapli ol 18 jaraj estis kolektikaj, ni prenos raciajn rimedojn por senprokraste forigi tiajn datumojn de niaj registroj.
Se vi ekscios pri iuj datumoj, kiujn ni kolektis de injanoj sub 18 jaroj, bonvolu kontakti nin ĉe
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.


## Kiuj estas viaj priveticaj rajtoj?

_**Resume**: En iuj regoinoj, kiel la Eŭropa Ekonomia Areo, vi havas rajtojn, kioj ebligas al vi pli grandon aliron kaj kontrolon de viaj personaj datumoj. Vi rajtas revizii, ŝagi aŭ forigi viajn informajn iam ajn._

En iuj regionoj (kiel la Eŭropa Ekonomia Areo), vi havas iujn rajtojn laŭ aplikeblaj leĝoj pri protektado de datumoj.
Tiuj povas inkluzivi la rajton
(i) peti aliron kaj akiri kopion de viaj personaj informoj;
(ii) peti korektadon aŭ forviŝadon;
(iii) limigi la prilaboradon de viaj personaj informoj;
kaj (iv) se aplikebla, al portebla datumo.
En iuj cirkonstancoj, vi ankaŭ rajtas kontraŭi la prolaboradon de viaj personaj informoj.
Por fari tian peton, bonvolu uzi la kontaktajn detalojn sube.
Ni pripensos kaj agos laŭ iu ajn peto laŭ aplikeblaj leĝoj pro protektado de datumoj.

Se ni dependas de via konsento por prilabori viajn personajn informojn, vi rajtas ĉesigi vian konsento iam ajn.
Bonvolu noti, tamen, ke ĉi tio ne influos la laŭleĝecon de la prilaborado antaŭ ĝia retiro.

Se vi loĝas en la Eŭropa Ekonomia Areo kaj vi kredas, ke ni kontraŭleĝe prolaboras viajn personajn informojn, vi ankaŭ rajtas plendi al via loka kontrola aŭtoritato pri datuma protekto.
Vi provas trovi iliajn kontaktajn detalojn tie: https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm.

**Kuketoj kaj similaj teknologioj**:
Plej multaj retumiloj estas difinitaj por akcepti kuketojn defaŭlte.
Se vi preferas, vi kutime povas elekti agordi vian retumilon por forigi kuketojn kaj malakcepti kuketojn.
Se vi elektas forigi kuketojn aŭ malakcepti kuketojn, tio povus influi iujn funkciojn de niaj Servoj.
Por pliaj informoj, bonvolu vidi nian kuketan politikon: [{{< ref path="/about/cookies-policy" >}}]({{< relref path="/about/cookies-policy" >}}).


## Datumrompo

Privateca rompo okazas kiam estas neaŭtorizita aliro al aŭ kolekto, uzo, malkaŝado aŭ forigo de personaj informoj.
Vi estos sciigita pri tiaj rompoj, kiam Keyboard Playing kredas, ke vi riskas aŭ gravas damaĝon.
Ekzemple, datumrompo eble kaŭzos gravan financan damaĝon aŭ damaĝon al via mensa aŭ fizika bonstato.
En la okazo, ke Keyboard Playing ekkonscias pri sekureca breĉo, kiu rezultigis aŭ povas rezultigi neaŭtorizitan aliron, uzon aŭ malkaŝon de personaj informoj, Keyboard Playing senprokraste esploros la aferon kaj informos la koncernan kontrola aŭtoritaton plej malfrue 72 horojn post kiam li eksciis de ĝi, krom se la rompo de personaj datumoj verŝajne ne riskos la rajtojn kaj liberecojn de naturaj personoj.


## Ĉu Kaliforniaj loĝantok havas specifajn privatecajn rajtojn?

_**Resume**: Jes, se vi estas loĝanto de Kalifornio, vi ricevas specifajn rajtojn pri aliro al viaj personaj informaj._

Sekcio de Karlifornio Civila Kodo 1798.83, ankaŭ konata kiel la leĝo "_Shine the Light_," permesas al niaj uzantoj, kiul estas loĝantoj de Kalifornio, peti akiri de ni, unufoje jare kaj senpage, informojn pri kategorioj de personaj informoj (se ekzistas) ni malkaŝita al triaj por rekta merkatado kaj la nomok kaj adreso de ĉiuj triaj, kun kiuj ni dividis personajn informojn en la tuj antaŭa kalendara jaro.
Se vi estas loĝanto de Kalifornio kaj ŝatus fari tian peton, bonvolu sendi vian peton skribe al per la kontaktaj informoj sube.

Se vi aĝas malpli ol 18  jarojn, loĝas en Kalifornio kaj havas registritan konton ĉe la Servoj, vi rajtas peti forigon de nedezirataj datumoj, kiujn vi publike afiŝas sur la Servoj.
Por peti forigon de tiaj datumoj, binvolu kontakti nin per la kontaktaj informoj sube, kaj inkluzivi la retpoŝtan adreson asciitan kun via konto kaj komunikaĵon, ke vi loĝasen Kalifornio.
Ni certigos, ke la datumoj ne publike aperas sur la Servoj, sed bonvolu konscii, ke la datumoj eble ne estas tute aŭ amplekse forigitaj de niaj sistemoj.


## Ĉu ni ĝisdatigas ĉi tiun politikon?

_**Resume**: Jes, ni ĝisdatigos ĉi tiun politikon kiel necese por resti konforma al koncernaj leĝoj._

Ni eble ĝisdatigos ĉi tiun privatecan politikon de tempo al tempo.
La ĝisdatigita versio estos indikita per ĝisdatigita "Reviziita" dato kaj la ĝisdatigita versio efikos tuj kiam ĝi estos alirebla.
Se ni faras materialajn ŝanĝojn al ĉi tiu privateca politiko, ni eble sciigos vin aŭ elstare afiŝante avizon pri tiaj ŝanĝoj aŭ rekte sendante al vi sciigon.
Ni kuraĝigas vin revizii ĉi tiun privatecan politikon ofte por esti informita pri kiel ni protektas viajn informojn.


## Kiel vi povas kontakti nin pri ĉi tiu politiko?

Se vi havas demandojn aŭ komentojn pri ĉi tiu politiko, vi povas retpoŝti nin ĉe
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.


## Kiel vi povas revizii, ĝistatigi aŭ forigi la datumojn, kiujn ni kolektas de vi?

Surbaze de la leĝoj de iuj landoj, vi eble rajtas peti aliron al la personaj informoj, kiujn ni kolektas de vi, ŝanĝi tiujn informojn aŭ forigi ilin en iuj cirkonstancoj.
Por peti revizii, ĝisdatigi aŭ forigi viajn personajn informojn, bonvolu retpoŝti nin al
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.
Ni respondos al via peto ene de 30 tagoj.
