---
title: Inspirations & challenges
description: |-
  I'm part of a community of apprentice writers.
  To stay motivated and keep writing, we create challenges on a weekly and bimonthly basis.
  Here are some of my contributions, which I deem acceptable.
  Please comment them to help me make them better still.
url: /stories/inspirations
---
