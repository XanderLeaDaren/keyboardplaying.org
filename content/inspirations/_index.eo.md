---
title: Inspiroj kaj defioj
description: |-
  Mi estars parto de komunmo de metilernantaj verkistoj.
  Por resti motivata kaj plu skribi, ni kreas malgrandajn defiojn, ĉiusemajne kaj dumonate.
  Mi dividas ĉi tie kelkajn, kiujn mi konsideras akcepteblaj.
  Ne hezitu komenti pri ili por helpi min plibonigi ilin.   
url: /rakontoj/inspiroj
---
