---
title: Songfics
description: |-
  Certains de nos défis sont d'écrire en s'inspirant d'une chanson.
  Le choix de la chanson est généralement laissé à l'auteur.
weight: 4
slug: songfic

termCover:
  banner: false
  src: debby-hudson-yNdzXDpW8n4-unsplash.jpg
  alt: Un stylo à côté d'un cahier sur lequel sont posés des écouteurs
  by: Debby Hudson
  authorLink: https://unsplash.com/@hudsoncrafted
  link: https://unsplash.com/photos/yNdzXDpW8n4
  license: Unsplash
---
