---
title: Vie de tous les jours
description: |-
  Certains événements de la vie de tous les jours peuvent valoir la peine d'être racontés, ou donnent des idées de nouvelles.
weight: 1
slug: vie-quotidienne
---
