---
title: Weekly challenges
description: |-
  The weekly challenge is specified each Friday.
  All of these share a common constraint: they should be short.
weight: 2
slug: weekly

termCover:
  banner: false
  src: thom-holmes-k-xKzowQRn8-unsplash.jpg
  alt: A notebook is open on a table, with both pages fully covered in writings
  by: Thom Holmes
  authorLink: https://unsplash.com/@thomholmes
  link: https://unsplash.com/photos/k-xKzowQRn8
  license: Unsplash
---
