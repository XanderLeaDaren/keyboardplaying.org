---
title: Stylo Hebdo
description: |-
  Le Stylo Hebdo est défini chaque vendredi.
  Il a une règle récurrente : rester court.
weight: 2
slug: hebdomadaires

termCover:
  banner: false
  src: thom-holmes-k-xKzowQRn8-unsplash.jpg
  alt: Un carnet est ouvert sur une table, les deux pages couvertes d'écriture
  by: Thom Holmes
  authorLink: https://unsplash.com/@thomholmes
  link: https://unsplash.com/photos/k-xKzowQRn8
  license: Unsplash
---
