---
date: 2020-04-01T18:50:41+02:00
title: Judgment
slug: aelg-judgment
authors: [ chop, tony ]
license: CC BY-NC-ND 4.0

description: |-
    Two convicts have are en route to the prisonship.
    To pass the time, they get to know each other.

universes: [ caravan ]
keywords: [ flash fiction, science fiction, aliens, spaceships ]

preface: |-
    My writing community introduced me to the podcast [Aliens et les Garçons](http://aliensetlesgarcons.space/), a French scifi podcast.
    We contacted them and finally put ourselves to the challenge: write a column for them on the theme of their next episode: \"judgment.\"
    Little additional challenge: we wrote it with [Tony](https://tonymalghem.com).
    For an audio version (in French), you can click [here](http://aliensetlesgarcons.space/2020/04/01/e33-hors-serie-nos-auditeurs-ont-du-talent-vol-1/).

cover:
  src: /img/covers/aelg.png
  alt: Aliens & les Garçons, a rather fun, French, scifi podcast
  by: Aliens & les Garçons
  authorLink: http://aliensetlesgarcons.space/
  link: http://aliensetlesgarcons.space/
---

The Citadel was far away, already.
The shuttle had docked the city ship, the seat of the Council where all races in the convoy met to make the decisions affecting the Caravan.
It had taken less than a minute for guards to push Räkô onboard and the transport vessel had sped away.
The pilots obviously didn't care whether their new passenger was properly harnessed.

He watched what he could through the window as the shuttle swerved between ships.
They were all different. In size, in shape, in color... as varied as the races they belonged to.
This was a Petogian ship.
This was a Bith.
That one, he had no idea.
And this one... This one, this was the Atituls'.
He wouldn't see his right away: the shuttle was taking him to the other end of the convoy.

A voice pulled him out of his reverie, coming from just below the window.
"Ya're a bufo, right?"

Räkô lowered his head and noticed a Tonian in the harness facing him.
"A bufo is an amphibian native to Tollana, which some raise in the Caravan for culinary purposes.
But yes, I am an Omeinmonti and my people are often called so."

"Sorry, I'd never seen one like ya.
Never seen a bufo either, actually.
Why d'they call ya that?"

"Because we're amphibians too, I surmise.
Or because of the way our skin looks.
People like to forget we're here."

"’Didn't want to be mean, but clearly, ya don' have a shot at becoming the next Miss Universe.
So, what ya doin' here?"

"I suppose we're doing the same thing, you and I: taking residence at the prison-ship."

"Are ya an arse on purpose, or is it by nature?
Of course, we're both goin' to jail!
_Why_ ya're goin's what I'm askin' ya."

Räkô realized that, indeed, the self-importance he appeared to show would not help him where he was going.
The prison-ship was a ship by name only.
It was actually much more of a metal block floating in space, at the head of the Caravan, described as habitable for want of a better word.
It was the only prison for all the delinquents and criminals of the convoy, regardless of race and reason for incarceration.
Making friends would be a smarter move than alienating his future neighbors before he had even moved in.
He sighed before answering.

"I'm sorry, I was too focused on my own problems.
Let's start over: my name is Räkô Førús.
Who might you be?"

"Togmal Neghym.
So, what's your story?"

"To make a long story short, let's say I've been convicted for stealing to a race that doesn't recognize the concept of property and therefore had no law to judge me."

"Ah, so the system screwed ya too?
Go on, tell me aboudit!
I'll feel less lonely."

"Where should I begin?
I suppose you know the Omeimontis are responsible for everything related with drinking water in the Caravan, at least for the races that are not self-sufficient in this area."

"I've mostly heard the bufi... Sorry, that you guys use a lot of it."

"That's why that they don't like us!
Yes, as amphibians, we _do_ need a lot of water and rumors that we keep it to ourselves have spread.
That's both true and false: we store water because we need it, but we share it willingly.
We became experts in the field out of necessity.
No space travel without it.

"That's why we work on the water recycling systems of many races in the Caravan.
And yet we struggle to get... Well, we struggle to get just about anything.
Since the races in the convoy think we're water thieves, they'll bargain any resource to the extreme.
The mechanical races are the most open-minded.
Both because they're more rational and because they have very little interest in water."

"That's a hell of a stupid if ya're the one we owe our water too!"

"I agree.
I even pleaded it before the Council several times.
They all agree in principle, but the facts remain.
Anyway, some systems ago, I worked on an Atitul ship.
Do you know anything about them?"

"The nobs?
Yeah, they say they're born with a platinum screwdriver in their hand, but to be too bone-lazy to use it."

"That's... oddly accurate.
I knew the rumors, but indeed, they live in indecent abundance.
They had a breakdown on a recycling system.
At one point, I needed a part their engineer didn't have.
He went in his colleague's room, shamelessly, and came back with the valve I needed.
When I asked about the billing of the part, he explained that the Atituls don't have the concept of ownership.
Everything belongs to everyone and everyone can help themselves.
It's a beautiful philosophy.
Especially when you live in opulence.
That doesn't really bother anyone."

"True.
Not everyone can dream of that."

"Unfortunately.
So I cracked.
In addition to not having any material worries, they indeed are very lazy.
They barely watch us and don't even try to learn.
In fact, they don't do any maintenance on their systems.
So I set up an operation to take from the too rich and recover for those who, like us, are in need.
I set up a network with the guys who intervene in their ships, so that they'd nick some parts on each intervention.
We always remained reasonable, of course; just grabbed a thing or two from their reserve, which could be useful to us but that they wouldn't lack."

"An' ya got caught, and since the 'sovereignty of justice' is up to each species, they judged ya and sentenced ya to jail."

"Not exactly.
They finally noticed our scheme and actually asked to face someone responsible, but they couldn't judge me.
Since no one _owned_ the part, the very notion of left is meaningless in their law.
So, instead of putting me on trial, they asked for the help of the Arbiter of the Citadel."

"What, ya mean the asexual guy... gal... thing that helps solving petty disagreements?"

"The very one.
The two in disagreement this time were the Atitul inventory manager and myself.
In this procedure, each party is entitled to two barristers to represent them, and three neutral barristers are there to make sure the vote cannot result in a tie."

The inertial dampeners prevented Räkô from feeling the shuttle slowing down, but he saw the ships passing by slower outside.
They were approaching the prison-ship.
He had to be brief.

"To cut a long story short, I took two of my guys as my barristers, in front of two Atituls.
The neutral ones were randomly selected from the Council members.
The main argument of the inventory manager was that tracking his inventory was hard work.
Seriously? I mean, it's in his title, right?
FRAK!
Sorry.
Fortunately, his barristers were a tad smarter.
They explained that, though ownership doesn't exist within the Atitul community, it is still this community which has the usufruct and was thus spoliated..."

"Wow, wow, wow!
Stop right there!
I don't speak lawyer, mate.
Why don't ya stick to the common tongue?"

"Right, sorry.
What we stole didn't belong to one Atitul in particular, but it belongs to the Atituls in general.
We explained that we were only taking things that they wouldn't miss and that we needed.
One of the neutral barristers did vote for us in the end.
What really hurt was that one of my guys voted against me, because I supposedly reinforced the bad image of the Omeimontis.
Gorram hypocrite!
He wasn't the last to nick a thing or two for himself."

"So the Arbiter sentenced ya to jail?"

"It was annoyed, because it's not usually his job, but yes, it did.
The Atituls had insisted on an exemplary punishment, to make sure everyone didn't start stealing 'their' property.
The Arbiter asked each barrister to pronounce what they thought was a reasonable sentence and averaged it out.
That's how I'm going to miss the next ten systems for lack of windows on the prison-ship."

"Ya received a ten-system sentence for taking stuff that didn' belong to anyone?
I'll give ya one thin', it's a steep one!
It almost makes me feel better."

The window was now in the shade.
The noise of magnetic repulsors replaced the one from impulse and died out too.
A slight metallic shock preceded the silence outside.
Concentrating on their conversation, they hadn't noticed a thing.

"So, your turn. What brings you in this ho...?"

The back door opened and a guard shouted,
"Shut the frak up what passes for a mouth in your race and follow me!"

The companions in misery got out of the shuttle, volunteers forced by the pistolasers the guards were pointing at them.
Two guards led the way, followed by Räkô and Togmal.
Two others brought up the rear, ready to draw their weapons if the new residents made a move to escape.
The small group advanced slowly, too much to the taste of the guards, who used their electric tonfas behind the prisoners' knees to make them hurry up, with relatively unproductive results.

Once the usual security controls had been carried out, with the help of plastic gloves and coughs, both criminals found themselves assigned to the same cell, by the greatest of chances of storytelling.

"I didn't expect us to meet again so soon, but this will allow you to tell me your story."

"And to think I called to book a single room..."

The Tonian sat on the lower bed, now his, and began his story.

"Well, the system put me one behind, too.
I got me four kids at home.
That may be nothin' for you batrachians, but it's rare for my kind.
Extremely rare, actually.
Our females usually have one fertilizable egg, and that's it.
Every once in a while though, one of them gets lucky and hello! She can have several kids.
True, it's not ideal for the survival of the species.
Those women are so rare that normally, when you detect their reproductive capacity, you take them and give them to leaders, thinkers...
Hell, to important leaders, not to minions like me."

"Well, your race sure respects its women..."

"Yeah, and family too.
Not only are the women treated like layin' hens, but what's more, kids are left behind.
Little bastards, they'd be a blot in their ivory towers."

"You have towers in your ship?"

"Make a gorram effort, dammit!
It's a picture."

A guard passed by, clanking his baton against the prison bar, creating an unbearable echo that bounced off the walls of the terrible metal block.

"And so they came one mornin', wantin' me to stay alone with my gal and triplets while they snatched my wife from me.
Two kind of assholes like the ones that brought us here, government-condoned scum.
But me, I love my wife, so I didn' give in.
I grabbed a stool, and bim!
Got his head good."

"Bim, got his head good?"

"Yep!
And I gave it another go just to be sure.
Skull cracked in clean halves by one of the bars.
Wasn' pretty to be sure, but he had it comin'."

"Soooo, you killed an official law representative?
In cold blood?
I don't see how the system disrespected you..."

"Oh ya wait, I'm gettin' there!
I'm not the first one to react that way, ya can guess.
There're precedents for when you attack those bast... people during a raid.
You get some trouble, some community service, but that's about it."

"It's... an interesting system.
But?"

"Well, the problem is, the guard I attacked was a Pneuma.
The special thin' about Pneumas, it's that, when they die, they come back as spirits.
And in the law of Pneumas, when a spirit appears before the court, it automatically becomes the main witness of the trial."

"Undeniably convenient, but that doesn't smell good for you."

"Just like ya say.
So, the trial, the guy tells what happens, but the worst part is that he managed to move the jury.
See, he said that his wife was pregnant with their first child, that they thought they wouldn' manage, but it actually just worked.
No need to tell you how the whole assembly wept.
Frak, _I_ cried.
I fucked up a family to save my own, so that in the end..."

His voice broke in a half sob.
He turned his face to the wall so that his cellmate wouldn't see the tear on his cheek.
He cleared his throat before resuming.

"So that in the end, my wife and children were executed.
I deprived a family from their father.
In retribution, they deprived a father of his family."

Silence fell again in the cell, broken only by the screams coming from the rest of the prison.
Räkô opened and closed his mouth, looking for something to say.
Nothing came to him and he thought it wiser to keep silent.
