---
date: 2020-03-13T22:33:13+01:00
title: Locked Up
slug: locked-up
author: chop
license: CC BY-NC-ND 4.0

description: |-
    Don't go out there!
    There's nothing but sickness and madness out there.

inspirations: [ weekly ]
keywords: [ flash fiction, claustrophobia, post-apocalyptic ]

challenge:
  period: 2020-03-13
  rules:
  - type: general
    constraint: In camera
---

Cathy's sitting on the sofa.
She's anything but relaxed, though.
She cannot express how tense she feels, but she can neither rest nor think.
She's like a statue, just as motionless and devoid of thought, except for the shivers running down her spine.
Suddenly, she gets up, without knowing why or what for, and she heads for the hall, stopping on its threshold.
As she's slowly turning her head to her right, her eyes focus on the door.
Her arm rises as in a dream, her hand stretching out towards the handle without reaching it.
Her left foot moves forward, almost stumbling, when a voice comes from the living room, behind her.

"Cathy? Where are you?
OH GOD NO, DON'T DO THAT!"

Frank's scream brings the young woman to her senses.
She turns and looks at him, her eyes still misty from the torpor she was in, as if she were waking up from a deep sleep.
Her elocution seems to go with this hypothesis.
Le cri de Frank sort la jeune femme de sa torpeur.

"Wazgunnon?"

"You almost opened that damn door, that's what's going on!"

"What?! Uh, sorry. I don't know what came over me."

The man calms down a bit and looks at her almost tenderly.
"You look like a sleepwalker.
You should get some sleep."

"You're right, I feel drained." Cathy goes to the stairs and each step up seems more tiring than the previous one.
She really doesn't feel well.

She passes the little window that normally opens on the outside.
Normally.
Like all the other windows, it's barricaded.

Frank has done a good job of it: the shutters are nailed together, wooden panels cover the openings who could not be hidden otherwise, tape was put on any tiny gap, the doors were caulked...
He was especially cautious to cover any air vent.
No light comes in from the outside.
It's a good thing there's still electricity!

She arrives in the room he lent her.
His daughter's room, he said.
As Cathy lies down on the bed, she feels dizzy.
She waits and the feeling passes, but she's happy she doesn't need to stand right now.
It's as if her head is wrapped in cotton wool.

She closes her eyes and thinks about her arrival here.
She'd left the city to join her parents in the country.
Her car was going fast in the twilight and she hoped to arrive before dinner.
She was coming into a treacherous curve when a van came in the opposite direction, encroaching on her lane.
She remembers steering to avoid it.
She remembers seeing, in slow motion as her car took off, the frightened look of the driver and, behind him, other people.
Numerous. Two women, children. Others perhaps.

After that, she only remembers opening her eyes to discover a ceiling and two faces she didn't know yet, Frank and Nelly.
She was lying in the bed on which she is falling asleep.
Nelly is a nurse and Cathy was lucky to have no serious injuries: only her arch and lip were open---she must have seen her steering wheel very closely---and a few bruises still hurt a week later.
Nelly was in the car at the same time---the van had swerved into the bend to pass her.
Unlike the offending vehicle, she had stopped and decided to take the girl home to take care of her.

When she returned home, she had found out from her husband the latest news from the village.
The wildest rumors spread: the United States was said to have created the disease in its trade war with China, but it got out of hand; the figures given by governments and health organizations were far below the real seriousness of the situation.
Crowds began to panic, people rushed to stores to stock up for quarantine or curfew.
Nelly and Frank barricaded the house, both to keep the panicked individuals and the virus out.
They have a large basement, as well as a Cold War-era bomb shelter.

Cathy would still like a television or radio to keep up to date, but her rescuers are used to get the news at the grocery store.
Nelly got Cathy's stuff back, but her smartphone was blown to pieces in the accident.
Only, after the free-for-all Frank attended, he refuses to go outside---and, like a good hypochondriac, he refuses to risk letting the virus in.
He certainly wouldn't have brought a stranger within these walls, would that have been up to him.

Memories fade, thoughts and the room follow, and Cathy sinks into sleep.

She wakes up with a start and opens her eyes.
In front of her, the ceiling and Frank's face, bent over her.

"What's going on?" she asks. "Is Nelly home?"

Frank scowls and answers, "No, still no news."

Cathy straightens herself up somehow before continuing,
"I need some air.
I'll go get her."

Frank's eyes suddenly look like saucers.
"You're not serious, are you?
Don't you dare thinking about it!
Do I need to remind you how chaotic it is out there?"

"What do we know about it?
We've been cooped up in here for a week and know nothing about what's going on out there!"

"And we're safe!
Look: Nelly wanted to get out and she never came back!"

"There's probably nothing dramatic.
She only left..."
Cathy looks at her watch.
She still feels muddy when attempting some mental calculation.
"Five o'clock. If I leave now, I have a chance of finding her before dark."

Frank's voice is getting stronger, and the tone he's using sounds less and less like worry and more and more like anger.
"Stay here, it's the only place you're safe!
There's nothing but sickness and madness out there!"

The vehemence of the man facing him awakens another, closer memory.
A memory five hours old.
While she was in the bedroom, she heard the voices of Nelly and Frank rising from the bottom of the stairs.
They had become more and more violent, until there was a slamming door sound, followed by the sound of something falling.

Cathy had come mid-stairs to ask if everything was all right.
Leaning on the banister, she had seen the coat rack in the vestibule on the floor and Frank walking back and forth between the front door and the one leading to the cellar.
When Cathy asked him, he had raised a closed face and said, "She absolutely insisted on going out to check what's up, no matter what I thought."

Then he had gone down to the cellar and slammed the door behind him.
