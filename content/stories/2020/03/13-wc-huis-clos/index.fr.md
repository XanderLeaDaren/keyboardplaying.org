---
date: 2020-03-13T22:33:13+01:00
title: Enfermés
slug: enfermes
author: chop
license: CC BY-NC-ND 4.0

description: |-
    Il ne faut pas sortir.
    Dehors, seules la maladie et la folie nous attendent.

inspirations: [ weekly ]
keywords: [ micronouvelle, claustrophobie, post-apocalyptique ]

challenge:
  period: 2020-03-13
  rules:
  - type: general
    constraint: Huis clos
---

Sur le canapé, Cathy est tendue.
Une tension qui ne s'exprime pas, mais qui empêche de se reposer tout comme elle empêche de réfléchir.
Elle est semblable à une statue, aussi immobile en pensée qu'en action, au détail des frissons qui la parcourent.
Soudain, elle se lève, sans réellement savoir pourquoi ou pour quoi, et se dirige vers le vestibule, sur le seuil duquel elle s'arrête.
Alors qu'elle tourne lentement la tête sur sa droite, ses yeux font le point sur la porte.
Son bras se lève comme dans un rêve, la main tendue vers la poignée sans pouvoir l'atteindre.
Son pied gauche avance, trébuchant presque, quand une voix arrive du salon, dans son dos.

--- Cathy ? Où es-tu ?...
NON, NE FAIS PAS ÇA !

Le cri de Frank sort la jeune femme de sa torpeur.
Elle se retourne et le regarde, les yeux embrumés, comme si elle émergeait d'un profond sommeil.
Son élocution semble d'ailleurs confirmer cette idée.

--- Gu'est-ce gui s'basse ?

--- Il se passe que tu as failli ouvrir la porte !

--- Hein ?! Heurf, désolée. Je ne sais pas ce qui m'a pris.

L'homme se calme un peu et la regarde d'un air attendri.

--- Tu as l'air d'une somnambule.
Tu devrais aller dormir un peu.

--- Tu as raison, je me sens vidée.

Cathy se dirige vers les escaliers et monte les marches d'un pas lourd.
Elle ne se sent vraiment pas dans son assiette.
Elle passe le carreau qui donne normalement sur l'extérieur.
Normalement.
Comme toutes les autres fenêtres de la maison, elle est barricadée.

Frank a bien fait son travail : volets cloués ensemble, panneaux en bois sur les ouvertures qui en étaient dépourvues, du scotch sur chaque interstice, les portes bien calfeutrées...
Surtout, surtout, bien couvrir les aérations.
Jamais cette vieille bicoque n'a été aussi étanche.
Aucune lumière ne parvient de l'extérieur.
Heureusement que l'électricité est toujours là !

Elle rejoint la chambre qu'il lui a prêtée.
La chambre de sa fille, a-t-il dit.
Alors que Cathy s'allonge sur le lit, elle est saisie de vertiges.
Elle attend et la sensation passe, mais elle ne se sent pas plus alerte pour autant.
C'est comme si sa tête était enveloppée de coton.

Elle ferme les yeux et repense à son arrivée ici.
Elle avait quitté la ville pour rejoindre ses parents à la campagne.
Sa voiture allait bon train dans le crépuscule et elle espérait arriver avant le repas du soir.
Elle arrivait dans une courbe un peu traître lorsqu'une camionnette déboula en sens inverse en empiétant sur sa voie.
Elle se souvient avoir donné un coup de volant pour l'éviter.
Elle se souvient avoir aperçu, au ralenti pendant que sa voiture décollait, le regard effrayé du conducteur, et, derrière lui, d'autres individus.
Nombreux. Deux femmes, des enfants. D'autres encore peut-être.

Après ça, elle se souvient uniquement avoir ouvert les yeux pour découvrir un plafond et deux visages qu'elle ne connaissait pas encore, Frank et Nelly.
Elle était allongée dans le lit sur lequel elle est en train de s'endormir.
Nelly est infirmière et Cathy avait la chance de n'avoir aucune blessure grave : seules son arcade et sa lèvre étaient ouvertes --- elle a dû voir son volant de très près --- et quelques ecchymoses la font encore souffrir une semaine plus tard.
Nelly était en voiture au même moment --- la camionnette avait fait un écart dans le virage pour la doubler.
Au contraire du véhicule fautif, elle s'était arrêtée et avait décidé de ramener la jeune fille chez elle pour pouvoir s'occuper d'elle.

En rentrant, elle avait découvert par son conjoint les dernières nouvelles du village.
Les rumeurs les plus folles se sont répandues : les États-Unis seraient à l'origine de la maladie dans leur guerre commerciale contre la Chine, mais elle a échappé à leur contrôle ; les chiffres communiqués par les gouvernements et organisations de santé étaient bien inférieurs à la gravité réelle de la situation.
La foule a commencé à paniquer, les gens se sont rués dans les magasins pour faire des réserves en cas de quarantaine ou de couvre-feu.
Nelly et Frank ont barricadé la maison, tant pour garder les individus paniqués que le virus dehors.
Ils ont un grand sous-sol, aussi bien fourni qu'un abri antiatomique de l'époque de la Guerre froide.

Cathy aimerait tout de même une télévision ou une radio pour se tenir au courant, mais ses sauveurs ont l'habitude de prendre leurs nouvelles à l'épicerie du coin.
Nelly a récupéré ses affaires, mais son smartphone a été réduit en morceaux dans l'accident.
Seulement, après la dernière foire d'empoigne à laquelle Frank a assisté, il refuse de sortir --- et, en bon hypocondriaque, de prendre le risque de laisser entrer le virus.
Il n'aurait d'ailleurs certainement pas amené une inconnue au sein de ces murs.

Les souvenirs s'estompent, les pensées et la chambre suivent, et Cathy sombre dans le sommeil.

Elle se réveille en sursaut et ouvre les yeux.
Face à elle, le plafond et le visage de Frank, penché sur elle.

--- Qu'est-ce qui s'passe ? Nelly est rentrée ?

Frank se renfrogne et répond :

--- Non, toujours aucune nouvelle.

Cathy se redresse tant bien que mal avant d'enchaîner.

--- J'ai besoin de prendre l'air.
Je vais aller la chercher.

Les yeux de Frank ressemblent soudain à des soucoupes.

--- Tu n'y songes pas ?
Je dois te rappeler le chaos que c'est dehors ?

--- Qu'est-ce qu'on en sait ?
On est enfermés ici depuis une semaine sans rien savoir de ce qui se passe dehors !

--- Et on est en sécurité !
Regarde : Nelly a voulu sortir et elle n'est jamais rentrée !

--- Il n'y a sans doute rien de dramatique.
Elle n'est partie qu'il y a...

Cathy, encore vaseuse, regarde sa montre.

--- Cinq heures. En partant maintenant, j'ai une chance de la trouver avant la nuit.

La voix de Frank gagne en puissance, et le ton qu'il emploie ressemble de moins en moins à de l'inquiétude et de plus en plus à de la colère.

--- Reste ici, c'est le seul endroit où tu es en sécurité !
Il n'y a que la maladie et la folie, dehors !

La véhémence de l'homme qui lui fait face réveille un autre souvenir, plus proche.
Un souvenir vieux de cinq heures.
Alors qu'elle était dans la chambre, elle a entendu les voix de Nelly et Frank s'élever en bas des escaliers.
Elles s'étaient faites de plus en plus violentes, jusqu'à un bruit de porte qui claque, suivi de bruits de chutes.
Cathy était descendue pour demander si tout allait bien.
En s'appuyant sur la rambarde de l'escalier, elle avait vu le porte-manteau du vestibule à terre et Frank marchant de long en large entre la porte d'entrée et celle de la cave.
À la question de Cathy, il avait levé un visage fermé et déclaré :

--- Elle a absolument tenu à sortir pour prendre des nouvelles, peu importe mon avis.

Puis il était descendu à la cave en claquant la porte derrière lui.
