---
date: 2020-03-28T10:23:00+01:00
title: Épuisement
slug: epuisement
author: chop
license: CC BY-NC-ND 4.0

description: |-
    Il est coincé dans une réunion alors qu'il pourrait être productif.
    Puis vient la question qui le fait craquer.

inspirations: [ weekly ]
keywords: [ micronouvelle, gestion de projet, burn out ]

challenge:
  period: 2020-03-27
  rules:
  - type: theme
    constraint: épuisement
---

Il écoute d'une oreille distraite.
Comme à peu près tout le monde, il a horreur de toutes ces réunions.
Son esprit est encore concentré sur le problème qu'il s'attelait à résoudre avant que ses trois supérieurs immédiats l'invitent à se joindre à eux.
Ils se sont installés dans le bureau de l'un des vice-présidents, absent aujourd'hui.

Il s'est assis à la table ronde de façon à pouvoir garder un œil sur le couloir à travers la baie vitrée, une vieille habitude chez lui.
Son chef de projet s'est assis à sa droite.
À sa gauche, dos au couloir, c'est leur directeur commercial, avec qui il a l'habitude de travailler et échanger depuis qu'il a fait son stage dans la boîte, il y a maintenant dix ans de cela.
Le dernier directeur présent est debout et fait les cent pas.
Cela vient équiprobablement juste de sa personnalité --- il a besoin de bouger et faire avancer les choses --- ou de la tension qu'il accumule à devoir gérer un client déjà impatient.

Quatre mois qu'il a rejoint le projet.
Il est l'architecte technique de l'équipe.
Le chef de projet organise et planifie, lui trouve les solutions techniques et s'assure que son équipe ne bloque pas.
Les difficultés sont multiples sur ce projet.
Tout d'abord, il n'a pas choisi ni même été consulté sur les briques sur lesquelles on allait s'appuyer pour construire la solution.
Il ne les connaissait d'ailleurs pas et a dû apprendre.
Ensuite, ils essaient de répondre à des spécifications techniques élaborées par une organisation complètement indépendante, qui a pour seule responsabilité de dire de quelle façon on répondra au besoin qu'ils veulent couvrir.
Le seul souci est qu'ils apprendront dans quelques semaines que cette organisation apprend elle-même et dans plusieurs mois qu'ils imposent parfois des contraintes qui sont en opposition avec certaines hypothèses du même document.
Difficile de résoudre tout ça.

Mais ce n'est pas ce dont on veut discuter aujourd'hui.
Là, les chefs essaient de comprendre ce qu'on fait mal qui conduit déjà à un certain stress dans l'équipe.
Pourquoi n'arrivons-nous pas à produire quelque chose de structuré ?
On a un projet, on sait où on veut arriver.
Que diable, on a même des spécifications toutes prêtes !
Et pourtant, on galère.

Son cerveau est scindé en deux.
Une moitié n'a toujours pas décroché du point qu'il analysait au moment où il a quitté son poste, l'autre écoute aussi attentivement qu'elle le peut avec les ressources qui restent disponibles.
Le chef de projet en est à expliquer les points de blocage techniques rencontrés et les mesures que lui, l'architecte, a proposées.
Le directeur commercial se tourne alors vers lui et lui demande :

--- Et tu penses qu'on aurait pu faire mieux ?

Il est interloqué.
Le problème de ce matin est instantanément oublié tandis qu'il ouvre la bouche pour répondre, prêt à être cinglant dans sa contrariété.
Pourtant, aucun son ne sort.
Au contraire, sa gorge se noue et il sent que quelque chose ne va pas.
Il se force juste à exprimer un « excusez-moi » avant de sortir du bureau.
Il se dirige en ligne droite vers son poste, évitant de croiser le regard de qui que ce soit.
Sans s'arrêter, il attrape son badge et sort du bâtiment.
Il a besoin d'être seul.
Personne n'est à l'espace où les fumeurs se retrouvent à leur pause, mais cela ne va pas durer.
Il se dirige vers l'arrière du bâtiment, là où personne n'a de raison de passer à 10 heures du matin, et finit par laisser les larmes couler.

Les perles d'eau ruissellent sur son visage, chaudes, presque brûlantes.
Il finit par se laisser tomber par terre, assis, les genoux ramenés vers lui.
Est-ce qu'il aurait pu faire mieux ?
Celui qui lui a demandé ça est celui qui lui dit habituellement qu'il est trop perfectionniste, en lui rappelant régulièrement avec bonhommie que « le mieux est l'ennemi du bien ».

Bordel !
Il a encore une fois consacré du temps perso à chercher des solutions aux problèmes que son équipe rencontre.
Son dernier week-end de trois jours a été consacré à aiguiser sa compréhension des briques techniques et mettre au point du code qui simplifierait la vie à tout le monde.

Ce n'est pas la première fois.
Déjà début mars, sa copine l'a laissé faire à contrecœur, mais a été stupéfaite lorsque, dans un élan de frustration, il a évacué sa rage en cognant sa tête contre le bureau.
Il a d'ailleurs commencé à réaliser à ce moment qu'il n'était plus dans son état normal.
Le lendemain, il devait se rendre à Paris et a failli fondre en larmes lorsqu'il s'était rendu compte qu'il s'était trompé dans l'horaire de son train --- cela avait dû se voir sur son visage, car le contrôleur s'était montré compréhensif et ne lui avait pas fait payer de supplément.

Ce qui a été une première, c'est le message qu'il a envoyé à son manager pour lui faire part de son mal-être.
Message suite auquel il a appris que sa copine l'a devancée.
Mais elle ne pouvait pas dire qu'il se sentait pris au piège, ni pourquoi.
Des décideurs avaient repris le projet de l'an dernier, abandonné au dernier moment par le client avec six mois d'impayés, et s'étaient dit qu'on pouvait construire autre chose en se basant dessus.
Ils avaient choisi des briques présentes chez les clients afin de faciliter l'adoption de ce futur produit.
Ensuite, c'était à lui, l'architecte, de se débrouiller pour réussir à brancher tout ça ensemble.
Et s'il n'y arrive pas, est-ce que cela veut dire qu'il ne mérite pas ce rôle ?
Ses supérieurs l'ont sélectionné pour participer à un programme « hauts potentiels » qui lui donnent du travail supplémentaire.
Là encore, le simple fait qu'il n'arrive pas à faire ce qu'on attend de lui n'entre-t-il pas en contradiction avec cette reconnaissance de sa valeur ?

Il entend une voiture passer le coin de l'immeuble.
Qui peut encore arriver à dix heures et demie ?
C'est une Mustang blanche ; un seul collègue chez eux conduit une telle voiture.
Il essaie d'essuyer ses larmes, se conformant ainsi aux vieux clichés de la virilité.
« _Boys don't cry._ »
Au-delà de ça, qui aime montrer ses faiblesses ?
Mais le développeur passe sans le voir, recroquevillé qu'il est --- ou en faisant semblant de ne pas le voir.
Une question fugace lui traverse l'esprit : est-il le seul à prêter attention à ce qui se passe autour de lui ?

Puis il revient à la question qui le torture depuis une demi-heure : est-ce qu'il aurait pu faire mieux ?
Certes, c'est le travail du directeur commercial de « _challenger_ » les décisions prises, comme le veut l'argot franglais de la profession, et de voir si on aurait effectivement pu faire mieux, mais qu'il ose lui poser la question à _lui_, c'est la goutte d'eau qui a fait déborder le vase.
Il se repasse en boucle les quatre mois de galères qui viennent de s'écouler.

Il est chargé de sensibiliser les nouveaux développeurs aux méthodes de travail qui permettent d'assurer la qualité des projets.
Il apprend à tous que ça commence par prendre le temps de se poser pour choisir les outils, les procédures, s'assurer qu'on aura toutes les ressources dont on aura besoin.
Ici, on a une équipe constituée d'un noyau de quatre personnes présentes depuis le début et qui resteront jusqu'à la fin du projet, et d'un ensemble de satellites, qui n'arrêtent pas de changer au gré des personnes n'étant pas affectées à une mission.
On avance parce qu'il le faut.
On est poussé par le grand chef, qui se vante des financements qu'il a pu obtenir pour que ce projet voie le jour.
Il y a tout ce qu'il faut travailler, hormis la structure.
On a essayé de les trouver, ces outils et procédures.
On tâtonne, on ajuste, on change radicalement d'approche, on revient en arrière.

Il essaie de se calmer.
Ça fait plus d'une heure qu'il est sorti.
Ses pleurs semblent moins abondants.
La respiration, son meilleur outil pour se calmer.
Il ne retournera pas travailler, il le sait.
Il doit juste se constituer un masque pour quelques minutes.
De toute façon, il sait qu'il ne tiendra pas plus longtemps.

Il remonte.
Comme précédemment, il marche en ligne droite et évite de croiser les regards.
Il voit, en passant devant le bureau, les trois directeurs toujours ensemble.
Il ne s'arrête pas avant d'être à son poste, où il prend juste le temps nécessaire à mettre son PC dans son sac à dos, et part.
Le directeur commercial l'attend sur le chemin vers la sortie.
Il ralentit à peine, et ne sait pas s'il lui dit qu'il s'en va ou s'il se contente d'un regard.
Aucune justification supplémentaire ne sera nécessaire de toute façon, il sent que le masque a déjà commencé à se fissurer.

Le bus arrive à l'arrêt, il a le temps de monter à bord.
Son masque n'est plus.
Il envoie un message à sa meilleure amie : « Mes nerfs ont fini par lâcher. Je suis encore en pleurs dans un bus pour rentrer chez moi. »
Elle a une force et une volonté de travail phénoménales.
Elle l'appelle pour prendre des nouvelles et lui donner des conseils.

La gare, une salade achetée sur place, le train, son appartement.
Les crises de larmes alternent avec un état second, où il n'est pas vraiment à même de réfléchir.
On est jeudi, il est chez lui avant quatorze heures.
Le directeur commercial le rappelle en lui disant de rester chez lui le lendemain et de se reposer.
Ils pensent que la réunion a porté ses fruits et que la suite sera plus sereine.

Ce n'est pourtant pas la fin des galères.
