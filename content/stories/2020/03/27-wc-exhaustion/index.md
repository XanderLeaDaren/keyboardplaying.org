---
date: 2020-03-28T10:23:00+01:00
title: Exhaustion
slug: exhaustion
author: chop
license: CC BY-NC-ND 4.0

description: |-
    He's stuck in a meeting when he could be actually useful.
    Then comes the question that makes him snap.

inspirations: [ weekly ]
keywords: [ flash fiction, project management, burn out ]

challenge:
  period: 2020-03-27
  rules:
  - type: theme
    constraint: exhaustion
---

He's listening.
That is, he hears what people say.
Like so many of his fellow developers, he hates all these meetings.
They break the focus and work.
Right now, his mind is still focused on the problem he was trying to solve before his three immediate superiors asked him to join them.
They have moved into the office of one of the vice-presidents, who is absent today.

He sat down at the round table so he could keep an eye on the hallway through the bay window.
An old habit of his.
His project manager sat to his right.
On his left, with his back to the corridor, is their sales manager.
He's been used to working with and talking to him since he did his internship in the company, ten years ago.
The last director in the meeting is standing and pacing.
It's probably just his personality---he needs to move and move things forward---or the tension he accumulated from having to deal with an already impatient customer.

Four months since he joined the project.
He is the technical architect of the team.
The project manager organizes and plans, finds technical solutions for him and makes sure his team doesn't get stuck.
There are many difficulties on this project.
First of all, he did not choose the bricks on which the solution would be built, nor was he even consulted.
He didn't know them and had to learn.
Second, they are trying to meet technical specifications developed by a completely independent organization, whose sole responsibility is to say how the need they want to cover will be met.
They learned two weeks ago that this organization itself was not expert on the topic.
In a few months, they'll have to deal with constraints that go against the very hypotheses of the document.
Solving all this is not an easy task.

But that is not today's discussion topic.
Today, the leaders are trying to understand how comes the team is under this constant stress.
Why can't we come up with something structured?
We have a project, we know where we want to get to.
What the hell, we even have the specifications ready!
And yet we're struggling.

His brain is split in two.
One half still hasn't gotten off the point he was analyzing when he left his desk, the other half is listening as carefully as it can with the scarce resources that are still available.
The project manager is explaining the technical bottlenecks they encountered and the measures that he, the architect, has suggested.
The sales manager acknowledges and then turns to him and asks,
"And do you think we could have done any better?"

He is taken aback.
This morning's problem is instantly forgotten as he opens his mouth to answer, ready to be scathing in his answer.
Yet no sound comes out.
On the contrary, his throat closes and he senses that something is wrong.
He just forces himself to say "I'm sorry" before leaving the office in a hurry.
He makes a bee line to his desk, avoiding anyone's gaze.
Without stopping, he grabs his badge and walks out of the building.
He needs to be alone.
No one is in the smoking area, but that's not going to last.
He heads to the back of the building, where no one has a reason to come by at 10 a.m.
By the moment he reaches the first corner, the tears already flow freely.

Water pearls stream down his face, hot, almost burning.
He ends up dropping to the floor, sitting with his knees brought back to him.
Could he have done any better?
The one who asked him this is the one who usually tells him that he is too much of a perfectionist, regularly reminding him with a trait of humor that "the best is the enemy of the good."

Damn it!
Once again, he has devoted his personal time to searching solutions to his team's problems.
His last three-day weekend was spent sharpening his understanding of the technical bricks and developing code that would make life easier for everyone.

It's not the first time.
Already in early March, his girlfriend reluctantly let him do it, but was stunned when, in a fit of frustration, he vented his rage by banging his head against his desk.
He began to realize at that moment that he was no longer in his normal state.
The next day, he had to go to Paris and almost burst into tears when he realized that he had taken the train after the one he had reserved---it must have shown on his face, because the controller had been understanding and had not charged him extra.

What was a first was the message he has sent to his manager tell him his discomfort.
In the answer, he learned that his girlfriend had beaten him to it.
But she couldn't say that he felt trapped or why.
Decision makers had taken over last year's project, abandoned at the last minute by the client with six months of unpaid bills, and figured it could be used to build something else.
They chose bricks their customers use to facilitate the adoption of this future product.
Then it was up to him, the architect, to figure out how to connect it all together.
And if he can't do it, does that mean he doesn't deserve the role?
His superiors have selected him to participate in a "high potential" program that gives him extra work.
Again, does not the mere fact that he cannot do what is expected of him contradict this recognition of his worth?

He hears a car driving by the corner of the building.
Who can still arrive at work at half past ten?
It's a white Mustang; only one person in the building drives such a car.
He tries to wipe away his tears, conforming to the old clichés of virility.
"Boys don't cry."
Beyond that, who likes to show weakness?
But the developer passes by without seeing him, cowering as he is--or maybe just pretending not to see him.
A fleeting question crosses his mind: is he the only one who pays attention to what's going on around him?

Then he returns to the question that has been torturing him for half an hour: could he have done better?
Of course, the commercial director is only doing his job when he challenges the decisions that were made.
It's how he can determine if things could have been done better, but daring to ask _him_ such a question was the last straw.
Four months of endless problems flash repeatedly before his eyes.

He's in charge of making new developers aware of the working methods that ensure the quality of projects.
He teaches everyone that it starts by taking the time to settle down to choose the tools, the procedures, to make sure all the required resources have been identified and will be available.
Here, the team is made up of a core group of four people who have been there from the beginning and who will stay until the end of the project, and a set of satellites, which keep changing at the whim of people who are not assigned to a mission.
They're pushing forward because they have to.
Because the big boss is pushing them too.
He's bragging about how this project will be the future of this office, how he could get funding from the group to get this project off the ground.
Everything that's required to work is here.
Everything but a structure.
They tried to find these tools and procedures.
They're groping, adjusting, radically changing their approach, going backwards...

He tries to calm down.
He's been out for over an hour.
His crying seems to have lessened.
Breathing, his best tool for calming down.
He's not going back to work, he knows that.
He just needs to put on a mask for a few minutes.
Either way, he knows he can't hold out much longer.

He's going back up.
As before, he walks in a straight line and avoids making eye contact.
As he passes the office, he sees that the three directors are still together.
He doesn't stop until he's at his desk.
He won't stay there for even a minute, just grabbing his backpack and pushing his laptop into it before leaving again.
The sales manager is waiting for him on his way out.
He barely slows down, and doesn't even know if he tells him he's leaving in so many words or if he just gives him a look.
The manager won't ask him anything anyway, they know each other too well for that.
He feels the mask has started to crack.

The bus arrives at the stop, but a brisk walk gets him there in time.
His mask is gone.
He sends a message to his best friend: "My nerves have finally given way. I'm still crying on a bus home."
She has phenomenal strength and willingness to work.
She calls him to check in and give him advice.

The train station, a salad bought on the spot, the train, his apartment.
The crying fits alternate with a second state, where he is not really able to think.
It's Thursday, he's home before two o'clock.
The sales manager calls him and tells him to stay home the next day and rest.
They think that the meeting has been successful and that the rest will be more serene.

They'll still meet their share of problems, though.
