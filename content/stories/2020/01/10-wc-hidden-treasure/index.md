---
date: 2020-03-01T18:45:31+01:00
title: A Hidden Treasure
slug: a-hidden-treasure
author: chop
license: CC BY-NC-ND 4.0

description: |-
  I'm going through the city to find this tree I discovered yesterday.
  I need to see it again.

inspirations: [ weekly ]
keywords: [ flash fiction, city, treasure, use the words ]

challenge:
  period: 2020-03-01
  rules:
  - type: general
    constraint: 'Use all the following words: tree, capillary, exaltation, Lambic, pizza, jump seat, urban'
---

I see the landscape passing, but I'm in a hurry to get there.
That's not because this jump seat is uncomfortable---though it _is_---but this urban crossing feels endless.

I'm impatient!
I need to find this tree again.
It was so high, so imposing...
No, "majestic."
That's the word!
What a hidden treasure behind the curve of an alley in this dreary city!
Merely seing it made me feel an exaltation I never knew, yesterday night.
I really need to see it by day.

Today, the exaltation I feel is capillary.
My skull hurts.
Yeah, I know, there's no nerve in the skull because it's a bone, but it feels like my hair is drilling through it.

There, that's the stop!
I jump out of the bus and head to the place where...
No, I can't be mistaken, it was here!
Why is there only this dead shrub here?!
No, I must be wrong.

What did I step in?
Puke?! Charming!
Wait, what are those weird bits?
Pineapple...
Fork, I remember...
I was the one who puked!
Those are the remains of the hawaian pizza I had.

Then, this dead tree?
I understand now why the pals were doubled up.
I really must stop with Lambic nights...
