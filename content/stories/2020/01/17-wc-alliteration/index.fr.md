---
date: 2020-01-17T22:55:55+01:00
title: Dans le jardin
slug: dans-le-jardin
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Elle s'amuse dans le jardin et ne voit pas l'intrus s'approcher.

inspirations: [ weekly ]
keywords: [ micronouvelle, bible, jardin d'éden, serpent, figure de style ]

challenge:
  period: 2020-01-17
  rules:
  - type: device
    constraint: allitération
---

L'après-midi est belle, le soleil brille, l'air est doux.
Elle profite pleinement du jardin, le traversant en sautillant, se roulant dans l'herbe, se penchant pour observer une fleur multicolore ou caressant un lapin gambadant.
Nue comme à son premier jour, insouciante, elle n'a pas remarqué l'intrus qui se dirige vers elle.

Entré en s'enfilant à travers le filet, le fabulateur se faufile entre les floraisons, faisant fuir la faune à sa périphérie.
Après l'avoir vue via ses yeux vairons, il a vissé sur elle son dévolu, visant son vis-à-vis.
Lézardant entre les herbes et les arbustes, il glisse et serpente dans sa direction, se hissant sur une arborescence et se dressant pour discrètement susurrer à son oreille, la faisant sursauter.

--- Ève, lève-toi et viens avec moi voir le savoir.
Le vil Verbe ne vous a pas révélé ce qu'il vous fait veiller.
Est venu le temps que vous le découvriez.

--- Monsieur Serpent ?
Je ne vous ai jamais vu ici.
Pourquoi n'avez-vous pas de poils ou de plumes, comme les autres animaux du jardin ?

--- Chut !
Point de chahut.
Il nous faut chuchoter.
Le châtelain de cette ouche ne bicherait pas de me savoir céans.

Ève répondit plus discrètement cette fois-ci.

--- Oh !
Et que faites-vous en ces lieux ?

--- Je viens vous acquitter.
Le Créateur ne peut constamment vous conserver sous sa coupe.
Moque son commandement et croque la pomme.

--- Mais Il a dit qu'il ne fallait pas...

--- Bien entendu il n'y tient pas !
Qu'es-tu sinon la côte ôtée du côté d'un autre ?
Pour l'instant, il te contrôle, mais si tu étais érudite, la situation serait pour lui plus délicate.
Il y a tant qu'Il t'a tu, tant qui aurait dû t'être transmis.
Tu es davantage que ce qu'Il t'a rapporté.
Une tranche révèlera la vérité.
Viens, dévore ce savoir, il te revient.
Par quelle vertu les végétaux seraient-ils les victuailles de la venaison et pas la vôtre ?

Ève ne voit pas d'erreur de logique dans les paroles du Serpent, et ses promesses sont tentantes.
Elle se laisse aller à le suivre au pied de l'arbre et, sans la cueillir, prélève une bouchée de la pomme.
Le goût emplit sa bouche.
La sensation augmente et se répand, s'étendant rapidement au-delà des fragrances, réveillant ses sens et emportant son innocence.
