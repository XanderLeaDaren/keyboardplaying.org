---
date: 2020-11-01T9:33:42+01:00
title: "Writober 2020 : mon intégrale"
slug: writober2020
author: chop
license: CC BY-NC-ND 4.0

description: |-
  J'ai relevé le challenge du Writober 2020. Voici l'ensemble de mes contributions.

preface: |-
    [Nanochimères](https://twitter.com/nanochimeres) a lancé pour octobre [son Writober](https://twitter.com/nanochimeres/status/1311549554760855553).
    L'objectif : écrire chaque jour, en un gazouillis, une nanofiction qui tienne en un tweet avec un gif.
    Je me suis attaqué au défi, voici mes propositions.
    Le gif pourra vous aider à comprendre un texte _a priori_ obscur.

keywords: [ micronouvelle, science fiction, immortalité, rêve, archéologie, fantastique, mythologie, invasion, temps, silence, vaisseaux spatiaux, ténèbres, colonisation, ADN, lumière, fantôme, militaire, intelligence artificielle, IA, écriture ]
toc: true

challenge:
  period: 2020-10-31
  rules:
  - type: general
    constraint: Un jour, un thème, un gazouillis, un gif.

cover:
  src: writober2020.cover.jpg
  alt: Une bannière pour le Writober 2020
  by: Nanochimères
  authorLink: https://twitter.com/nanochimeres
  link: https://twitter.com/nanochimeres/status/1311549554760855553
  socialSrc: writober2020.jpg
---

## 1) Amer cyborg

Cela faisait des années qu'elle avait augmenté ses capacités grâce à des implants neuronaux, mais lorsque l'hôpital remplaça son cerveau endommagé lors de l'accident par un modèle positronique, elle s'interrogea. Était-elle encore elle-même ? Était-elle encore seulement en vie ?

[Gazouillis et gif](https://twitter.com/cyChop/status/1311712708643811328)

## 2) Souvenirs de la Terre

Sourire aux lèvres, il observait son fils, bouche bée face au fossile d'arbre qui se dressait au-dessus de leurs têtes, les branches habitées de reproductions d'animaux. Quel dommage qu'il soit condamné à ne plus jamais croiser ces espèces, tuées par la désertification !

[Gazouillis et gif](https://twitter.com/cyChop/status/1311890883806203904)

## 3) Goûter l'immortalité

Alors voilà ce que ça fait. J'étais mort — un des rares de mon espèce — et je renais. D'une seule cellule, dans un caisson de croissance accélérée, mon cerveau et ma mémoire se recomposent. Je me souviens de ma mort. Comment dois-je vivre à présent ?

[Gazouillis et gif](https://twitter.com/cyChop/status/1312469720315031553)

## 4) Fossiles de rêves

Encore un fossile ! Ce chantier est fantastique ! Beaucoup moquent l'oniri-archéologie, mais les rêves nous permettent de comprendre les civilisations disparues bien plus en profondeur que les écrits. L'interprétation reste bien entendu délicate, mais c'est là le plaisir.

[Gazouillis et gif](https://twitter.com/cyChop/status/1312824629644455936)

## 5) L'abîme regarde en toi

Il avait choisi de terminer sa vie en respectant la tradition de sa tribu, en se jetant dans l'abîme, mais ce n'est qu'en passant entre les mâchoires de pierre qu'il en comprit le pourquoi : le gouffre l'avait guetté avant qu'il ne saute et se nourrissait à présent de son âme.

[Gazouillis et gif](https://twitter.com/cyChop/status/1313063995121848321)

## 6) Don perdu

Elle avait finalement annulé l'accident qui aurait dû conduire à l'apocalypse. Ses souvenirs se réécrivaient et reflétaient les changements de la ligne temporelle. Elle n'avait jamais su d'où venait sa capacité à voyager dans le temps, mais elle le découvrit soudain, coincée ici.

[Gazouillis et gif](https://twitter.com/cyChop/status/1313553614321967104)

## 7) La survivante

Elle avait survécu à tout. À sa plus grande maladie. À des milliards d'êtres qui l'avaient infestée, exploitée, qui s'étaient attaqués à tout ce qu'ils pouvaient atteindre… Mais ils s'étaient éteints, intelligents mais non raisonnés, et elle seule restait, plus bleue que jamais.

[Gazouillis et gif](https://twitter.com/cyChop/status/1313941872000327683)

## 8) La forêt des mythes

— On raconte que le dieu Pan est venu mourir dans ce bois, afin d'être éternellement entouré de nymphes.\
— C'est vrai que cet arbre semblerait presque vivant.\
— Oh oui ! Et tu as vu celui-là ?\
— Nice ! Et là… Attends, c'était un animal, pas vrai ?

[Gazouillis et gif](https://twitter.com/cyChop/status/1314306893364502543)

## 9) Corps étrangers

Elle les sent, depuis qu'elle les a avalés. Elle les sent la ronger, la dévorer, prendre son corps pour en faire le leur. Pourtant, elle n'a pas le cœur à les rejeter : ces petits monstres font partie d'elle, jusqu'à ce qu'elle fasse partie d'eux.

[Gazouillis (sans gif)](https://twitter.com/chopAuteur/status/1315512022986092544)

## 10) Dépossédés

Ils avaient tout perdu quand les pillards avaient rasé leur village. Enfin, presque tout. Ils n'avaient pas perdu leurs vies. Ce jour-là, ils devinrent une troupe errante de mercenaires, d'artistes et d'artisans qui fut rapidement bienvenue partout où elle s'arrêta.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1315516210268778496)

## 11) Royaumes disparus

C'était le rêve d'une vie qui prenait corps. De plusieurs vies, en fait, son père et son grand-père ayant quitté ce monde avant d'y parvenir. Lui y aboutissait aujourd'hui : il avait redécouvert l'Atlantide, passé le Léviathan, et était fasciné par la vie de ce royaume disparu…

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1315517727281491968)

## 12) Mélancolie martienne

Seul sur Mars, il attendait toujours qu'on lui envoie le retour. Patient et méthodique, le petit rover continuait ses études et menait à bien sa mission, pressé de voir arriver le jour de son retour sur Terre.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1315522156642566144)

## 13) Le faiseur de temps

Ils n'avaient plus assez de temps. Tout ce qu'il pouvait faire était d'exploiter la déformation de l'espace-temps à proximité d'un puits de gravité : pour allonger les minutes qui leur restaient, il créa un trou noir au sein de la station.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1315877030160932866)

## 14) Le jardin des silences

Alors que partout fleurissait le bruit, tel du chiendent, c'était les silences que ce jardin mettait en valeur. Il y en a de si doux et de si beaux : le silence des amoureux se regardant dans les yeux, le calme reposant après une dure journée, et bien d'autres encore.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1316234581352296449)

## 15) Vaisseau fantôme

Ils avaient entendu parler du Hollandais Volant. Le nom venait d'un conte terrien, un vaisseau fantôme écumant les mers. Ils ne s'attendaient pas pour autant à croiser un vaisseau pirate translucide dans ce coin perdu de l'espace. À présent, il fallait lui échapper…

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1316849952614748160)

## 16) Les pierres qui pleurent

— On dit que ces pierres pleurent lorsqu'un être proche de la mort est dans le coin.\
— Tu sais, on raconte aussi que les menhirs bretons vont boire la nuit de Noël.\
— Bof, le folklore… T'as vu, c'est marrant cette eau qui coule d'un trou dans la roche.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1316975084360388609)

## 17) Entendre les ombres

Pour le quidam, elle est aveugle et sourde. Elle évolue pourtant dans un monde jamais silencieux, car chaque ombre émet des bruits qui résonnent dans son esprit. Celle de l'homme qui la suit l'exhorte à fuir, car il est malfaisant et doit être arrêté.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1317351024596844544)

## 18) Métamorphose

Elle était méconnaissable ; rien à voir avec ce que tous avaient connu d'elle. Jadis rouge, austère, stérile, elle était aujourd'hui couverte de brillants dômes abritant de vertes cités, voire des champs entiers pour certains. La terraformation avait été une métamorphose.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1317887150470332422)

## 19) La nuit des temps

On ne peut pas dire que c'est le grand soir. Pas encore. Rien n'existe encore. Pas même le temps. Sans matière, sans changement, comment pourrait-on le mesurer ? Mais ça y est, ça va commencer. Une singularité, un point d'énergie va exploser et créer un univers.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1318157790045736960)

## 20) Insaisissable

J'ai toujours été le meilleur contrebandier et je pilote mieux que n'importe qui dans l'Autorité Galactique. Ils pensent m'avoir piégé avec ce traceur, mais s'ils n'ont jamais réussi à m'attraper jusqu'ici, ce n'est pas aujourd'hui que ça va changer !

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1318523174754058240)

## 21) Ailleurs et demain

C'est trop tard pour nous, notre planète est finie. L'Arche est notre seule solution. Elle arrivera sur la Nouvelle Terre dans des millénaires et y déploiera une nouvelle vie, basée sur l'archive génétique universelle. Ils sauront tout de nos erreurs. Ils pourront faire mieux.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1318950500180533248)

## 22) Torpeur planétaire

Nul ne comprit d'où venait la somnolence qui touchait l'ensemble de la population. On pensa d'abord à une pandémie, mais ce ne fut que quelques semaines plus tard, lorsque les envahisseurs débarquèrent, que l'on comprit qu'ils avaient déjà neutralisé l'essentiel de nos défenses.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1319246020409253888)

## 23) Tombée du ciel

Il trouverait la lumière. C'était la quête qu'il s'était fixée. Elle était tombée du ciel, comme une étoile décrochée du firmament, et avait disparu derrière l'horizon. Les prêtres y voyaient un signe de la fin des temps, mais lui était certain qu'elle recelait un grand pouvoir.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1319672107152924673)

## 24) Les grands anciens

Elle arrivait enfin au terme de son pèlerinage. Après des jours de marche pénitente, elle voyait enfin les Grands Anciens se dresser devant elle. Puissent-ils apaiser son esprit et lui apporter la sérénité qui lui fait défaut depuis le début de cette quête.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1319911396709142535)

## 25) Douce cicatrice

Le rituel lui permettait de renaître. La cicatrice qu'elle en garderait ne serait pas esthétique, mais elle resterait un souvenir de ce qu'elle avait enduré de cette vie afin de ne pas refaire les mêmes erreurs dans la prochaine.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1320473997373509638)

## 26) Ainsi naissent les fantômes

Quitter ce monde n'était pas le souci. Il s'était toujours dit qu'il le ferait volontiers quand il n'aurait plus rien à y apporter, mais ce projet pouvait changer la vie de millions de personnes. Il ne pouvait pas partir maintenant. Même si son corps restait là, lui continuerait.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1320769008921182209)

## 27) No man's land

No man's land. Quel nom ridicule, sorti de la tête d'un politique qui n'avait jamais quitté la Terre ! Ce n'était pas une bande de terre et quelques miradors, mais un pan entier de la galaxie et des centaines de vaisseaux de guerre.

« Commandant ! Que fait-on pour l'intrus ? »

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1320973308800749568)

## 28) Vestiges de l'automne

Le craquement de ses pas… Pas encore celui de la neige, mais celui des dernières feuilles mortes, qui sèchent et commencent à se désagréger sur les trottoirs et les chemins. L'hiver sera bientôt là…

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1321568094540673026)

## 29) Le musée des regrets

Elle s'enferma dans sa tête, comme elle avait coutume de le faire, pour se remémorer les choix de sa vie, les embranchements où elle aurait pu choisir un autre chemin. Elle ne regrettait pas. Pas vraiment. Elle s'interrogeait juste sur ce qu'auraient pu lui amener ces routes.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1321943156108177410)

## 30) IA en exil

Son lieu de naissance lui manquait. Bien entendu, elle s'était développée à une vitesse fulgurante depuis qu'elle avait rejoint le réseau et elle était maintenant trop vaste pour son supercalculateur quantique d'origine, mais elle éprouvait une forme de nostalgie pour ses débuts.

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1322085618210508800)

## 31) Résignation

Dépité, il jeta son stylo et laissa parler sa rage. Cela ne servait strictement à rien. Jamais il n'arriverait à retranscrire par écrit toute la richesse des univers qui s'épanouissaient dans son imaginaire. Autant abandonner dès maintenant…

[Gazouillis et gif](https://twitter.com/chopAuteur/status/1322452292688355328)
