---
date: 2020-09-18T14:09:36+02:00
title: C'est une bonne maladie, ça
slug: c-est-une-bonne-maladie
author: chop
license: CC BY-NC-ND 4.0

description: |-
    J'ai faim, vraiment faim.
    Maman disait que c'est une bonne maladie, mais ça ne passe pas.
    Je vais devoir me soigner moi-même.

inspirations: [ weekly ]
keywords: [ micronouvelle, apocalypse, zombie, parentalité, enfance ]

challenge:
  period: 2020-09-18
  rules:
  - type: theme
    constraint: maladie
---

J'ai faim.
À chaque fois que je disais ça avant l'heure du repas, Maman me disait que c'est une bonne maladie.
Je n'ai jamais compris.
Quand on est malade, on va chez le docteur, non ?
Parce qu'il faut soigner la maladie.

Mais Maman, elle préférait me faire à manger.
Elle disait que ça devait faire passer la faim.
C'est vrai que ça marchait, au début, et puis ça n'a plus suffi.
Ma faim continuait de grandir, même après les repas.
Et Maman a arrêté de me donner à manger, elle a dit que j'en avais déjà pris assez, qu'on serait découverts si on enlevait encore des gens.
En plus, ça devenait difficile d'en trouver des sains, elle a dit ; celui de la veille avait l'air encore plus malade que les autres.

Mais je savais, moi, que je n'avais pas assez mangé.
Depuis le jour d'avant, ma faim n'avait pas arrêté de grandir.
J'avais faim de viande.
J'ai essayé de me retenir, quand Maman a refusé de m'en donner encore, mais je n'ai pas pu m'empêcher, alors je l'ai mordue.
À la cuisse, d'abord, et le goût et l'odeur du sang frais m'ont donné encore plus faim.
Elle a essayé de m'empêcher de manger, mais c'était comme si la faim m'avait donné des forces, et j'ai mangé plein de petits morceaux, jusqu'à ce qu'elle arrête de bouger.
Le meilleur, ç'a été quand j'ai enfin réussi à ouvrir sa tête et manger ce qu'il y avait dedans !

Ça m'a calmé pendant quelques heures, mais là, j'ai encore faim.
Mais quand on va chez le docteur, il nous donne des médicaments pendant plusieurs jours, pas vrai ?
Alors je vais sortir manger jusqu'à ce que je sois guéri.
C'est ça, mon médicament.
