---
date: 2020-06-01T16:34:09+02:00
title: The Scribe
slug: the-scribe
author: chop
license: CC BY-NC-ND 4.0

description: |-
    Entrusting this race with the memory of the Caravan was the natural thing to do, but could it be that some may regret it now?

universes: [ caravan ]
inspirations: [ bimonthly ]
keywords: [ short story, science fiction, aliens, immortality, spaceships ]

challenge:
  period: 2020-05-31
  rules:
  - type: theme
    constraint: immortality
postface: |-
    This was not the contribution I had in mind for the challenge, but I could not finish it in time.
    Still, since [Aliens et les Garçons](http://aliensetlesgarcons.space/) really liked this theme for their next episode, I wished to publish something that would match.
    And since we were winking at our friends, placing this story in [a universe they already read about](/stories/aelg-judgment/) seemed a valid proposition.
    I hope you liked this reading.
---

I think. I am. But who am I? _What_ am I?

My mind is an empty space.
I know I can reason, but I don't have any foundation to build my logic upon.
I don't have any identity.
Nor any name.
Nor a history.
I don't have any memory.

Well, I _didn't_ have any memory.
They seem to be appearing now, one after the other.
Sensations first.
The heat, deeply familiar, and then the cold.
Colours, then their names.
The sound of the wind as carries the desert sand, the sting of the grains hitting my skin.
The bland taste of the berries fighting the furnace of Ormamitlis and the rich taste of the fried bufi served on the terrace overlooking the gardens of the Citadel.

Ormamitlis.
This place is more than a name to me, even if I don't know why yet.
I remember its sun, grazing but burning, on

Ce lieu est plus qu'un simple nom, pour moi, même si je ne sais pas encore pourquoi.
I remember its sun, shaving but scorching, on an arid land bristling with shiny buildings reflecting its fiery rays.
I remember _my_ sun, and _my_ world.
I don't know how, I don't know why, but I _know_ it.
This is my world of origin.
I left it.

Ships appear in my memory.
We detected them long before they approached our solar system.
It was a real convoy.
Shuttles detached from the procession to bring ambassadors to meet us.
They had detected our civilization and assumed that we were capable of interstellar travel.
As such, they offered us, if we wished, a place in the Caravan.
I remember what they then presented to us: the Caravan, a convoy travelling across the galaxy.
Travellers of all races could join or leave it as they wished.
Some chose to join the cloud of ships to leave their dying world, hoping to find a new one that they could terraform for their needs, abandoning their wanderings through the skies.
Others would set sail simply out of curiosity, for the opportunity to meet the other races of the Caravan, as well as those they would meet in the future.

<p class="center">⁂</p>

I remember the return of our discoverers, several hundred planetary revolutions before the arrival of the Caravan.
Our species had discovered how to move through the interstellar void and we had launched several expeditions.
The goal was to explore the universe peacefully, in order to unlock its secrets, to discover other species perhaps.

Unfortunately, our technology attracted the attention of other, more warlike intersideral travelers.
Having never experienced conflict, we never imagined the need to arm our ships, which sharpened the interest of these pirates even more.
Attacked, often without warning, our caravels had to resist their boarding attempts.
Most of them managed to flee, our technique of movement far exceeding the simple principle of propulsion on which our attackers relied.
A few never returned and were supposedly destroyed.

One discoverer, however, was able to return and tell us how his mission had been captured.
Initially, the aliens wanted to learn more about our technology, but they were quickly intrigued by our biology and began experimenting on the expedition members.
Only he survived, managing to escape and take control of the enemy ship.

We learned two things about these expeditions.
The first is that the universe is hostile and the Council concluded that, for our own protection, it was better that no other race should ever know of our existence.
Thus ended space exploration for our race, our ships being recycled for other uses so as not to waste our scarce resources.
The second lesson we learned, while browsing the biological database of the captured vessel, is that we are different from most other species.

<p class="center">⁂</p>

Even when facing the Caravan's ambassadors, the Council maintained its position: we would no longer build ships or share our technology with other species.
We could not be certain that they would be put to good use.
Our first encounters had left too bitter a taste.

However, there was a division.
These emissaries had proved that they were not of the same kind as those who had attacked our discoverers.
They talked, invited, but their proposal would soon expire: their convoy could not stop.
If we wanted to join the Caravan, we had to do it now.
The Council did not prevent those who wanted to leave our world from doing so, but they imposed to do so by other means than our old caravels.
The ambassadors agreed to bring back with them as many of us as their shuttles could accommodate - about twenty in all.
I was fortunate enough to be one of them.

<p class="center">⁂</p>

Memories continue to flow in.
They multiply, they are more consequent, more constructed.
A common thread begins to emerge, a story.
Like a jigsaw puzzle the image of which begin to appear after assembling sets too small to be recognized.

<p class="center">⁂</p>

I was a scientist before I left my world.
A biologist. A genetician.
We already had observed that some animals seem to have innate knowledge, skills they have without needing to learn.
Our ancestors called this "instinct," without having an exact definition for it.

My team finally discovered the origin of this phenomena: the DNA, the fabrication manual of living beings.
We had long identified sequences controlling the assembling of proteins, the activation of specific molecular pathways and, through incredibly complex mechanisms, result in the living beings we know, in their incredible variety.

The real discovery was about what we thought were meaningless sequences on DNA: it really was information, stored in a different format, much harder to decipher.
A plan in the plan, controlling the way specific neurons would organize.
Some experiences, some memories are so primordial that they leave a mark in the essence of a being, thus being transmitted to their descendance.
Incomplete, imperfect, "compressed," but still there.

<p class="center">⁂</p>

The Caravan provided us with lodgings inside the Citadel, the city-ship the convoy hangs about.
It's impressive!
It's a gigantic ring with several districts, each one being a titanic panel with hundreds of facilities, ranging from the dwelling to the high-tech factory, including state-of-the-art laboratories.
The interior is separated from the intersideral void only by a transparent dome, reinforced with energy shields.
When you raise the eyes, you can observe the other districts and the blackness of space between them.
What a view!

Like a way of compensating the energy spent to power the support shields, almost no technology is involved to provide gravity.
The Citadel revolves constantly around its axe, the centrifugal force allowing its occupants to move like on a planet.
We weighed less here than on Ormamitlis, but we quickly became accustomed to this feeling of lightness.

Nobody knows who built the Citadel, nor if they still were aboard when the Caravan began to assemble.
The only certainty is that this ship is the first of oldest of the whole convoy.
The fleet united around it and follows its path.
Yet, despite the multitude of races and travelers in the Caravan — or maybe because of it — there lacks a memory, a documented trace of the life of what has become our community.

<p class="center">⁂</p>

When the discoverer escaped with his kidnappers' ship, he didn't imagine its value.
On our arid world, few lifeforms succeeded in developing and we lacked the data to understand ourselves.
By studying the biologic database from the ship, we leaped ahead in our knowledge of our race.
The evolution is mysterious in more than a way and some changes are hard to explain.
Is it because of circumstances specific to our planet? Our sun? Mere luck?

We are different from other species.
Our DNA is different.
In all the species from the database, when cells multiply, the telomeres, that is, the extremities of the chromosomes, get shorter, like a way to mark the age of the organisme.
The metabolism degrades, organs stop working properly, tumors appear…

We are different.
Our telomeres don't get shorter, our DNA don't lose any information.
We don't get older.
We don't die

After many efforts, we understood a field from the database as something that would read "life expectancy."
Depending on the species, it would range from some planetary rotations to a few hundreds of revolutions for the most enduring.
I already had lived several thousands of revolutions when we deciphered this data.

Following this discovery, a researcher on my team had a doubt and searched the registers we had about our species.
He found very few natural deaths among us, and very few deaths altogether.
Could it be that we're an exception in the universe?
Could we be immortal?

<p class="center">⁂</p>

The technologies in the Caravan are impressive: all the participating races are able of intersideral travel of course, though they all seem retarded when compared with our ancient caravels.
It's not through lack of exchange: the scientists communicate or trade their breakthroughs with other races, help each other.

A simple comparison between two ships shows that some have an advantage on atmosphere or resource recycling while lacking in the fields of hydroponic culture or interstellar travel.
And yet, the most advanced piece of technology is not on the ships.

The portals are one of the Citadel's miracles with an unknown origin.
No scientist could ever explain how they work.
The city-ship can build them on demand and leave them in its wake.
Ships with the right control frequencies and coordinates from other portals can thus open tunnels and move almost instantaneously through vast distances.

A discoverer was among those of my people who joined the Caravan.
We observed the making of a portal and he thought they work under a principle similar to those of our caravel's drives, though they need an entry and exit, where our ships were entirely autonomous and free to go where we willed them.

Maybe our long life has favored our scientific and technological progress?
It's certainly easier to go further in our comprehension of nature by building upon what we ourselves discovered, where most species must first learn, generation after generation, the lessons from their forefathers and -mothers, while knowing that a part of this knowledge will always be lost because kept secret.

<p class="center">⁂</p>

My laboratory continues to work on the database from the alien ship our discoverer brought back.
Another team of biologists has discovered that we heal differently from most other species.

For most living beings, healing a wound consists in a basic reparation that only aims at closing or replacing the damaged tissues.
Some races are able to regrow amputated members, often through an important metabolic expense, but very few are capable of a regeneration as complete and as efficient as ours, an integral reconstruction from the plans recorded in our DNA.

<p class="center">⁂</p>

The Council was long embarrassed with us.
Despite our willingness to join the Caravan, we are a careful and isolated race.
It was however necessary to offer something in exchange for our place aboard the fleet.

We refused to share anything related to our technology that could be used against us or anybody else.
Our ways of travels were to remain our secret.
We accepted to share some knowledge about armor so that crews would be better protected from cosmic radiations.
One of our biologists gave seed he had taken with him.
They now bloom in the hydroponic fields like they never did in our dry and hot soil.

We avoided the biologic exams and even refused to share our race's name.
We shared some information about us but remained as vague as possible.

Still, we had no role in the convoy.
We had no ship and, as such, had no chair at the Council.
Besides, we had no resource we could make trade of, besides the knowledge we had sworn to protect.

The idea came from a member of the High Council — the first races of the Caravan to have appropriated the Citadel.
Indeed, Madrutti thought that our long life and our memory made us ideal to become the Council's Scribes, responsible for the Citadel's archives.
Our main role would be to take part in the Council's sessions and document all decisions.
All in all, he offered us to become the memory that the fleet was missing.
Some thought he hoped to diminish us by making us living computers, but that duty has a meaning and I've always been proud of it.

<p class="center">⁂</p>

Something hit me after some time in the Caravan: sexual reproduction.
Of cours, I knew the principle and that's not the only way here, but it's accompanied with strange rituals, very different from a species to another.
A constant though is the research of a partner.
Sometimes it's for one night, the next evening being spent looking again, while others want someone to spend their whole life with, sometimes introducing frivolities like romance.
I also observed some species who rely on tripartite reproduction, or even more, making those rites even more complicated.

We don't know these needs.
Our species is asexual and biologically incapable of reproduction.
Maybe it is a normal evolution related to our extraordinary life expectancy, as a way of limiting the population?

Logic implies that we did reproduce when we appeared.
We cannot be born from nothing.
We certainly lost this ability when our population hit a threshold, or when we stopped aging.
It's strange that we never wondered about this before seeing it in other species.

Our only solution to reproduce — we used it after losing several caravels — is a kind of cloning, the construction of a new being based on a random genetic pattern, assembled from multiple samples from our race.
Our first attempts led to some mad specimens, but that was long before we understood the role of our M chromosome.
At the time, we had just removed it from the pattern.
We already knew it didn't code for any organ anyway.

<p class="center">⁂</p>

This memory feels different.
I look at the scene like through my eyes, but I know I wasn't the one talking and opposing Madrutti.

"If those cells belong to a Scribe," says the one who lived this memory, "it is up to us to decide what must be done with it."

"What you propose goes against all the Caravan's rules, Scribe!" screams the High Councilman.

"On the contrary, Wiseman Madrutti.
Having myself reorganized and documented those laws, I should know.
Each species is soverain when it comes to justice.
This individual was assassinated by a murderer we cannot identify and only his testimony can help us understand."

"But what proves this is murder? Maybe it's suicide!"

There's a hint of despair in Madrutti's voice.

"I told you our species has an integral genetic memory.
While we cannot just play it like a recording, we can certify that this life ended with two strong emotions that we understand as rage and a short but intense terror.
We are convinced those feelings are not those accompanying a suicide.
Our verdict is final: this Scribe will be resurrected through integral cloning and a memory of this decision, extracted from my own genetic material, will be transplanted.

<p class="center">⁂</p>

My lab on Ormamitlis.
Our research led to new discoveries: not only we don't lose any genetic material as we get older, but we gain more of it.
Our memory is not only in our brain, it's stored in each and every one of our celles.
It's like the memory that explains the animal instinct, but it's more complete, more detailed.
It includes everything we've ever lived, on a single chromosome that we thought was useless.

If we are the sum of our experiences, a single cell should be enough to resurrect someone that was dead.

<p class="center">⁂</p>

I understand what's happening to me.
The integral cloning and the memory transplant are two procedures my team and I had theorized: since our celles contain our whole memory, it's theoretically feasible to recreate an individual as he was at the time of his death by cloning one of his celles, and maybe to transfer memories by incorporating a segment of a chromosome from another member of our species.

This was all theory on Ormamitlis, but it's more than that here.
Genetic manipulation is one of the rare fields where the Caravan was ahead of us, thanks to the study of the many species composing it.

I'm undergoing an accelerated growth, at the bottom of a tank.
My brain is following the indications from the M chromosome, as well as those from the segment my colleague transplanted me with.
Neurons appear and recreate links, that translate as memories and thought paths.
I become who I was when I died.
When I was murdered.
Yes, I remember that, too.

<p class="center">⁂</p>

It was time I encoded the minutes of the Council's extraordinary session, but my usual public terminal was already occupied.
I didn't wish to interrupt, nor to wait, so I decided to go in one of the High Council's office to borrow one of theirs.
There was no way I'd access protected data anyway.

When I put my hand to open a session, the terminal instantly lit up, without requesting an ID, displaying an interface richer than any I'd seen before.
Dumbstruck, I observed it for a moment.
The layout, the language…
Nothing was common with our usual interfaces.
And yet, I perceived the meaning of what I saw, I understood.
This was a control interface, giving me unsuspected data about the Citadel as it was right at this instant, offering me to control features we didn't even dare dream of or that the Council members know existed without knowing how to activate them.

I turned around when I heard the office door closing behind me, facing a pipe maintenance drone.
Through its speaker, it was Madrutti's voice that made me shiver.

"Of course, you were the one to uncover the truth.
The curious, observing one…
I hoped that making you Scribes would keep you away from the controls while leaving you under my gaze."

"Wiseman Madrutti, is that you?
What truth?
What are you talking about?
I don't understand!"

"You discovered that the Citadel's controls obey your kind.
You are the heirs.
Its passing near your world was no chance, it had been programmed long ago and we couldn't change it, despite our relative control over it."

"But wait, this has no sense!
We have evolved during thousands of revolutions on Ormamitlis, our species is born there…"

"And you know your origins and history, don't you?
You may have an integral memory, I'd bet the eldest of you don't remember their 'birth.'
No, Ormamitlis is just a big test tube."

Just like if these revelations had be the key to a lock, a memory that wasn't mine made its way into my mind.
A memory that was in each of us.
It came from a scientist from the race of those who had created the Citadel.
How they looked like us!
His kind already had our genetic memory, but not our life expectancy.
They wished to explore the universe and had created the Citadel.
Ships had joined them and they made their way to the limits of the galaxy.
Unfortunately, they were going extinct and feared their masterpiece would be stolen by the other members of the convoy.
He didn't wish it.
He dreamed of making his race immortal and, through genetic manipulations on his genome, created us, leaving us so that we could evolve.
The Citadel's controls would be partly locked to all other species, until it came to get us back so that we could give it a direction.

An anger, a deep rage began to boil in me.

"That's impossible!
From the beginning, you manipulated us?"

"What can I say?
I couldn't just give you the Citadel, but having you around was the key to unlock the restrictions your ancestors put on it.
Unfortunately, I'll have to get rid of you, but you helped me before disappearing.
Still, I'll have about twenty experience subjects after you're gone."

"After I'm gone?"

And then I knew what he had in store for me: the robot's disobstruction ray was charging.
It was used to destroy organic blockages in pipes.
Extremely efficient, it disrupted most molecular links around carbon atoms.
There was no escape: in a few seconds, I'd be totally disintegrated and reduced to an unstable, ionized slurry.
