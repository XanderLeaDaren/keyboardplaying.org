---
date: 2020-06-01T16:34:09+02:00
title: Le Scribe
slug: le-scribe
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Il était naturel de confier à cette race le rôle de scribe et de mémoire de la Caravane, mais cela pourrait-il aujourd'hui se retourner contre certains ?

universes: [ caravan ]
inspirations: [ bimonthly ]
keywords: [ nouvelle, science fiction, aliens, immortalité, vaisseaux spatiaux ]

challenge:
  period: 2020-05-31
  rules:
  - type: theme
    constraint: immortalité
postface: |-
    Ceci n'est pas la contribution que j'imaginais pour le défi bimestriel.
    Je n'ai pas encore terminée celle-ci.
    Cependant, comme notre thème plaisait beaucoup à l'équipe d'[Aliens et les Garçons](http://aliensetlesgarcons.space/) pour leur prochain épisode, j'avais vraiment envie de publier quelque chose qui corresponde.
    Et quitte à faire un clin d'œil à nos amis chroniqueurs, placer cette contribution dans [un univers qu'ils ont déjà lu](/histoires/aelg-jugement/) ne paraissait pas déplacé.
    J'espère que vous avez apprécié cette lecture.
---

Je pense. Je suis. Mais qui suis-je ? _Que_ suis-je ?

Mon esprit n'est qu'un espace vide.
Je me sais doué de raison, mais il me manque les fondations sur lesquelles bâtir ma logique.
Je n'ai pas d'identité.
Pas même de nom.
Pas d'histoire.
Je n'ai aucun souvenir.

Enfin, je n'_avais_ aucun souvenir.
Ils semblent soudain se matérialiser, les uns après les autres.
Tout commence par des sensations.
La chaleur, qui me semble profondément familière, puis le froid.
Les couleurs, puis leur nom.
Le son du vent qui soulève le sable du désert et la sensation de piqûre lorsque les grains ainsi portés entrent en contact avec ma peau.
Le goût fade des baies qui luttent contre la fournaise d'Ormamitlis et celui, riche, des bufi frits servis en terrasse, avec vue sur les jardins de la Citadelle.

Ormamitlis.
Ce lieu est plus qu'un simple nom, pour moi, même si je ne sais pas encore pourquoi.
Je me souviens de son soleil, rasant mais brûlant, sur une terre aride hérissée de bâtiments brillants reflétant ses rayons ardents.
Je me souviens de _mon_ soleil, et de _mon_ monde.
J'ignore comment, j'ignore pourquoi, mais je le _sais_.
C'est mon monde d'origine.
Je l'ai quitté.

Des vaisseaux apparaissent dans ma mémoire.
Nous les détectâmes bien avant qu'ils approchent notre système solaire.
Ils formaient un véritable convoi.
Des navettes se détachèrent du gros de la procession pour amener des ambassadeurs à notre rencontre.
Ils avaient détecté notre civilisation et nous supposaient capables de voyage interstellaire.
À ce titre, ils nous proposèrent, si nous le souhaitions, une place au sein de la Caravane.
Je me souviens de ce qu'ils nous ont alors présenté : la Caravane, un convoi parcourant la galaxie.
Des voyageurs de toute race pouvaient s'y joindre ou la quitter à leur gré.
Certains choisissaient de se rallier à la nuée de vaisseaux pour quitter leur monde en train de périr, dans l'espoir d'en trouver un nouveau qu'ils pourraient terraformer pour leurs besoins, abandonnant alors leur errance à travers les cieux.
D'autres prenaient le large simplement par curiosité, pour l'opportunité de rencontrer les autres races de la Caravane, ainsi que celles qu'elle croiserait à l'avenir.

<p class="center">⁂</p>

Je me souviens du retour de nos découvreurs, plusieurs centaines de révolutions planétaires avant l'arrivée de la Caravane.
Notre espèce avait découvert comment se déplacer à travers le vide interstellaire et nous avions lancé plusieurs expéditions.
Le but en était d'explorer pacifiquement l'univers, afin d'en percer les secrets, de découvrir d'autres espèces peut-être.

Malheureusement, notre technologie attira l'attention d'autres voyageurs intersidéraux, plus belliqueux.
N'ayant jamais connu de conflit, nous n'avions jamais imaginé le besoin d'armer nos vaisseaux, ce qui aiguisa encore davantage l'intérêt de ces pirates.
Attaqués, souvent sans sommation, nos caravelles durent résister à leurs tentatives d'abordage.
La plupart parvint à fuir, notre technique de déplacement dépassant de loin le simple principe de propulsion sur lequel nos assaillants s'appuyaient.
Quelques unes ne sont jamais revenues et ont été supposées détruites.

Un découvreur put cependant rentrer et nous raconter comment sa mission avait été capturée.
Au départ, les aliens souhaitaient en apprendre davantage sur notre technologie, mais ils furent rapidement intrigués par notre biologie et commencèrent à expérimenter sur les membres de l'expédition.
Lui seul survécut, en parvenant à s'échapper et à prendre le contrôle du vaisseau ennemi.

Nous retînmes deux choses de ces expéditions.
La première, c'est que l'univers est hostile et le Concile conclut que, pour notre propre protection, il valait mieux que jamais aucune autre race ne soit au courant de notre existence.
Ainsi prit fin l'exploration spatiale pour notre race, nos vaisseaux étant recyclés pour d'autres usages afin de ne pas gaspiller nos rares ressources.
La deuxième leçon que nous avons retenue, en parcourant la base de données biologiques du vaisseau capturé, c'est que nous sommes différents de la plupart des autres espèces.

<p class="center">⁂</p>

Face aux ambassadeurs de la Caravane, le Concile maintint sa position : nous ne construirions plus de vaisseaux ni ne partagerions notre technologie avec d'autres espèces.
Nous ne pouvions pas être certains qu'ils seraient utilisés à bon escient.
Nos premières rencontres avaient laissé un goût trop amer.

Pourtant, un schisme se créa.
Ces émissaires avaient prouvé qu'ils n'étaient pas de la même trempe que ceux qui avaient attaqué nos découvreurs.
Ils dialoguaient, invitaient, mais leur proposition expirerait bientôt : leur convoi ne pouvait pas s'arrêter.
Si nous souhaitions rejoindre la Caravane, il nous fallait le faire maintenant.
Le Concile n'empêcha pas ceux qui voulaient quitter notre monde de le faire, mais ils imposèrent de le faire par d'autres moyens que nos anciennes caravelles.
Les ambassadeurs acceptèrent de ramener avec eux autant d'entre nous que leurs navettes pourraient accueillir — une vingtaine, en tout et pour tout.
J'eus la chance d'en faire partie.

<p class="center">⁂</p>

Les souvenirs continuent à affluer.
Ils se multiplient, ils sont plus conséquents, plus construits.
Un fil rouge commence à se dégager, une histoire.
Comme un puzzle dont on commence à voir l'image après n'avoir assemblé que des ensembles trop petits pour être reconnus.

<p class="center">⁂</p>

J'ai été scientifique avant de quitter mon monde.
Biologiste. Généticien.
Nous avions déjà observé que certains animaux semblent avoir des connaissances innées, des savoirs qui les imprègnent sans qu'ils aient besoin de les apprendre.
Nos ancêtres qualifiaient ceci « d'instinct », sans pouvoir le définir.

Mon équipe a finalement découvert l'origine de ce phénomène : l'ADN, le plan de fabrication des êtres vivants.
Nous avions depuis longtemps identifié des séquences servant à fabriquer les protéines, qui activent des voies moléculaires spécifiques et, par des mécanismes incroyablement compliqués, aboutissent aux êtres vivants que nous connaissons, dans toute leur variété.

La véritable découverte a porté sur ce que nous pensions être des séquences inutiles des brins d'ADN : de l'information, stockée sous un autre format, bien plus complexe à interpréter.
Un plan dans le plan, conditionnant l'agencement de certains neurones.
Certaines expériences, certains souvenirs sont si primordiaux qu'ils s'inscrivent dans l'essence même d'un être et sont ainsi transmis à ses descendants.
Incomplètes, imparfaites, « compressées », mais présentes.

<p class="center">⁂</p>

La Caravane nous offrit des logements au sein de la Citadelle, le vaisseau-cité autour duquel s'est formée le convoi.
Il est impressionnant !
Il s'agit d'un gigantesque anneau sur lequel sont disposés plusieurs quartiers, chacun étant un pan titanesque hébergeant des centaines d'installations, allant de l'habitation à l'usine de haute technologie en passant par le laboratoire de pointe.
L'intérieur n'est séparé du vide intersidéral que par un dôme transparent renforcé par des boucliers d'énergie.
En levant les yeux, il est possible d'observer les autres quartiers, séparés par le noir de l'espace.
La vue est imprenable !

Comme pour compenser l'énergie nécessaire à l'alimentation des boucliers de soutien, pratiquement aucun effort technologique n'est à fournir pour générer une pesanteur artificielle.
La Citadelle pivote constamment autour de son axe, la force centrifuge permettant à ses occupants de se déplacer comme au sein d'un champ de gravité.
Elle est moindre que celle d'Ormamitlis, mais nous nous habituâmes rapidement à ce sentiment de légèreté.

Nul ne sait qui a construit la Citadelle, ni même s'ils l'occupaient encore lorsque la Caravane a commencé à se former.
La seule certitude est que ce vaisseau est le premier et le plus ancien du convoi.
La flotte s'est réunie autour de lui et suit le même itinéraire.
Pourtant, malgré la multitude de races et de voyageurs au sein de la Caravane — ou peut-être en raison de cette multitude — il manque une mémoire, une trace documentée de la vie de ce qui est devenu notre communauté.

<p class="center">⁂</p>

Lorsque le découvreur s'échappa en prenant le vaisseau de ses ravisseurs, il n'imaginait pas la valeur de celui-ci.
Sur notre monde aride, peu de formes de vie avaient su se développer et nous manquions de données pour nous comprendre.
En étudiant la base de données biologique du vaisseau, nous fîmes un bond en avant dans notre connaissance de nous-mêmes.
L'évolution recèle bien plus d'un mystère et bien malin celui qui pourra expliquer comment certains changements se produisent.
Est-ce dû à des circonstances propres à notre planète ? À notre soleil ? À un heureux hasard ?

Nous sommes différents des autres espèces.
Notre ADN est différent.
Chez toutes les espèces référencées, lorsque les cellules se multiplient, les extrémités des chromosomes, les télomères, raccourcissent, marquant le vieillissement de l'organisme.
Le métabolisme se dégrade, les organes cessent de fonctionner normalement, des tumeurs se forment…
Mais pas chez nous.
Nos télomères ne raccourcissent pas, notre ADN ne perd pas d'information.
Nous ne vieillissons pas.
Nous ne mourons pas.

Nous parvînmes à déchiffrer une information dans la base de données qui pourrait se traduire par « espérance de vie ».
Selon les espèces, elle varie de quelques rotations planétaires à quelques centaines de révolutions pour les plus endurantes.
J'avais déjà vécu plusieurs milliers de révolutions lorsque nous comprîmes cette donnée.

Suite à cette découverte, un chercheur de mon équipe fut pris d'un doute et vérifia les registres dont nous disposions sur notre espèce.
Il ne trouva que très peu de morts naturelles parmi les nôtres, et très peu de décès tout court.
Serions-nous une exception dans l'univers ?
Serions-nous immortels ?

<p class="center">⁂</p>

Les technologies au sein de la Caravane sont impressionnantes : toutes les races qui la composent sont capables de voyage intersidéral, et elles semblent pourtant arriérées au regard de nos antiques caravelles.
Ce n'est pas faute d'échange : nombreux sont les scientifiques à communiquer ou marchander leurs avancées avec d'autres races, à s'entraider.

Une simple comparaison entre deux vaisseaux montre que certains sont en avance sur le recyclage d'atmosphère ou de ressources tout en étant à la traîne concernant la culture de nourriture en milieu spatial ou le déplacement interstellaire.
Pourtant, la technologie la plus avancée au sein du convoi ne se trouve pas sur les vaisseaux.

Les portails, dont nul scientifique de la flotte n'a su expliquer le fonctionnement, font partie des miracles de la Citadelle dont on ignore l'origine.
Le vaisseau-cité peut en fabriquer sur demande et les déposer dans son sillage.
Les vaisseaux connaissant les fréquences de contrôle et les coordonnées des autres portails peuvent ainsi ouvrir des tunnels et franchir presque instantanément de vastes distances.

Parmi ceux de mon espèce ayant rejoint la Caravane figurait un découvreur.
Nous observâmes tous deux la fabrication d'un des portails et il pense qu'ils fonctionnent selon des principes similaires à ceux qui régissaient les moteurs de nos caravelles, mais ils ont besoin d'un point d'entrée et de sortie, là où nos vaisseaux étaient entièrement autonomes et libres de leur destination.

Peut-être notre longue vie a-t-elle favorisé notre progrès scientifique et technologique ?
Il est certainement plus simple d'avancer dans notre compréhension de la science en s'appuyant sur ce que nous avons nous-mêmes découvert, là où la plupart des espèces doit réapprendre, génération après génération, les découvertes de leurs aïeux, tout en sachant qu'une partie du savoir de ceux-ci sera toujours irrémédiablement perdue car gardée secrète.

<p class="center">⁂</p>

Mon laboratoire continue à travailler sur la base de données du vaisseau alien ramené par le découvreur.
Une autre équipe de biologistes a découvert que nous cicatrisons différemment de la plupart des autres espèces.

Pour la plupart des êtres vivants, la guérison d'une blessure consiste en une réparation sommaire dont l'unique but est de fermer ou remplacer les tissus abîmés.
Quelques races sont capables de faire repousser des membres après une amputation, souvent au prix d'une dépense métabolique importante, mais très peu sont capables d'une régénération aussi complète et efficiente que la nôtre, une reconstruction complète d'après les plans enregistrés dans notre ADN.

<p class="center">⁂</p>

Le Conseil demeura longtemps gêné, ne sachant que faire de nous.
En dépit de notre volonté de rejoindre notre Caravane, nous restons une race méfiante et isolée.
Il était pourtant nécessaire d'offrir quelque chose en contrepartie de notre place au sein de la flotte.

Nous nous refusâmes à partager quoi que ce soit concernant notre technologie qui puisse être utilisé contre nous ou n'importe qui d'autre.
Notre méthode de voyage restera notre secret.
Nous acceptâmes d'offrir quelques connaissances sur les blindages afin de réduire l'exposition des équipages aux rayonnements cosmiques.
Un de nos biologistes donna quelques semences qu'il avait prises avec lui.
Elles fleurissent aujourd'hui au sein des serres hydroponiques comme elles ne l'avaient jamais fait dans notre terre chaude et sèche.

Nous nous sommes soustraits aux examens biologiques et sommes allés jusqu'à refuser de partager le nom de notre race.
Nous partageâmes quelques informations à notre sujet, tout en demeurant aussi vagues que possible.

Restait le problème de notre rôle au sein du convoi.
Nous n'avions pas de vaisseau et, à ce titre, ne pouvions pas être représentés au Conseil.
Par ailleurs, nous n'avions aucune ressource dont nous pouvions faire commerce, en dehors de ces savoirs que nous nous faisions un devoir de protéger.

L'idée est venue de l'un des membres du Conseil restreint — les premières races de la Caravane à s'être approprié la Citadelle.
Madrutti songea en effet à profiter de notre espérance de vie et de notre capacité à nous souvenir : il a proposé que nous devenions les Scribes du Conseil, responsables des archives de la Citadelle.
Notre principal rôle serait de participer aux sessions du Conseil et de documenter les décisions prises.
Il nous a offert d'être la mémoire qui manquait à la flotte.
Il espérait peut-être nous dévaloriser en nous reléguant au rang d'ordinateurs vivants, mais ce rôle a un sens et j'ai toujours été fier de l'accomplir.

<p class="center">⁂</p>

Quelque chose m'a frappé au bout de quelque temps au sein de la Caravane : la reproduction sexuée.
Je connaissais bien entendu le principe, et ce n'est pas l'unique mode que l'on peut observer, mais elle s'accompagne d'étranges rituels, très variables d'une espèce à l'autre.
Un point commun est la recherche d'un compagnon.
C'est parfois pour la nuit, avec une nouvelle recherche le soir suivant, alors que d'autres veulent quelqu'un avec qui partager leur vie entière, en introduisant parfois des notions de romance.
J'ai également pu observer quelques espèces pour lesquelles la reproduction est tripartite ou plus, et les rites en sont d'autant plus complexes.

Nous ne connaissons pas ces besoins.
Notre espèce est asexuée et biologiquement incapable de se reproduire.
Peut-être s'agit-il d'une évolution normale avec une espérance de vie aussi longue que la nôtre, afin d'assurer une forme de contrôle de la population ?

La logique veut que nous ayons été capables de nous reproduire lorsque nous sommes apparus.
Nous ne pouvons pas être nés de rien.
Nous avons certainement perdu cette capacité de reproduction une fois que notre population a atteint un certain seuil, ou lorsque nous avons cessé de vieillir.
Il est étrange que nous ne nous soyons jamais interrogés sur ce sujet avant de le constater chez d'autres espèces.

Notre unique solution pour nous reproduire — nous l'avons mise à profit après la perte de plusieurs caravelles — est une forme de clonage, la construction d'un nouvel être sur base d'un modèle génétique aléatoire assemblé à partir de multiples échantillons de notre race.
Nos premières tentatives ont entraîné quelques spécimens fous, mais c'était bien avant que nous comprenions le rôle de notre chromosome M.
Nous l'avions à l'époque simplement retiré du modèle.
Nous savions alors qu'il ne codait de toute façon aucun organe.

<p class="center">⁂</p>

Ce souvenir a une teinte particulière.
Il me revient comme si c'était le mien, mais je sais que ce n'est pas moi qui parlais et m'opposais à Madrutti.

— Si ces quelques cellules appartiennent à un Scribe, dit celui qui a vécu cette scène, il nous revient de décider de ce qui doit en être fait.

— Ce que tu proposes va à l'encontre de toutes les règles de la Caravane, Scribe ! s'écria le membre du Conseil.

— Ma proposition respecte au contraire toutes les lois, Sage Madrutti.
Ayant moi-même réorganisé et remis par écrit ces lois, je suis bien placé pour le savoir.
Chaque espèce est souveraine en termes de justice.
Cet individu a été assassiné par un meurtrier que nous ne pouvons pas identifier, et seul son témoignage pourra nous aider à y voir plus clair.

— Mais qu'est-ce qui prouve qu'il s'agit d'un meurtre et pas d'un suicide ?

Une pointe de désespoir semblait s'être glissée dans la voix de Madrutti.

— Je vous ai révélé que notre espèce bénéficie d'une mémoire génétique intégrale.
Nous ne sommes pas capables de la déchiffrer intégralement, mais nous pouvons vous certifier que cette vie s'est terminée par deux émotions fortes, que nous interprétons comme un élan de rage suivi d'une brève terreur.
Ces émotions ne sont pas habituellement celles associées à un suicide.
Notre verdict est posé et n'est plus discutable : ce Scribe sera ressuscité par clonage intégral et un souvenir de cette décision, issu de mon propre matériel génétique, lui sera greffé.

<p class="center">⁂</p>

Mon laboratoire sur Ormamitlis.
Nos recherches ont abouti à de nouvelles découvertes : non seulement nous ne perdons aucun matériel génétique en vieillissant, mais nous en acquérons.
Notre mémoire n'est pas que dans notre cerveau, elle est stockée dans chacune de nos cellules.
Elle ressemble à la mémoire génétique derrière l'instinct animal, mais elle est bien plus complète et bien plus détaillée.
Elle comprend tout ce que nous avons vécu, regroupé sur un unique chromosome que nous pensions jusqu'ici complètement inutile.

Si nous sommes la somme de nos expériences, une seule de nos cellules doit suffire à ressusciter un être que l'on pensait décédé.

<p class="center">⁂</p>

Je comprends ce qui m'arrive.
Le clonage intégral et la greffe mémorielle sont deux procédures que moi et mon équipe avions théorisées : puisque nos cellules contiennent toute notre mémoire, il est théoriquement possible de recréer un individu tel qu'il était au moment de sa mort en clonant une de ses cellules, et peut-être d'y transférer des souvenirs en y incorporant un segment d'un chromosome d'un autre membre de notre espèce.

Cela était resté théorique sur Ormamitlis, mais c'est faisable ici.
La manipulation génétique est l'un des rares domaines où la Caravane est en avance sur nous, notamment grâce au brassage et à l'étude des nombreuses espèces qui la composent.

Je suis donc certainement en train de subir une croissance accélérée, au fond d'une cuve.
Mon cerveau suit les indications du chromosome M, ainsi que celles du segment provenant de la mémoire de mon collègue.
Des neurones se forment et recréent des liaisons, qui se traduisent en souvenirs et schémas de réflexion.
Je redeviens qui j'étais au moment de ma mort.
Au moment de mon meurtre.
Oui, je m'en souviens aussi.

<p class="center">⁂</p>

Il était l'heure d'encoder mon compte-rendu de la session extraordinaire du Conseil, mais le terminal public que j'utilisais habituellement était déjà occupé.
Ne souhaitant ni déranger, ni attendre, je choisis d'aller dans un des bureaux du Conseil restreint pour emprunter l'un des leurs.
Il n'y avait de toute façon aucune chance que j'accède à des données protégées.

Lorsque je posai ma main pour ouvrir une session, le terminal s'illumina instantanément sans me demander d'identification, m'affichant une interface plus riche que toutes celles que j'avais vues auparavant.
Ébahi, je l'observai pendant quelques instants.
La présentation, le langage, rien n'était commun avec nos interfaces habituelles.
Pourtant, je percevais le sens de ce que je voyais, je comprenais.
Il s'agissait d'une interface de contrôle, qui me fournissait des informations insoupçonnées sur la Citadelle à ce moment précis et m'offrait de contrôler des fonctionnalités dont nous n'aurions pas osé rêver ou dont les membres du Conseil connaissaient l'existence sans savoir les activer.

Je me retournai en entendant la porte du bureau se refermer derrière moi, me retrouvant face à face avec un drone de maintenance des canalisations.
Par son haut-parleur, c'est la voix de Madrutti qui fit parcourir un frisson le long de mon dos.

— Évidemment, c'est toi qui as fini par découvrir la vérité !
Le curieux, l'observateur…
J'espérais pourtant que cette tâche de Scribe me permettrait de vous éloigner suffisamment des contrôles tout en vous laissant sous mes yeux.

— Sage Madrutti, c'est vous ?
Quelle vérité ?
Je ne comprends pas ce qui se passe.

— Tu as découvert que les contrôles de la Citadelle obéissent aux tiens.
Vous en êtes les héritiers.
Son passage près de votre monde n'est pas un hasard, il avait été programmé il y a longtemps et, en dépit du contrôle relatif que nous avions sur son itinéraire, c'est le point que nous ne pouvions pas déprogrammer.

— Mais enfin, ce que vous dites n'a pas de sens !
Nous avons évolué pendant des milliers de révolutions planétaires sur Ormamitlis, notre espèce y est née…

— Et vous y avez des traces de vos origines et de votre histoire, n'est-ce pas ?
D'ailleurs, vous avez beau avoir une mémoire intégrale, je suis certain que les plus anciens d'entre vous ne se souviennent pas de leur « naissance ».
Non, Ormamitlis n'est qu'une gigantesque éprouvette.

Comme si ces révélations étaient la clé d'un verrou, un souvenir qui n'était pas le mien remonta.
Un souvenir implanté en chacun de nous.
Il nous venait d'un scientifique de la race qui avait créé la Citadelle.
Comme ils nous ressemblaient !
Les siens avaient déjà notre mémoire génétique, mais pas notre espérance de vie.
Ils souhaitaient explorer l'univers et avaient créé la Citadelle.
Des vaisseaux s'étaient joint à eux et ils continuaient leur route vers les confins de la galaxie.
Malheureusement, ils s'éteignaient et craignaient que leur chef d'œuvre ne soit pris par les autres membres du convoi.
Il ne le souhaitait pas.
Il rêva de rendre sa race immortelle et, par le biais de manipulations génétiques sur son génome, nous créa, nous laissant le temps d'évoluer.
Les contrôles de la Citadelle seraient partiellement verrouillés à toutes les autres espèce, jusqu'à ce qu'elle revienne nous chercher pour lui donner un nouveau cap.

Une colère, une rage sourde et incompréhensible commença à bouillir en moi.

— Non, c'est impossible !
Vous nous avez manipulés, depuis le début ?

— Que veux-tu ?
Je ne pouvais pas vous céder le contrôle de la Citadelle, mais vous avoir à proximité était la clé pour lever le verrouillage imposé par vos ancêtres.
Pas de chance pour moi, je vais devoir me passer de toi pour ça, mais tu as malgré toi fait avancer le sujet.
Heureusement, il me restera une vingtaine de cobayes après ta disparition.

— Ma disparition ?

Soudain, je saisis ce qui m'attendait : le rayon de désobstruction du robot de maintenance chargeait.
Il était utilisé pour détruire les bouchons organiques dans les canalisations.
Extrêmement efficace, il rompait la plupart des liaisons moléculaires autour des atomes de carbone.
Je n'avais aucune échappatoire possible : dans quelques secondes, j'allais être intégralement désintégré et réduit à l'état d'une instable bouillie ionisée.
