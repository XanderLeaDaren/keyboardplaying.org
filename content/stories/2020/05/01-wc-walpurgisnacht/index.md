---
date: 2020-05-01T16:33:42+02:00
title: Walpurgis Night
slug: walpurgis-night
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Tonight, my little shop for natural remedies closes early.
  It's an important day.

inspirations: [ weekly, songfic ]
keywords: [ flash fiction, songfic, witchcraft, herbalist, magic, celtic, wiccan ]
songfic:
  title: Walpurgisnacht
  link: https://youtu.be/nLgM1QJ3S_I
  artist: Faun
  artistLink: http://faune.de/

challenge:
  period: 2020-05-01
  rules:
  - type: theme
    constraint: witchcraft
  - type: songfic
    personal: true

cover:
  src: faun-walpurgisnacht.cover.jpg
  alt: Witches dancing around a fire
  by: Faun
  authorLink: http://faune.de/de/media/
  link: https://www.instagram.com/p/B_nVrYZngni/
  socialSrc: faun-walpurgisnacht.jpg
---

It's nearly four o'clock, but some customers still inspect the shelves of my little shop.
The sign at the entrance clearly announces that I'm closing earlier today, as I do every year on April 30<sup>th</sup>.
Most of my regulars assume it's to make the most of May 1<sup>st</sup>.
Well, that's not entirely true, but they're not entirely wrong either.

I shouldn't complain.
My business is doing well.
You'd think a small village like Buchendorf wouldn't have room for a shop selling natural remedies and products.
Starting was the hard part.
My first customers were essentially curious people, but the word quickly got around after they were satisfied.
Now, some of them don't hesitate to drive the twenty or so kilometres between here and Munich, sometimes just to ask me for advice or for "the pleasure of seeing me"---yes, some come only to flirt.

This young man's one of them.
He often comes to ask for remedies for his mother and sister.
Perhaps he's trying to show me how well he takes care of the women of his life?
Today, he's asking about something to help his mother with the pains in her knees.
I made sure it's not osteoarthritis and I led him to a shelf where I grabbed an ointment from high up.
As I turn around, I catch his eyes going up and a slightly guilty look on his face.
I guess his gaze went down rather than following my hand when my back was turned.
Let's forget about it, he's nice enough to be forgiven that.
Comme ce jeune homme, qui vient souvent demander des remèdes pour sa mère et sa sœur.

"Give her this ointment, this should help.
There's white willow and laurel, they've long been used in the treatment of rheumatism---aspirin comes from willow, you know.
There's also meadowsweet for joint and muscle pain.
The chili and turmeric will warm the area up a bit, it will help."

"Ah, uh, thank you," he replies, as shy as ever. "And ... can I buy you dinner tonight?"

This brings a smile to my face. He finally dared to ask. What a pity he's not my type at all, and tonight was out of the question anyway. "I'm sorry, I already have plans. Good evening to you."

I walk him to the door.
He was the last and I can finally lock the door when another man, whom I've never seen but who's much more to my taste, comes to the door.

"I'm sorry, Sir, but I'm closing."

"Oh, I see," he answers with a bit of embarrassment.
He pauses and looks at me for a moment, before continuing, "I'm sorry, I don't want to be a bother, but I need some oaks and apricots twigs for tonight.
I'd be very grateful if you could sell me those."

These two essences, for tonight? Could it be that...?
Well, I added oak leaves as the Celtic symbol of hospitality on my sign.
I wouldn't be much of a host if I didn't help with something so simple.

The oak, which the Celts considered as a symbol of invincible strength, and the apricot tree, food for the gods as well as a sign of passion and sensuality.
I go to my private storeroom, the one only a few customers versed in esotericism know about, and bring back a few branches of each.
I don't charge him, not for so little.
What's more, I'm in a hurry to get ready for tonight;
Before leaving, he looks at me with a wonderful smile and says, "See you tonight, maybe."
It wouldn't be bad news to find him tonight, indeed.

I close up the ship, and go home to change and grab everything I need for tonight.
Several customers asked me why I moved to this tiny little place for my shop.
Few of them could understand the importance of the _nemeton_ not far from here.
For someone of my religion, the Wicca, Celtic sacred places are places of power.

Night begins to fall when I reach the shrine, and the fire is already lit.
I am obviously the last of my coven to get here, but there are many people tonight, Wiccans or not.
Today is Beltane for us: the Horned God reaches manhood and desires the Mother Goddess, and they unite.
But it's a Sabbath we share with other orders and movements.
Tonight is also the night of Walpurgis: the deities of spring are spreading in nature to end winter.
Together, without distinction of faith, we celebrate the return of life and the triumph over the cold.

We dance, drink, eat, dance and drink again.
Some covens force their members to be naked.
To me, shedding my clothes feels just natural in this evening of communion with nature.

Comes the time of the black mass, which is a mass only in name.
We don't pray or invoke black creatures.
Instead, we mock the Church that tried to impose its faith by forbidding or cults, by excommunicating ours without ever understanding who and what they were.

The ceremony ends and the dances begin again, the bodies coming closer together.
Some lie down and merge.
After all, we celebrate the return of the gods and goddesses of fertility, and the union of the Horned One and the Goddess.
What would this celebration be without some carnal bonding?

A branch falls on my shoulder and I grab it: it's an apricot twig.
I turn around and face my late customer, still adorned with his beautiful smile.

"The oak tree was my offering to the gods," he tells me, "but the apricot tree is for you."

This will be the opportunity to measure his gratitude. This Beltane will undoubtedly be even more memorable than the others.
