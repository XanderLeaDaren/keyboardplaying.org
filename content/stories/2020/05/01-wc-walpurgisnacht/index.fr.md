---
date: 2020-05-01T16:33:42+02:00
title: La nuit de Walpurgis
slug: la-nuit-de-walpurgis
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Ce soir, ma petite boutique de remèdes naturels ferme tôt.
  C'est une date importante.

inspirations: [ weekly, songfic ]
keywords: [ micronouvelle, songfic, sorcellerie, magie, herbosterie, celtique, wiccan ]
songfic:
  title: Walpurgisnacht
  link: https://youtu.be/nLgM1QJ3S_I
  artist: Faun
  artistLink: http://faune.de/

challenge:
  period: 2020-05-01
  rules:
  - type: theme
    constraint: sorcellerie
  - type: songfic
    personal: true

cover:
  src: faun-walpurgisnacht.cover.jpg
  alt: Des sorcières dansant autour d'un feu
  by: Faun
  authorLink: http://faune.de/de/media/
  link: https://www.instagram.com/p/B_nVrYZngni/
  socialSrc: faun-walpurgisnacht.jpg
---

Le coup de seize heures approche, mais quelques clients inspectent encore les rayons de ma petite boutique.
La pancarte à l'entrée annonce pourtant clairement que je ferme plus tôt aujourd'hui, comme chaque 30 avril.
La plupart de mes habitués supposent que c'est pour mieux profiter du 1<sup>er</sup> mai.
Ce n'est pas tout à fait exact, mais ils n'ont pas totalement tort non plus.

J'aurais tort de me plaindre. Mon affaire marche bien.
On aurait pu penser qu'un petit village comme Buchendorf n'aurait pas de place pour une échoppe de remèdes et produits naturels.
Le démarrage a été difficile, mes premiers clients étant surtout des curieux, mais le bouche-à-oreille a rapidement fait son effet après que ceux-là ont été satisfaits.
À présent, certains n'hésitent pas à prendre leur voiture pour parcourir la vingtaine de kilomètres qui nous séparent de Munich, parfois juste pour me demander conseil ou « pour le plaisir de me voir » --- oui, certains viennent juste pour flirter.

Comme ce jeune homme, qui vient souvent demander des remèdes pour sa mère et sa sœur.
Peut-être cherche-t-il à me montrer qu'il est prévenant et s'occupe bien de celles qui l'entourent ?
Aujourd'hui, il est venu me demander si j'avais quelque chose pour soulager sa mère des douleurs dont elle souffre dans les genoux.
Après m'être assurée qu'il ne s'agit pas d'arthrose, je l'accompagne devant un rayon et attrape un petit onguent en hauteur.
En me retournant, je surprends ses yeux qui remontent et un petit air coupable.
Visiblement, son regard n'avait pas suivi ma main lorsque mon dos était tourné. Qu'importe.

« Donnez-lui cet onguent, il devrait aider. On a du saule blanc et du laurier, utilisés depuis longtemps dans le traitement des rhumatismes --- c'est du saule que vient l'aspirine, vous savez.
Il y a aussi de la reine-des-prés pour lutter contre les douleurs articulaires et musculaires.
Le piment et le curcuma chaufferont un peu la zone, ça ne fera pas de mal.

— Ah, euh, merci.
Et... est-ce que je peux vous proposer un dîner ce soir ? »

Je souris gentiment.
Il a finalement osé faire le premier pas.
Quel dommage qu'il ne soit absolument pas mon genre.
Ce n'était de toute façon pas envisageable ce soir.
« Je suis désolée, j'ai déjà quelque chose de prévu. Bonne soirée à vous. »

Je le raccompagne à la porte.
Il était le dernier et je m'apprête à verrouiller la porte lorsqu'un autre jeune homme, que je n'ai jamais vu mais qui est bien plus à mon goût, se présente à la porte.

« Je suis désolée, Monsieur, mais je dois fermer.

— Oh, je vois, » répond-il, visiblement embarrassé.
Il me regarde quelques instants avant de reprendre : « Pardonnez-moi, je ne veux pas vous retenir, mais j'ai besoin de rameaux de chêne et d'abricotier pour ce soir.
Je vous serais très reconnaissant si vous pouviez me vendre ça. »

Ces deux essences, pour ce soir.
Serait-il possible que... ?
Oh et zut ! Mon enseigne comporte des feuilles de chêne, emblème d'hospitalité chez les Celtes.
Quelle hôtesse serais-je si je ne l'aidais pas avec un besoin si simple ?

Le chêne, que les Celtes considéraient comme symbole de la force invincible, et l'abricotier, nourriture des dieux et signe de passion et de sensualité.
Je vais dans ma réserve privée, celle dont seuls quelques clients versés dans l'ésotérisme connaissent l'existence, et lui rapporte quelques rameaux de chaque essence.
Je ne le fais pas payer, pas pour si peu.
D'autant que je veux me préparer pour ce soir.
Avant de partir, il me regarde avec un merveilleux sourire et me glisse : « À ce soir, peut-être. »
Ce ne serait pas une mauvaise nouvelle de le retrouver ce soir, en effet.

Je ferme la boutique, et file chez moi pour me changer et prendre tout ce dont je pourrais avoir besoin pour ce soir.
Plusieurs clients m'ont déjà demandé pourquoi j'étais venue m'installer dans ce tout petit patelin pour ma boutique.
Peu d'entre eux pourraient comprendre l'importance du _nemeton_ voisin pour quelqu'un de ma religion, la Wicca : les sanctuaires druidiques celtes sont des lieux de pouvoir.

La nuit commence à tomber lorsque j'y parviens, et le feu est déjà allumé.
Je suis visiblement la dernière de mon coven, mais il y a beaucoup de monde, wiccans ou non.
Aujourd'hui, nous fêtons Beltaine : le Dieu Cornu a atteint l'âge d'homme et désire la Grande Déesse, à qui il s'unit.
Mais c'est un sabbat que nous partageons avec d'autres ordres et mouvements de sorcières.
Ce soir, c'est la nuit de Walpurgis : les divinités du printemps se répandent dans la nature pour mettre fin à l'hiver.
Tous ensemble, sans distinction de culte, nous célébrons le retour de la vie et le triomphe sur le froid.

Nous dansons, buvons, mangeons, dansons et buvons encore.
Certains covens forcent leurs membres à pratiquer nus.
En ce qui me concerne, cela m'apparait juste comme une évidence en cette soirée de communion avec la nature.

Vient l'heure de la messe noire, qui n'a de messe que le nom.
Nous n'invoquons pas de créature noire, mais moquons l'Église qui a voulu imposer sa foi en interdisant nos cultes, en excommuniant les nôtres sans jamais comprendre ce qu'elles étaient.

La cérémonie se termine et les danses reprennent, les corps se rapprochant.
Certains s'allongent et fusionnent.
Après tout, nous célébrons le retour des dieux et déesses de la fécondité, ainsi que l'union du Cornu et de la Déesse.
Que serait cette fête sans quelques rapprochements charnels ?

Une branche se pose sur mon épaule et je l'attrape : c'est un rameau d'abricotier.
Je me retourne et me retrouve face à mon client retardataire, toujours paré de son magnifique sourire.

« Le chêne, c'était mon offrande pour les dieux, m'annonce-t-il. L'abricotier, c'est pour toi. »

Ceci va être l'occasion de mesurer sa reconnaissance. Ce Beltaine sera sans nul doute encore plus mémorable que les autres.
