---
date: 2021-02-28T7:33:42+01:00
title: Writever février 2021
slug: writever-02-2021
author: chop
license: CC BY-NC-ND 4.0

description: |-
  J'ai relevé le challenge du Writever de février 2021.
  Tous les épisodes de mon petit feuilleton fantasy sont réunis sur cette page.

preface: |-
    [Keyy StwrD](https://twitter.com/k_tastrof) a lancé pour février [son Writever](https://twitter.com/k_tastrof/status/1355974417692684288).
    L'objectif : écrire chaque jour, en un gazouillis, une nanofiction qui tienne en un tweet.
    [J'ai relevé le défi](https://threadreaderapp.com/thread/1356282783811952647.html), avec l'objectif de faire un feuilleton le temps d'un mois.
    Voici mes propositions.

keywords: [ micronouvelle, fantasy, aventurier, errant, herboristerie, magie, ténèbres, feu, amour, parentalité, enfance ]
toc: true

challenge:
  period: 2021-02-28
  rules:
  - type: general
    constraint: Un jour, un thème, un gazouillis.

cover:
  src: writever-02-2021.cover.jpg
  alt: Une bannière pour le Writever février 2021
  by: Aïtana
  authorLink: https://twitter.com/DeaAitana
  link: https://twitter.com/k_tastrof/status/1355974417692684288
  socialSrc: writever-02-2021.jpg
---

## 1/ Il était une fois

Le marchand itinérant était interdit : du village où il s'arrêtait chaque année, il ne restait que des cendres.
Il reprit les rênes lorsqu'un mouvement l'arrêta, un nouveau-né dans les cendres, vivant.
Il l'enveloppa dans un linge et fuit le lieu maudit.


## 2/ L'aventurier

La vie au relai était plaisante.
Il était heureux de rendre service aux voyageurs, et ceux-ci le lui rendaient bien : les bardes lui racontaient des histoires, les soldats lui apprenaient à se battre…
Jusqu'au jour où arriva un homme vêtu d'une capuche.


## 3/ Un autre monde

L'inconnu n'était pas passé inaperçu lorsqu'il était entré dans la salle.
Sollicité, il narrait ses aventures, ses rencontres avec des bandits ou des créatures mythiques.
Tout en faisant le service, le garçon l'écoutait avidement, des rêves plein les yeux.


## 4/ Combien de temps

De ce moment, une nouvelle ferveur l'emplit.
Les années suivantes, il utilisa son travail à la forge pour développer ses muscles, s'entraîna à vaincre les soldats de passage lors de leurs exercices, apprit les secrets des herbes…
Puis enfin, prêt, il partit.


## 5/ Cas zéro

L'aventure…
Il en rêvait sans savoir qu'en attendre, suivant la route, traversant la forêt, jusqu'à ce qu'un cri résonne à ses oreilles.
Courant vers le son, il découvrit une créature noire plaquant une femme au sol de ses serres.
Sans plus réfléchir, il dégaina.


## 6/ J'ai vu

De retour au village, Tara entraîna son sauveur à la taverne, racontant comment elle avait vu la fin de sa vie arriver lorsque le héros était arrivé.
Le père de Tara, rebouteux, prodigua au jeune homme les soins nécessaires après son combat, et le tavernier lui offrit le gîte.


## 7/ Ça

Alors c'était ça, l'aventure : errer sur les routes, rencontrer des personnes à aider et des monstres menaçants à occire, être remercié mais rarement payé…
C'était plus séduisant narré que vécu, mais il ne regrettait pas ses choix, même sans le sou et sans gîte cette nuit.


## 8/ La groupie

Tara avait quitté son village pour suivre les rumeurs de son héros.
Tous l'appelaient « le Brûlé » en regard à ses cicatrices.
Ce soir-là, elle le trouva sous son abri de fortune et lui offrit une nuitée payée par la bourse qu'elle remplissait par de petits boulots.


## 9/ Résiste

Pourquoi cette femme lui proposait-elle le gîte ?
En tant qu'aventurier, il ne pouvait demander l'aumône ; il se devait de mériter l'accueil de ses hôtes.
Il refusa : s'il était capable de vaincre des monstres ténébreux, il pouvait passer une nuit à la belle étoile.


## 10/ Antisocial

Suite à cette rencontre, il évita ses congénères, ne les approchant que pour apprendre comment se rendre utile.
Le confort et la compagnie ne faisaient pas partie de son rôle d'aventurier errant, uniquement le service à ceux qui en avaient besoin.


## 11/ États d'âme

Éviter ses pairs avait des limites : il réalisa rapidement qu'aider ne donnait pas la même satisfaction s'il ne pouvait pas constater le résultat de son labeur.
Cela limitait aussi ses actions : repousser les monstres, oui, mais pas maintenir l'ordre public.

## 12/ Mistral gagnant

Fut un soir où un fort vent lui empêchait une nuit en extérieur, sans aucune grotte à proximité.
Il se résolut à aller au prochain village, sans le sou à force de ne pas se faire payer, et faire appel à la générosité.
Quelle surprise lorsqu'il croisa Tara…


## 13/ Nuit de folie

Rien n'avait été prémédité, mais ce fut comme si une force supérieure les réunissait.
Il ne restait qu'une chambre dans la taverne, dont ils partagèrent le lit.
La nuit avançant, ils cédèrent aux pulsions jusqu'alors inconnues, s'initiant aux plaisirs charnels.


## 14/ Oh l'amour

Sa vie changea.
Il courait toujours les routes pour aider son prochain, mais sans éviter ses semblables.
Surtout, il retrouvait régulièrement Tara dans leur havre de paix et lui remettait ses rétributions, qu'elle complétait par ses compétences en herboristerie.


## 15/ Fruit de la passion

Ignia naquit de leur amour.
De son père, elle hérita ses cheveux noir de jais et ses yeux d'obsidienne ; de sa mère, sa grâce et sa beauté naturelles.
Ses premières années au village furent un ravissement pour tous ceux qui la virent grandir.


## 16/ Confidence

Lorsqu'il sauva un moine vêtu d'écarlate d'une créature, celui-ci l'observa, extasié.

— Le Brûlé ?
Est-elle née ?
Ignia, celle dont la flamme doit chasser les Ténèbres ?

Mais avant qu'il ait pu répondre, l'homme de foi rendit son dernier souffle.


## 17/ Maldonne

Pris d'inquiétude, il s'élança pour rejoindre sa famille mais fut stoppé par un poignard.
Un second moine écarlate se dressa au-dessus du corps gisant.

— Désolé, Brûlé, il a trop parlé.
La prophétie dit que tu devais mourir avant d'apprendre la nature de ta fille.


## 18/ Bêtises

Ignia était pleine de vie.
Elle cassait les pots de sa mère, renversait ses plants, mais restait une petite fille adorable.
Tara se demandait quand rentrerait son époux et ne pouvait pas deviner que les bêtises de sa fille étaient son deuil de celui-ci.


## 19/ Chagrin d'amour

Ce fut par un barde itinérant que Tara apprit la mort de son aimé, poignardé à côté d'un moine qu'il avait tenté de sauver.
Le chagrin la brisa et elle dépérit à vue d'œil, suscitant l'inquiétude du village, où personne ne savait comment la réconforter.


## 20/ Toute seule

Ignia avait sept ans lorsque le chagrin finit par tuer Tara.
Elle sut continuer son commerce d'herbes, s'appuyant sur les journaux de sa mère.
Elle trouva également ceux de son père, ainsi que sa réserve, et utilisa tout son temps libre à s'entraîner à son tour.


## 21/ Bébé

Entrèrent un jour trois clients qui s'étonnèrent de voir l'herboristerie tenue par une enfant de douze ans.
Ils se moquèrent de son jeune âge, mais fuirent rapidement face à son courroux et son bo, surprenant un aventurier errant venu rendre hommage à la famille du Brûlé.


## 22/ Je te survivrai

L'aventurier fut heureux de voir qu'Ignia savait se défendre.
Il raconta que les moines écarlates voyaient en elle la concrétisation d'une prophétie et qu'un ordre dissident voulait sa mort.
Elle résolut de ne pas mourir de leur main comme son père.


## 23/ Fuis

Il ne lui fallut pas attendre longtemps pour mettre sa résolution à l'épreuve, lorsque les Dissidents attaquèrent son échoppe.
Elle saisit son bo et l'aventurier dégaina son épée, mais elle fut forcée de fuir face au nombre, l'errant faisant barrière derrière elle.


## 24/ Petit coup

Lorsque l'errant la rejoignit, elle remarqua le sang autour de la plaie qu'il tenait derrière sa main.

— Ce n'est qu'une blessure superficielle.
Tu dois aller au monastère écarlate, fais attention aux Dissidents.

Il l'accompagna, mais mourut rapidement de sa lésion.


## 25/ Est-ce que tu viens pour les vacances ?

Le clerc faillit tomber à la renverse en découvrant Ignia à la porte du Monastère.
Elle fut accueillie comme une déesse et apprit la prophétie, comment son père était né des flammes de la Non-Vie pour pouvoir l'engendrer elle.
Ainsi lui vint une idée.


## 26/ Hygiaphone

Elle chercha et trouva un rituel, l'Hygiaphone, permettant de communiquer avec la Non-Vie.
Pas avec sa mère, qui avait terminé son voyage en ce monde, mais pour son père…
Il était originaire de la Non-Vie.
En sacrifiant un peu de sa vie, ils pourraient parler.


## 27/ Douanier

— Écarte-toi de ma route, Gardien.

— Je ne sais pas pour qui tu te prends, mais ces flammes ne sont pas là pour faire joli.
Y entrer, c'est rencontrer sa fin ultime.

— Elles m'ont fait naître une fois à la vie, elles me permettront de me battre aux côtés de ma fille.


## 28/ Cendrillon

Un errant se rendait à la ferme hantée afin d'en chasser la créature qui avait fait fuir ses propriétaires, mais ne trouva qu'un champ de cendres encore chaudes.
Abasourdi, il le parcourut, jusqu'à ce qu'une main surgisse et lui attrape la cheville.
