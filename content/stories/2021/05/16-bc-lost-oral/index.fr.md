---
date: 2021-05-24T19:30:00+02:00
title: Le jour où j'me suis perdu
slug: le-jour-ou-j-me-suis-perdu
license: CC BY-NC-ND 4.0

description: |-
  Laisse-moi te raconter une histoire de ouf.
  C'était un jour où je m'ennuyais et j'ai tenté de me perdre dans ma ville.
  À la place, j'ai trouvé un musée, mais c'est là que c'est devenu délirant…

cover:
  banner: false
  src: metz-lavoir-et-pontiffroy.jpg
  alt: Le lavoir des thermes avec en arrière-plan le quartier « moderne » du Pontiffroy à Metz
  by: Marc de Metz
  link: http://marcmetzmoselle.eklablog.com/metz-la-meme-moselle-mais-d-autres-berges-a92909147

keywords: [ nouvelle, fantastique, musée, art, histoire, rome antique, moyen âge, révolution, pirate, voyage dans le temps ]

postface: |-
  Cette histoire a été écrite dans le cadre d'un Défi du Stylo.
  Je m'étais dans un premier temps fixé comme objectif supplémentaire de l'écrire au passé composé, mais le résultat ne fut pas probant, trop déconstruit pour un récit « traditionnel », mais pas assez pour une imitation du style oral.

  J'ai donc choisi de réécrire le même défi de deux façons : l'une qui imite un récit de vive voix, entre deux amis ; la seconde qui respecte les codes que l'on attend des récits écrits, au passé simple et avec une construction qui vise à faciliter la lecture.

  Vous avez certainement reconnu l'inspiration d'un récit oral sur cette page.
  L'autre version est disponible [en cliquant ici](../se-perdre/)

challenge:
  period: 2021-05-15
  rules:
  - type: theme
    constraint: perdu
---

Est-ce que tu as déjà essayé de te perdre dans ta ville ?
Je veux dire, te perdre vraiment.
À ne plus savoir où tu es, ni comment retourner d'où tu viens.
Chercher à avoir le même sentiment que quand tu es en voyage loin de chez toi et que tu te rends compte que les bâtiments ne sont pas construits de la même façon.
Ce moment où tu te rappelles que tout le monde ne vit pas comme toi.

Moi, je l'ai fait.
Essayer, hein !
Parce que se perdre dans la ville où on a grandi, si tu tentes aussi, tu verras que c'est pas simple.

Tu te demandes certainement d'où m'est venue cette idée.
C'est simple : j'avais rien à faire.
J'étais en première, c'étaient les vacances, j'avais plié mes devoirs, mes potes étaient partis, mes parents au boulot…
Rien à faire, j'te dis.

Il faisait beau, un temps à profiter de l'extérieur.
Le problème, c'est que notre petit appart', il en avait pas, d'extérieur.
Même pas un demi balcon sur lequel mettre une chaise longue pour bouquiner.
Alors m'est venue l'idée d'aller me balader, mais pour aller où ?
C'est pas comme si c'était très touristique, par chez moi.

Et donc c'est là qu'est venue l'idée : plutôt que de me balader sans but précis, je pouvais tenter de me paumer.
Plutôt que de me traîner à pied, j'ai choisi de prendre mes rollers, donc j'ai mis tout ce qu'il fallait dans un sac et j'ai décollé.

Quitte à me perdre, je suis parti là où je connaissais le moins : à l'opposé du lycée.
J'ai un peu vadrouillé.
À chaque bifurcation, si je voyais une rue que je ne connaissais pas, je m'y engouffrais.
Tu vois le genre ?
« Tiens, ça me dit rien, ça. Allez hop ! »

Manque de bol, ça débouchait presque à chaque fois dans une artère où on passait régulièrement en voiture.
Du coup, je m'appuyais sur un autre critère : choisir ce qui semblait m'éloigner le plus rapidement de chez moi.
En toute logique, ça devait m'aider à perdre mes repères plus vite.

Ça m'a pris une ou deux heures avant que ça devienne intéressant.
J'étais dans un quartier d'immeubles qui semblaient dater des années 60 ou 70, tu sais, comme celui où je vivais.
Les trucs carrés tous moches qui devaient paraître modernes à l'époque, avec des fenêtres en alu qui font rentrer le froid plutôt que de le laisser dehors, tu vois le genre ?
Et là, au milieu de ça, t'avais un petit parc qui descend le long de la rivière, avec un lavoir au milieu.
C'était vraiment le truc qu'avait pas l'air à sa place.

J'ai continué, pas très loin, quelques centaines de mètres, et je suis tombé sur une vue qui m'a laissé sur le cul.
D'un côté, t'avais les bâtiments comme ma résidence, et de l'autre, c'était que des vieilles baraques, genre XV ou XVIe siècle.
C'était comme si la route était une barrière entre deux époques, tu visualises un peu le truc ?
Comme quand des mecs s'amusent à superposer une photo d'une ville dans Assassin's Creed sur le même endroit aujourd'hui.

Il m'a fallu un temps pour m'en remettre, mais je me suis secoué et j'ai pris la rue la plus proche du côté Moyen Âge.
J'ai continué à me promener comme avant et j'ai découvert que la ville n'était pas aussi plate que je le pensais.
Après une belle grosse côte, je me suis posé, essoufflé bien comme il faut.
J'en ai profité pour regarder un peu autour de moi.
Au bout de la rue, droit devant moi, il y avait une façade caractéristique et, un peu sur ma droite, j'apercevais les pointes de la cathédrale par dessus les toits.
J'étais à une centaine de mètres de la place d'armes, tu vois, là où il y a la mairie.
Juste derrière, c'était le centre-ville.
Dans le genre perdu, on fait mieux.

Derrière moi en revanche, il y avait une surprise : un vieux bâtiment, genre abbaye, avec sur la façade des lettres fixées pour annoncer « Musée de la Cour d'or ».
Je me suis dit que ça pouvait être un moyen plus intéressant de passer le temps, donc j'ai mis mes baskets aux pieds et les rollers dans le sac et je suis entré.
La nana à l'accueil me voyait farfouiller dans mon portefeuille et elle m'a arrêté en me disant que c'était gratuit.
Je lui ai laissé mon sac parce qu'elle ne voulait pas que je le prenne à l'intérieur et j'ai passé la première porte.

C'était sympa, bien présenté.
Le début de la visite est en sous-sol, ils ont fait un sol et des murs noirs, et ce qui est exposé est bien mis en valeur par l'éclairage.
À ce niveau, c'est surtout des découvertes archéologiques.
Tu savais, toi, que la ville était déjà là du temps des Romains ?
Ils ont retrouvé des thermes.
Askip, elle avait même une certaine importance commerciale.

Un peu plus loin, j'ai retrouvé la lumière du jour.
On était dans une salle avec une porte qui donnait sur le cloître.
Il y avait une guide avec un groupe de gamins.
Elle leur racontait que le mot « copain » venait de ce temps-là, dans les légions, et qu'il signifiait littéralement « celui avec qui on partage le pain ».
Y a eu une p'tite lumière dans ma tête, à ce moment-là.
C'est logique, en fait.
Tu me connais, j'ai jamais fait de latin, mais j'ai pensé à Astérix, quand César parle de « _panem et circenses_ ».
Du coup, _co-panem_, c'est assez explicite, quand t'y penses.

Je crois que t'as jamais fait un musée avec moi, mais tant mieux.
Mes potes aiment pas ça, je prends trop de temps parce que j'essaie d'en absorber un max sur ce qui est exposé.
Du coup, la nana et les mioches sont partis pendant que je lisais encore les affichettes.
J'ai hésité à la suivre pour bénéficier des infos, mais j'ai préféré rester en retrait.

Quand j'ai eu fini, je suis sorti à mon tour et je me suis retrouvé bête : il y avait des gens en toge un peu partout.
Si nos regards se croisaient, ils levaient la main droite et disaient « Ave » en souriant.
Je savais pas trop quoi faire alors je les ai juste imités.
Là, tu dois penser que j'ai été con ; c'était juste une reconstitution pour les gamins, pas vrai ?

C'est ce que j'ai fini par me dire aussi et je me suis un peu promené.
Un légionnaire est passé, a donné un message à un des « nobles » qui était là, et s'est barré.
Franchement, j'avoue que son costume m'a bluffé.
Souvent, dans ce genre de spectacle, tu vois assez rapidement que c'est du toc, t'es pas d'accord ?
Mais là, le cuir ressemblait à du cuir, le métal n'avait pas l'air d'être du plastique.
Je me suis demandé comment ils finançaient ce niveau de qualité sans faire payer la visite.
Ils avaient même décoré le cloître !
Les piliers ressemblaient à colonnes grecques, avec leurs rainures.

Il y avait des bruits de métal qui provenaient d'un coin.
Je me demandais ce que c'était et me suis laissé guider par le son.
Et là, je suis tombé sur une forge.
T'imagines le truc ?
Au milieu d'une abbaye déguisée en villa romaine, ils avaient installé une vraie forge, avec un mec qui chauffait et tapait l'acier comme à l'époque !
C'était pas du chiqué ; même à plusieurs mètres du machin, je transpirais tant ça chauffait.
À chaque fois qu'il trempait son morceau d'acier dans la bassine d'eau, ça faisait un gros nuage de vapeur.
On se serait cru dans un sauna !

J'ai repris la visite.
Derrière la porte suivante, c'était une toute autre ambiance : c'était des obsèques.
J'ai essayé de suivre quelques minutes, mais c'était pas du français.
Je veux dire, j'entendais quelques sons qui ressemblaient, mais pas plus.
J'ai quand même compris que le perso central de la scène était Charlemagne et qu'on enterrait sa femme.
J'avoue que l'ambiance était un peu glauque et le fait de rien piger m'a un peu gonflé, donc j'ai pris la porte.

Je me suis retrouvé au Moyen Âge. Littéralement.
Pas celui auquel on pense par défaut, avec des châteaux forts, des catapultes et des chevaliers en armure.
Plutôt la fin du Moyen Âge.
Pas encore la Renaissance, mais le truc commençait à venir quand même.
Ça se voyait aux costumes et ça s'entendait au parler.
Là, on reconnaissait le français, mais c'était quand même pas _notre_ français.
On était dans une salle ; un gars qui semblait être le propriétaire des lieux discutait avec un artisan de la peinture du plafond, et plusieurs mecs étaient perchés sur des escabeaux à faire de leur mieux pour le satisfaire.

En y regardant de plus près, j'ai été impressionné par leur travail.
Ils avaient reproduit des emblèmes dans tous les sens, comme une immense carte.
J'ai reconnu celui de notre région, tu vois lequel, les trois piafs blancs sur une bande rouge avec un fond jaune, mais y en avait plein d'autres.
Il y en avait un qui ressemblait au drapeau breton sans les rayures, il y avait un lion noir sur fond jaune, des poissons jaunes sur fond bleu ou blancs sur fond rouge.
J'aurais kiffé m'y connaître un peu plus pour comprendre ce que ça représentait et pourquoi ils étaient mis dans cet ordre précis.
J'ai failli demander aux acteurs, mais ils étaient tellement dans leur truc que je ne sais pas s'ils m'auraient répondu.

La porte suivante me ramena dans le cloître et tu me croiras pas, mais ils l'avaient complètement changé.
Les Romains s'étaient fait la malle, forge et tout, mais aussi les décorations.
Ça ressemblait bien à un cloître médiéval.
Les costumes ressemblaient à des soldats, je pense à peu près à la même époque que dans la salle ou un peu plus tard, mais pas encore la Renaissance.
Ça causait allemand.
Tu sais que c'était ma LV2, non ?
Mes parents avaient insisté, parce qu'on n'est pas loin, que notre ville a été allemande un certain nombre de fois, que l'Europe, tout ça…
Bref, avec mes quatre ans d'allemand dans les pattes, j'ai réussi à comprendre quelques mots de ce qu'ils baragouinaient.
Ils parlaient des armées françaises, du roi Charles, mais ça ne m'éclairait pas tellement.
Leurs reconstitutions n'étaient pas claires.

C'est seulement là que j'ai pigé.
Tu m'as peut-être pas attendu, mais il m'a fallu du temps : on fait pas des reconstitutions en latin, en vieux français ou en allemand ; on s'assure plutôt que tout le monde va capter.
Au moins, on met des panneaux pour que les gens comprennent de quoi il s'agit.
Si c'étaient pas des reconstitutions, à quoi on avait affaire ?
C'était clair : si c'était pas du faux, c'était du vrai.
Je voyais le vrai truc, j'avais voyagé dans le temps.

T'imagines un peu le vertige ?
En passant de simples portes, j'avais fait des sauts dans le temps, de cinq-cents, mille, deux-mille ans.
C'était un truc de malade !
Mets-toi dans mes bottes : quelques pas, des centaines d'années.
J'avais trop envie d'en voir plus, de ne pas juste suivre un parcours balisé, d'aller me promener dans le temps.

C'était comme si un petit démon s'était posé sur mon épaule pour me souffler des mauvaises idées.
J'ai regardé autour de moi si quelqu'un m'observait.
J'étais une caricature d'espion dans un mauvais film et je pense que n'importe qui d'un peu attentif m'aurait grillé tout de suite, mais j'étais trop excité.
Je me suis dirigé vers une porte derrière un cordon, dernier coup d'œil autour, et j'ai entrouvert.

Et là, pu… réé, c'était le jackpot !
C'était une rue, mais pas comme les rues de chez nous.
Ça m'a fait penser à des coins de Paris intra muros.
Je suis passé de l'autre côté et j'ai fermé la porte, histoire de ne pas me faire pincer, avant de regarder un peu plus autour de moi.
À ma droite, à quelques centaines de mètres, il y avait des barricades, mais personne derrière.
J'avais envie de voir de plus près de quoi il s'agissait et j'ai fait un pas quand j'ai entendu des cris derrière moi.
Non, pas des cris.
Des hurlements, des rugissements…
C'était animal.
Je me suis retourné en quatrième vitesse et ce que j'ai vu m'a fait penser à deux choses.

La première, c'était le tableau de Delacroix.
Il manquait que la liberté et son nibard à l'air.
Sauf que ça bougeait et ça criait, et c'est ça qui m'a évoqué un autre truc.
J'ai cru voir une armée de fans de l'OM se ruer sur un footballeur du PSG isolé.
On pourrait même pas qualifier ça de marée humaine parce que la rage dans leurs yeux n'avait plus rien d'humain.
J'ai plongé vers la seule issue que je connaissais pour les éviter, la porte par laquelle j'étais arrivé, et là, tu vas pas me croire…

J'étais dans une maison.
J'ai limite encore plus flippé que face au troupeau.
Non mais rigole pas !
Bien sûr, ça paraît logique : je suis dans une rue, je passe une porte, je suis dans une maison.
Sauf que je te rappelle que quand j'avais passé cette porte quelques secondes plus tôt, je venais d'un cloître à un autre endroit et une autre époque.
J'ai commencé à baliser, j'avais sérieusement pas envie d'être coincé à Paris en pleine révolution française.

J'ai ouvert la porte et elle donnait sur la rue.
Quelques-uns des mecs de la ruée m'ont regardé d'un air surpris et j'ai refermé rapidement.
J'ai commencé à courir dans la baraque, à tester toutes les portes.
Je suis sorti par une autre porte, j'ai essayé la cahute d'à côté.
Finalement, il y en a une autre qui s'est ouverte sur ailleurs, mais c'était ni mon époque, ni le cloître.

J'ai hésité un moment : sauter au hasard ou essayer de trouver d'autres portes là où j'étais ?
Il m'avait fallu plusieurs dizaines d'essais pour trouver cette porte, j'étais absolument pas certain d'en trouver une autre.
Peut-être qu'il y en aurait plus de l'autre côté.
T'aurais fait quoi, à ma place ?
Moi, j'ai sauté.

Et j'ai recherché une autre porte.
Encore, et encore.
J'ai perdu le compte des époques et des lieux que j'ai traversés.
Je ne regardais même plus, je voulais juste rentrer.

Il y en a une où j'ai vraiment eu les chocottes : j'ai débouché d'une porte dont il ne restait que l'encadrement.
Y avait que des plantes, partout des plantes qui poussaient sur des ruines.
Tous les passages que j'apercevais étaient cassés, le chambranle ne faisait plus un rectangle complet.
Tu penses qu'un passage temporel peut exister dans ces conditions ?
En tout cas, j'en ai vu aucun, jusqu'à trouver quelques portes intactes, dont celle qui m'a permis de reprendre.

À la fin, j'ai atterri sur un bateau.
Amerri serait plus juste, du coup.
J'étais encore en train de regarder autour de moi et chercher les portes à essayer quand j'ai senti une grosse main attraper le col de mon t-shirt et me soulever du sol.
J'ai pas pu me tourner pour voir la tête du mec quand il a gueulé à côté de mon oreille avec sa grosse voix.
« Cap'taine ! » qu'il a braillé. « On dirait qu'on a un passager clandestin. »

J'aurais voulu me débattre que j'aurais pas pu, pis vu la force du machin qui me tenait et les quelques regards en coin des autres gus sur le pont, ç'aurait pas été malin de ma part.
Un gars est descendu de la barre.
Tu voyais à ses fringues que c'était le capitaine, mais même sans ça, t'aurais pu le deviner rien à sa prestance.
Le mec s'est approché, il m'a regardé de haut en bas et de bas en haut, pas un mot, que dalle.
Il s'est juste tourné vers celui qui me tenait et a dit « La planche » avant de simplement retourner à sa place.

Tous ceux qui regardaient en coin se sont arrêtés, et les autres aussi, un sourire narquois en travers de la tronche.
T'aurais pu croire que c'était tout ce qu'ils attendaient.
Ils ont dégagé le chemin et le gars qui me tenait m'a traîné à travers le pont.
Vraiment traîné !
Il avait la force de me porter, ce fils de p… irate, mais il a préféré relâcher le bras, histoire de pas faire trop d'effort.

Il m'a jeté au début de la planche, c'est tout juste si j'ai pas roulé-boulé à la baille directement.
Mes mains ont trouvé le bord et elles se sont accrochées sans que je sache vraiment comment.
J'ai bloqué, complètement.
Tu sais, comme quand tu ouvres les yeux au bord du vide et que tu ne peux rien faire.
Pareil !
J'allais crever en mer, loin de toute côte, tué par des pirates, et personne ne trouverait mon corps.

Le gars qui m'avait jeté me hurlait d'avancer, mais je pouvais pas bouger.
C'est une autre voix qui m'a tiré de ma torpeur.
Elle était forte mais pas rauque comme celle des marins.
« Capitaine, vous oubliez notre arrangement ! »
J'ai tourné la tête.
Le mec était une armoire à glace en costume noir, devant la porte par laquelle j'étais arrivé.

Le capitaine l'a regardé en soupirant et a répondu un truc du genre : « Envoyez pas des divertissements à mes hommes pour leur retirer au dernier moment. »
Je savais pas trop ce qui m'attendait, mais le gars qui m'avait posé sur la planche s'est éloigné de moi.
Il avait les yeux fixé sur l'armoire à glace, et j'arrivais pas vraiment à lire sa tronche.
Ça ressemblait à une forme de crainte.
Ou de défi, je sais pas.

Pendant ce temps, le videur se promenait sur le pont comme s'il était chez lui ; personne ne cherchait à l'arrêter.
Il m'a chopé par le colbac au moment où le roulis d'une vague a failli me faire basculer, il m'a soulevé comme si je ne pesais rien, m'a posé sur mes pieds et a passé son bras autour de mes épaules.
Il m'a solidement agrippé et m'a guidé vers la cabine du capitaine.
Les matelots ne nous quittaient pas du regard, mais ils ne bougeaient pas non plus.

Le mec a ouvert la porte et c'était la Grèce Antique derrière.
Il m'a à moitié guidé, à moitié poussé, et on y est allés.
Il parlait dans sa barbe et m'engueulait en me disant que le parcours n'est pas balisé pour rien.
Il a dit des trucs à propos du glissement géographique, du contrôle des sauts, mais j'ai pas tout pigé.
J'ai pas osé la ramener ou poser de question ; le type avait l'air aussi aimable qu'une porte de prison.
C'est ça, rigole, mais t'aurais pas fait le malin non plus, à ma place.

En tout cas, il savait où il allait.
Il n'a jamais hésité, il m'a toujours conduit directement d'une porte à la suivante, et la dernière s'est ouverte à côté de la caisse du musée.
Il m'a lâché au milieu de la pièce, a fermé la porte et s'est barré sans un mot.
J'aurais voulu lui balancer un « merci », mais j'ai pas réussi à parler.

La dame à l'accueil avait regardé le truc en silence.
Elle a jeté un coup d'œil à sa montre et a ressorti mon sac de derrière son bureau.
J'ai ouvert la bouche pour la remercier, mais elle a juste levé le doigt et sa tête voulait tout dire.
Ouais, comme la prof de français quand t'essayais de justifier tes bavardages et qu'elle ne voulait rien entendre.
Donc je me suis contenté d'attraper mon sac et de me barrer sans faire le fier.

Y a pas eu de souci sur le chemin du retour.
J'étais pas perdu du tout, finalement.
Pas dans la ville, en tout cas, mais j'avais bel et bien réussi à me paumer dans le temps.

J'ai essayé de retrouver ce musée.
J'ai refait l'itinéraire, j'ai monté la grande rue en pente, je suis parti de la place d'armes.
Je suis sûr que c'était à cet endroit, mais pas moyen.
C'est comme s'il avait jamais existé.
Sinon, je t'aurais emmené faire une visite, plutôt que de te raconter.
Ça m'aurait évité que tu me prennes pour un demeuré.
Ouais, fous-toi de moi.
Je sais quand même que c'était pas une hallu.
