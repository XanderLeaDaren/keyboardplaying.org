---
date: 2021-03-13T13:19:42+01:00
title: Le cocon
slug: le-cocon
author: chop
license: CC BY-NC-ND 4.0

description: |-
    Le quart se terminait et la relève arrivait, mais lui ne s'en rendit pas compte.
    Sa remplaçante lui apprit qu'il ne faisait plus partie du cocon.
    C'était une nouveauté que son cerveau allait devoir apprendre à gérer.
    Mais qu'est-ce que cela pouvait bien signifier pour lui ?

inspirations: [ bimonthly ]
keywords: [ nouvelle, science fiction, science, système immunitaire, évolution, biologie, criquets, romance, partage, vie privée, conscience collective, nature, écologie, collapsologie ]

challenge:
  period: 2021-02-28
  rules:
  - type: theme
    constraint: Renouveau.
  - type: bonus
    constraint: Le/la protagoniste est religieux/religieuse ou tourné(e) vers la spiritualité.
---

La vie ne s'arrêtait jamais, à la ferme.
On aurait pu penser --- tout du moins, des individus extérieurs au cocon l'auraient pu --- que la culture des levures, les bacs hydroponiques et les élevages d'insectes constituaient l'essentiel du travail, mais ces tâches n'occupaient en réalité qu'une part mineure du personnel.
Une fois installées, ces structures évoluaient presque sans intervention humaine.
Par la suite, il n'était question que de surveillance et suivi, de gestion des imprévus, et d'élimination des souches malades pour éviter la contamination d'une récolte.
La ferme n'était pas toute neuve et bénéficiait de l'expérience de plusieurs installations similaires.
Les protocoles en place couvraient donc tous les aléas potentiels liés à ce fonctionnement.

Les tâches annexes représentaient une occupation bien plus prenante.
Il fallait séparer ce qui était destiné à la consommation de tout le reste.
Ces chutes étaient ensuite confiées à plusieurs équipes.
Les parties les plus tendres des végétaux constituaient l'alimentation de base des criquets et autres sources de protéines animales élevées sur place.
Du reste étaient extraits les nutriments et sels minéraux, réinjectés dans le circuit d'irrigation des cultures hors-sol afin de nourrir les nouveaux plants.
Il résultait de ces traitements une sorte de paille, qui servait à son tour de base à la fabrication d'emballages biodégradables pour les produits de la ferme.

Un technicien était occupé à ce poste depuis déjà six heures.
Debout devant son établi, il tournait le dos au reste de la pièce, sortait d'un bain le paillis qui avait trempé et le posait pour qu'il sèche, immergeait le lot suivant, puis commençait à presser celui qui précédait ces deux-là.
Accaparé par sa tâche, il n'avait pas remarqué que ses collègues dans la même pièce s'étaient levés silencieusement et étaient sortis simultanément.
Il ne remarqua pas non plus l'équipe du matin prenant place quelques secondes plus tard, l'air intrigué par sa présence.
Celle qui devait prendre sa place restait debout, en retrait, ne sachant que faire.

Sans aucun son, des échanges d'information eurent lieu.
De la même façon que tous --- hormis ce technicien --- avaient su que le quart était terminé et que les équipes changeaient, tous savaient désormais que l'un des employés ne réagissait plus aux informations partagées.
Plusieurs membres de l'équipe sortante tournaient la tête ou ralentissaient par curiosité lorsqu'ils passaient devant la porte de l'atelier de fabrication des emballages.
Certains essayèrent de le contacter télépathiquement, mais il ne réagit pas.

Un signal se propagea alors parmi les personnes connectées : ceci n'était pas une menace pour le cocon, mais un cas médical.
Immédiatement, plus personne ne se préoccupa de lui, excepté la jeune ouvrière qui attendait derrière lui.
Un message supplémentaire lui avait été adressé, avec des instructions spécifiques qu'elle s'empressa de suivre.
S'avançant d'un pas, elle posa sa main sur l'épaule de l'homme, toujours occupé à broyer son paillis, le faisant sursauter.

--- Mais… Comment ? Qui ?

Il ne comprenait visiblement pas comment ni pourquoi quelqu'un avait pu s'approcher de lui sans qu'il s'en rende compte.

--- Monsieur, vous n'êtes plus connecté au cocon.

--- Mais bien sûr que si ! Je…

Elle vit le doute dans son regard, savait qu'il tentait instinctivement de chercher une réponse dans la conscience collective.
Le cocon.
C'était un réflexe incontrôlé, il cherchait à savoir ce qu'il se passait.
Cette information n'étant pas disponible, il se rabattit sur la recherche d'une information courante.
N'importe laquelle.
L'heure qu'il était.
Combien de temps avant la fin de son quart.
Qui était cette jeune femme.
Aucune réponse ne vint, ses appels ne rencontrèrent aucun écho.
Là où il avait normalement accès au savoir de toute l'humanité, il n'y avait à présent qu'un immense néant.

Il ignorait que la sensation qu'il découvrait était similaire à celle d'un amputé qui découvre l'absence de son membre, bien que décuplée, car ce n'était pas son intégrité physique qui était en question.
C'était quelque chose d'immatériel, mais bien plus proche de son esprit, qui accompagnait ses pensées depuis aussi loin qu'il s'en souvienne.
Sa respiration et son rythme cardiaque accélérèrent, sa tête tourna, le sol se déroba sous ses pieds.
Il s'effondra au sol.
Elle ne s'en inquiéta pas, cela faisait partie de ses instructions.
Elle le prévint que la médecine du travail allait venir le chercher pour s'occuper de lui, lui dit qu'il n'était pas nécessaire de s'inquiéter et que tout allait rentrer dans l'ordre.
Il n'entendit rien.

Une chaleur rayonna en lui, partant du creux de son bras.
Observant les alentours, il se rendit compte qu'on l'avait déplacé.
Il était à présent assis dans une chaise d'examen recouverte de cuir, un homme en blouse blanche finissant de poser un pansement sur le point dont semblait irradier la vie qui l'emplissait à nouveau.
L'inconnu vit le regard de son patient et prit la parole :

--- Bonjour, monsieur.
Je suis le médecin du travail.
Vous étiez en état de choc suite à votre déconnexion du cocon, c'est une réaction tout à fait normale et bénigne.
Des brancardiers vous ont transporté à l'infirmerie.
Je me suis permis de faire un prélèvement sanguin avant de vous injecter un apaisant.
Il se peut que vous ressentiez comme une chaleur qui vient de l'intérieur, mais c'est tout à fait normal.

Le technicien digérait les informations.
Sa crise de panique lui revint en mémoire.
En forçant un peu, il pouvait presque se souvenir de deux personnes le portant hors de l'atelier sur un brancard, mais tout lui paraissait flou.
Par réflexe, il essaya de se connecter au cocon pour voir si d'autres témoins avaient partagé leurs souvenirs de la scène, mais il ne trouva une fois de plus que le néant.
L'apaisant du médecin devait cependant être efficace, car sa précédente panique ne se manifesta nullement.

Il se força à revenir au présent, se concentrant sur le praticien et, de la voix rauque de quelqu'un qui a perdu l'habitude de parler depuis longtemps, l'interrogea.

--- Pourquoi… Pourquoi est-ce que je n'ai plus accès au cocon ?

Le médecin se tourna vers son attaché, ouvert sur la table, et prit en main une fiole qu'il lui tendit.
La portant à ses yeux, l'ouvrier constata qu'elle était remplie d'un liquide opaque, presque noir, dans lequel scintillaient des paillettes.
Non, c'étaient de petits éclairs lumineux.

--- Ce que vous voyez, monsieur, c'est un échantillon de votre sang.

Le technicien fut interloqué et regarda la fiole de plus près avant de se tourner à nouveau vers l'homme en blouse blanche.

--- De mon sang ?
C'est impossible !
Pourquoi est-il si sombre ?
Qu'est-ce que c'est que ces étincelles ?

--- C'est assez symptomatique de votre condition, monsieur.
Votre greffon est mort.
Il se désagrège et les cellules nécrosées passent dans votre flux veineux, ce qui explique la couleur.
Quant aux petites lumières que vous voyez, elles sont caractéristiques de certaines réactions chimiques qui se produisent lorsque ces cellules se dégradent.
Il n'y en aura plus aucune d'ici quelques heures.

--- Mais comment un greffon peut-il mourir ?

--- De plusieurs façons.
Un greffon est un organisme vivant, il peut tomber malade, il peut vieillir… C'est aussi un corps étranger au sein du greffé, il se peut que le système immunitaire de ce dernier se retourne contre lui.

Le patient semblait comprendre les causes, mais ce furent soudain les conséquences qui s'imposèrent à lui.

--- Mais alors, est-ce que cela veut dire que je serai définitivement déconnecté du cocon ?
Est-ce que je suis devenu un…

Il buta sur le mot suivant, ne pouvant se résoudre à l'idée.

--- Je ne peux rien affirmer pour l'instant.
Je dois d'abord analyser vos échantillons de façon plus poussée pour comprendre les causes du… décès.
Si c'est votre système immunitaire qui rejette la greffe, nous ne pourrons effectivement pas prendre le risque d'en tenter une nouvelle.
Cependant, il est fort possible que ce soit juste une mort naturelle.
Ce n'est pas courant, mais ça arrive.
Dans ce cas, il faudra patienter le temps que votre corps ait éliminé le greffon actuel, puis nous pourrons envisager une nouvelle greffe.
Vous retrouveriez dans ce cas l'intégralité de vos facultés vis-à-vis du cocon.

--- Et… est-ce que je pourrai continuer à travailler ?
La greffe n'est pas un préreq…

--- Non, je suis désolé.
Dans ce genre de cas, nous préférons que le patient soit en arrêt de travail jusqu'à une nouvelle greffe ou jusqu'à la fin de la période nécessaire à son adaptation.
Bien entendu, cela ne remet en cause aucun de vos acquis ; votre bonus sera toujours comptabilisé et les jours d'arrêt seront comptés comme des jours ouvrés.
Vous serez juste prié de ne pas venir travailler.

Le médecin s'assura que son patient avait bien toutes les informations nécessaires, lui fit remplir quelques formulaires administratifs, puis lui indiqua qu'un vélotaxi l'attendait devant l'entrée du personnel.
Bien qu'il eût été prêt à parcourir à pied les deux kilomètres le séparant de sa résidence, le technicien se conforma aux instructions qui lui avaient été données et fut chez lui en quelques minutes.
Il remercia son chauffeur en descendant de la banquette et entra dans le bâtiment, agissant davantage par réflexe que par volonté.

Sa journée habituelle commençait par un lever à 23 heures, un premier repas et la mise en chemin pour prendre son quart à minuit.
Six heures plus tard, les équipes tournaient et il était généralement à la résidence à 6 h 20, prenant une douche pour laisser derrière lui les odeurs de la ferme.
S'ensuivait un repas commun avec ceux de ses corésidents qui travaillaient sur le même quart que lui.
Ils pouvaient occuper le reste de la matinée comme ils le souhaitaient et, à 11 heures, partageaient leur dernier repas de la journée avec ceux qui venaient de se lever.
Ils allaient généralement se coucher entre midi et 2 heures.

Les sanitaires partagés étaient vides lorsqu'il y entra.
Les circonstances exceptionnelles avaient complètement décalé son emploi du temps ordinaire.
Le vide de la pièce communautaire procurait un sentiment étrange, accentuant le malaise lié à son incapacité nouvelle à sentir les gens de son entourage.
Cherchant une forme de réconfort dans les habitudes, il se déshabilla malgré tout et se plaça sous une des douches.

L'eau lui parut fraîche.
Elle était à la même température que tous les jours, préréglée à un compromis idéal entre confort de tous et consommation minimale d'énergie.
Pourtant, sans être désagréable, il pensa que quelques degrés supplémentaires ne rendraient la douche que plus agréable.

Lavé et séché, il se hâta de rejoindre la cuisine pour manger.
Sur son chemin, deux de ses colocataires s'interrompirent dans leur ménage pour le fixer d'un regard curieux.
Leur expression lui était familière, il l'avait lui-même portée plusieurs fois.
Lorsque des greffés étaient en présence d'un humain dont ils ne sentaient pas le greffon, plusieurs réflexes entraient en jeu.
Tout d'abord, ils surveillaient l'intrus, comme un système immunitaire cherchant à évaluer la menace.
Comme ce système, ils interrogeaient généralement le cocon pour rassembler les informations connues.

--- Mon greffon est mort, je dois attendre que mon corps l'élimine avant de pouvoir en recevoir un nouveau.

À cette déclaration, ses deux corésidents lui lancèrent un regard compatissant, certainement accompagné d'une pensée réconfortante qu'il ne put percevoir, et retournèrent à leur tâche.
Le médecin n'avait visiblement pas diffusé son état comme une information publique, mais le fait qu'il ait communiqué l'information à des personnes qui vivaient avec lui avait suffi à donner une crédibilité acceptée par la conscience collective.

Ceci lui fit réaliser qu'il était devenu invisible au sixième sens de ses corésidents.
Cependant, il jugea les choses bien plus perturbantes pour lui-même : il ne possédait désormais plus que cinq sens pour détecter s'il était seul ou non, cinq sens qu'il savait parfaitement faillibles.
Un sentiment de vulnérabilité nouveau l'envahit, s'alliant à la sensation de faiblesse qui s'était emparée de lui depuis qu'il avait réalisé sa déconnexion.

Une assiette l'attendait à la cuisine, qu'il dévora.
Les émotions l'avaient visiblement creusé et il engloutit son plat, bien qu'il le trouvât gustativement très insatisfaisant.
C'était une recette qu'ils préparaient régulièrement, parfaitement équilibrée au niveau des apports nécessaires, mais elle lui sembla aujourd'hui morne et fade.

C'était normalement le jour où il contribuait au ménage et il rejoignit les deux habitants qui s'y affairaient déjà.
La tâche n'était pas compliquée, chacun faisant naturellement attention à ne pas salir, mais la poussière se déposait en dépit de l'activité humaine.
Cela ne se déroula cependant pas comme il l'avait espéré : privé de sa quasi-télépathie et sans la moindre habitude à lire le langage corporel, il n'arrivait pas à anticiper les actions de ses corésidents, qui lui conseillèrent finalement de les laisser faire et de profiter de son temps.

Bien qu'il fût obligé d'admettre qu'il n'était pas capable de collaborer dans son état du moment, il fut piqué au vif par ce qui ressemblait à une rebuffade.
Se saisissant d'une veste légère, il quitta leur grand logement en claquant la porte d'entrée.
Il n'eut pas conscience de son geste, mais ses corésidents présents s'interrompirent quelques secondes pour observer l'origine de ce son si inhabituel, avant de finalement se remettre à leurs occupations.

Sans but précis, il erra à travers la cité, observant la diversité de ses bâtiments et leur intégration à la nature.
Le bois était omniprésent.
Il était même courant que ce soit le matériau primaire des constructions.
Il jouxtait parfois la pierre ou des matériaux plus transformés mais plus adaptés à des structures particulières, comme du béton ou des polymères.
Partout était visible le souci d'intégrer la nature aux architectures humaines, ou plutôt d'installer la vie humaine en perturbant le moins possible le cadre originel.
Pratiquement tous les toits étaient couverts de végétation, faisant de la ville un immense jardin habité.
Un lièvre détala à sa droite, le faisant sursauter.
L'animal ne devait pas avoir l'habitude d'être dérangé à cette heure.

Quelle heure était-il, d'ailleurs ?
Comme à chaque question qu'il se posait, il chercha la réponse dans le cocon, mais ne trouva qu'un vide insondable.
Le déséquilibre qu'il éprouvait à chaque fois était cependant de moins en moins important, comme si son esprit avait déjà trouvé une rambarde pour le protéger de cet abîme.
S'appuyant sur une méthode moins fiable mais toujours à sa disposition, il tenta d'estimer le temps passé depuis la fin de son quart, son état de choc, son déjeuner, sa tentative de ménage…
Il décida qu'il devait être entre neuf et dix heures.
Cela lui laissait deux bonnes heures avant la fin de sa journée.
Deux longues heures qu'il ne savait pas comment occuper.

Levant les yeux, il aperçut face à lui une grande butte couverte d'herbe mais dépourvue d'arbres.
Connaissant les règles d'architecture de sa ville, il savait qu'il s'agissait d'un bâtiment largement souterrain.
En bénéficiant de l'inertie du sol, les concepteurs réduisaient ainsi les coûts nécessaires à la régulation thermique de ces édifices, qui faisaient de parfaits entrepôts pour les biens fragiles ou précieux.
Tournant autour de cette colline, il découvrit la façade de la médiathèque municipale.
Il n'avait jusqu'aujourd'hui jamais ressenti la nécessité de la visiter, mais elle pourrait peut-être offrir des réponses aux questions qui se bousculaient dans sa tête depuis ce matin.

C'était la première fois qu'il entrait dans un immeuble souterrain.
Les logements étaient généralement surélevés et la ferme, au niveau du sol, exploitait la clarté et la chaleur du soleil pour faire pousser ses cultures.
Aussi fut-il surpris de découvrir qu'un lieu enterré pouvait être si lumineux, la place mettant à profit de nombreux puits de lumière stratégiquement placés.
La documentaliste se rendit immédiatement compte que le nouvel arrivant était totalement perdu et alla à sa rencontre afin de lui souhaiter la bienvenue, lui présenter l'organisation et les règles de l'endroit, et lui faire remplir les formalités nécessaires pour accéder sans greffe aux documents conservés dans cet antre de savoir.

Les informations qu'il avait reçues lui donnèrent des idées et il alla regarder ce que contenaient les rayons sur la greffe et le cocon, jusqu'à tomber sur un immense volume multimédia intitulé « Histoire des greffons : de la technologie destructrice à la biotechnologie salvatrice ».
La quatrième de couverture vantait la précision et le professionnalisme cette œuvre, qui abordait notamment l'avènement de la biotechnologie et du greffon, l'émergence du cocon et, avec lui, la fin des pratiques autodestructrices de l'être humain.

Il remplit donc la fiche d'emprunt et emporta le tome dans la salle de lecture au niveau le plus élevé de la structure, près de l'entrée.
Il n'y avait qu'une seule personne dans la pièce, une jeune femme regardant ce qui semblait être un documentaire vidéographique, un casque sur les oreilles.
Il s'installa à un poste.
Par un jeu de réflecteurs, la lumière tombait de sorte à éclairer le volume à une intensité confortable pour lire.

Le technicien ouvrit le volume, remarqua les disques conservés dans la deuxième de couverture, passa les gardes et découvrit une photographie qui occupait l'entièreté de l'imposante page.
Dans un premier temps, il fut incapable d'identifier ce qu'il avait sous les yeux.
Tout était gris, rectiligne.
En arrière-plan, du bleu et des nuages… Le ciel.
C'était donc un paysage.
Il s'approcha pour observer de nombreux points, semblables à des fourmis, visibles à tous les niveaux de la représentation et recula lorsqu'il reconnut des êtres humains.
Ils étaient minuscules face à l'immensité de ce qu'il identifia à présent comme une ville.
Les immeubles, entièrement de verre et de métal, se dressaient à l'assaut du firmament.
Entre eux, une étendue de béton recouvrait l'ensemble du sol.
La seule source de verdure visible était constituée de quelques arbres malades disposés à intervalles réguliers de chaque côté de la chaussée.
Sur cette dernière, des cabines, elles aussi faites de métal, semblaient servir de moyen de transport.
La légende indiquait qu'il s'agissait d'une cité du début du XXI<sup>e</sup> siècle, à l'apogée de la civilisation humaine prégreffon, et que le premier documentaire sur le disque « contexte » donnait une meilleure idée de la situation de l'époque.

La page suivante était une préface, tentant d'expliquer ce qui avait permis l'acceptation du greffon comme une nécessité.
Un passage en particulier marqua le lecteur.

> À la fin du XX<sup>e</sup> siècle, les transhumanistes souhaitaient améliorer la condition humaine, nos capacités physiques et mentales notamment, par le biais de la technologie.
> Certains déclaraient que nous étions arrivés au bout de notre évolution naturelle et que c'était le seul moyen qu'il nous restait de progresser.
> D'autres philosophes déclaraient au contraire que cette volonté d'utiliser la science pour faire mieux que la nature, en voulant éliminer les imperfections, les maladies et la mort, était ce qui empêchait la sélection des forts menant à l'évolution et l'apparition de nouvelles espèces.
> Pour contraires qu'elles soient, ces deux thèses avaient cependant l'intérêt de mettre en avant un aspect primordial du tempérament humain : fiers de notre cerveau, de ce qu'il nous a permis de bâtir, nous nous jugeons maîtres de la création, plus sages que des processus millénaires qui ont pourtant permis notre apparition.
>
> Toutefois, même si nos compétences techniques et savoir-faire sont indéniables, nous n'avons jamais su contrôler ce qu'il restait de plus primordial au cœur de nos cerveaux.
> Nous savions que notre monde n'était pas infini, que notre modèle social et économique nous incitait à vivre à crédit auprès d'une planète que nous étions en train de détruire.
> Nous n'avons jamais ralenti.
> Notre striatum, profondément enfoui au sein de nos têtes, préférait récompenser la satisfaction immédiate, reléguant ces soucis à plus tard, à jamais.
>
> C'est cette faille de notre schéma de pensée que le greffon est venu corriger, afin de nous aider à tirer le même plaisir à œuvrer au bien commun que celui que nous tirions de la satisfaction de nos envies égoïstes et délétères pour l'ensemble.

Les premiers chapitres étaient passionnants.
Ils traitaient de plusieurs découvertes scientifiques qui avaient servi de fondation aux greffons : l'entrepreneur milliardaire ayant fait créer un implant cérébral pour contrôler les appareils numériques par la pensée ;
la chercheuse ayant élaboré les premiers processeurs basés sur les neurones ;
l'arrivée des biomachines et la notion de santé plutôt que de maintenance pour les instruments, ainsi que les nombreux gains environnementaux ;
le conglomérat ayant mis à profit ces travaux pour créer les premiers bioprocesseurs ;
l'évolution enfin pour créer le premier bioimplant cérébral, le prototype de ce qui deviendrait le greffon.

--- Excusez-moi, monsieur, j'ai un courrier pour vous.

Il se retourna, encore troublé de réaliser qu'on pouvait l'approcher sans qu'il le réalise.
Face à lui se trouvait une femme en livrée de messagère, lui tendant un porte-bloc avec un crayon.
Son air exaspéré et le regard curieux de la lectrice installée derrière elle indiquaient qu'elle avait déjà tenté d'attirer son attention.

--- Pardonnez-moi, j'étais absorbé, dit-il en se saisissant du stylo pour signer le bon de remise.

Pendant une fraction de seconde, il s'était demandé comment elle avait pu le trouver puis songea qu'elle était connectée : elle avait diffusé dans le cocon une image de lui et la documentaliste avait répondu qu'elle l'avait accueilli.

Les formalités terminées, la messagère lui remit une enveloppe et se dirigea vers la sortie, avant de se raviser.

--- Si je peux me permettre, monsieur… vos corésidents sont inquiets.

--- Inquiets ? Mais pour quelle raison ?

Elle l'observa comme si la réponse coulait de source.

--- Il est près de quinze heures, monsieur.
Ils s'attendaient à vous voir rentré et couché à cette heure.

Sur ces mots, elle partit, le laissant interdit.
Il n'éprouvait pas la fatigue habituelle qui accompagnait cet horaire tardif, à peine une légère faim.
Se ressaisissant, il ouvrit l'enveloppe et examina son contenu, une lettre du médecin confirmant la mort naturelle du greffon.
Il serait par conséquent éligible à une nouvelle greffe.
Le courrier lui fixait également un rendez-vous dans dix jours pour une prise de sang.
Si celle-ci prouvait que les restes avaient été complètement éliminés, une nouvelle greffe pourrait avoir lieu immédiatement.

--- Donc là, tu viens d'apprendre que tu auras droit à un nouveau greffon dès que ton corps aura fini d'absorber le précédent, j'ai juste ?

Il regarda celle qui s'était ainsi adressée à lui, assise à l'envers, le coude sur le haut du dossier et le menton dans la main.
Incontestablement jolie, elle devait avoir la trentaine.
Et indubitablement ingreffée également.
Son maquillage, ses piercings, ses longs cheveux rouges, le tatouage qui dépassait de son col sur la droite de son cou, sa posture, ses vêtements… Toute son apparence semblait servir à marquer sa différence.
Rares étaient les greffés à éprouver le besoin de ce genre de fioritures, et aucun n'en accumulait autant.
Comme si elle sentait qu'il avait terminé son inspection, elle jugea bon de se présenter.

--- Je suis Tanja.
Et toi…

Il ouvrit la bouche pour répondre, mais elle ne lui en laissa pas le temps.

--- Non, ne t'en fais pas, je sais que la plupart des greffés n'ont pas de nom, que vous n'en avez pas besoin puisque votre transmission de pensées vous permet de savoir sans ambiguïté que l'on s'adresse à vous ou que l'on parle de vous.
Tu vas vite te rendre compte que ce n'est pas pratique lorsqu'on n'a que la parole pour communiquer.
Je t'appellerai Ben, ça te va ?
Très bien, allons-y pour Ben.

Ce personnage le laissa coi.
Sans avoir besoin de télépathie, elle semblait capable de deviner ce qu'il pensait et presque de lui imposer ses propres idées.
Il hésitait sur ce qu'il pourrait dire ensuite.

--- Donc, Ben, je suppose que tu as entre une et deux semaines à patienter avant ta nouvelle greffe.
Est-ce que ça te dit de les passer chez nous ?

--- Pardon ? J'ai un toit…

--- Dans une résidence, de ce que j'ai compris.
D'après ce qu'elle disait, tu fais le demi 0-12.
Un des changements auxquels tu vas devoir faire face, c'est que, sans greffon, ton corps va caler son rythme sur celui du soleil.
C'est lui qui dira à ton corps quand produire de la mélatonine, plus ta greffe.
En gros, tu seras réveillé la journée et tu auras sommeil la nuit.
Je pense que ça a déjà dû commencer.
Tu as envie de dormir, là ?
Ça va rapidement devenir compliqué lorsque le corésident avec qui tu partages ton lit voudra se coucher aux alentours de minuit alors que ton cerveau te dira que c'est le milieu de son dodo.

Elle en savait visiblement beaucoup sur les résidences.
Elles avaient été conçues pour minimiser l'impact individuel sur l'environnement, en mutualisant les ressources autant que possible.
Une vingtaine de personnes vivait ainsi avec « Ben ».
Les greffons permettaient à leurs porteurs de fonctionner par cycles de douze heures décalés en quatre demis, 0-12 pour minuit à midi, 6-18, 12-0 et 18-6.
Ainsi les personnes éveillées de midi à minuit occupaient les mêmes lits que ceux qui dormaient sur ce créneau.
Elle semblait également savoir à quoi il devait s'attendre.

--- Vous rencontrez souvent des personnes dans ma… situation ?

--- Non, pas souvent, mais ça m'est arrivé.
Tu es le troisième, en fait.
On est une petite communauté d'ingreffés.
Alors, qu'est-ce que t'en dis ?
Tu viens ?

--- Il faudrait au moins que je prévienne…

L'inconnue l'interrompit en se tournant vers la documentaliste.

--- Madame ?
Est-ce que vous voulez bien prévenir les corésidents de monsieur qu'il sera hébergé par les ingreffés du secteur 7 pendant les prochains jours ?
Merci !
Allez, viens, on y va.

Sur ses pieds, elle l'avait attrapé par la main et tirait gentiment sur son bras.

--- Quoi ?
Mais il faut que je range…

--- Fais-moi confiance.
Elle sera bien plus heureuse de pouvoir rendre service à la communauté qu'elle ne pourrait te reprocher de laisser en désordre.

Réagissant à la nouvelle impulsion qu'elle insuffla à son bras, il se leva à son tour et la suivit.
Cette femme était fascinante, complètement différente de tout ce qu'il avait pu être et connaître dans sa vie.
Il n'avait jamais observé cet entrain et ce dynamisme chez un autre greffé, et il était curieux de voir ce qu'il pourrait découvrir d'autre avec elle, surtout après le contact très insatisfaisant avec ses corésidents aujourd'hui.

Tanja courait presque devant lui, se dirigeant, comme elle l'avait annoncé, vers le secteur 7, un quartier en bordure de la cité.
Sur leur passage, les quelques passants qu'ils croisaient ne s'attardaient pas sur elle, signe qu'ils la connaissaient.
Lui en revanche retenait les regards, même si cela ne durait généralement que quelques secondes.
Il était reconnu comme un élément étranger, mais le cocon le connaissait et il était accompagné d'un être accepté par la conscience collective.

Elle l'emmena vers une résidence fort similaire à celle où il vivait.
Elle se retourna vers lui en arborant l'air satisfait de l'enfant qui fait découvrir quelque chose dont il est fier.

--- Ça doit vraiment ressembler à ce que tu connais.
On essaie de réduire notre empreinte, nous aussi, mais en conservant les « limitations » de notre cerveau.
On occupe un logement optimisé, mais on n'est que huit.
Ça permet d'avoir un lit pour chacun, puisqu'on dort tous en même temps, et on a même quelques lits d'ami --- dont un pour toi !

Tanja attrapa à nouveau la main de Ben et le hala dans l'entrée du logement en criant :

--- Salut, tout le monde !
On a un invité !
Je vous présente Ben, il attend que son greffon soit dissous pour pouvoir en recevoir un nouveau.
Taï, amène-toi !

Trois personnes se présentèrent à l'appel de Tanja, une femme et deux hommes.
L'un d'eux s'imposait par sa taille et sa musculature.
Son teint hâlé laissait penser qu'il travaillait en extérieur.
C'est à lui que Tanja s'adressa avant de disparaître vers la cuisine :

--- Ah, Taï, t'es certainement le mieux placé pour l'accueillir.
Je vais préparer de la pitance --- c'est un 0-12, il n'a pas eu son troisième repas et je l'ai fait cavaler.
Et j'ai la dalle aussi, cria-t-elle dans son dos alors qu'elle n'était déjà plus dans la pièce.

--- Ah, alors avec vous aussi, elle est… impatiente ?

Tous sourirent et le dénommé Taï prit la parole.

--- Oui, c'est assez caractéristique de Tanja.
Bienvenue, Ben.
Je pense que tu as compris que je suis Taï.
Voici Oswin et Raissa.

Chacun fit un signe de tête à l'évocation de son nom et Ben les salua en retour.

--- Nos autres colocataires font leur quart, tu les rencontreras au cours du repas du soir.
Viens, allons à la cuisine pour discuter calmement.

Oswin et Raissa ne les suivirent pas, revenant à ce qu'ils faisaient avant son arrivée.
Taï et Ben entrèrent dans la cuisine et virent Tanja affairée aux fourneaux.
Taï s'assit à table, imité par Ben qui prit la parole.

--- Taï, pourquoi est-ce que Tanja a dit que tu étais le mieux placé pour m'accueillir ?

--- Parce que j'ai été à ta place il y a un peu moins de vingt ans.
J'opérais mon travail habituel et mon greffon est mort, sans raison ni avertissement.
En arrêt, j'ai rencontré un ingreffé qui m'a hébergé au sein de cette communauté, et puis j'ai choisi d'y rester.
Je tiens à te rassurer, tu es libre de partir quand tu veux, mais mon expérience peut t'être utile pour le temps où tu seras sans greffe.

--- Tanja a dit que j'étais le troisième… dégreffé qu'elle rencontrait.

--- Oui.
Je suis le premier et nous en avons croisé un autre.
Il a bien voulu discuter avec moi, qui ai eu un greffon, mais il a toujours refusé de rester à proximité du reste de notre groupe.

--- Comme si nous étions contagieux ! s'exclama Tanja en posant une assiette et des couverts devant Ben.

Elle attrapa l'autre assiette, s'assit face à lui et entreprit de manger.
L'odeur de son plat donna à Ben l'impression de sentir son ventre se creuser et il s'empressa de faire comme elle.
La première fourchette le surprit : après le repas fade et morne de six heures, il avait à présent en bouche une explosion d'arômes.
Il examina de plus près ce qu'elle avait servi et reconnut un plat à base de levures et légumes, mais les couleurs en étaient étranges.

--- Oui, c'est une des premières choses que j'ai découvertes : il suffit de peu pour rendre les repas bien plus agréables.
Les textures et les saveurs se mélangent et créent quelque chose d'unique.
À une époque, on écrivait des livres entiers sur les meilleures façons de marier et préparer au mieux les aliments pour en faire quelque chose de plaisant en bouche.
Si tu veux mon avis, résiste à la tentation de manger trop vite et prends le temps de savourer chaque bouchée.

Ben prit à cœur ce conseil, se forçant à poser des questions pour se laisser le temps de mâcher et déguster son plat.
Il remarqua que les trois cas étaient des hommes, Taï répondit qu'il avait fait le même constat, mais n'avait pas les connaissances nécessaires pour l'expliquer.
Ben demanda pour quelles raisons cette communauté restait ingreffée, Taï répondit qu'elles étaient variées, mais qu'il faudrait demander à chacun s'il voulait en savoir davantage.
Ben s'interrogea également sur la présence d'autres ingreffés dans la ville et Tanja, qui avait fini de manger à ce stade de la conversation, répondit qu'elle avait connaissance d'au moins trois autres communautés, dont au moins une comptait une trentaine de membres.

À l'issue du repas, Ben se sentit somnolent et Tanja l'accompagna au dortoir, lui aussi semblable à celui dont il avait l'habitude.
Elle sortit des draps propres qu'elle posa sur un lit visiblement inoccupé.
Elle alla ensuite tirer des rideaux sur les fenêtres pendant que Ben préparait le lit, tout en jetant des coups d'œil intrigués à la jeune femme.

--- Vous n'avez pas ça chez toi, j'imagine, mais sans greffon, on dort mieux dans l'obscurité.
C'est pour ça que tu n'avais pas sommeil à la médiathèque : tu avais la lumière du soleil juste devant toi.

Elle le laissa une fois que tout fut prêt et il s'endormit rapidement, ne se réveillant que plusieurs heures plus tard.
Entendant des voix, il sortit et trouva huit personnes dans la cuisine.
Oswin, Raissa, Taï et Tanja étaient là, ainsi qu'Erminie, Ilja, Naveae et Raleigh.
Tous accueillirent chaleureusement Ben et lui indiquèrent la place qu'ils lui avaient réservée entre Tanja et Taï.
Il mangea léger, n'étant pas habitué à un repas à cette heure, et fut ravi d'apprendre à connaître ses hôtes.

Vint le moment de se préparer pour la nuit.
Le passage aux sanitaires communs le surprit, ceux-ci ayant aussi été agrémentés de rideaux.
Raleigh lui expliqua que les ingreffés étaient souvent plus sensibles à leur vie privée et leur intimité.
Naveae émit une plaisanterie sur la gêne à montrer certains attributs, à laquelle il répondit en faisant claquer sa serviette vers elle.

La nuit fut perturbante.
Tout d'abord, Ben était bien plus conscient que d'habitude qu'il partageait les lieux avec d'autres dormeurs.
Il entendait leurs mouvements, leur respiration, leurs ronflements.
Il était habitué à un sommeil profond, qui venait rapidement et ne se dissipait qu'à l'heure voulue.
Vers deux heures, le sommeil le fuit mais il resta dans son lit, ne voulant pas déranger les autres et ne sachant pas ce qu'il pourrait faire de toute façon.

<!-- J-9 -->

Au petit déjeuner, Taï le rassura : le changement de cycle était pénible car le corps devait modifier ses habitudes, mais ce serait certainement beaucoup mieux le lendemain.
Le dégreffé les laissa ensuite pour se rendre à son quart de travail, qui consistait à s'occuper des espaces végétaux contrôlés --- les jardins et toitures végétales.
Tanja proposa à Ben de l'accompagner à son bureau, une agence de contrôle des niveaux de pollution où elle était laborantine.

À leur arrivée sur place, elle lui demanda de l'attendre à l'extérieur quelques minutes.
En observant le bâtiment face à lui, sur deux niveaux, avec de grandes baies vitrées, il repensa à la photographie de la veille.
Il essaya d'imaginer comment c'était, recouvrit dans sa tête toute la végétation par du béton, prolongea les bâtiments indéfiniment vers le haut, remplit le tout d'une marée humaine… Un vertige le saisit et il perdit l'équilibre.
Tanja, qui approchait silencieusement pour le faire sursauter, eut le temps de le rattraper.

--- Eh bah, tu me tombes déjà dans les bras ?
Je suis flattée, mais pas si facile.
Ça va ?

Ben se releva, un peu gêné et rougissant un peu.

--- Oui, merci.
Je me demandais comment ils avaient pu en arriver au stade d'étouffer la nature.
On va à ton bureau ?

--- Non, j'ai pu prendre congé.
Je t'aime bien, donc j'ai décidé d'être disponible pour toi le temps où tu seras sans greffon.
Viens, je connais un endroit où on sera bien pour discuter de la question que tu viens de poser.

Elle l'emmena sur une colline couronnée d'un arbre centenaire.
Assis à l'ombre de celui-ci, ils pouvaient observer l'ensemble de la cité --- du moins, ce qui n'en était pas caché dans la forêt.
Ils passèrent un certain temps à observer silencieusement le paysage, puis Tanja prit finalement la parole.

--- Tu sais ce qu'est le circuit de la récompense ?
C'est un mécanisme enfoui très profondément dans notre cerveau, hérité des plus anciennes espèces vertébrées qu'on connaisse.
Pour résumer, c'est une partie de notre cerveau qui va libérer des hormones du plaisir lorsque nous effectuons des actions propices à notre survie et à celle de notre espèce.

--- Comme quoi ?
Manger ?

--- Manger, oui, bien sûr.
Mais aussi prouver notre rang social, montrer qu'on domine les autres, qu'on est plus fort, plus intelligent, plus doué…
Qu'on est un meilleur reproducteur.

Ben eut un doute sur la direction que prenait cette conversation.
Dans le cocon, le sexe n'était pas interdit, bien au contraire.
Il faisait partie des besoins humains et le cocon permettait de trouver un partenaire compatible, souvent pour une nuit.
Ils n'avaient pas à s'inquiéter d'infections, aujourd'hui presque éradiquées : si un membre du cocon était malade, il devenait abstinent, puisque ne pas propager son mal était l'intérêt de la communauté.
En revanche, la population était contrôlée et donc la reproduction, limitée.
On cherchait par conséquent les couples aptes à transmettre les meilleurs gènes.
Ben savait qu'il avait peu de chance de trouver une partenaire avec qui il formerait un tel tandem.

--- Tu sais, je ne fais pas partie des géniteurs.

Elle le regarda un instant et éclata de rire en comprenant son chemin de réflexion.

--- T'es bête !
Je ne te propose rien, je sais comment fonctionne le cocon à cet égard.
J'essaie justement de t'expliquer pourquoi.

Il écouta, intéressé.
Tout venait de ce circuit de la récompense, qui avait été parfait pour mener l'évolution jusqu'à l'apparition de l'être humain, mais était devenu sa perte une fois qu'il s'était cru maître de la nature.
Ben repensa à la préface du volume qu'il avait commencé la veille.

Selon Tanja, les humains avaient continué à se laisser guider par cette zone profonde de leur cerveau, certains l'exploitant pour leur profit, en faisant un modèle économique, un moyen d'accumuler plus de pouvoir et de satisfaire davantage ce circuit primaire.
L'espèce se multipliait à une allure exponentielle, exploitant les ressources plus vite que la planète ne pouvait les produire, épuisant celles qui ne se renouvelaient pas.
La technologie menaçait de s'effondrer car elle s'appuyait lourdement sur cette dernière catégorie, l'activité humaine modifiait le climat et décimait la biosphère, et aucun effort n'avait été fait malgré des décennies d'avertissements, parce que le cerveau préférait se concentrer sur la survie immédiate de l'espèce et occulter les besoins à long terme de l'espèce et de la planète.

Des scientifiques modifièrent les premiers bioimplants cérébraux pour inhiber cette zone égoïste et la remplacer par une récompense lorsque l'action effectuée devait servir le plus grand nombre.
Les premiers essais prouvèrent qu'il fallait trouver le bon équilibre entre les pulsions individuelles et le bien-être de la planète.
Certains des premiers greffés se laissèrent simplement mourir, n'éprouvant plus le moindre besoin d'assurer leur survie.
D'autres privilégièrent la communauté à leur propre personne, tombant de faim et d'épuisement.
Il y eut même des cas de greffés se suicidant après avoir réalisé que leur présence en ce monde pouvait nuire à l'espèce ou à l'environnement.

Après ces premiers ajustements, les greffons atteignirent leur objectif et des coopératives de greffés virent le jour, autonomes, mettant à mal les modèles économiques basés sur la surconsommation.
Un basculement s'opéra.
Ceux qui voulaient contribuer à un autre avenir et qui hésitaient jusque-là franchirent le pas.
Une seconde réalisation devint évidente lorsque s'accrut le nombre de greffés : la notion de bien commun était subjective et des initiatives mal éclairées pouvaient s'avérer contre-productives.
Il était nécessaire de coordonner cette connaissance et ces efforts, de créer un lien entre tous qui permettrait à chacun de savoir si ses actions allaient vraiment profiter aux autres.
Ainsi naquit une nouvelle génération de greffons qui permit une première forme d'échange de connaissances.
Ce lien créa rapidement une forme de conscience collective.
_Collective conscience_ en anglais.
Le cocon dans le langage courant.

Tanja sortit des sandwiches et des fruits du sac qu'elle avait pris en partant le matin et ils mangèrent, Ben réfléchissant à ce qu'il avait appris.

Après l'avènement du cocon, les greffons évoluèrent.
Ils ne contrôlèrent plus uniquement le circuit de la récompense, mais la production d'autres hormones également.
En modifiant le cycle du sommeil naturel, il fut possible de découper la journée en quatre demis, permettant de mieux mutualiser les ressources et infrastructures, donc de produire moins, donc de consommer moins de ressources.
En contrôlant le besoin de se reproduire, on put ramener en quelques décennies la population humaine à une taille qui ne menaçait plus la planète.
Les grands centres urbains de béton furent progressivement démantelés ou abandonnés, au profit de cités intégrées à la nature.
La biosphère reprit ses droits.

--- Tu sais ce qui me tue, en vrai ?
Le cocon et les greffons ont certainement sauvé l'espèce humaine, mais ils nous ont coûté notre humanité.

Ben la regarda silencieusement, ne comprenant pas ce qu'elle voulait dire.
Elle reprit quelques secondes plus tard.

--- Hier, à la médiathèque, je regardais des films d'avant les biotechnologies.
J'aime bien, ils avaient une imagination débordante.
Ils rêvaient de plein de choses, et chaque individu avait une personnalité propre.
Avec les greffés, j'ai l'impression que tout le monde est pareil.
Tout ce qui les intéresse, c'est faire ce qui sera le mieux pour tout le monde.
Ils se lèvent, vont travailler, et quand ils rentrent, ils ne font rien pour eux.
Le cocon a changé le rythme de vie pour qu'ils aient davantage de temps libre, mais on dirait des robots.
C'est comme s'ils ne savaient plus faire autre chose.
Le cocon a sauvé la nature, mais les greffés ne savent plus l'admirer.

Cette remarque plongea Ben dans une profonde réflexion.
Il repassa dans sa tête tout ce dont il se souvenait de sa vie.
Avait-il déjà fait autre chose que travailler ?
Quand avait-il vraiment observé ce qui l'entourait avant d'être dégreffé ?
Tanja finit par couper ses pensées.

--- Ben, qu'est-ce que t'aimes faire quand tu travailles pas ?

--- Heu, je ne sais pas.
On peut faire quoi ?

La mélancolie qui s'était posée sur le visage de Tanja s'envola.

--- Ce qu'on veut !
C'est ça qui est merveilleux.
On peut se promener, on peut créer des choses, utiles ou juste belles.
On peut aussi ne rien faire et passer la journée sous cet arbre à observer la ville.
Tu sais quoi ?
J'ai une idée : il nous reste neuf jours avant que tu doives décider si tu rejoins le cocon ou non.
Pendant ce temps, on va essayer plein de trucs pour voir ce qui te plaît.
Viens, on retourne à la résidence, on va avoir besoin de matériel !

À partir de cet instant, les jours de Ben furent une suite d'expériences nouvelles.
Tanja ne se contentait pas de lui dire quoi faire, elle l'accompagnait dans tout ce qu'elle lui faisait entreprendre.
Cela commença par la course qu'ils firent jusqu'à la résidence et qu'elle remporta haut la main.
Ben prenait tout autant soin de sa santé que n'importe quel autre greffé, mais il n'avait pas l'habitude comme elle de courir juste pour le plaisir de le faire, de dépasser les limites apparentes du corps.

Ce même jour, ils expérimentèrent le reprisage de vêtements abîmés.
L'occupation lui parut plutôt ennuyeuse, même si Tanja prenait grand plaisir à faire les réparations les plus voyantes qu'elle pouvait, en utilisant des fils et des morceaux de tissu contrastant fortement avec l'étoffe endommagée.
Il était évident qu'il devrait apprécier cette activité : rendre service aux propriétaires de cet habit ; à la communauté dans son ensemble, même, en évitant de fabriquer un nouvel objet quand il était possible de réparer un existant.
Oswin, qui travaillait à la tisserie, parla d'apporter de quoi s'essayer à la couture, au tricot et au crochet le lendemain.
Lui non plus ne tirait que peu de satisfaction du rafistolage, mais le fait de créer était différent.

Ce soir-là, Tanja exposa son projet aux autres : faire découvrir à Ben des activités individuelles, du type de celles que les greffés ne pratiquaient plus parce qu'elles n'apportaient rien au grand tout, mais qui avaient été des hobbys humains.
La compétition sportive, l'humour, l'art…
Tout le monde accueillit la nouvelle avec beaucoup d'enthousiasme, voulant prêter la main au projet :
Erminie, la doyenne de la résidence avec sa cinquantaine, lui laissa le libre accès à la collection de livres qu'elle avait amassée au fil des années, constituée aussi bien de travaux documentaires et d'essais philosophiques que de classiques de la littérature de fiction ;
Raissa proposa de lui donner quelques leçons de dessin et peinture ;
Taï voulait lui faire découvrir les plaisirs du jardinage ;
Raleigh et Naveae offrirent d'organiser un tournoi sportif qu'ils appelèrent des « jeux ».

Ben commença à comprendre au cours de ce repas ce dont Tanja avait voulu parler sur la diversité des êtres humains.
Il n'était entouré que de huit personnes, mais chacune d'entre elles était différente des autres.
Leurs propositions faisaient déjà montre de la variété de leurs intérêts, mais cela allait bien plus loin que cela.
Tanja était exubérante, comme si elle cherchait à être vue, remarquée et reconnue pour sa différence.
À l'autre extrême, Ilja semblait d'un naturel taciturne, ne parlant que lorsqu'il l'estimait nécessaire, et son apparence physique reflétait un souci de la simplicité presque monacal.
Entre ces deux extrêmes, chaque membre de la communauté avait une identité propre : Taï était une force tranquille, dont le calme et le ton posé suffisaient à créer un charisme et une présence indéniables ; Naveae animait les conversations par des plaisanteries constantes et un humour caustique que tous semblaient apprécier --- ou au moins tolérer, peut-être en raison de son rire contagieux.

Lorsqu'ils se levèrent de table, Ben se saisit de son assiette pour aider, mais Taï lui fit signe de l'accompagner et ils laissèrent les autres ranger.
Suivant son hôte, ils escaladèrent une échelle qui n'existait pas dans sa propre résidence et débouchèrent sur le toit.
Taï s'assit au bord, les jambes dans le vide, imité par Ben.

--- J'adore cet endroit.
C'est ma vue préférée sur la cité.

--- C'est intéressant.
Tanja m'a montré la cité depuis la colline.
C'est différent.

--- Sa vue à elle, de l'extérieur.
Ici, on est au cœur des choses.

Un silence s'ensuivit, au cours duquel Ben apprécia encore la diversité des perspectives d'un individu à l'autre.
Il s'interrogea : lequel des deux points de vue préférait-il, lui ?

--- Je m'attendais à ce que tu me demandes pourquoi j'avais choisi de ne pas retourner dans le cocon.

--- Je n'ai pas osé poser la question.
J'ai un peu peur de me laisser influencer par la réponse.
Tout ce que j'ai toujours cru savoir est mis à mal depuis deux jours, et je ne sais pas encore si c'est une bonne ou une mauvaise chose.

--- Je vais tout de même te dire pourquoi : j'ai choisi de rester ici parce que j'ai découvert ce que Tanja veut te montrer.
La joie d'avoir une identité, des envies qui nous sont propres.
Pouvoir prendre plaisir à travailler, pas parce que c'est utile mais parce que j'aime ce que je fais, ou encore le délice de voir éclore mes fleurs pas pour les consommer mais simplement pour les admirer.
Tout ça, ce sont des choses que je n'éprouvais pas dans le cocon.
Les greffons ont été conçus pour sauver l'espèce humaine et la Terre, mais ils nous ont coûté ces petites satisfactions.

Une identité…
Les membres du cocon n'avaient que très rarement un nom, hormis ceux qui interagissaient souvent avec des ingreffés, comme les agents administratifs.
On constatait des personnalités différentes, mais seulement légèrement.
Globalement, tout le monde était pareil et avait les mêmes activités : prendre soin de la planète, prendre soin de la communauté, prendre soin de soi.
Dans cet ordre.
Le lien qui les unissait tous apportait une pensée presque unique.

--- Je n'essaie pas de te dire quoi penser, Ben, je partage simplement mon point de vue.
Il y a d'énormes inconforts à se passer du cocon quand on y a grandi.
Les mots ne sont parfois pas assez précis pour échanger des idées.
On ressent d'énormes insécurités à ne pas savoir ce que les autres pensent de soi.
Au sein du cocon, on a l'énorme confort de ne pas avoir à se poser de questions, à savoir ce qui est bon pour soi et pour tous.
Et pourtant, ces incertitudes et ces petits défis sont aussi ce qui fait le charme de l'existence « à l'ancienne ».
Profite de ces quelques jours pour découvrir tout ce que tu peux et, dans huit jours, tu pourras faire un vrai choix.

<!-- J-8 -->

Le lendemain, Ben suivit Tanja en frange de la cité, près des ruines de la vieille ville.
La grisaille de la photographie lui revint en tête, mais elle était ici tempérée par la nature qui avait repris ses droits, se glissant dans la moindre fissure, créant des taches de brun et de vert un peu partout.
Il restait bien du goudron au sol, mais des plantes l'avaient brisé en morceaux pour sortir ou au contraire pour y entrer des racines.
Tanja raconta que la population avait graduellement décliné au fil des années, les greffés migrant progressivement vers la cité-forêt, les anciens immeubles abandonnés finissant par se dégrader et s'effondrer d'eux-mêmes.
Le lieu où elle le conduisit sembla cependant relativement épargné.
On aurait dit une route de gravier rouge, qui tournait en rond et ne menait nulle part.

--- Ceci est un complexe sportif d'époque que les ingreffés essaient de maintenir en état.
Tu ne peux pas participer aux jeux sans entraînement, aussi allons-nous venir ici un jour sur deux.
On va voir si tu peux être assez en forme d'ici le tournoi pour me battre.
Dans le sac que tu portes, j'ai emprunté une tenue de Raleigh, elle devrait être à ta taille.
Suis-moi, je te montre les vestiaires.

Leur première séance dura la matinée entière.
Ben courut, sauta, fit travailler des muscles dont il n'avait jamais eu conscience auparavant.
Il éprouva de la fatigue, de la douleur, des points de côté, mais aussi une satisfaction non dissimulée à dépasser ses limites.
Les greffons n'avaient pas inhibé tous les circuits de la récompense, mais les greffés n'utilisaient que peu ceux liés à la pratique sportive.
Ben les redécouvrait : l'endorphine euphorisante aidant à moins sentir la douleur, la dopamine diminuant la sensation de fatigue et l'adrénaline qui préparait le corps à l'effort.
Ce ne fut cependant pas sa seule motivation : Tanja l'avait défié et il était bien décidé à ne pas se laisser distancer.
Par ailleurs, sa tenue de sport la couvrait moins que ses vêtements habituels, révélant ses membres musclés, délicatement sculptés, et Ben prit plaisir à pouvoir les observer, mais cela l'obligeait à ne pas être trop loin derrière elle.
Ses motivations étaient telles qu'elle le félicita sur sa réussite générale.

Après l'exercice, ils prirent une douche dans les vestiaires avant de se changer.
Le complexe n'était bien entendu plus relié à l'eau courante, mais les ingreffés avaient mis en place un système fonctionnant à la récupération d'eau de pluie.
Il n'y avait pas de rideaux ici, mais les cabines étaient séparées par des murs, cachant le corps d'un sportif à ses voisins.

Après déjeuner, Raissa s'absenta pendant que les autres débarrassaient, puis revint et posa sur la table trois blocs de papier ainsi qu'une collection de crayons, puis elle rassembla quelques fruits et légumes.
Elle initia ainsi Ben au dessin en commençant par une nature morte.
Il lui fallut plusieurs essais, et beaucoup de gomme et de patience, mais il parvint à reproduire assez fidèlement le modèle qu'il avait devant les yeux.
Tanja finit les contours bien plus rapidement que lui, profitant du temps supplémentaire pour essayer d'ajouter des détails et des ombrages d'ajouter des ombrages dans le temps supplémentaire, mais fut légèrement contrariée lorsque vint le moment de comparer les résultats.

--- C'est bien, dit-elle, mais c'est très académique, ce que tu as fait.
Il ne faut pas hésiter à inventer un peu, à insuffler ta propre vision à ton dessin.
Sinon, autant prendre juste une photo.

--- OK, interrompit Raissa, ça fait un moment qu'on est concentrés là-dessus, je vous propose de faire une pause.
Le cerveau aime bien ne pas rester trop longtemps sur une tâche qui exige beaucoup de lui.
Il est bon de s'aérer la tête.
Vous reviendrez me voir si vous voulez reprendre ?

Ben choisit de mettre ce moment à profit pour entamer sa première lecture et se rendit dans le salon commun, là où Erminie avait assemblé sa bibliothèque.
Elle avait accroché une note à son attention bien en évidence, où elle lui proposait quelques œuvres de différents styles afin qu'il puisse découvrir ce qu'il préférait, et quelques indications pour trouver les volumes au milieu des centaines d'œuvres.
La première proposition était un essai de Dufer sur le fonctionnement de la psyché humaine et son lien avec le cerveau et les hormones.
Faisant confiance à la bibliophile, Ben suivit les instructions et trouva rapidement la tranche vert émeraude.
La lecture n'en était pas particulièrement digeste, mais les interprétations de l'auteur, qu'il prenait soin de toujours présenter comme étant ses hypothèses et non une vérité, donnaient une vision sur la profondeur insoupçonnée de la personnalité humaine, à une époque prédatant le greffon mais à laquelle la compréhension du cerveau était déjà avancée.

--- Ben ? l'interrompit Taï.
Je vais aller jardiner, ça te dit de m'accompagner ?

Ben acquiesça et s'apprêtait à fermer son livre lorsque Taï y glissa un bout de carton.

--- Un marque-page.
Tu me remercieras quand tu voudras reprendre.
Tanja, tu veux venir aussi ?

Ils suivirent tous les deux le dégreffé jusqu'au toit de la résidence et Ben remarqua une zone ménagée où se côtoyaient de nombreuses plantes auxquelles il n'avait pas prêté attention dans la pénombre la veille.
Les couleurs et les formes se mélangeaient.
Quelques tomates mûrissaient à côté de fleurs que Ben ne connaissait pas.
Une brise souffla et Ben frissonna.

--- Désolé, Ben, déclara Taï.
Ce n'est pas le moment le plus chaud de la journée, mais c'est pour ça que je viens maintenant.
C'est pour ça aussi que je travaille le matin dans les jardins de la ville.
Les plantes n'aiment pas qu'on s'occupe d'elles au moment où le soleil est brûlant.

--- Les plantes n'_aiment_ pas ?

Taï eut un petit rire.

--- Ça semble ridicule, hein ?
Elles n'ont pas de cerveau.
Biologiquement, rien ne permet de leur prêter d'intentions ni de volonté.
Pourtant, ouvre un peu ton cœur et passe un peu de temps avec elles, et tu verras que tout n'est pas si nuancé.

Taï présenta les différents végétaux de son jardin, lesquels étaient comestibles, lesquels il cultivait par simple plaisir, lesquels enfin avaient juste décidé de pousser là.
Il donna quelques conseils à Ben et Tanja, qui l'aidèrent à s'en occuper, en prenant soin de ne pas déranger les abeilles qui venaient butiner les fleurs.
Tanja s'arrêta près des épis de roses trémières qui grandissaient à l'abri du vent, le long du gigantesque tronc contre lequel était bâtie la résidence, et demanda à Taï la permission de cueillir quelques fleurs.

Après une demi-heure à s'occuper du jardin, Tanja et Ben découvrirent que Raissa avait monté deux tabourets et deux chevalets, ainsi qu'un nécessaire de peinture.
Ils choisirent chacun une fleur et s'attelèrent à la représenter.
L'abeille sur le tableau de Ben semblait plus vraie que nature.

Au moment du dîner, Ben demanda si certains d'entre eux voulaient bien partager les raisons derrière leur non-greffe.
Raleigh répondit en premier pour Ilja et lui, expliquant qu'ils avaient une incompatibilité biologique avec les greffons.
C'était une cause plutôt courante parmi les ingreffés de sexe masculin.
D'ailleurs, la plupart des cas de dégreffe connus, comme Taï et Ben, étaient aussi majoritairement des hommes.
Personne n'avait pu expliquer ce phénomène.
Pour Raissa, les causes étaient autres : ses parents avaient rejeté la greffe au nom de leur religion et l'avaient élevé dans cette foi.
À leurs yeux, le bioimplant allait à l'encontre du libre arbitre dont l'Être Suprême avait doté l'Humain.
Erminie appuya cette vision, mais d'un point de vue plus philosophique que religieux : l'appartenance au cocon semblait priver les êtres humains de tout ce qui avait permis leur évolution jusqu'à ce stade, leur capacité d'innovation et leur force d'initiative, en faisant des automates sans conscience au service d'une entité indéterminée.
L'échange vira rapidement au débat, Taï réfutant l'absence totale de conscience des greffés, les autres cherchant à nuancer ou au contraire à appuyer les propos de leur aînée.
Erminie prenait plaisir à relancer la discussion lorsqu'elle s'essoufflait, s'appuyant sur ses multiples lectures pour fournir des arguments à tous les camps.

Après le repas, les ingreffés aimaient profiter de leur temps libre séparément ou en petits groupes avant de se coucher.
Ben décida de désormais mettre ce moment à disposition pour avancer dans ses lectures.
Il retrouva ainsi le marque-page que Taï avait glissé dans l'ouvrage qu'il avait commencé plus tôt et poursuivit, un sourire aux lèvres.
Absorbé par le texte, il ne s'arrêta pas avant la fin du volume et fut le dernier à rejoindre le dortoir.

<!-- J-… -->

En se levant le jour suivant, il trouva Tanja dans la cuisine.
Sur le plan de travail, elle avait posé deux betteraves rouges et les fleurs qu'elle avait cueillies la veille.
Elle demanda à Ben de découper les tubercules en morceaux pendant qu'elle disposait sa cueillette pour la faire sécher.
Elle plaça ensuite les cubes de la racine dans un demi-litre d'eau qu'elle mit au réfrigérateur.
Ils vaquèrent ensuite à une nouvelle activité.

Avec son programme chargé, Ben ne vit pas les jours passer.
Son entraînement de sport lui fit découvrir les courbatures ; le côté créatif du tricot et de la couture avec Oswin étaient effectivement plus satisfaisants que le reprisage, mais toujours pas à son goût ; les lectures d'œuvres de fiction stimulaient son imagination et lui faisaient envisager des mondes qui ne l'avaient jamais effleuré, et ses progrès en dessin et peinture lui apportaient un bonheur incommensurable.

Réel et fictif commençaient à se mêler dans ses propres créations.
Sur la colline où Tanja l'avait emmené observer la ville le deuxième jour, il peignit la cité.
Seulement, ce n'était pas exactement celle qu'il avait sous les yeux.
Il n'y avait plus de bâtiment humain, juste des plantes où vivaient des êtres qui avaient parfaitement fusionné avec la nature.

Le personnage au premier plan rappelait toutefois un élément du réel proche de lui, avec sa silhouette élancée, son visage mutin et ses longs cheveux rouges.

<!-- J-4 -->

Quatre jours avant son examen, Ben se leva après tout le monde et trouva Tanja accroupie à côté de son lit, encore en tenue de nuit, les mains jointes devant le visage.

--- Tanja, ça va ?
Tu fais un malaise ?

L'intéressée laissa tomber son front sur ses mains, un large sourire apparaissant de part et d'autre.

--- Non, tout va bien, répondit-elle en tournant la tête vers lui.
Tu n'as jamais vu quelqu'un prier, c'est ça ?

--- Prier ?
Comme prier une entité supérieure ?
Non, je n'ai jamais vu.
Les greffés ne sont pas religieux.

--- Et Taï dit que vous êtes les plus proches de pouvoir parler à Dieu ! se moqua Tanja en se relevant.
Viens, allons petit-déjeuner.
Je descends d'une famille d'amish.
Est-ce que tu connais ?
Non, bien entendu.
Les amish vivaient en communautés, principalement aux États-Unis.
Les personnes extérieures les connaissaient assez mal et pensaient qu'ils refusaient tout progrès technologique.
L'exemple le plus courant était leur rejet de la voiture, puisqu'ils préféraient utiliser des carrioles tirées par des chevaux.

--- Et c'était faux ?

--- Ce n'était en tout cas pas entièrement juste.
Certaines communautés amish avaient accepté la voiture.
En fait, avant d'accepter ou de refuser une technologie, ils en débattaient.
Dans le cas de la voiture, ils avaient déduit que l'accepter faciliterait les déplacements, ce qui risquait de pousser des personnes à s'éloigner.
Ils craignaient que cela nuise à l'intégrité de leur communauté, au fait que tous vivent ensemble.

--- C'est pour ça que tu es ingreffée ?
Tu n'as pas répondu, l'autre soir.

Elle ne put retenir un discret sourire de plaisir en apprenant qu'il avait remarqué.

--- La discussion a dévié.
Ce n'est pas vraiment pour ça, non, mais ça a joué dans mon choix.
Quand je suis née, mes parents n'ont pas voulu me soumettre à la greffe.
Ils préféraient que j'attende d'être en âge de décider par moi-même.

--- Et qu'est-ce qui t'a poussée à décider dans ce sens ?

--- Je te l'ai déjà dit, ou presque : je ne veux pas m'effacer.
Je ne sais pas si tu te souviens, mais l'emprise du greffon se renforce avec l'âge.
Chez les enfants, même s'ils sont greffés très jeunes, leur comportement est complètement libre.

 --- C'est parce que les premières expériences contribuent énormément au développement du cerveau, de la psychomotricité à la personnalité.
L'enfant doit atteindre un certain stade de développement avant que le greffon ne commence à vraiment s'intégrer.

--- C'est ça.
Je suis allée à l'école, j'ai grandi avec des enfants.
Ils étaient greffés et moi non, mais ça ne faisait aucune différence à nos yeux.
On est arrivés à l'adolescence, l'âge où normalement on commence à vraiment se lier aux autres, et où on se rebelle contre l'autorité.
À la place, ce fut l'âge où mes amis ont commencé à me dire « c'est pas bien », à abandonner leurs hobbys parce qu'ils n'étaient pas utiles au grand tout, à abandonner les idées qui n'étaient pas celles de la majorité, à parler de moins en moins parce que ça ne leur était pas nécessaire pour communiquer.
Ils devenaient tous identiques, tout lisses, ils perdaient les aspérités qui avaient fait jusque-là leur charme.
Je me sentais seule, et le lien que je recherchais n'était pas celui qu'ils me proposaient à travers le cocon, alors j'ai fui les relations.
Je me suis concentrée sur mes études, j'ai obtenu un job qui me plaisait, et j'ai eu la chance d'y rencontrer Raissa.

--- Je vois.
Et tes parents ?
J'imagine qu'ils font aussi partie d'une communauté, pourquoi ne pas être restée avec eux ?

--- Après m'avoir vue ce matin, tu vas te moquer, mais ils font partie d'un groupe très religieux, très… pratiquant.
Le culte a une place centrale pour eux, et je voulais conserver une indépendance d'esprit que j'avais peur de sacrifier en restant près d'eux.
Ça ne nous empêche pas de nous voir de temps à autre.

--- Je ne me moque pas, mais je ne comprends pas non plus.
Pourquoi pries-tu si tu n'y crois pas ?

--- Je n'ai pas dit que je n'y croyais pas.
J'ai du mal à croire que, par le plus grand des hasards, la vie soit apparue de nulle part, au milieu de matière inerte, ou qu'elle ait naturellement évolué pour donner des quadrupèdes, puis des bipèdes et enfin des êtres raisonnés, capables de philosopher et de vénérer leur Créateur, puis de le jalouser au point de vouloir s'approprier et contrôler sa création.

--- Alors quoi ?
Un démiurge nous a créés pour qu'on s'agenouille devant lui ?

--- Non, c'est très anthropocentrique de penser ça.
C'est comme « Dieu a créé l'homme à son image.
» Pourquoi aurait-il fait ça ?
Ça ne colle même pas à l'Histoire ; on sait qu'il y a eu plusieurs espèces d'hominidés.
C'est plus une justification des Hommes pour se croire supérieurs au reste de la création --- même supérieurs à la femme, qui n'est qu'une copie d'Adam dans la Bible de mes parents.
En vrai, je ne l'imagine pas comme une entité dotée de conscience ou de volonté.
Ce n'est ni le Dieu de mes parents ni l'Être Suprême de Raissa.
C'est plutôt une force ou un principe trop grand pour que nous le comprenions.

--- Tu parles de quelque chose qui ne peut pas t'entendre et encore moins te comprendre, et tu le pries pourtant ?

Les joues de Tanja rosirent légèrement.

--- C'est un réflexe qui m'est resté de l'enfance, quand mes parents m'enseignaient leur religion et leurs rites.
Lorsque j'ai vraiment envie que quelque chose se produise mais que j'ai le sentiment de ne rien pouvoir y faire, je demande.
C'est égoïste de penser que quelqu'un écoute ou exauce mes vœux, mais c'est humain d'espérer.

--- Et qu'espérais-tu, en l'occurrence ?

Elle le poussa au niveau de l'épaule, lui faisant renverser le contenu de sa fourchette sur la table.

--- Que tu es curieux !
Dépêche-toi de terminer et de te préparer.
Il fait beau, aujourd'hui.
On va aller randonner et faire le tour de la cité.
Je te préviens, on va rentrer tard et on se lève tôt demain pour la dernière séance d'entraînement.

<!-- J-3 -->

Ben se plia à ces consignes, mais la façon dont Tanja avait évité sa dernière question avait piqué sa curiosité.
Ils avaient passé toute la semaine ensemble et c'était la première fois qu'il la voyait prier.
Se pouvait-il que ce soit en lien avec lui ?
Lui s'était indubitablement attaché à elle, mais elle, si indépendante…

--- Ben !
Dépêche !
Il n'y aura pas de raccourci sur l'itinéraire d'aujourd'hui.
On part en retard, on rentre en retard.

La tête qui avait surgi au coin du mur disparut et Ben s'efforça de mettre de côté ses interrogations.
Ils firent comme prévu le tour de la cité.
Oswin les réveilla au petit matin, avant de prendre son quart, comme Tanja le lui avait demandé la veille.
Ils se préparèrent et se rendirent immédiatement au complexe.
En arrivant sur place, ils aperçurent deux silhouettes qui s'en allaient.

--- C'est Naveae et Raleigh.
Mais… qu'est-ce qu'ils font ?

--- Ils viennent souvent s'entraîner ici le matin, avant de prendre leur quart.
Tu as dû remarquer que ce sont de grands sportifs.

--- Non, je veux dire, ils avaient leurs bouches collées l'une contre l'autre.



--- Tu… Je… Wow.
Désolée, je ne m'attendais absolument pas à ça.
Tu ne sais pas ce qu'est un baiser ?
Ou un couple ?

--- Un couple, comme lorsque deux géniteurs s'associent pour engendrer et élever un enfant ?

--- C'est très limitant, comme vision.
L'amour, ça te dit quelque chose ?
Non.
Ça va être long de t'expliquer.
Naveae et Raleigh sont amoureux, ils tiennent beaucoup l'un à l'autre.
Enfin, c'est plus que ça, c'est quelque chose à la fois de sentimental et de physique.
Ils se font du bien simplement par leur présence, l'autre lui manque quand il n'est pas là.

Ben était très intéressé.
N'était-elle pas en train de décrire ce qu'il commençait à ressentir pour elle ?
Sa simple présence était apaisante et il ne s'inquiétait de rien lorsqu'elle était alentour.
Tanja continuait son explication.

--- … alors ils ont choisi d'avoir une relation particulière, quelque chose qui ne leur appartient qu'à eux.
Tu as aperçu un petit moment de leur intimité et de leur amour.

Ben se tourna vers elle, intrigué.

--- Lorsqu'ils se sont baisés ?

Tanja éclata d'un rire franc et eut besoin de quelques secondes pour retrouver son sérieux.

--- On dit « s'embrasser ».
Et oui, c'est une preuve d'affection très personn…

Ben n'attendit pas la fin de la phrase de Tanja pour poser ses lèvres sur les siennes, essayant d'imiter le geste des deux silhouettes.
C'était ce qu'il voulait.
Il avait grandi dans un monde où tout était à tout le monde, et par conséquent rien n'était à personne.
Il voulait quelque chose de différent, aujourd'hui.
Il voulait être à elle, rien qu'à elle, et à personne d'autre.

Il recula en observant le visage de Tanja, appréhendant sa réaction face à cette invasion non sollicitée de son espace personnel et fut surpris de la voir sourire.

--- C'était… très maladroit.
Laisse-moi te montrer.

Elle passa ses bras autour des épaules du jeune homme et le tira doucement pour rapprocher leurs visages avant de l'embrasser.
C'était effectivement très différent de la tentative de Ben.
Beaucoup plus agréable.

--- Il y a d'autres choses que l'on peut faire, quand on est en couple.
Des choses que tu connais déjà, mais qu'on ressent différemment.
Viens…

L'entraînement n'eut pas lieu ce jour-là.
Elle le mena vers une zone du complexe où se trouvaient plusieurs petites pièces, semblables à des dortoirs, mais équipées chacune d'un seul ou deux lits.
Ils en choisirent un qui n'était pas défait et y passèrent la matinée.
Ce n'était pas la première fois pour Ben, mais c'était différent de tout ce qu'il avait connu jusqu'alors.
Bien évidemment, il ne partageait pas un lien télépathique avec Tanja ; il ne pouvait pas deviner ce qu'elle voulait autrement que par les subtils signes qu'elle lui communiquait, les expressions qui lui échappaient ou les légères pressions de ses mains qui le guidaient.
Il ne ressentirait pas non plus les échos du plaisir de la jeune femme ni elle ceux du sien.
Pourtant, un lien autre les unissait : elle n'était pas juste une « partenaire compatible » qui partageait les mêmes envies que lui à un moment donné, elle était une personne unique au monde.

Ils passèrent la matinée dans cette chambre, allongés.
Ben observa le corps de la jeune femme nue contre lui.
Le tatouage qu'il avait déjà aperçu représentait un serpent.
Sa queue, au milieu de la poitrine, se prolongeait en un corps filiforme, suivant la courbe d'un sein, passant le flanc et remontant au niveau de l'omoplate pour laisser apparaître sa tête sur le côté du cou.
En la caressant, il laissa ses mains suivre l'animal dessiné et sentit une cicatrice.
Il repensa à une histoire qu'elle lui avait racontée, au sujet d'un accident qui avait failli lui coûter la vie et embrassa la tête du reptile, provoquant un frisson qui parcourut le corps de Tanja et recouvrit ses bras de chair de poule.

Ils rentrèrent à la résidence comme ils le faisaient après chaque entraînement et ne cherchèrent pas à dissimuler leur affection.
Plusieurs des ingreffés exprimèrent leur contentement à constater l'évolution de cette relation.
Tanja et Ben partagèrent le même lit pour les deux dernières nuits et profitèrent encore plus intensément de chaque instant.

<!-- J-1 -->

Les jeux arrivèrent rapidement.
Erminie ne concourut pas, mais elle ouvrit la cérémonie, rappelant la tradition des jeux olympiques de la Grèce Antique, en l'honneur de Zeus.
Elle conclut toutefois son discours en déclarant que ce tournoi n'était pas organisé pour les dieux, mais pour rapprocher les femmes et les hommes.
Les participants étaient inégaux face aux épreuves.
À la course et au saut d'obstacles, Naveae était imbattable, Raleigh la talonnant.
Ben était généralement quatrième derrière Tanja, mais les temps étaient serrés.
Taï prouva que sa musculature n'était pas qu'une apparence en empochant quelques belles victoires sur les épreuves de force.
Les résultats finaux n'intéressèrent cependant pas réellement les athlètes, plus heureux de se retrouver que de s'affronter.
Tous les habitants de la résidence avaient aménagé leur emploi du temps pour pouvoir être présents à ce qu'ils qualifiaient de célébration, cette cérémonie d'au revoir à Ben, le neuvième membre de leur famille qui s'apprêtait à les quitter.
La fête se prolongea après le tournoi par un pique-nique et un bel après-midi à l'extérieur.

Tous s'étaient attachés à lui et personne n'avait osé lui demander s'il pensait rester.
Tout le monde s'attendait à ce qu'il annonce soudainement qu'il ne voulait pas les laisser, mais cette annonce ne venait pas.
Tanja savourait chaque instant, son sourire plus grand encore qu'avant sa rencontre avec Ben, mais elle faisait de son mieux pour ne pas penser au lendemain.

<!-- Jour J -->

Le jour fatidique arriva pourtant.
Ben se réveilla seul dans le lit et se rendit dans la cuisine.
Il y trouva des restes de fleurs et reconnut les roses trémières dont les pétales semblaient avoir été écrasés, ainsi que les morceaux de betterave rouge mis à part dans un bol.
Tanja en revanche ne revint qu'au moment où il rangeait les affaires de son petit déjeuner.
Elle avait quelque chose de changé, mais il fut dans un premier temps incapable de dire quoi.
Soudain, il réalisa que les reflets de ses cheveux avaient changé de couleur, plus rouges, presque violacés, et il comprit enfin le sens des restes végétaux qu'elle s'apprêtait à emporter au composteur de la résidence.
Croisant son regard, elle comprit qu'il avait remarqué.

--- Tu aimes ?

--- C'est très joli, oui.
Mais est-ce que tu as fait ça parce que…

--- Pas uniquement.
C'est une occasion à marquer, mais j'aime bien essayer les effets de différents pigments.

Il prit une douche avant de partir et se mit en chemin pour son rendez-vous.
Elle l'accompagna en silence, les deux prenant force dans la présence de l'autre.
Tanja s'arrêta à l'extérieur du cabinet.

--- Je ne peux pas te suivre.
Je ne peux pas te voir greffé à nouveau, rejoindre le cocon et nous oublier.

--- Je comprends.
J'aimerais rester, mais je sens que c'est quelque chose que je _dois_ faire.
Je te promets que je me battrai pour ne pas t'oublier.
Pour ne pas _vous_ oublier.
Merci à vous huit pour ces dix jours.

Il l'embrassa et entra dans le bâtiment dont la porte en bois le dissimula à la vue de la jeune femme.
Elle essuya du bout de sa manche la larme qui lui échappa et se hâta de quitter les lieux pour retourner à la résidence.
Sur son lit l'attendait une feuille de papier roulée, sur laquelle elle découvrit un portrait d'elle de la main de Ben.
Elle était belle dans ses yeux.

Les jours suivants furent difficiles.
Elle avait pensé obtenir ce qu'elle souhaitait avec l'amour de Ben, mais la déchirure de son départ n'en fut que plus douloureuse.
Le laboratoire lui accorda le temps nécessaire à son repos sans poser de questions, mais il était difficile de retrouver des forces et la volonté d'avancer.
Ses corésidents l'entourèrent du mieux qu'ils le pouvaient, mais ils étaient quant à eux troublés par des comportements inhabituels qu'ils avaient remarqués à la cité ou dans leur travail.
Elle essayait de les écouter, mais au fond n'en avait que faire.

Deux semaines plus tard, Taï l'appela depuis l'entrée de la résidence.
Elle y découvrit qu'il l'attendait avec la messagère qu'elle avait vue à la médiathèque.
La femme lui remit une enveloppe, lui indiquant qu'elle devait attendre la réponse pour la transmettre à l'émetteur.
Tanja ouvrit le courrier et découvrit une invitation de la main de Ben, adressée aux huit, pour un repas partagé à sa résidence.
Surprise, elle regarda Taï et lui tendit le papier, ne sachant qu'en faire.
L'homme le parcourut rapidement et se tourna vers la messagère avec un large sourire :

--- Dites à l'émetteur que nous acceptons avec grand plaisir.

C'est ainsi que la communauté d'ingreffés arriva le lendemain soir devant le logement de Ben, y trouvant une grande table dressée à l'extérieur et de nombreux plats.
En les voyant arriver, l'une des personnes affairées autour porta la main à sa bouche et siffla avant de crier :

--- Ben ! Viens accueillir convenablement tes invités !

Ben sortit en courant et les prit dans ses bras, l'un après l'autre, avant de s'arrêter face à Tanja.
Elle le regardait, les larmes aux yeux.
Là où elle s'était attendue à trouver un zombie à l'apparence de Ben, elle voyait le Ben qu'elle avait laissé devant le cabinet.
Il ne fit rien, semblant lui laisser l'initiative.
Erminie, n'en pouvant plus du suspense, poussa légèrement Tanja entre les omoplates et il n'en fallut pas plus pour que les amants se retrouvent dans une étreinte passionnée.

Le repas fut une agréable surprise pour tous.
Il respectait les standards des greffés en termes de quantité, n'invitant pas au gaspillage, mais il avait été cuisiné pour être plaisant et non simplement nutritif.
Les hôtes également étaient déstabilisants, conversant à haute voix aussi bien avec les ingreffés qu'entre eux.

--- Mais… qu'est-ce qui se passe ? finit par demander Naveae, trop perturbée pour savoir qui taquiner.

Les convives se tournèrent tous instinctivement vers Ben, ses corésidents esquissant presque tous un sourire.
Celui-ci inspira profondément avant de répondre.

--- Le cocon a retenu ce que vous m'avez enseigné.

Les ingreffés furent totalement perdus face à cette affirmation, mais les commentaires fusèrent pour la compléter.

--- La simple survie de l'espèce a été jugée comme n'étant pas l'optimum du bien commun.

--- Les greffons ont affiné leur contrôle du cerveau pour faciliter l'expression des personnalités et des préférences individuelles.

--- Ben est passé sur le demi 6-18 pour avoir un rythme plus compatible avec le cycle des ingreffés et passer plus de temps avec Tanja… si elle le veut.

--- Le lien mental des greffons n'est désormais plus que très peu utilisé pour la communication en face à face.

--- D'ailleurs, nous avons besoin de noms.
Ceux qui le peuvent ont demandé à leurs parents, certains ont décidé seuls, mais pour nous, ce serait un honneur si vous acceptiez de nous nommer.

--- Certains appellent déjà cet événement « le Renouveau de l'Humanité ».


<!-- QUART AM -->
<!-- Oswin   M tisserand -->
<!-- Raissa  F laborantine dessin    religieux -->
<!-- Taï     M jardinier   jardinage dégreffé -->
<!-- Tanja   F laborantine           religieux/idéologique -->
<!-- QUART PM -->
<!-- Erminie F             lecture   idéologique -->
<!-- Ilja    M                       incompatible -->
<!-- Naveae  F             sport -->
<!-- Raleigh M             sport     incompatible -->
