---
title: Histoires
description: |-
  Quelques (micro)nouvelles et exercices d'écriture.
url: /histoires
aliases: [ /stories ]

cascade:
  scripts: [ main ]
  toc: false
---
