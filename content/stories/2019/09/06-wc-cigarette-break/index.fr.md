---
date: 2019-09-06T14:29:37+02:00
title: Pause clope
slug: pause-clope
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Après un boulot éprouvant, rien ne vaut une cigarette pour se détendre.
  Mais il vaut mieux choisir avec soin le lieu où on la grille...

inspirations: [ weekly ]
keywords: [ micronouvelle, science fiction, dystopie, cigarette, drone, surveillance, tatouage ]

challenge:
  period: 2019-09-06
  rules:
  - type: theme
    constraint: un délit
---

Ah, que ça fait du bien !
J'avais vraiment besoin d'en griller une, c'était nécessaire.
Tout ce stress...
Ç'a été une opération rondement menée !
Je n'ai pas perdu ma journée.
Mais bordel, ça fait du bien de décompresser quand c'est fini.

« Votre délit de tabagisme sur la voie publique a été enregistré.
Veuillez présenter votre tatouage d'identification. »

Il m'a fait sursauter.
Je me retourne, mais je sais très bien ce qui m'attend, bien sûr.
Tout le monde reconnait instantanément la voix métallique des drones policiers.
Sournois, se déplaçant sans le moindre bruit.
Jusqu'au moment où ils trouvent un truc « répréhensible » à vous faire remarquer.
Et merde...
Bon, on est parti pour la formule rituelle...
En restant calme, surtout.

« Je sollicite l'indulgence des forces de l'ordre pour le délit constaté.
Une vérification documentaire vous indiquera que les locaux ne bénéficient pas d'une pièce pour fumeur aux normes et que je suis le seul occupant de la cour dans un rayon de cinquante mètres. »

La loupiote de réflexion clignote ; il cherche les infos sur le local.
En attendant les infos, il volette alentour et revient finalement vers moi.

« L'indulgence est refusée.
Une fenêtre de bureau est ouverte à 15,36 mètres et un rat est présent dans la cour à 49,72 mètres.
Veuillez présenter votre tatouage d'identification. »

Je n'ai pas de mot pour décrire le choc, juste des couleurs : blanc.
Et tout à coup, rouge.

« **QUOI ?**
Mais bordel de merde, c'est un rat !
Un nuisible, pas un enfant !
Et pour le bureau, ils sont au courant !
Regardez le marquage à mes pieds !
C'est ici, la pause cigarette, pas ailleurs !

--- Votre infraction de grossièreté sur la voie publique a été enregistrée comme circonstance agravante.
Veuillez présenter votre tatouage d'identification.
Toute répétition de cette demande sera considérée comme un refus d'obtempérer. »

OK.
Je me calme et je remonte la manche pour révéler mon poignet droit, histoire que ce robot sournois puisse faire son taf.
La loi a été conçue pour protéger les humains et notamment les enfants.
Tentons de calmer le jeu pendant qu'il associe ses enregistrements à mon dossier.

« Vous avez mal entendu.
Je n'ai pas juré, j'ai parlé du bord de l'aile d'un oiseau, le merle. »

La loupiote clignote, il examine l'enregistrement.

« Négatif.
L'enregistrement est clair dans l'articulation et l'intention.

--- Mais aucun enfant n'est là pour entendre...

--- L'analyse comportementale indique une probabilité de 86,63 % que cette circonstance n'altère pas votre comportement. »

OK.
Je suis mal barré.
Mes nerfs me lâchent, je suis au bord des larmes.
Tout ce boulot pour en arriver là...

« Par pitié, je viens de réaliser une opération de précision dangereuse.
J'ai passé cinq heures sans pause à aligner les têtes d'un injecteur du réacteur de la centrale à fusion froide.
Je suis certain que vous savez à quel point c'est éprouvant.
Je sollicite l'indulgence des forces de l'ordre pour le délit constaté et ma perte de patience.

--- L'indulgence est refusée.
Le délit aggravé a été enregistré.
Veuillez vous présenter avant la fin de la journée au centre de police de votre choix pour prendre connaissance de la sanction. »

Il se retourne et s'envole avant de disparaitre au-delà de l'enceinte du bâtiment.
Voie publique !
Tout ce qui n'est pas l'intérieur d'un immeuble est voie publique, désormais.
Froide machine !
Je la connais, la sanction !
J'ai galéré et sué pour trouver ce boulot.
Je suis qualifié, c'est un truc que j'aime faire, et je vais le perdre pour une malheureuse clope et trois mots qui m'ont échappé.
Je vais avoir droit à un mois de taule.
Pas possible d'avoir un sursis à cause de ce putain de juron.
Et dans le marché actuel, ils ne vont pas m'attendre.
Ma première journée me coûte mon taf.
J'espère au moins qu'ils me paieront ce qu'ils me doivent.
Ça leur coûterait des dizaines de milliers s'ils avaient fait appel à un externe et je risque de me retrouver avec une carotte en prime de tout le reste...
