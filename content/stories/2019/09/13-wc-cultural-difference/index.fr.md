---
date: 2019-09-13T16:47:42+02:00
title: Différence culturelle
slug: difference-culturelle
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Ils sont tombés en lieux inconnus et doivent choisir leur prochaine direction.

inspirations: [ weekly ]
keywords: [ micronouvelle, science fiction, dialogue, exploration, archéologie ]

challenge:
  period: 2019-09-13
  rules:
  - type: theme
    constraint: une culture différente
  - type: general
    constraint: Écrire un dialogue uniquement, sans incise
    personal: true
---

--- Ah, ma tête...

--- Oh, vous êtes réveillés ‽
Allez-y doucement, c’était une belle chute.
Vous n’êtes pas blessé ?

--- J’ai mal au genou et à la tête.
Ok, une belle bosse en haut...

--- Mais pas de sang, j’ai vérifié.

--- En effet.
Et en bas... Ah-ïe...
Ça a l’air d’être une entorse.
Je vais me bricoler une attelle.
Je suis resté longtemps dans les vapes ?

--- Non, à peine cinq minutes.
Tenez, ce barreau devrait être assez solide pour caler votre jambe.

--- Merci.
Il nous est arrivé quoi, au juste ?

--- Nous pensions marcher sur de la terre battue, mais celle-ci recouvrait un vaste complexe souterrain, a priori abandonné.
Un plafond a cédé et nous sommes passés au travers.

--- Et vous n’avez rien ?

--- Quelques bleus, mais je m’en remettrai.
Il faut qu’on trouve comment sortir d’ici, nous ne pouvons pas atteindre notre point d’entrée.

--- Aïe !
Bon, ça devrait tenir.
Vous avez dit que le complexe est vas...
Ah, effectivement, la vue est vertigineuse.
Vous avez vu un moyen de quitter cette passerelle ?

--- Trois portes à cet étage.
La noire est pour ainsi dire inaccessible, la passerelle est branlante de ce côté et ne supportera certainement pas mon poids.

--- Ni le mien lorsque je vous demanderai de m’aider...

--- Restent donc une porte qui a dû être rouge vif et l’autre cramoisie.
C’est amusant, le métal et le cuir semblent avoir été à l’honneur lorsque ce bâtiment a été construit.
L’architecture est entre la révolution industrielle européenne et l’univers fantastique steampunk, avec des touches rappelant les bases militaires russes de la Guerre Froide...

--- Par pitié, arrêtons les cours d’architecture...

--- Désolé de la digression...

--- ... et cherchons comment sortir d’ici.
Ouvrez la porte brune !

--- C’est incroyable comme vous retombez dans vos habitudes à la moindre contrariété !
Je vous rends service en tant que congénère, je ne suis pas militaire.
Regardons ça...
D’ailleurs, pourquoi cette porte plutôt que l’autre ?

--- Bah, ça me semble évident.
L’autre est rouge, et rouge égale danger.

--- Comment ça, rouge égale danger ?
Mais nous n’en savons rien !
Nous ne connaissons rien de la culture des habitants de cette planète !

--- Nous savons qu’ils font humanoïdes et que leur sang est rouge !
Si on verse le sang, c’est mauvais signe.
Donc le rouge est mauvais signe.

--- Vous n’avez donc encore rien appris de moi ‽

--- À croire que non, au vu de votre réaction.

--- Le rouge, c’est bien le sang, oui.
Mais certaines cultures le vénèrent comme étant le fluide de vie !
Et la porte couleur sang coagulé me semblerait plus dangereuse, de ce point de vue.

--- Ah. Effectivement, ce point de vue se défend...

--- Chaque culture a ses propres références et ses propres interprétations, tout comme deux individus ne voient pas la même chose à travers les mêmes filtres, en raison de leurs expériences différentes !
On l’oublie trop souvent et on ne voit les choses que comme on a l’habitude de les voir.
Tiens, par exemple, savez-vous pourquoi, lors de la mondialisation, certains hypermarchés occidentaux ont failli rater leur installation en Chine ?

--- Parce qu’ils étaient de la mauvaise couleur ?

--- Cela vous fait rire, mais c’est exact !
Notre joli blanc symbole de pureté était en Chine la couleur du deuil.
La clientèle n’est venue qu’après le passage de décorateurs locaux qui ont installé des décorations rouges.

--- Ah. Et c’était quoi, le rouge, pour les Chinois ?

--- C’était une couleur de fête.
Elle se rapportait au soleil.
Et concernant notre troisième porte, le noir pouvait aussi bien symboliser la force que le désastre.

--- OK.
Donc ça veut dire que, selon vous, on devrait laisser tomber cette porte et se concentrer sur l’autre ?

--- ... Non, pas nécessairement.
Il faudra bien en choisir une.
C’est simplement que la couleur est un critère très subjectif.
Nous avons intérêt à ne pas en tenir compte et à redoubler de prudence en l’ouvrant.
