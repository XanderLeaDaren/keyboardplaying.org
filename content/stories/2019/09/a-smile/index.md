---
date: 2019-09-07T13:39:37+02:00
title: A Smile
slug: a-smile
author: chop
license: CC BY-NC-ND 4.0

description: |-
  A smile is a great gift to receive.
  Misinterpreted, though, it could get you into trouble.

inspirations: [ daily-life ]
keywords: [ flash fiction, city, dialogue, military, gun ]
---

I get off the train and go up the corridors of the station to reach the platform where I'll get on the train to finally leave the capital and go home.
It won't leave before half an hour.
I have time and I use it to wander a bit.

Around a corner, I cross ways with a military patrol.
One woman and three men, khaki fatigues, beret on the head, famas across the chest, right hand on the handle.
Even though their weapons are not loaded, they form an impressive sight.
They move together in a coordinated way, without having to communicate.
As always when I meet such a patrol, I can feel a smile appear on my face.
That smile will bring me trouble someday.

I am actually surprised I don't have to deal with "random checks" from the police already.
With my white, Caucasian face, my skin-short hair, the hood of my hoodie tucking out of my old leather jacket, my loose-ended mittens and my old shoulder-strapped bag, I often think I must like a drug addict or a skinhead.
Maybe both.
And yet, people who meet me seem to implicitly trust me.

Altough ...
I may have spoken too fast.
One soldier stares at me as I walk by them.
I keep going, my silly smile nailed to my face, when I hear a voice behind me.

"Excuse me, sir. May I inspect your bag, please?"

I turn to face the patrol, grabbing my bag in order to open it and present it to the man who spoke to me. "Of course, sir."

Keeping his right hand on his unloaded machine gun all the while, he discards a notebook and the laptop to see the pens and other gizmos at the bottom of the bag before letting it go.
I can see to his sour look that he hoped for something else.
His left hand falls back to the side of his body as his eyes refocus on mine.
There is a question on his mind.
Most certainly whether or not I'm mocking him.

"You can go, sir.
Sorry for the bother."

"Thank you, sir.
Um, sir?" I ask tentatively as he turns back.

He looks back at me.

"I suppose you wondered why I was smiling?"

He looks like kid caught with his hand in the cookie jar.

"Er ... Yeah, a little."

"I wasn't making fun of you, if that is any reassurance.
On the contrary.
I always admire professional know-how, whether it's the art of a master craftsman or a soldier moving with discipline and efficiency.
What's more, you spend your life ensuring our safety.
A small smile is the least of the thanks we can give you.
Have a good day!"
