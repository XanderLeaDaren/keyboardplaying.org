---
date: 2019-10-04T12:35:29+02:00
title: Après le Krach
slug: apres-le-krach
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Une nouvelle porte à pousser. Que trouverai-je derrière celle-ci ?

inspirations: [ weekly ]
keywords: [ micronouvelle, science fiction, post-apocalyptique, ville ]

challenge:
  period: 2019-10-04
  rules:
  - type: start
    constraint: « Je poussais la porte. À l'intérieur, le silence était total »
---

Je poussais la porte.
À l'intérieur, le silence était total et je laissais retomber le bras qui tenait mon fusil.
En même temps, c'était prévisible.
Depuis deux semaines que je parcourais la ville à la recherche de toute tech récupérable, je n'avais pas croisé âme qui vive.
Même les rats semblaient avoir déserté les égoûts.
La ville était morte avec le Grand Krach.

Et dire que, quelques décennies avant ça, l'homme s'imaginait encore conquérir l'espace dans de gros vaisseaux intelligents et capable de tout faire sans intervention humaine !
Ils n'avaient jamais cherché à recycler leur tech.
Ce n'était pas la faute des scientifiques qui leur avaient dit plusieurs fois que les ressources nécessaires étaient épuisables.
Les gouvernements, appuyés par les grosses sociétés, avaient préféré se concentrer sur les profits immédiats.
Jusqu'au mur.
Jusqu'au crash.
Jusqu'au Krach.

Les ressources s'étaient taries.
Les hommes devinrent incapable de fabriquer, puis de réparer quoi que ce soit de numérique.
Tout était tombé en panne, au fur et à mesure : les avions, les voitures, les ascenceurs...
Il avait fallu réapprendre à vivre.
On se serait cru revenu à l'époque de la révolution industrielle !

Les anciennes « grande villes » étaient abandonnées, personne ne voulant vivre dans des gratte-ciels s'il fallait y escalader à pied des dizaines d'étages.
Seuls quelques malades comme moi, les « pilleurs », les parcouraient, espérant encore dégotter une ou deux pièces détachées qu'on pourrait assembler pour ressusciter un peu de tech.
Allez, j'en étais à l'avant dernier appartement avant de fouiller l'immeuble suivant...
