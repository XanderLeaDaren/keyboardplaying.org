---
date: 2019-10-04T12:35:29+02:00
title: After the Krach
slug: after-the-krach
author: chop
license: CC BY-NC-ND 4.0

description: |-
  A new door, but what's behind that one?

inspirations: [ weekly ]
keywords: [ flash fiction, science fiction, post-apocalyptic, city ]

challenge:
  period: 2019-10-04
  rules:
  - type: start
    constraint: "I pushed the door. Inside, the silence was absolute."
---

I pushed the door.
Inside, the silence was absolute and I dropped the arm that was olding my rifle.
It was predictable.
For two weeks I'd been scouting this city looking for any recoverable tech, not seeing a living soul.
Even rats seemed to have left the place.
This city had died from the Great Krach.

Only a few decades ago, man still thought he could conquer space in massive, intelligent ships that would be able to do anything without a human having to work!
They never tried to recycle their tech.
Scientists had warned them, told them repeatedly that the resources they required were exhaustible.
The governments, backed by big corporations, had preferred to focus on immediate profits.
To the wall.
To the crash.
To the Krach.

Resources had dried up.
Men became incapable of making, then of repairing, anything digital.
Everything broke down, one thing after the other: planes, cars, lifts ...
People had had to relearn how to live.
It had been like being back in the industrial revolution!

The old "big cities" were abandoned, forgotten.
Nobody wanted to live in a tall, fancy skyscraper when you had to climb dozens of floors by foot.
Only a few nutties like me, the "looters" as they call us, still explored them, hoping to get our hands onto one or two spare parts that we could assemble to resuscitate a bit of tech.
Come on, let's move. There's only one flat in this building before moving to the next ...
