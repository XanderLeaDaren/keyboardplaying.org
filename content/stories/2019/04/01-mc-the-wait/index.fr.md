---
date: 2019-04-01T10:52:27+02:00
title: L'attente
slug: l-attente
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Elle attend, comme à chaque fois qu'elle a besoin de lui.
  L'impatience commence à la gagner.

inspirations: [ bimonthly ]
keywords: [ nouvelle, fantastique, sommeil, impatience, ténèbres, étudiant, ville, tentacule, thé, livre, rêve ]

challenge:
  period: 2019-03
  rules:
  - type: theme
    constraint: le sommeil
---

L'agacement commence à avoir raison de sa patience.
Bien entendu, elle a conscience que cela ne lui apportera rien : il ne vient jamais lorsqu'elle s'énerve.
Mais ce soir, elle a besoin de lui.
Elle a fait tout ce qu'elle pouvait pour préparer son concours de demain.
À présent, tout repose sur son bon vouloir.

Il est malheureusement comme un trousseau de clés : c'est toujours lorsqu'elle a le plus besoin de lui qu'il se fait le plus désirer.
En fait, il ne vient pratiquement jamais lorsqu'il est attendu.
À croire qu'il aime que son arrivée soit une surprise.
Il faut lui reconnaître un certain talent pour la rejoindre sans qu'elle s'en rende compte.

Elle se retourne, son mouvement rendu brusque par la frustration qu'elle tente ainsi d'évacuer, vainement.
Le temps passe et, toujours, aucun signe de lui.
De nouveau, l'impatience la gagne.

Une infusion lui ferait certainement du bien.
Elle se lève et se dirige vers la cuisine, où la bouilloire est retournée sur l'égouttoir.
Elle s'en saisit et la remplit d'eau avant de la poser sur la plaque de cuisson, qu'elle allume.
Pendant que l'eau chauffe, elle prépare un mélange de tilleul et de verveine dans un petit sachet, puis verse deux cuillerées de miel au fond de son mug.

La bouilloire commence à siffler.
Elle coupe la plaque et verse l'eau dans le mug avant d'y plonger son sachet de plantes.
Elle lève les yeux et observe son logement étudiant, dont elle peut pratiquement embrasser l'intégralité du regard sans avoir à se déplacer.

La cuisine, un passage à peine suffisamment large pour ouvrir les tiroirs et les portes des meubles dans lesquels elle range vaisselle et vivres, n'offre qu'un plan de travail, comportant un évier et des plaques de cuisson trop proches pour respecter n'importe quelle norme de sécurité électrique.
Un bar, sur lequel est posé son mug, surplombe cet espace et ouvre sur la pièce principale, son bureau-salon-salle à manger.
Au centre de la pièce se trouve sa table basse, une planche posée sur un tréteau à hauteur réglable.
Sous le bar, c'est le clic-clac qu'elle a racheté au précédent locataire pour le tiers du prix original.
Son regard fait le tour de la pièce et rencontre le fauteuil assorti, puis une autre planche de bois, sur deux tréteaux, faisant office de bureau.

Le sac posé sur ce dernier lui donne une idée.
Elle fait le tour du bar et se dirige droit vers lui, afin d'en extraire le livre qu'elle lit pour occuper ses vingt minutes de bus matin et soir.
Les transports rebutent plusieurs de ses camarades, mais elle y voit l'occasion de se changer les idées, de faire une vraie coupure dans sa journée.

Précisément ce dont elle a besoin pour arrêter de penser à lui !
Elle pose le livre sur sa table basse puis se dirige vers son bar pour inspecter le contenu de son mug.
En le saisissant pour le porter à son nez, elle constate déjà que la couleur est magnifique, avec une légère teinte ambrée.
Elle hume les arômes et est satisfaite : l'infusion est parfaite !
Elle extrait le sachet et le presse contre le bord de la tasse avec sa cuillère pour en extraire tout le liquide, avant de le jeter vers la poubelle.
Raté !
Tant pis, elle le ramassera demain.

Elle remue consciencieusement le breuvage afin de dissoudre tout le miel, dont l'essentiel était encore au fond, puis lève la tasse à ses lèvres pour goûter le résultat de sa préparation.
C'est trop chaud et elle manque de se brûler.
Elle pose le mug sur la table basse, se saisit du livre et s'assoit dans le fauteuil.

Elle ouvre le livre à la page indiquée par son marque-page, prête à lire, mais s'interrompt, surprise : le texte est à l'envers.
Elle est pourtant certaine qu'elle tenait son livre dans le bon sens.
Perplexe, elle referme l'ouvrage pour en examiner la couverture.
Celle-ci est effectivement dans le bon sens, mais ce n'est pas le cas du titre.

La fatigue lui joue des tours, mais lui ne vient toujours pas.
Elle regarde sa montre.
La nuit est tombée, mais il n'est pas encore si tard ; elle peut faire un tour de quartier pour se vider la tête.
L'infusion sera à température idéale à son retour et elle pourra prendre une bonne douche chaude — dans la mesure où il lui serait possible d'avoir de l'eau chaude — pour se délasser.
Il ne devrait pas mettre longtemps à arriver après ça.

Son téléphone reste sur la table : elle part dans le but de se détendre et le flux continu d'informations aurait l'effet inverse.
Elle enfile une veste légère, ses tennis et sort de sa chambre étudiante, qu'elle verrouille.
L'ascenseur est en panne, bien évidemment, mais elle prend de toute façon toujours les escaliers, depuis le jour où, à sept ans, elle est restée coincée seule pendant près de trois heures dans une de ces maudites cabines.

Sortie de la résidence, elle bifurque immédiatement à droite.
Le quartier n'a rien d'exceptionnel à offrir pour une promenade.
Rien que des bâtiments, tous plus gris les uns que les autres.
La maison des élèves, certainement blanche au départ, est sans doute l'immeuble le plus récent du quartier.
Aujourd'hui, elle a pris une teinte jaunâtre et l'ombre des rebords de fenêtre semble imprimée sur la façade.

Pourtant, à quelques centaines de mètres, il reste un petit carré de verdure où persistent une dizaine d'arbres.
Bien entendu, une part non négligeable de ce coin préservé a été transformée en parc à chien, afin que les compagnons à quatre pattes puissent se soulager ailleurs que sur les trottoirs.
Ce petit contact avec la nature suffit cependant toujours à lui faire beaucoup de bien.

Elle arrive au carrefour et tourne encore une fois à droite.
Son petit espace vert est là, à sa place.
L'éclairage public ne traverse pas complètement le feuillage des arbres et les immeubles entourant ce minuscule parc, en faisant une île isolée au milieu de l'océan urbain.
S'asseyant sur le banc, elle ferme les yeux.
L'herbe sous ses pieds et l'humidité qui commence à s'y condenser, la lumière raréfiée, le bruissement des feuilles dans la brise légère…
Elle se représente le jardin de sa grand-mère, en pente, avec le banc tout en haut, qui permet de voir la rue par-dessus les haies de troènes.
Voilà de quoi se détendre un peu !
Elle ouvre les yeux.
Elle ne saurait l'expliquer, mais quelque chose n'est pas normal.
C'est comme si elle ressentait une présence.
Quelque chose qui serait tapi dans l'obscurité.
Ce bruit ! Ce n'est pas celui des feuilles.

Elle se lève, sautant presque du banc, et se retourne vers le son, dans le coin le plus sombre du parc, mais ne distingue rien dans l'obscurité.
Elle scrute la zone noire avant de réaliser l'incohérence de la situation : le lampadaire dans son dos éclaire directement l'obscurité insondable devant elle.

Son cerveau refuse l'illogisme de la situation, mais tandis qu'elle observe, les ténèbres grandissent.
Pas comme si l'éclairage baissait, non.
Plutôt comme si cette obscurité étendait d'immatériels tentacules d'ombre.
Elle se sent engourdie, incapable de réagir, comme paralysée par l'absurdité de la situation.
Elle est cartésienne, elle fait des études pour devenir scientifique, mais ce qui se passe en face d'elle dépasse sa compréhension.
L'un des tentacules semble sur le point d'atteindre sa propre ombre.
C'est le moment où ses instincts les plus profonds resurgissent, lorsqu'il faut choisir entre combat et fuite.
Et face à un adversaire si inconnu, si intangible, la seule réponse possible est la fuite !
Elle prend ses jambes à son cou et s'élance.
Aussi vite qu'elle le peut.
Elle court.
Son unique but est de trouver un abri, et le seul qui lui vient à l'esprit est sa chambre étudiante.
Elle ne ralentit pas, mais ne reconnaît pas les rues.
C'est comme si elle s'était soudain retrouvée à l'autre bout de la ville.
Elle court sans se retourner, faisant en même temps appel à toutes ses capacités cognitives.

Son bon sens, soutenu par ses cours d'optique, lui dit que tout ceci est impossible.
En physique, l'ombre n'est que l'absence de lumière.
Elle ne peut pas exister dans une zone directement éclairée.
Ce n'est pas une entité vivante et dotée de tentacules.
Par ailleurs, elle-même n'a pas pu être à un endroit à un moment et à un autre l'instant suivant, juste en quelques enjambées.
À moins que…
Elle n'entrevoit qu'une réponse à ce mystère, mais comment s'en assurer ?
Là ! Dans l'ouverture d'une ruelle, sur sa gauche, elle aperçoit la résidence.
Elle s'arrête et fait demi-tour pour se précipiter dans le passage étroit.
L'entrée lui fait face, à quelques dizaines de mètres.
Elle court le plus vite possible, à bout de souffle.

Parvenue à la porte, elle tire dessus, mais celle-ci est fermée.
Elle va pour prendre son trousseau dans sa poche, seulement pour se rendre compte que celle-ci est vide.
Où sont ses clés ?
Les a-t-elle perdues en chemin ?
Les a-t-elle seulement entendues tinter dans sa course ?
Elle regarde derrière elle, la ruelle qu'elle vient d'emprunter.
L'ombre semble en franchir le coin, comme si elle la suivait.
Une cinquantaine de mètres, c'est tout ce qui les sépare.
Comment entrer ?
L'interphone est en panne depuis des semaines.

Elle se tourne à nouveau vers l'immeuble et lève des yeux larmoyants vers sa chambre, désespérée, avant de se figer un instant : sa fenêtre est ouverte, au deuxième étage.
Elle est saisie d'une idée folle, mais a-t-elle seulement le choix ?
Un rapide coup d'œil derrière elle lui confirme que l'ombre continue d'approcher, engloutissant tout sur son passage.
En désespoir de cause, elle s'élance et entreprend de se hisser le long de la façade.
Elle a beau pratiquer l'escalade en club depuis l'enfance, elle ne se risquerait pas à grimper un immeuble en temps normal, mais cette ombre n'a rien de normal.
Les risques liés à une chute semblent dérisoires à côté de ce qui pourrait se passer si elle se laissait rattraper.

Atteindre le premier étage n'est pas difficile, les garde-corps fournissant d'excellentes prises.
Dans l'encadrement de la fenêtre sous la sienne, se tenant aux côtés de l'ouverture, elle se dresse, les pieds sur le rebord puis sur la barre à laquelle elle s'est accrochée.
Elle se trouve à présent à trois ou quatre mètres du sol.
Sa main gauche est calée en haut du renfoncement.
Il lui faut maintenant atteindre le rebord de sa propre fenêtre.
Si le premier étage a été aisément franchi, elle se trouve à présent en difficulté : son bras tendu n'atteint pas sa cible.
Elle baisse alors les yeux et constate que l'ombre étend ses tentacules jusqu'au pied du bâtiment.
Plus question de reculer !
Tentant le tout pour le tout, elle pousse sur ses jambes et, du bout des doigts, attrape la prise qu'elle visait.
Contre toute attente, elle parvient, à la force de ses bras, à se hisser et à appuyer son buste sur le garde-corps.

Puisant dans ses dernières forces, tirant sur ses bras puis poussant sur ses jambes, elle parvient à entrer dans sa chambre.
Elle se relève et ferme immédiatement la fenêtre.
Elle recule, les yeux rivés sur la vitre, et trébuche contre son propre lit, tombant assise dessus.
Sa main heurte quelque chose. Une jambe ?
Sa tête pivote malgré elle et elle voit.
Elle est couchée dans son lit, endormie.

Enfin, elle comprend !
Elle n'attendait pas le sommeil, elle rêvait seulement de cette attente.
Baissant les yeux, elle constate qu'elle est déjà en tenue pour dormir ; il ne lui reste qu'à se coucher pour profiter pleinement de cette nuit réparatrice.
Elle s'installe confortablement et ferme les yeux, laissant l'ombre recouvrir sa fenêtre et son rêve s'achever dans l'obscurité.
