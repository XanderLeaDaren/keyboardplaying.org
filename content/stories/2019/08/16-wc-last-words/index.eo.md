---
date: 2019-08-16T12:09:52+02:00
title: Lastaj vortoj
slug: lastaj-vortoj
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Li estis blokita, kun nenie plu iri.

inspirations: [ weekly ]
keywords: [ fulmfikcio, urbo, ĉasado ]

challenge:
  period: 2019-08-16
  rules:
  - type: theme
    constraint: lastaj vortoj
---

Ĝi finiĝis.
Li nenie restis por iri.
Supre de ĉielskrapanto, ne estis espero pri eliro aŭ rifuĝejo.
Fuĝante ak ka supro, kia brila ideo!
Li turnis kiam li aŭdi la podo bati: liaj perskutantoj alvenis, ne plu rapidante.
Ne necesitas urĝi: ilia predo estis kondamnita.
Ili povus preni ilian tempon por pagigi lin.
Por la ĉasado kai ĉio antaŭe.

Li tamen ŝatis ĝin rapide.
Falsante lasta provo de flugo, li retiriĝis, paŝon post paŝon, sen deturni la rigardo de la aliaj.
La malantaŭo de lia femuro renkontris la mureton kaj li aŭdis mokojn ĉirkaŭ li.
Sed li ridetis.
Li levis unu piedon sur la muron, poste la alian.
La rideto de unu el liaj torturistoj forfandiĝis de lia vizaĝo sub la efiko de kompreno nuancita de konfuzo --- aŭ ĉi estis inverse?

Alia el liaj ekzekutistoj provis ekkapti lin, sed jam estis tro malfrue: li kliniĝis malantaŭen kaj, kiam li estis preskaŭ horizontala, puŝis sur liajn piedojn.
Dum la konstruaĵo preterpasis lin rapide, kiel trajno vidata de la kajo, la vento vipante lian dorson, li diris: "Nu, kiu pensus? Ĝis nun, ĝi ne estas tiel terura."
