---
date: 2019-08-30T18:15:52+02:00
title: Un truc qui manquait
slug: un-truc-qui-manquait
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Mais quoi donc ?

inspirations: [ weekly ]
keywords: [ micronouvelle, disparition, bibliothèque, argot ]

challenge:
  period: 2019-08-30
  rules:
  - type: general
    constraint: Ne pas utiliser la lettre _E_
---

Il y avait un truc pas normal.
Son ciboulot tournait, mais pas fort rond.
Il y avait un accroc.
Un fourbi lui manquait.
Un bazar toujours dans son cassis pourtant aujourd'hui disparu.
Quoi donc ?

Il cogitait, raisonnait, sondait, philosophait, mais sans fruit.
Si point dans son front, où saisir un savoir disparu ?

Bon sang, tout quidam, y compris abruti, irait là où moults bouquins sont à disposition.
Un olibrius ainsi qu'amis avait jadis inscrit tous savoirs dans un grand almanach.
Là s'offrirait la solution !

Courant, fonçant, fusant, bousculant gars ou nanas sur son trimard, il arriva, ahanant, à son but.
Il prit du rayon l'opus puis l'ouvrit, parcourant la foliation sans savoir pour quoi.
Rapido, un tort s'imposa à sa vision : tout blabla avait du flou.
Ou plutôt du trou.
Un mot, ou plutôt tous, dissous ; dans tous, un trou ou plus.

Son instinct lui dit qu'il s'agit d'un logo pour bâtir nos mots, mais pourquoi la disparition d'un insignifiant picto aurait un impact sur son soi profond ?
