---
title: Stories
description: |-
  Some short (or flash) fictions and writing exercise that we'd like to share.

cascade:
  scripts: [ main ]
  toc: false
---
