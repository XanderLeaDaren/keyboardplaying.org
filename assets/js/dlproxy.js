// Import required JS
import { initInteractiveForm } from './dlproxy/form-buttons-state-management';
import { initSelector } from './dlproxy/selected-value-caption-management';

initInteractiveForm(document.getElementById('dl-p'));
initSelector();
