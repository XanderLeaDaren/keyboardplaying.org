// Import required JS
import { loadAllLazyNodes } from './main/lazyload-es5';

if (console) {
    console.log("Couldn't load ES next features, making sure the site's functional.");
}
loadAllLazyNodes();
