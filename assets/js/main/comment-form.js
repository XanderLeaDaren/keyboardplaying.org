/** ID of the comment submission form. */
const STATICMAN_FORM_ID = 'staticman-form';

/** Query selector to select all "reply to thread" buttons. */
const STATICMAN_REPLY_BTN_SELECTOR = '#staticman-comments .reply-btn';

/** ID of the span used to display the author being replied to. */
const REPLYING_TO_AUTHOR_ID = 'replying-to';

const serialize = form => {
    const data = new URLSearchParams();
    for (const pair of new FormData(form)) {
        data.append(pair[0], pair[1]);
    }
    return data;
};

/**
 * Submits a form to the Staticman server asynchronously.
 *
 * @param {*} e the form submission event
 */
const processCommentForm = e => {
    // Prevent default behaviour
    if (e.preventDefault) e.preventDefault();

    const form = e.currentTarget;
    form.classList.remove('success', 'error');
    form.classList.add('loading');

    fetch(form.action, {
        method: form.method,
        mode: 'cors',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body: serialize(form)
    })
        .then(response => {
            if (response.status >= 200 && response.status <= 299) {
                return response.json();
            } else {
                throw Error(response.statusText);
            }
        })
        .then(data => {
            if (data.success) {
                form.reset();
                form.classList.add('success');
            } else if (data.errorCode === 'IS_SPAM') {
                form.classList.add('spam');
            } else {
                form.classList.add('error');
            }
            form.classList.remove('loading');
        })
        .catch(error => {
            console.error(error);
            form.classList.remove('loading');
            form.classList.add('error');
        });

    // Prevent the form from being submitted
    return false;
};

const resetCommentFormHandler = e => {
    // Prevent default behaviour
    if (e.preventDefault) e.preventDefault();

    resetCommentForm(e.currentTarget);
};

const resetCommentForm = form => {
    form.reset();
    form.querySelector('[name="fields[replyThread]"]').value = null;
    form.classList.remove('replying', 'loading', 'success', 'error');
};

const setReplyTarget = form => {
    return e => {
        // The default behaviour is not prevented, but some additional behaviour are set

        resetCommentForm(form);
        const data = e.currentTarget.dataset;
        form.querySelector('[name="fields[replyThread]"]').value = data.thread;

        const comment = document.getElementById(data.comment);
        document.getElementById(REPLYING_TO_AUTHOR_ID).innerText = comment.querySelector('.comment-author').textContent;
        form.classList.add('replying');
    };
};

/**
 * Initializes the comment form with required handlers.
 */
const initCommentForm = () => {
    // Handle Staticman forms
    const form = document.getElementById(STATICMAN_FORM_ID);
    if (form) {
        form.addEventListener('submit', processCommentForm);
        form.addEventListener('reset', resetCommentFormHandler);

        const replyBtnHandler = setReplyTarget(form);
        const replyBtns = document.querySelectorAll(STATICMAN_REPLY_BTN_SELECTOR);
        replyBtns.forEach(btn => {
            btn.addEventListener('click', replyBtnHandler);
        });
    }
};

export { initCommentForm };
