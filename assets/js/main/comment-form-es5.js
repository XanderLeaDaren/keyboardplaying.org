// ESBuild doesn't support let nor var for ES5 scripts
/* eslint-disable no-var */
/** ID of the comment submission form. */
var STATICMAN_FORM_ID = 'staticman-form';

/** Query selector to select all "reply to thread" buttons. */
var STATICMAN_REPLY_BTN_SELECTOR = '#staticman-comments .reply-btn';

/** ID of the span used to display the author being replied to. */
var REPLYING_TO_AUTHOR_ID = 'replying-to';

/**
 * Sets the form in a "loading" state so that user is aware of it being processed.
 *
 * @param {*} e the form submission event
 */
var processCommentForm = e => {
    e.currentTarget.classList.add('loading');
};

var resetCommentFormHandler = e => {
    // Prevent default behaviour
    if (e.preventDefault) e.preventDefault();

    resetCommentForm(e.currentTarget);
};

var resetCommentForm = form => {
    form.reset();
    form.querySelector('[name="fields[replyThread]"]').value = null;
    form.classList.remove('replying', 'loading', 'success', 'error');
};

var setReplyTarget = form => {
    return e => {
        // The default behaviour is not prevented, but some additional behaviour are set

        resetCommentForm(form);
        var data = e.currentTarget.dataset;
        form.querySelector('[name="fields[replyThread]"]').value = data.thread;

        var comment = document.getElementById(data.comment);
        document.getElementById(REPLYING_TO_AUTHOR_ID).innerText = comment.querySelector('.comment-author').textContent;
        form.classList.add('replying');
    };
};

/**
 * Initializes the comment form with required handlers.
 */
var initCommentForm = () => {
    // Handle Staticman forms
    var form = document.getElementById(STATICMAN_FORM_ID);
    if (form) {
        form.addEventListener('submit', processCommentForm);
        form.addEventListener('reset', resetCommentFormHandler);

        var replyBtnHandler = setReplyTarget(form);
        var replyBtns = document.querySelectorAll(STATICMAN_REPLY_BTN_SELECTOR);
        for (var index = 0; index < replyBtns.length; ++index) {
            replyBtns[index].addEventListener('click', replyBtnHandler);
        }
    }
};

export { initCommentForm };
