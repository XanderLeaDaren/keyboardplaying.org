// ESBuild doesn't support let nor const for ES5 scripts
/* eslint-disable no-var */
/**
 * Sets src for all lazy nodes.
 *
 * This method is offered as a non-functionality workaround for IE11.
 */
var loadAllLazyNodes = () => {
    var nodes = document.querySelectorAll('.lazy');
    for (var index = 0; index < nodes.length; ++index) {
        var element = nodes[index];
        if (element.dataset.src) {
            element.src = element.dataset.src;
        }
    }
};

export { loadAllLazyNodes };
