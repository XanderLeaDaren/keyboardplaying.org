// Import required JS
import { initLazyObserver } from './main/lazyload';
import { initCollapsibles } from './main/collapsibles';
import { initCommentForm } from './main/comment-form';

initLazyObserver();
initCollapsibles();
initCommentForm();
