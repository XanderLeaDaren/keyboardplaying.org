const SELECTOR_FORM_NAME = 'transformation';
const SELECTION_CAPTION_ID = 'caption';

const hideAll = () => {
    document.getElementById(SELECTION_CAPTION_ID)
        .querySelectorAll('section')
        .forEach(section => { section.style.display = 'none'; });
};

const showCaption = name => {
    document.getElementById(SELECTION_CAPTION_ID)
        .querySelector(`[data-tab=${name}]`)
        .style.display = 'initial';
};

const toggleCaption = radio => {
    hideAll();
    if (radio.checked) {
        showCaption(radio.value);
    }
};

const initSelector = () => {
    hideAll();
    const radios = document.querySelectorAll(`input[name=${SELECTOR_FORM_NAME}]`);
    radios.forEach(radio => {
        if (radio.checked) {
            showCaption(radio.value);
        }
        radio.addEventListener(
            'change',
            event => toggleCaption(event.target)
        );
    });
};

export { initSelector };
