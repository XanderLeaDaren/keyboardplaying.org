const switchButtonsToActiveState = form => {
    form.querySelectorAll('input[data-active]')
        .forEach(e => {
            e.disabled = true;
            e.value = e.dataset.active;
        });
};

const switchButtonsToOriginalState = form => {
    form.querySelectorAll('input[data-active]')
        .forEach(e => {
            e.disabled = false;
            e.value = e.dataset.original;
        });
};

const initInteractiveForm = form => {
    form.addEventListener('submit', event => {
        switchButtonsToActiveState(event.target);
    });
    form.addEventListener('reset', event => {
        switchButtonsToOriginalState(event.target);
    });
};

export { initInteractiveForm };
