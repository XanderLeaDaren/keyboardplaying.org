# About Vincent Déniel's drawings

This directory contains [Vincent Déniel's drawings](https://vincentdnl.com/drawings/).
They are in this directory only so that your browsers won't have to open connections to a third-party site.

These drawings are licensed under a [Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) license](https://creativecommons.org/licenses/by-nc/4.0/).
