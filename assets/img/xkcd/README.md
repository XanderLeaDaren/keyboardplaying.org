# About XKCD comics

This directory contains [XKCD comics](https://xkcd.com/).
They are in this directory only so that your browsers won't have to open connections to a third-party site.

**These comics are under [CC BY-NC 2.5](https://creativecommons.org/licenses/by-nc/2.5/).**
