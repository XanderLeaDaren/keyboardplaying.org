// These classes are those hugo may not detect (dynamic, some exceptions...)
const additionalClasses = [
    // Dynamic classes
    // - Comment form
    'replying',
    'loading',
    'success',
    'spam'
];

const purgecss = require('@fullhuman/postcss-purgecss')({
    content: ['./hugo_stats.json'],
    defaultExtractor: content => {
        const els = JSON.parse(content).htmlElements;
        return [...additionalClasses, ...els.tags, ...els.classes, ...els.ids];
    }
});

module.exports = {
    plugins: [
        require('autoprefixer'),
        ...(process.env.HUGO_ENVIRONMENT === 'production' ? [purgecss] : []),
        require('cssnano')({ preset: 'default' })
    ]
};
