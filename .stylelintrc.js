const sortOrderSmacss = require('stylelint-config-property-sort-order-smacss/generate');

module.exports = {
    extends: 'stylelint-config-sass-guidelines',
    rules: {
        indentation: 4,
        'max-nesting-depth': 3,
        'order/properties-order': [ sortOrderSmacss() ],
        'order/properties-alphabetical-order': null
    }
};
